package com.brainlines.weightloggernewversion.activity.newtest;

import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoggerFunctionActivity;
import com.brainlines.weightloggernewversion.adapter.newtestadapter.NewTestBagLengthAdapter;
import com.brainlines.weightloggernewversion.broadcastreceiver.NetworkChangeReceiver;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.MasterToleranceTable;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NewTestBagLengthActivity extends BaseActivity implements View.OnClickListener {

    RecyclerView rv_new_test_bag_length_reading;
    ImageButton img_add_new_test_bag_length_reading,btn_send_bag_length,btn_abort;
    Button btn_sync;
    EditText ed_new_test_bag_length_reading;
    public ArrayList<String> bag_length_list = null;
    Button alert_button_ok;
    TextView alert_message;
    AlertDialog alertDialog;
    String dataloggerName,dataloggerIp,current_tag_id,datalogger_mac_id;
    DataBaseHelper helper;
    String target_weight;
    double max_reading_value=0.0;
    double min_reading_value = 0.0;
    double mean_of_readings = 0.0;
    double range = 0.0;
    double totals_sum =0.0;
    private DecimalFormat decimalFormat = new DecimalFormat("0.000");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private String str_tarhetwt_uom = "";
    public static ArrayList<MasterToleranceTable> tolMasterArr = null;
    String status = "FAIL",str_oinum,weight_test_status;
    double str_kg_value_from_local_db;
    private NetworkChangeReceiver networkChangeReceiver;
    double baglengthtolerance;
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_test_bag_lengh);
        initUi();

        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        dataloggerName = getIntent().getStringExtra("LoggerName");
        dataloggerIp = getIntent().getStringExtra("LoggerIP");
        datalogger_mac_id = getIntent().getStringExtra("mac_id");
        current_tag_id = getIntent().getStringExtra("current_tag_id");
        str_oinum = getIntent().getStringExtra("so_oinum");
        weight_test_status = getIntent().getStringExtra("weight_test_status");
        str_tarhetwt_uom = getIntent().getStringExtra("targetwt_uom");
        str_kg_value_from_local_db = getIntent().getDoubleExtra("IntKgValue",0.0d);
        bag_length_list = new ArrayList<String>();


    }

    private void initUi() {
        rv_new_test_bag_length_reading = findViewById(R.id.rv_new_test_bag_length_reading);
        img_add_new_test_bag_length_reading = findViewById(R.id.img_add_new_test_bag_length_reading);
        btn_send_bag_length = findViewById(R.id.btn_sendBagLengthConfirmBtn);
        btn_abort = findViewById(R.id.btn_sendBagLengthAbortBtn);
        btn_sync = findViewById(R.id.btn_sync);
        ed_new_test_bag_length_reading = findViewById(R.id.ed_new_test_bag_length_reading);
        img_add_new_test_bag_length_reading.setOnClickListener(this);
        btn_send_bag_length.setOnClickListener(this);
        btn_abort.setOnClickListener(this);
        btn_sync.setOnClickListener(this);
        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerNetworkBroadcastForNougat();
    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==img_add_new_test_bag_length_reading.getId())
        {
            String bag_length_reading = ed_new_test_bag_length_reading.getText().toString();
            if(bag_length_reading != null && bag_length_reading.equals("")){
                alertMessage();
                alert_message.setText("Invalid entry please check!!");
                alert_button_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
            }
            else
            {
                bag_length_list.add(bag_length_reading);
                LinearLayoutManager linearLayoutManager = new GridLayoutManager(NewTestBagLengthActivity.this,4);
                rv_new_test_bag_length_reading.setLayoutManager(linearLayoutManager);
                rv_new_test_bag_length_reading.setAdapter(new NewTestBagLengthAdapter(NewTestBagLengthActivity.this,bag_length_list));
                ed_new_test_bag_length_reading.setText("");
            }
        }

        else if (view.getId()==btn_send_bag_length.getId())
        {
            InsertIntoDb();
            BaglengthCalculation();
            Intent i = new Intent(NewTestBagLengthActivity.this, NewTestPaperShiftReadingActivity.class);
            i.putExtra("LoggerName",dataloggerName);
            i.putExtra("LoggerIP",dataloggerIp);
            i.putExtra("mac_id",datalogger_mac_id);
            i.putExtra("current_tag_id",current_tag_id);
            i.putExtra("so_oinum",str_oinum);
            i.putExtra("weight_test_status",weight_test_status);
            i.putExtra("IntKgValue",str_kg_value_from_local_db);

            i.putExtra("targetwt_uom",str_tarhetwt_uom);

            startActivity(i);

        }

        else if (view.getId()==btn_abort.getId())
        {
            //abort();
            status = "Abort";
            helper.updateOverallStatusForTag(status,current_tag_id);
            Intent i = new Intent(NewTestBagLengthActivity.this, LoggerFunctionActivity.class);
            i.putExtra("LoggerName",dataloggerName);
            i.putExtra("LoggerIP",dataloggerIp);
            startActivity(i);
            finish();
        }
    }

    public void alertMessage() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.layout_alert_dialog, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        alert_button_ok= dialogView.findViewById(R.id.alert_btn_ok);
        alert_message= dialogView.findViewById(R.id.alert_message);
        alertDialog = builder.create();
        alertDialog.show();

    }

    public void InsertIntoDb() {
//        DataBaseHelper helper=new DataBaseHelper(NewTestBagLengthActivity.this);
//        SQLiteDatabase db = helper.getWritableDatabase();
        String currentDateandTime = sdf.format(new Date());

        for (int i = 0;i < bag_length_list.size();i++)
        {
            ContentValues cv=new ContentValues();
            cv.put(Constants.BAG_LENGTH_VARIATION_BAG_LENGTH_ID,1);
            cv.put(Constants.BAG_LENGTH_VARIATION_OEM_ID,1234);
            cv.put(Constants.BAG_LENGTH_VARIATION_MAC_ID,datalogger_mac_id);
            cv.put(Constants.BAG_LENGTH_VARIATION_LOGGER_ID,dataloggerName);
            cv.put(Constants.BAG_LENGTH_VARIATION_TAG_ID,current_tag_id);
            cv.put(Constants.BAG_LENGTH_VARIATION_OINUM,str_oinum);
            cv.put(Constants.BAG_LENGTH_VARIATION_LENGTH,Float.parseFloat(bag_length_list.get(i)));
            cv.put(Constants.BAG_LENGTH_VARIATION_MODIFIED_DATE,currentDateandTime);
            cv.put(Constants.BAG_LENGTH_VARIATION_IS_SYNC,false);
            long d=db.insert(Constants.BAG_LENGTH_VARIATION_RESULT,null,cv);
            Log.d("Success", String.valueOf(d));
            if (d == -1){
                Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                //wakeup();
            }
        }
    }

    public void BaglengthCalculation() {
        //maximum reading
        if (bag_length_list.size() != 0){
            max_reading_value = Double.parseDouble(bag_length_list.get(0));
            for (int cnt = 1; cnt < bag_length_list.size(); cnt++) {
                if (Double.parseDouble(bag_length_list.get(cnt)) > max_reading_value) {
                    max_reading_value = Double.parseDouble(bag_length_list.get(cnt));
                }
            }
            //minimum reading
            min_reading_value = Double.parseDouble(bag_length_list.get(0));
            for (int cnt = 1; cnt < bag_length_list.size(); cnt++) {
                if (Double.parseDouble(bag_length_list.get(cnt)) < min_reading_value) {
                    min_reading_value = Double.parseDouble(bag_length_list.get(cnt));
                }
            }

            //mean calculation
            for (int cnt = 0; cnt < bag_length_list.size(); cnt++) {
                totals_sum += Double.parseDouble(bag_length_list.get(cnt));
            }
            //mean_of_readings = totals_sum / bag_length_list.size();
            mean_of_readings = (max_reading_value - min_reading_value)/2 + min_reading_value;

            //range calculation
            //range = max_reading_value - min_reading_value;
            range = (max_reading_value - min_reading_value)/2;
            //checkIfReadingsAreDone(bag_length_list.size());

            double rangeDb = Double.valueOf(range);
            double tolerance = 2.0;






            if (rangeDb <= tolerance) {
                status = "PASS";
            }
            else
            {
                status = "FAIL";
            }

            //update overall status in C_tags table
            if (weight_test_status.equals("PASS") && status.equals("FAIL"))
            {
                helper.updateOverallStatusForTag("RESUME",current_tag_id);
            }
            else if (weight_test_status.equals("FAIL"))
            {
                helper.updateOverallStatusForTag("FAIL",current_tag_id);
            }

            helper.updateBaglengthstatus(status,current_tag_id);

//            DataBaseHelper helper=new DataBaseHelper(NewTestBagLengthActivity.this);
//            SQLiteDatabase db = helper.getWritableDatabase();
            String strrangewithpm = "+/-" +String.valueOf(decimalFormat.format(range));

            String currentDateandTime = sdf.format(new Date());
            ContentValues cv=new ContentValues();
            cv.put(Constants.BAG_LEN_ANA_ID,1);
            cv.put(Constants.BAG_LEN_ANA_OEM_ID,1234);
            cv.put(Constants.BAG_LEN_ANA_LOGGER_ID,dataloggerName);
            cv.put(Constants.BAG_LEN_ANA_TAG_ID,current_tag_id);
            cv.put(Constants.BAG_LEN_ANA_MIN_READING,Float.parseFloat(String.valueOf(min_reading_value)));
            cv.put(Constants.BAG_LEN_ANA_MAX_READING, Float.parseFloat(String.valueOf(max_reading_value)));
            cv.put(Constants.BAG_LEN_ANA_RANGE_READING,strrangewithpm);
            cv.put(Constants.BAG_LEN_ANA_RUN_STATUS,status);
            cv.put(Constants.BAG_LEN_ANA_MODIFIED_DATE,currentDateandTime);
            cv.put(Constants.BAG_LEN_ANA_MEAN_READING,Float.parseFloat(String.valueOf(mean_of_readings)));
            cv.put(Constants.BAG_LEN_ANA_IS_SYNC,false);

            long d=db.insert(Constants.BAG_LENGTH_ANALYTICS_TABLE,null,cv);
            Log.d("Success", String.valueOf(d));
            if (d == -1){
                Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                //wakeup();
            }

        }
    }

    public boolean checkIfReadingsAreDone(int resultsCnt){
        //todo check for target wt if empty
        double targetWtD = Double.parseDouble(target_weight);

        boolean result = false;
        int numOfReadings = 2;
        if(resultsCnt >= numOfReadings){
            result = true;
        }

        return result;
    }



    private String abort() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String tag = current_tag_id;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String dataPort=Constants.DATA_LOGGER_PORT+"";
        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;

    }




}
