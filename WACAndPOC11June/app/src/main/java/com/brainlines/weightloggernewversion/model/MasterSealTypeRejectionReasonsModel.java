package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterSealTypeRejectionReasonsModel implements Parcelable {
    String reasonId;
    int oemId;
    String productType;
    String reasonDescription;
    boolean isActive;
    boolean isDeleted;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String  modifiedBy;

    public MasterSealTypeRejectionReasonsModel(String reasonId, int oemId, String productType, String reasonDescription, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        this.reasonId = reasonId;
        this.oemId = oemId;
        this.productType = productType;
        this.reasonDescription = reasonDescription;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
    }

    protected MasterSealTypeRejectionReasonsModel(Parcel in) {
        reasonId = in.readString();
        oemId = in.readInt();
        productType = in.readString();
        reasonDescription = in.readString();
        isActive = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
    }

    public static final Creator<MasterSealTypeRejectionReasonsModel> CREATOR = new Creator<MasterSealTypeRejectionReasonsModel>() {
        @Override
        public MasterSealTypeRejectionReasonsModel createFromParcel(Parcel in) {
            return new MasterSealTypeRejectionReasonsModel(in);
        }

        @Override
        public MasterSealTypeRejectionReasonsModel[] newArray(int size) {
            return new MasterSealTypeRejectionReasonsModel[size];
        }
    };

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public int getOemId() {
        return oemId;
    }

    public void setOemId(int oemId) {
        this.oemId = oemId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(reasonId);
        dest.writeInt(oemId);
        dest.writeString(productType);
        dest.writeString(reasonDescription);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
    }
}
