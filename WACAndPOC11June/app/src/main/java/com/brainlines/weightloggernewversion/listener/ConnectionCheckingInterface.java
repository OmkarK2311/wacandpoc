package com.brainlines.weightloggernewversion.listener;

public interface ConnectionCheckingInterface {
    public void connectionLost(boolean value);
}
