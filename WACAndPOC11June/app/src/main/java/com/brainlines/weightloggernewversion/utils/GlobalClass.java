package com.brainlines.weightloggernewversion.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.util.Log;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.IdAndModifiedDateModel;
import com.brainlines.weightloggernewversion.model.UserModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GlobalClass {
    private static final String TAG = "Global";

    public static String db_global_name;


    Context context;
    DataBaseHelper helper;

    private List<IdAndModifiedDateModel> serverDataList = new ArrayList<>();
    private List<IdAndModifiedDateModel> localDataList = new ArrayList<>();
    private List<IdAndModifiedDateModel> commonDataList = new ArrayList<>();
    private List<IdAndModifiedDateModel> uniqueDataList = new ArrayList<>();
    private List<IdAndModifiedDateModel> deleteDataList = new ArrayList<>();
    private List<IdAndModifiedDateModel> addDataList = new ArrayList<>();

    public GlobalClass(Context context, DataBaseHelper helper) {
        this.context = context;
        this.helper = helper;
    }
    public void syncSingleTable(final String MASTER_TABLE_NAME, final String MASTER_CHECKID, ProgressDialog dialog){
        //Recieve array of tableNames
        //For each tableName do following process
        API api = Retroconfig.baseMethod().create(API.class);
        Call<ResponseBody> call = api.syncThisMasterTable(1234,MASTER_TABLE_NAME);
        dialog = new ProgressDialog(context);
        dialog.setMessage("Please wait");
        dialog.setCancelable(false);
        dialog.show();

        final ProgressDialog finalDialog = dialog;
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body()!=null){
                    finalDialog.dismiss();
                    String MASTER_MODIFIED_DATE = "ModifiedDate";
                    try {
                        serverDataList.clear();
                        localDataList.clear();
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            if (object.get("statusCode").equals("200")) {
                                JSONArray array = object.getJSONArray("Data");
                                if (array.length() != 0) {
                                    JSONObject object1 = array.getJSONObject(0);
                                    Iterator<String> keys = object1.keys();
                                    ArrayList<String> keyList = new ArrayList<>();
                                    while (keys.hasNext()) {
                                        String key = keys.next();
                                        keyList.add(key);
                                    }
                                    Log.d(TAG, "onResponse: KeyList -> " + keyList);
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject singleObj = array.getJSONObject(i);
                                        serverDataList.add(new IdAndModifiedDateModel(singleObj.getString(MASTER_CHECKID), singleObj.getString(MASTER_MODIFIED_DATE)));
                                    }//for i loop
                                    Log.d(TAG, "CompareData: ServerDataList -> " + serverDataList.toString());
                                    Cursor cursor = helper.getIdAndModifiedDate(MASTER_TABLE_NAME, MASTER_CHECKID, MASTER_MODIFIED_DATE);
                                    if (cursor != null && cursor.getCount() > 0) {
                                        cursor.moveToFirst();
                                        do {
                                            localDataList.add(new IdAndModifiedDateModel(cursor.getString(0), cursor.getString(1)));

                                        } while (cursor.moveToNext());

                                        Log.d(TAG, "CompareData: LocalDataList -> " + localDataList.toString());
                                        compareServerAndLocalData1(localDataList, serverDataList, array, MASTER_TABLE_NAME, MASTER_CHECKID, keyList);
                                    } else {
                                        helper.updateDataInLocalDB(MASTER_TABLE_NAME, MASTER_CHECKID, serverDataList, "A", array, keyList);
                                    }//cursor!= null && cursor.getCount() > 0
                                }//array.length() != 0
                                //dialog.dismiss();
                            }
                            else {
                                Log.d("Tag","No data found in json");
                            }//status code not equal to 200
                        }//res != null
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure:Failed");
                Toast.makeText(context,t.toString(),Toast.LENGTH_SHORT).show();
                finalDialog.dismiss();

            }
        });
    }

    private void compareServerAndLocalData1(List<IdAndModifiedDateModel> localDataList,
                                            List<IdAndModifiedDateModel> serverDataList,
                                            JSONArray serverDataArray,String tableNameMaster,String masterCheckID,
                                            ArrayList<String> keyList) throws JSONException {

        /*
         * Finding CommonDataList
         * */
        Set<IdAndModifiedDateModel> union = new HashSet<IdAndModifiedDateModel>(localDataList);
        union.addAll(serverDataList);
        // Prepare an intersection
        Set<IdAndModifiedDateModel> intersection = new HashSet<IdAndModifiedDateModel>(localDataList);
        intersection.retainAll(serverDataList);
        for (IdAndModifiedDateModel n : intersection) {
            commonDataList.add(n);
        }
        Collections.sort(commonDataList, IdAndModifiedDateModel.StuNameComparator);
        Log.d(TAG, "CompareData: commonElementList -> " + commonDataList);

        //UpdateData List
        List<IdAndModifiedDateModel> updateDataList = new ArrayList<>();

        for (int i = 0 ; i < commonDataList.size() ; i++){
            String localID = commonDataList.get(i).getId();
            String localModifiedDate = commonDataList.get(i).getModifiedDate();
            for (int j = 0 ; j < serverDataList.size() ; j++){
                String serverId = serverDataList.get(j).getId();
                String serverModifiedDate = serverDataList.get(j).getModifiedDate();
                if (localID.equals(serverId)){
                    if (localModifiedDate.equals(serverModifiedDate)){
                        Log.d(TAG, "compareServerAndLocalData1: Skip Record");
                    }else {
                        updateDataList.add(commonDataList.get(i));
                        Log.d(TAG, "compareServerAndLocalData1: Update Data List Added -> "+commonDataList.get(i));
                    }
                    Log.d(TAG, "compareServerAndLocalData1: J Jumped Off -> "+j);
                    j = serverDataList.size();
                }else {
                    Log.d(TAG, "compareServerAndLocalData1: !Equal LocalId and ServerId -> "+j);
                }
            }
//            IdAndModifiedDateModel localModel = commonDataList.get(i);
//            int pos = localDataList.indexOf(localModel);
//            Log.d(TAG, "compareServerAndLocalData1: Position -> "+pos+" -> " +localModel.toString());
//            IdAndModifiedDateModel serverModel = serverDataList.get(pos);
//            if (localModel.getModifiedDate().equals(serverModel.getModifiedDate())){
//                Log.d(TAG, "compareServerAndLocalData1: Skip Record");
//            }else {
//                updateDataList.add(commonDataList.get(i));
//            }
        }
        Log.d(TAG, "compareServerAndLocalData1: UpdateDataList -> " +updateDataList);


        /*
         * Finding UniqueDataList
         * */
        // Subtract the intersection from the union
        union.removeAll(intersection);
        // Print the result
        for (IdAndModifiedDateModel n : union) {
            uniqueDataList.add(n);
        }
        Log.d(TAG, "CompareData: uniqueElementList -> " +uniqueDataList);

        /*
         * Finding DeleteDataList
         * */
        Set<IdAndModifiedDateModel> intersection1 = new HashSet<IdAndModifiedDateModel>(localDataList);
        intersection1.retainAll(uniqueDataList);
        for (IdAndModifiedDateModel n : intersection1) {
            deleteDataList.add(n);
        }
        Log.d(TAG, "CompareData: deleteElementList -> " +deleteDataList);


        /*
         * Finding Add DataList
         * */
        uniqueDataList.removeAll(deleteDataList);
        for (IdAndModifiedDateModel n : uniqueDataList) {
            addDataList.add(n);
        }
        Log.d(TAG, "CompareData: addElementList -> " +addDataList);

        if (updateDataList.size() != 0){
            helper.updateDataInLocalDB(tableNameMaster,masterCheckID,updateDataList,"U",serverDataArray,keyList);
        }
        if (addDataList.size() != 0){
            helper.updateDataInLocalDB(tableNameMaster,masterCheckID,addDataList,"A",serverDataArray,keyList);
        }
        if (deleteDataList.size() != 0){
            helper.updateDataInLocalDB(tableNameMaster,masterCheckID,deleteDataList,"D",serverDataArray,keyList);
        }
    }

    public JsonArray getTableDataToJSon(Cursor cursor) {
        /*String myPath = DB_PATH + DB_NAME;// Set path to your database
        SQLiteDatabase myDataBase = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.CREATE_IF_NECESSARY);


        String searchQuery = "SELECT * FROM "+tableName;
        Cursor cursor = myDataBase.rawQuery(searchQuery, null);*/

        JsonArray resultSet = new JsonArray();

        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int totalColumn = cursor.getColumnCount();
            JsonObject rowObject = new JsonObject();

            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {

                        if (cursor.getString(i) != null) {
//                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.addProperty(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.addProperty(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
//                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }
            resultSet.add(rowObject);
            cursor.moveToNext();
        }
        cursor.close();
//        Log.d("TAG_NAME", resultSet.toString());
        return resultSet;
    }

    public static void getDbNameBasedOnModuleSelected(UserModel model,Context context){
        GlobalClass.db_global_name = "FG_"+model.getOEMID()+"_"+model.getModuleID()+".db";
        AppPreferences.setDatabseName(context,GlobalClass.db_global_name);
    }
}
