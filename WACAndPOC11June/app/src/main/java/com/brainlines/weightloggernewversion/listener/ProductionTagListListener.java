package com.brainlines.weightloggernewversion.listener;

import com.brainlines.weightloggernewversion.Production.Model.ProductionTagHistoryListModel;

public interface ProductionTagListListener {
    void isTagClicked(ProductionTagHistoryListModel model);
}
