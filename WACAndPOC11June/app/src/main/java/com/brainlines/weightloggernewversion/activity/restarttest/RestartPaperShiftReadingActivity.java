package com.brainlines.weightloggernewversion.activity.restarttest;

import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoggerFunctionActivity;
import com.brainlines.weightloggernewversion.adapter.newtestadapter.NewTestPaperShiftReadingAdapter;
import com.brainlines.weightloggernewversion.adapter.recalltestadapter.Record_paper_shift_reading_Adapter;
import com.brainlines.weightloggernewversion.broadcastreceiver.NetworkChangeReceiver;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.PaperShiftAnalyticsModel;
import com.brainlines.weightloggernewversion.model.PaperShiftResultsModel;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RestartPaperShiftReadingActivity extends BaseActivity implements View.OnClickListener{

    EditText ed_paper_shift_reading;
    ImageButton img_add_paper_shift_reading;
    ImageButton btn_observation,btn_abort;
    RecyclerView rv_paper_shift_reading;
    TextView alert_message;
    Button alert_button_ok,alert_button_cancel,btn_sync;
    AlertDialog alertDialog;
    public ArrayList<String> paper_shift_list = null;
    private ArrayList<PaperShiftResultsModel> paperShiftResultsModelArrayList = new ArrayList<>();
    private ArrayList<PaperShiftAnalyticsModel> paperShiftAnalyticsModelArrayList = new ArrayList<>();
    Record_paper_shift_reading_Adapter record_paper_shift_reading_adapter;
    String dataloggerName,dataloggerIp,current_tag_id;
    double max_reading_value=0.0;
    double min_reading_value = 0.0;
    double mean_of_readings = 0.0;
    double range = 0.0;
    double average = 0.0;
    double totals_sum =0.0;
    String status = "FAIL";
    private DataBaseHelper helper;

    String so_oinum ="";
    private NetworkChangeReceiver networkChangeReceiver;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss.SSS");
    private String weight_test_status;
    private DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private String str_tarhetwt_uom = "";
    double str_kg_value_from_local_db;
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_test_paper_shift_reading);
        initUi();
        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        dataloggerName = getIntent().getStringExtra("LoggerName");
        dataloggerIp = getIntent().getStringExtra("LoggerIP");
        current_tag_id = getIntent().getStringExtra("current_tag_id");
        so_oinum = getIntent().getStringExtra("so_oinum");
        weight_test_status = getIntent().getStringExtra("weight_test_status");
        str_kg_value_from_local_db = getIntent().getDoubleExtra("IntKgValue",0.0d);
        str_tarhetwt_uom = getIntent().getStringExtra("targetwt_uom");
        paper_shift_list = new ArrayList<String>();

    }
    private void initUi()
    {
        ed_paper_shift_reading = findViewById(R.id.ed_paper_shift_reading);
        rv_paper_shift_reading = findViewById(R.id.rv_paper_shift_reading);
        img_add_paper_shift_reading = findViewById(R.id.img_add_paper_shift_reading);
        btn_observation = findViewById(R.id.btn_sendPaperReadingsObs);
        btn_abort = findViewById(R.id.btn_sendBagLengthAbortBtn);
        btn_sync = findViewById(R.id.btn_sync);

        img_add_paper_shift_reading.setOnClickListener(this);
        btn_observation.setOnClickListener(this);
        btn_abort.setOnClickListener(this);
        btn_sync.setOnClickListener(this);
        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerNetworkBroadcastForNougat();
    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==img_add_paper_shift_reading.getId())
        {
            String bag_length_reading = ed_paper_shift_reading.getText().toString();
            if(bag_length_reading != null && bag_length_reading.equals("")){
                alertMessage();
                alert_message.setText("Invalid entry please check!!");
                alert_button_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
            }
            else
            {
                paper_shift_list.add(bag_length_reading);
                LinearLayoutManager linearLayoutManager = new GridLayoutManager(RestartPaperShiftReadingActivity.this,4);
                rv_paper_shift_reading.setLayoutManager(linearLayoutManager);
                rv_paper_shift_reading.setAdapter(new NewTestPaperShiftReadingAdapter(RestartPaperShiftReadingActivity.this,paper_shift_list));
                ed_paper_shift_reading.setText("");
            }
        }
        else if (view.getId()==btn_observation.getId())
        {
            InsertIntoDb();
            PaperShiftCalculation();
            //InsertIntoPaperShiftCalculationAnalyticsTable();
            Intent i = new Intent(RestartPaperShiftReadingActivity.this, RestartObservationActivity.class);
            i.putExtra("LoggerName",dataloggerName);
            i.putExtra("LoggerIP",dataloggerIp);
            i.putExtra("current_tag_id",current_tag_id);
            i.putExtra("so_oinum",so_oinum);
            i.putExtra("weight_test_status",weight_test_status);
            i.putExtra("IntKgValue",str_kg_value_from_local_db);
            i.putExtra("targetwt_uom",str_tarhetwt_uom);

            startActivity(i);

        }
        else if (view.getId()==btn_abort.getId())
        {
            try {
                abort();
                status = "Abort";
                helper.updateOverallStatusForTag(status,current_tag_id);
                Intent i = new Intent(RestartPaperShiftReadingActivity.this, LoggerFunctionActivity.class);
                i.putExtra("LoggerName",dataloggerName);
                i.putExtra("LoggerIP",dataloggerIp);
                i.putExtra("tagid",current_tag_id);
                startActivity(i);
                finish();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if (view.getId()==btn_sync.getId()){
            InsertIntoDb();
            PaperShiftCalculation();
            //InsertIntoPaperShiftCalculationAnalyticsTable();

        }
    }

    public void alertMessage() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.layout_alert_dialog, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        alert_button_ok= dialogView.findViewById(R.id.alert_btn_ok);
        alert_message= dialogView.findViewById(R.id.alert_message);
        alertDialog = builder.create();
        alertDialog.show();

    }

    public  void InsertIntoDb() {
//        DataBaseHelper helper=new DataBaseHelper(RestartPaperShiftReadingActivity.this);
//        SQLiteDatabase db = helper.getWritableDatabase();

        String currentDateandTime = sdf.format(new Date());

        for (int i = 0;i<paper_shift_list.size();i++)
        {
            ContentValues cv=new ContentValues();
            cv.put("OEMID","1234");
            cv.put("mac_id","18:fe:34:8b:60:75");
            cv.put("LoggerID",dataloggerName);
            cv.put("tagid",current_tag_id);
            cv.put("shift",paper_shift_list.get(i));
            cv.put("oinum",so_oinum);
            cv.put("modifieddate",currentDateandTime);
            cv.put("IsSync","0");
            long d=db.insert(Constants.PAPER_SHIFT_RESULTS,null,cv);
            Log.d("Success", String.valueOf(d));

        }
    }

    public void PaperShiftCalculation() {
        if (paper_shift_list.size() != 0){
            //maximum reading
            max_reading_value = Double.parseDouble(paper_shift_list.get(0));
            for (int cnt =1;cnt < paper_shift_list.size(); cnt++)
            {
                if (Double.parseDouble(paper_shift_list.get(cnt)) > max_reading_value)
                {
                    max_reading_value = Double.parseDouble(paper_shift_list.get(cnt));
                }
            }

            //minimum reading
            min_reading_value = Double.parseDouble(paper_shift_list.get(0));
            for (int cnt =1; cnt < paper_shift_list.size(); cnt++)
            {
                if (Double.parseDouble(paper_shift_list.get(cnt)) < min_reading_value)
                {
                    min_reading_value = Double.parseDouble(paper_shift_list.get(cnt));
                }
            }

            //mean calculation
            for (int cnt = 0 ; cnt< paper_shift_list.size() ; cnt++)
            {
                totals_sum += Double.parseDouble(paper_shift_list.get(cnt));
            }
            //mean_of_readings = totals_sum/paper_shift_list.size();
            mean_of_readings = (max_reading_value - min_reading_value)/2 + min_reading_value;

            //range calculation
            //range = max_reading_value - min_reading_value;

            range = (max_reading_value - min_reading_value)/2;

            double rangeDb = Double.valueOf(range);
            double tolerance = 2.0;

            if (rangeDb <= tolerance) {
                status = "PASS";
            }
            else
            {
                status = "FAIL";
            }
            helper.updatepapershiftstatus(status,current_tag_id);


            //update overall status in C_tags table
            if (weight_test_status.equals("PASS") && status.equals("FAIL"))
            {
                helper.updateOverallStatusForTag("RESUME",current_tag_id);
            }
            else if (weight_test_status.equals("FAIL"))
            {
                helper.updateOverallStatusForTag("FAIL",current_tag_id);
            }

//            DataBaseHelper helper=new DataBaseHelper(RestartPaperShiftReadingActivity.this);
//            SQLiteDatabase db = helper.getWritableDatabase();
            String currentDateandTime = sdf.format(new Date());
            String strrangewithpm = "+/-" +String.valueOf(decimalFormat.format(range));

            ContentValues cv=new ContentValues();
            cv.put("OEMID","1234");
            cv.put("LoggerID",dataloggerName);
            cv.put("TagID",current_tag_id);
            cv.put("MinReading",decimalFormat.format(min_reading_value));
            cv.put("MaxReading",decimalFormat.format(max_reading_value));
            cv.put("RangeReading",strrangewithpm);
            cv.put("RunStatus",status);
            cv.put("ModifiedDate",currentDateandTime);
            cv.put("MeanReading",decimalFormat.format(mean_of_readings));
            cv.put("IsSync","0");

            long d=db.insert(Constants.PAPER_SHIFT_ANALYTICS_TABLE,null,cv);
            Log.d("Success", String.valueOf(d));


            //InsertIntoPaperShiftCalculationAnalyticsTable();
        }
    }


    private String abort() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String tag = current_tag_id;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String dataPort=Constants.DATA_LOGGER_PORT+"";
        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;

    }


    @Override
    protected void dialog(boolean value) {
        super.dialog(value);
//        if(value){
//            InsertIntoDb();
//            PaperShiftCalculation();
//            InsertIntoPaperShiftCalculationAnalyticsTable();
//            getAllEntriesFromPaperShiftResults();
//            getAllEntriesFromPaperShiftAnalytics();
//            Toast.makeText(this, "DATA Pushed", Toast.LENGTH_SHORT).show();
//        }else {
//            Toast.makeText(this, "DATA Not Pushed", Toast.LENGTH_SHORT).show();
//        }
    }
}
