package com.brainlines.weightloggernewversion.utils;

public class Queries {
    public static final String DROP_PRODUCTION_KG_VALUES_MASTER_TABLES = "DROP TABLE IF EXISTS " +Constants.PRODUCTION_KG_VALUES_MASTER_TABLES;
    public static final String CREATE_PRODUCTION_KG_VALUES_MASTER_TABLES= "CREATE TABLE " + Constants.PRODUCTION_KG_VALUES_MASTER_TABLES+" ("
            + Constants.PRODUCTION_GRAMMAGE_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_GRAMMAGE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_KG_VALUES +" VARCHAR(50))";
    public static final String CREATE_USER_LOGIN_TABLE = "CREATE TABLE "
            + Constants.USER_LOGIN_TABLE+" ("
            + Constants.LOGIN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.USER_ID+" INTEGER ,"
            + Constants.ROLE_ID+" INTEGER ,"
            + Constants.USER_NAME+" VARCHAR(50) ,"
            + Constants.PASSWORD+" VARCHAR(50) ,"
            + Constants.IS_ACTIVE+" BIT ,"
            + Constants.IS_DELETED+" BIT ,"
            + Constants.CREATED_DATE+" VARCHAR(255) ,"
            + Constants.MODIFIED_DATE+" VARCHAR(255))";


   /* //Create Tables
    //Create User Login Table

    //Create Data Logger Table
    public static final String CREATE_DATA_LOGGER_TABLE =
            "CREATE TABLE "
                    + Constants.DATA_LOGGER_TABLE+" ("
                    + Constants.LOGGER_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + Constants.OEMID+" INTEGER,"
                    + Constants.LOGGER_SERAIL_NUM+" VARCHAR(50) ,"
                    + Constants.LOGGER_NAME+" VARCHAR(150) ,"
                    + Constants.LOGGER_IP_ADDRESS+" VARCHAR(50) ,"
                    + Constants.LOGGER_MAC_ID+" VARCHAR(150) ,"//*********************Check
                    + Constants.LOGGER_LOCATION+" VARCHAR(150) ,"
                    + Constants.LOGGER_CREATE_TIMESTAMP+" VARCHAR(255) ,"
                    + Constants.LOGGER_CREATED_BY+" VARCHAR(50) ,"//************Check
                    + Constants.LOGGER_UPDATE_TIMESTAMP+" VARCHAR(100) ,"
                    + Constants.LOGGER_MODIFIED_BY+" VARCHAR(50) ,"
                    + Constants.LOGGER_IS_ACTIVE+" BIT ,"
                    + Constants.LOGGER_IN_USE+" BIT)";

    //Create Master Filler Type Table
    public static final String CREATE_MASTER_FILLER_TABLE = "CREATE TABLE "
            + Constants.MASTER_FILLER_TYPE_TABLE+" ("
            + Constants.FILLER_TYPE_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.FILLER_TYPE_OEMID+" INTEGER ,"
            + Constants.FILLER_TYPE_PRODUCT_TYPE_ID+" VARCHAR(200) ,"
            + Constants.FILLER_TYPE_FILTER_TYPE+" VARCHAR(200) ,"
            + Constants.FILLER_TYPE_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.FILLER_TYPE_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.FILLER_TYPE_CREATED_BY+" VARCHAR(255) ,"
            + Constants.FILLER_TYPE_MODIFIED_BY+" VARCHAR(255) ,"
            + Constants.FILLER_TYPE_IS_ACTIVE+" BIT ,"
            + Constants.FILLER_TYPE_IS_DELETED+" BIT)";

    //Create Master Head Table
    public static final String CREATE_MASTER_HEAD_TABLE = "CREATE TABLE "
            + Constants.MASTER_HEAD_TABLE+" ("
            + Constants.MASTER_HEAD_HEAD_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MASTER_HEAD_OEM_ID+" INTEGER ,"
            //+Constants.MASTER_HEAD_HEAD_ID_+" INTEGER ,"
            + Constants.MASTER_HEAD_HEAD_TYPE_+" VARCHAR(500) ,"
            + Constants.MASTER_HEAD_IS_ACTIVE+" BIT ,"
            + Constants.MASTER_HEAD_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_HEAD_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_HEAD_CREATED_BY+" VARCHAR(255) ,"
            + Constants.MASTER_HEAD_MODIFIED_BY+" VARCHAR(255))";

    //Create Comments Table
    public static final String CREATE_COMMENTS_TABLE = "CREATE TABLE "
            + Constants.COMMENTS_TABLE+" ("
            + Constants.COMMENTS_ID+" INTEGER ,"
            + Constants.COMMENTS_OEM_ID+" INTEGER ,"
            + Constants.COMMENTS_MAC_ID+" INTEGER ,"
            + Constants.COMMENTS_LOGGER_ID+" INTEGER ,"
            + Constants.COMMENTS_TAG_ID+" VARCHAR(900) ,"
            + Constants.COMMENTS_COMMENTS_COLUMN+" VARCHAR(300) ,"
            + Constants.COMMENTS_FUN_COMMENTS_1+" VARCHAR(255) ,"
            + Constants.COMMENTS_FUN_COMMENTS_2+" VARCHAR(255) ,"
            + Constants.COMMENTS_FUN_COMMENTS_3+" VARCHAR(255) ,"
            + Constants.COMMENTS_FUN_COMMENTS_4+" VARCHAR(255) ,"
            + Constants.COMMENTS_FUN_COMMENTS_5+" VARCHAR(255) ,"
            + Constants.COMMENTS_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.COMMENTS_IS_SYNC+" BIT)";

    //Create Bag Length Analytics
    public static final String CREATE_BAG_LENGTH_ANALYTICS_TABLE = "CREATE TABLE "
            + Constants.BAG_LENGTH_ANALYTICS_TABLE+" ("
            + Constants.BAG_LEN_ANA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.BAG_LEN_ANA_OEM_ID+" INTEGER ,"
            + Constants.BAG_LEN_ANA_LOGGER_ID+" INTEGER ,"
            + Constants.BAG_LEN_ANA_TAG_ID+" VARCHAR(900) ,"
            + Constants.BAG_LEN_ANA_MIN_READING+" VARCHAR(45) ,"
            + Constants.BAG_LEN_ANA_MAX_READING+" VARCHAR(45) ,"
            + Constants.BAG_LEN_ANA_RANGE_READING+" VARCHAR(45) ,"
            + Constants.BAG_LEN_ANA_RUN_STATUS+" VARCHAR(45) ,"
            + Constants.BAG_LEN_ANA_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.BAG_LEN_ANA_MEAN_READING+" VARCHAR(255) ,"
            + Constants.BAG_LEN_ANA_IS_SYNC+" BIT)";

    //Create SD Mean Table
    public static final String CREATE_SD_MEAN_TABLE = "CREATE TABLE "
            + Constants.SD_MEAN_TABLE+" ("
            + Constants.SD_MEAN_OEM_ID+" INTEGER ,"
            + Constants.SD_MEAN_LOGEGR_ID+" INTEGER ,"
            + Constants.SD_MEAN_TAG_ID+" VARCHAR(900) ,"
            + Constants.SD_MEAN_STD_DEVIATION+" VARCHAR(255) ,"
            + Constants.SD_MEAN_MEAN_COLUMN+" VARCHAR(255) ,"
            + Constants.SD_MEAN_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.SD_MEAN_IS_SYNC+" BIT)";

    //Create Observations Table
    public static final String CREATE_OBSERVATIONS_TABLE = "CREATE TABLE "
            + Constants.OBSERVATIONS_TABLE+" ("
            + Constants.OBSERVATIONS_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.OBSERVATIONS_OEM_ID+" INTEGER ,"
            + Constants.OBSERVATIONS_MAC_ID+" VARCHAR(50) ,"
            + Constants.OBSERVATIONS_LOGGER_ID+" VARCHAR(50) ,"
            + Constants.OBSERVATIONS_TAG_ID+" VARCHAR(900) ,"
            + Constants.OBSERVATIONS_AUGER_COUNT+" VARCHAR(50) ,"
            + Constants.OBSERVATIONS_AUGER_SPEED+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_AUGER_PRE_AUGER+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_AUGER_NO_OF_AGITATORS+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_VERTICAL_TEFLON+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_HORIZONTAL_LAW_OPENING+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_HORIZONTAL_LAW_MOVEMENT+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_VERTICAL_ALIGN_CENTER+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_MACHINE_VERTICAL_LAW_MOVEMENT+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_WIRE_CUP+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_EMPTY_POUCHES+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_BAG_LENGTH_VARIATION+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_PAPER_SHIFTING+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_VERTICAL_OVERLAP+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_POUCH_SYMMETRY+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_OBSERV_SPEED+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_SEAL_REJECTION_REASON+" VARCHAR(100) ,"
            + Constants.OBSERVATIONS_PACKAGE_IS_SYNC+" BIT)";

    //Create Paper Shift Analytics
    public static final String CREATE_PAPER_SHIFT_ANALYTICS_TABLE = "CREATE TABLE "
            + Constants.PAPER_SHIFT_ANALYTICS_TABLE+" ("
            + Constants.PAPER_SHIFT_ANALYTICS_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PAPER_SHIFT_ANALYTICS_OEM_ID+" INTEGER ,"
            + Constants.PAPER_SHIFT_ANALYTICS_LOGGER_ID+" INTEGER ,"
            + Constants.PAPER_SHIFT_ANALYTICS_TAG_ID+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_MIN_READING+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_MAX_READING+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_RANGE_READING+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_RUN_STATUS+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_MEAN_READING+" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_ANALYTICS_IS_SYNC+" Bit)";

    //Create Weight Results Analytics
    public static final String CREATE_WEIGHT_RESULTS_ANALYTICS_TABLE = "CREATE TABLE "
            + Constants.WEIGHT_RESULTS_ANALYTICS_TABLE+" ("
            + Constants.WEIGHT_RESULTS_ANALYTICS_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_OEM_ID+" INTEGER ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_LOGGER_ID+" INTEGER ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_TAG_ID+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_MIN_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_MAX_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_RANGE_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_MEAN_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_SD_READING+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_REMARKS+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_RUN_STATUS+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_GM+" VARCHAR(60) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_GM_PERCENT+" VARCHAR(60) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD+" VARCHAR(60) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD_PERCENT+" VARCHAR(60) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_AVERAGE_GIVE_AWAY+" VARCHAR(255) ,"
            + Constants.WEIGHT_RESULTS_ANALYTICS_IS_SYNC+" BIT)";

    //Create Master Products
    public static final String CREATE_MASTER_PRODUCTS_TABLE = "CREATE TABLE "
            + Constants.MASTER_PRODUCTS_TABLE+" ("
            + Constants.MASTER_PRODUCTS_PRODUCTS_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MASTER_PRODUCTS_OEM_ID+" INTEGER ,"
            + Constants.MASTER_PRODUCTS_PRODUCTS+" VARCHAR(50) ,"
            + Constants.MASTER_PRODUCTS_TYPE_ID+" INTEGER ,"
            + Constants.MASTER_PRODUCTS_IS_ACTIVE+" BIT ,"
            + Constants.MASTER_PRODUCTS_IS_DELETED+" BIT ,"
            + Constants.MASTER_PRODUCTS_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_PRODUCTS_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_PRODUCTS_CREATED_BY+" VARCHAR(255) ,"
            + Constants.MASTER_PRODUCTS_MODIFIED_BY+" VARCHAR(255),"
            + Constants.MASTER_PRODUCTS_PRICE_PER_KG+" VARCHAR(255),"
            + Constants.MASTER_PRODUCTS_CURRENCY+" VARCHAR(255))";

    //Create Weight Results Analytics
    public static final String CREATE_MASTER_POUCH_SYMMETRY_TABLE = "CREATE TABLE "
            + Constants.MASTER_POUCH_SYMMETRY_TABLE+" ("
            + Constants.MASTER_POUCH_SYMMETRY_ID+" INTEGER ,"
            + Constants.MASTER_POUCH_OEM_ID+" INTEGER ,"
            + Constants.MASTER_POUCH_ID+" INTEGER ,"
            + Constants.MASTER_POUCH_TYPE_ID+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_DESC+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_IS_ACTIVE+" BIT ,"
            + Constants.MASTER_POUCH_IS_DELETED+" BIT ,"
            + Constants.MASTER_POUCH_CREATED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_MODIFIED_DATE+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_CREATED_BY+" VARCHAR(255) ,"
            + Constants.MASTER_POUCH_MODIFIED_BY+" VARCHAR(255))";


    public static final String CREATE_C_TAGS_TABLE = "CREATE TABLE " + Constants.C_TAGS_TABLE+" ("
            + Constants.C_TAG_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.C_TAG_OEM_ID +" INT ,"
            + Constants.C_TAG_MACID +" VARCHAR(60) ,"
            + Constants.C_TAG_LOGGER_ID +" VARCHAR(60) ,"
            + Constants.C_TAG_TAGS_ID +" VARCHAR(1000) ,"
            + Constants.C_TAG_OEM_NUMBER +" VARCHAR(100) ,"
            + Constants.C_TAG_TARGET_WIGHT +" VARCHAR(100) ,"
            + Constants.C_TAG_CUSTOMER_NAME +" VARCHAR(60) ,"
            + Constants.C_TAG_MACHINE_TYPE_ID +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT_TYPE_ID +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT_TYPE_NAME +" VARCHAR(60) ,"
            + Constants.C_TAG_MACHINE_TYPE_NAME +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT +" VARCHAR(60) ,"
            + Constants.C_TAG_OPERATOR +" VARCHAR(60) ,"
            + Constants.C_TAG_TARGET_WEIGHT_UNIT +" VARCHAR(60) ,"
            + Constants.C_TAG_INSPECTOR +" VARCHAR(100) ,"
            + Constants.C_TAG_FILEM_TYPE +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT_FEED +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_MOTOR_FREQ +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_MODE +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_OD +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_ID +" VARCHAR(60) ,"
            + Constants.C_TAG_AGITATOR_PITCH +" VARCHAR(60) ,"
            + Constants.C_TAG_TARGET_ACCOUNT_TYPE +" VARCHAR(60) ,"
            + Constants.C_TAG_TARGET_ACCOUNT_VALUE +" VARCHAR(60) ,"
            + Constants.C_TAG_MACHINE_SERIAL_NUMBER +" VARCHAR(60) ,"
            + Constants.C_TAG_PRODUCT_BULK_MAIN +" VARCHAR(60) ,"
            + Constants.C_TAG_TARGET_SPEED +" VARCHAR(60) ,"
            + Constants.C_TAG_HEAD +" VARCHAR(50) ,"
            + Constants.C_TAG_AIR_PRESSURE +" VARCHAR(50) ,"
            + Constants.C_TAG_QUANTITY_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_BAG_LENGTH_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_FILLVALVE_GAP +" VARCHAR(50) ,"
            + Constants.C_TAG_FILL_VFD_FREQUENCY +" VARCHAR(50) ,"
            + Constants.C_TAG_HORIZONTAL_CUT_SEAL_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_HORIZONTAL_BAND_SEAL_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_VERTICAL_SEAL_SETTING +" VARCHAR(50) ,"
            + Constants.C_TAG_AUGER_AGITATOR_FREQUENCY +" VARCHAR(50) ,"
            + Constants.C_TAG_AUGER_FILL_VALVE +" VARCHAR(50) ,"
            + Constants.C_TAG_AUGER_PITCH +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_WIDTH +" VARCHAR(60) ,"
            + Constants.C_TAG_POUCH_LENGTH +" VARCHAR(60) ,"
            + Constants.C_TAG_POUCH_SEAL_TYPE +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_HORIZONTAL_FREQUENCY +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_HZB +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERTICAL +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERT_FL +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERTICAL_FREQUENCY +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERT_BL +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_TEMP_VERT_BR +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_GUSSET +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_FILM_THICKNESS +" VARCHAR(50) ,"
            + Constants.C_TAG_PUCH_PRODUCT_BULK +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_EQ_HORIZ_SERRATIONS +" VARCHAR(50) ,"
            + Constants.C_TAG_POUCH_FILL_eVENT +" VARCHAR(50) ,"
            + Constants.C_TAG_OVERALL_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_REMARK +" VARCHAR(255) ,"
            + Constants.C_TAG_SEAL_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_DROP_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_TARGET_SPEED_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_WEIGHT_TEST_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_BAG_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_PAPER_STATUS +" VARCHAR(255) ,"
            + Constants.C_TAG_FY_YEAR +" VARCHAR(255) ,"
            + Constants.C_TAG_FILLER_TYPE +" VARCHAR(255) ,"
            + Constants.C_TAG_SEAL_REJECTION_REASON +" VARCHAR(255) ,"
            + Constants.C_TAG_CUSTOMER_PRODUCT +" VARCHAR(50) ,"
            + Constants.C_TAG_CUSTOMER_FILM +" VARCHAR(50) ,"
            + Constants.C_TAG_NEGATIVE_WEIGHT +" VARCHAR(50) ,"
            + Constants.C_TAG_LOGGER_CREATE_TIMESTAMP +" VARCHAR(50) ,"
            + Constants.C_TAG_CREATED_DATE +" VARCHAR(255) ,"
            + Constants.C_TAG_MODIFIED_DATE +" VARCHAR(255) ,"
            + Constants.C_TAGS_IS_SYNC +" BIT)";

    public static final String CREATE_MASTER_WIRE_CUP_TABLE = "CREATE TABLE " + Constants.MASTER_WIRE_CUP_TABLE+" ("
            + Constants.MASTER_WIRE_CUP_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MASTER_WIRE_CUP_OEM_ID +" INT ,"
            + Constants.MASTER_WIRE_CUP +" INT ,"
            + Constants.MASTER_WIRE_CUP_IS_ACTIVE +" INT ,"
            + Constants.MASTER_WIRE_CUP_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.MASTER_WIRE_CUP_MODIFIED_DATE +" VARCHAR(100) ,"
            + Constants.MASTER_WIRE_CUP_CREATED_BY +" VARCHAR(60) ,"
            + Constants.MASTER_WIRE_CUP_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_TOLERANCE_TABLE = "CREATE TABLE " + Constants.TOLERANCE_TABLE+" ("
            + Constants.TOLERANCE_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.TOLERANCE_OEM_ID +" INT ,"
            + Constants.TOLERANCE_PRODUCT_TYPE +" VARCHAR(100) ,"
            + Constants.TOLERANCE_TARGET_WEIGHT +" INT ,"
            + Constants.TOLERANCE_MIN_TARGET_WEIGHT +" INT ,"
            + Constants.TOLERANCE_MAX_TARGET_WEIGHT +" INT ,"
            + Constants.TOLERANCE_MIN_WEIGHT_SAMPLES +" INT ,"
            + Constants.TOLERANCE_MIN_PAPER_SHIFT_SAMPLES +" INT ,"
            + Constants.TOLERANCE_PAPER_SHIFT_TOLERANCE +" INT ,"
            + Constants.TOLERANCE_MIN_BAG_LENGTH_SAMPLES +" INT ,"
            + Constants.TOLERANCE_BAG_LENGTH_TOLERANCE +" VARCHAR(60) ,"
            + Constants.TOLERANCE_LOGIN_ID +" INT ,"
            + Constants.TOLERANCE_USER_ID +" INT ,"
            + Constants.TOLERANCE_ROLE_ID +" INT ,"
            + Constants.TOLERANCE_USER_NAME +" VARCHAR(100) ,"
            + Constants.TOLERANCE_PASSWORD +" VARCHAR(100) ,"
            + Constants.TOLERANCE_IS_ACTIVE +" VARCHAR(50) ,"
            + Constants.TOLERANCE_IS_DELETED +" VARCHAR(60) ,"
            + Constants.TOLERANCE_CREATED_DATE +" VARCHAR(60) ,"
            + Constants.TOLERANCE_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.TOLERANCE_CREATED_BY +" VARCHAR(60) ,"
            + Constants.TOLERANCE_MODIFIED_BY +" VARCHAR(255) ,"
            + Constants.TOLERANCE_POSITIVE_TOLERANCE + " VARCHAR(255) ,"
            + Constants.TOLERANCE_NEGATIVE_TOLERANCE + " VARCHAR(255))";

    public static final String CREATE_SEAL_REJECTION_TABLE = "CREATE TABLE " + Constants.SEAL_REJECTION_TABLE+" ("
            + Constants.SEAL_REJECTION_REASON_ID +" INT ,"
            + Constants.SEAL_REJECTION_OEM_ID +" INT ,"
            + Constants.SEAL_REJECTION_LOGGER_ID +" INT ,"
            + Constants.SEAL_REJECTION_PRODUCT_TYPE +" VARCHAR(100) ,"
            + Constants.SEAL_REJECTION_REASON_DESCIPTION +" VARCHAR(100) ,"
            + Constants.SEAL_REJECTION_IS_ACTIVE +" VARCHAR(100) ,"
            + Constants.SEAL_REJECTION_IS_DELETED +" VARCHAR(100) ,"
            + Constants.SEAL_REJECTION_CREATED_DATE +" VARCHAR(50) ,"
            + Constants.SEAL_REJECTION_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.SEAL_REJECTION_CREATED_BY +" VARCHAR(60) ,"
            + Constants.SEAL_REJECTION_MODIFIED_BY +" VARCHAR(60) ,"
            + Constants.SEAL_REJECTION_IS_SYNC +" BIT)";


    public static final String CREATE_TARGET_ACCURACY_TABLE = "CREATE TABLE " + Constants.TARGET_ACCURACY_TABLE+" ("
            + Constants.TARGET_ACCURACY_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.TARGET_ACCURACY_OEM_ID +" INT ,"
            + Constants.TARGET_ACCURACY_TARGET_ID +" INT ,"
            + Constants.TARGET_ACCURACY +" INT ,"
            + Constants.TARGET_ACCURACY_DESCRIPTION +" VARCHAR(100) ,"
            + Constants.TARGET_ACCURACY_IS_ACTIVE +" VARCHAR(100) ,"
            + Constants.TARGET_ACCURACY_IS_DELETED +" VARCHAR(100) ,"
            + Constants.TARGET_ACCURACY_CREATED_DATE +" VARCHAR(50) ,"
            + Constants.TARGET_ACCURACY_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.TARGET_ACCURACY_CREATED_BY +" VARCHAR(60) ,"
            + Constants.TARGET_ACCURACY_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_MACHINE_TYPE_TABLE = "CREATE TABLE " + Constants.MACHINE_TYPE_TABLE+" ("
            + Constants.MACHINE_TYPE_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MACHINE_TYPE_OEM_ID +" INT ,"
            + Constants.MACHINE_TYPE_MACHINE_ID +" INT ,"
            + Constants.MACHINE_TYPE_PRODUCT_TYPE_ID +" INT ,"
            + Constants.MACHINE_TYPE_MACHINE_NAME +" VARCHAR(100) ,"
            + Constants.MACHINE_TYPE_MACHINE_DESCRIPTION +" VARCHAR(100) ,"
            + Constants.MACHINE_TYPE_IS_ACTIVE +" VARCHAR(100) ,"
            + Constants.MACHINE_TYPE_IS_DELETED +" VARCHAR(50) ,"
            + Constants.MACHINE_TYPE_CREATED_DATE +" VARCHAR(60) ,"
            + Constants.MACHINE_TYPE_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.MACHINE_TYPE_CREATED_BY +" VARCHAR(60) ,"
            + Constants.MACHINE_TYPE_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_POUCH_ATTRIBUTES_TABLE = "CREATE TABLE " + Constants.POUCH_ATTRIBUTES_TABLE+" ("
            + Constants.POUCH_ATTRIBUTES_ID +" INT ,"
            + Constants.POUCH_ATTRIBUTES_OEM_ID +" INT ,"
            + Constants.POUCH_ATTRIBUTES_PRODUCT_TYPE_ID +" INT ,"
            + Constants.POUCH_ATTRIBUTES_DESCRIPTION +" TEXT ,"
            + Constants.POUCH_ATTRIBUTES_IS_ACTVIE +" INT ,"
            + Constants.POUCH_ATTRIBUTES_IS_DELETED +" VARCHAR(100) ,"
            + Constants.POUCH_ATTRIBUTES_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.POUCH_ATTRIBUTES_MODIFIED_DATE +" VARCHAR(50) ,"
            + Constants.POUCH_ATTRIBUTES_CREATED_BY +" VARCHAR(60) ,"
            + Constants.POUCH_ATTRIBUTES_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_SEAL_TYPE_TABLE = "CREATE TABLE " + Constants.SEAL_TYPE_TABLE+" ("
            + Constants.SEAL_TYPE_SEAL_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.SEAL_TYPE +" VARCHAR(100) ,"
            + Constants.SEAL_TYPE_OEM_ID +" INT ,"
            + Constants.SEAL_TYPE_PRODUCT_TYPE_ID +" INT ,"
            + Constants.SEAL_TYPE_IS_ACTIVE +" INT ,"
            + Constants.SEAL_TYPE_IS_DELETED +" VARCHAR(100) ,"
            + Constants.SEAL_TYPE_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.SEAL_TYPE_MODIFIED_DATE +" VARCHAR(50) ,"
            + Constants.SEAL_TYPE_CREATED_BY +" VARCHAR(60) ,"
            + Constants.SEAL_TYPE_MODIFIED_BY +" VARCHAR(255))";

    public static final String CREATE_P_TAG_RESULT_TABLE = "CREATE TABLE " + Constants.P_TAG_RESULT_TABLE+" ("
            + Constants.P_TAG_RESULT_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.P_TAG_RESULT_OEM_ID +" INT ,"
            + Constants.P_TAG_RESULT_MAC_ID +" INT ,"
            + Constants.P_TAG_RESULT_LOGGER_ID +" INT ,"
            + Constants.P_TAG_RESULT_RUN_STATUS_ID +" INT ,"
            + Constants.P_TAG_RESULT_TAG_ID +" VARCHAR(100) ,"+ Constants.P_TAG_RESULT_BATCH_ID +" VARCHAR(100) ,"
            + Constants.P_TAG_RESULT_TARGET_WEIGHT +" VARCHAR(100) ,"
            + Constants.P_TAG_RESULT_HEAD +" VARCHAR(50) ,"
            + Constants.P_TAG_RESULT_STATUS_ABORTED +" VARCHAR(60) ,"
            + Constants.P_TAG_RESULT_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.P_TAG_RESULT_IS_AUTHORIZED +" VARCHAR(60) ,"
            + Constants.P_TAG_RESULT_MODIFIED_BY +" VARCHAR(255),"
             + Constants.P_TAG_RESULT_IS_SYNC +" BIT)";

    public static final String CREATE_PRODUCTION_TAG_TABLE = "CREATE TABLE " + Constants.PRODUCTION_TAG_TABLE+" ("
            + Constants.PRODUCTION_TAG_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_TAG +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TAG_OEM_ID +" INT ,"
            + Constants.PRODUCTION_TAG_MAC_ID +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TAG_BATCH_ID +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TAG_PRODUCT_ID +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TAG_GM_PER_POUCH +" VARCHAR(50) ,"
            + Constants.PRODUCTION_TAG_UNIT +" VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_TOTAL_QTY +" VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_TOTAL_QTY_UOM +" VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_MODIFIED_DATE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_SHIFT + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_NO_OF_POUCHES + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_SESSION_POSITVIE_TOLERANCE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_SESSION_NEAGTIVE_TOLERANCE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_MASTER_TOLERANCE_POSITIVE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_MASTER_TOLERANCE_NEGATIVE + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_UCL_GM + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_LCL_GM + " VARCHAR(60) ,"
            + Constants.PRODUCTION_TAG_OPERATOR_NAME + " VARCHAR(50) ,"
            + Constants.PRODUCTION_TAG_BATCH_STATUS + " VARCHAR(250) ,"
            + Constants.PRODUCTION_TAG_IS_SYNC +" BIT)";

    public static final String CREATE_P_WEIGHT_READING_TABLE = "CREATE TABLE " + Constants.P_WEIGHT_READING_TABLE+" ("
            + Constants.P_WEIGHT_READING_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.P_WEIGHT_READING_OEM_ID +" INT ,"
            + Constants.P_WEIGHT_READING_MAC_ID +" INT ,"
            + Constants.P_WEIGHT_READING_LOGGER_ID +" INT ,"
            + Constants.P_WEIGHT_READING_WEIGHT_ID +" INT ,"
            + Constants.P_WEIGHT_READING_BATCHID +" VARCHAR(60) ,"
            + Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME +" VARCHAR(100) ,"
            + Constants.P_WEIGHT_READING_WEIGHT +" VARCHAR(100) ,"
            + Constants.P_WEIGHT_READING_SHIFT +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_TARGETGMPERPOUCH +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES +" VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG + " VARCHAR(50) ,"
            + Constants.P_WEIGHT_READING_MODIFIED_DATE +" VARCHAR(60) ,"
            + Constants.P_WEIGHT_READING_STATUS +" VARCHAR(60) ,"
            + Constants.P_WEIGHT_READING_CREATED_DATE +" VARCHAR(60) ,"
            + Constants.P_WEIGHT_READING_CREATED_BY +" VARCHAR(255) ,"
            + Constants.P_WEIGHT_READING_MODIFIED_BY +" VARCHAR(255) ,"
            + Constants.P_WEIGHT_READING_IS_SYNC +" BIT)";

    public static final String CREATE_PRODUCTION_WEIGHT_REPORT_TABLE = "CREATE TABLE " + Constants.PRODUCTION_WEIGHT_REPORT+" ("
            + Constants.PRODUCTION_WEIGHT_REPORT_REPORTID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_OEMID +" INT ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_BATCH_ID +" INT ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_TAGID +" INT ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_MIN_WEIGHT +" VARCHAR(100) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_MAX_WEIGHT +" VARCHAR(100) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_MEAN_WEIGHT +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_AVERAGE_WEIGHT +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_RANGE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_SD +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_UNFILLED_QUANTITY +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_FILLED_QUANTITY +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_UNFILLED_POUCHES +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_FILLED_POUCHES +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_EXCESS_GIVEAWAY_PRICE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_EXCESS_GIVEAWAY_GM +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_REASON +" VARCHAR(60) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_REMARK +" VARCHAR(60) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_ISACTIVE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_CREATED_DATE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_MODIFIED_DATE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_WEIGHT_REPORT_IS_SYNC +" BIT)";

    public static final String CREATE_BAG_LENGTH_VARIATION_RESULT_TABLE = "CREATE TABLE " + Constants.BAG_LENGTH_VARIATION_RESULT+" ("
            + Constants.BAG_LENGTH_VARIATION_OEM_ID +" INT ,"
            + Constants.BAG_LENGTH_VARIATION_MAC_ID +" INT ,"
            + Constants.BAG_LENGTH_VARIATION_LOGGER_ID +" INT ,"
            + Constants.BAG_LENGTH_VARIATION_BAG_LENGTH_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.BAG_LENGTH_VARIATION_TAG_ID +" VARCHAR(100) ,"
            + Constants.BAG_LENGTH_VARIATION_LENGTH +" VARCHAR(100) ,"
            + Constants.BAG_LENGTH_VARIATION_OINUM +" VARCHAR(100) ,"
            + Constants.BAG_LENGTH_VARIATION_MODIFIED_DATE +" VARCHAR(255) ,"
            + Constants.BAG_LENGTH_VARIATION_IS_SYNC +" BIT)";

    public static final String CREATE_PAPER_SHIFT_RESULT_TABLE = "CREATE TABLE " + Constants.PAPER_SHIFT_RESULTS+" ("
            + Constants.PAPER_SHIFT_RESULTS_OEM_ID +" INT ,"
            + Constants.PAPER_SHIFT_RESULTS_MAC_ID +" INT ,"
            + Constants.PAPER_SHIFT_RESULTS_LOGGER_ID +" INT ,"
            + Constants.PAPER_SHIFT_RESULTS_PAPER_SHIFT_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PAPER_SHIFT_RESULTS_TAG_ID +" VARCHAR(100) ,"
            + Constants.PAPER_SHIFT_RESULTS_SHIFT +" VARCHAR(100) ,"
            + Constants.PAPER_SHIFT_RESULTS_OINUM +" VARCHAR(100) ,"
            + Constants.PAPER_SHIFT_RESULTS_MODIFIED_DATE +" VARCHAR(255) ,"
            + Constants.PAPER_SHIFT_RESULTS_IS_SYNC +" BIT)";

    public static final String CREATE_MASTER_UOM_TABLE = "CREATE TABLE " + Constants.MASTER_UOM_TABLE+" ("
            + Constants.UOM_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.UOM_TABLE_OEMID +" INTEGER  ,"
            + Constants.UOM_PRODUCT_TYPE +" VARCHAR(50) ,"
            + Constants.UOM_PRODUCT_UOM +" VARCHAR(50) ,"
            + Constants.UOM_PRODUCT_IS_ACTIVE +" BIT ,"
            + Constants.UOM_PRODUCT_IS_DELETED +" BIT ,"
            + Constants.UOM_PRODUCT_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.UOM_PRODUCT_MODIFIED_DATE +" VARCHAR(100) ,"
            + Constants.UOM_PRODUCT_CREATED_BY +" VARCHAR(100) ,"
            + Constants.UOM_PRODUCT_MODIFIED_BY +" VARCHAR(100))";

    public static final String CRETAE_TABLE_SEAL_REJECTION_REASON_MASTER_TABLE =
            "CREATE TABLE "
                    + Constants.SEAL_REJECTION_REASON_MASTER_TABLE+" ("
                    + Constants.SEAL_REJECTION_REASON_REASON_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + Constants.SEAL_REJECTION_REASON_OEM_ID +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_PRODUCT_TYPE +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_REASON_DESC +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_IS_ACTIVE +" BIT ,"
                    + Constants.SEAL_REJECTION_REASON_IS_DELETED +" BIT ,"
                    + Constants.SEAL_REJECTION_REASON_CREATED_DATE +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_MODIFIED_DATE +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_CREATED_BY +" VARCHAR(100) ,"
                    + Constants.SEAL_REJECTION_REASON_MODIFIED_BY +" VARCHAR(100))";

    public static final String CREATE_PRODUCT_TABLE = "CREATE TABLE " + Constants.PRODUCT_TYPE_TABLE+" ("
            + Constants.PRODUCT_OEM_ID +" INT ,"
            + Constants.PRODUCT_TYPE_ID +" INT ,"
            + Constants.PRODUCT_IS_ACTVIE +" INT ,"
            + Constants.PRODUCT_IS_DELETED +" INT ,"
            + Constants.PRODUCT_CREATED_BY +" VARCHAR(100) ,"
            + Constants.PRODUCT_MODIFIED_BY +" VARCHAR(100) ,"
            + Constants.PRODUCT_UNIT_OF_MEASUREMENT +" VARCHAR(100) ,"
            + Constants.PRODUCT_UNIT_OF_MEASUREMENT_ID +" VARCHAR(50) ,"
            + Constants.PRODUCT_CREATED_DATE +" VARCHAR(60) ,"
            + Constants.PRODUCT_MODIFIED_DATE +" VARCHAR(60))";

    public static final String CREATE_WEIGHT_TEST_RESULT = "CREATE TABLE " + Constants.WEIGHT_TEST_RESULT+" ("
            + Constants.WEIGHT_TEST_RESULT_TEST_WEIGHT_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.WEIGHT_TEST_RESULT_OEMID +" INT ,"
            + Constants.WEIGHT_TEST_RESULT_MAC_ID +" INT ,"
            + Constants.WEIGHT_TEST_RESULT_LOGGER_ID +" VARCHAR(100) ,"
            + Constants.WEIGHT_TEST_RESULT_TAG_ID +" VARCHAR(100) ,"
            + Constants.WEIGHT_TEST_RESULT_WEIGHT +" VARCHAR(100) ,"
            + Constants.WEIGHT_TEST_RESULT_OINUM +" VARCHAR(100) ,"
            + Constants.WEIGHT_TEST_RESULT_MODIFIED_DATE +" VARCHAR(50) ,"
            + Constants.WEIGHT_TEST_RESULTS_IS_SYNC +" BIT)";

    public static final String CREATE_PRODUCTION_REASON_TABLE = "CREATE TABLE " + Constants.PRODUCTION_REASON_MASTER+" ("
            + Constants.PRODUCTION_REASON_REASONID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_REASON_REASONDESC +" VARCHAR(100) ,"
            + Constants.PRODUCTION_REASON_CREATED_DATE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_REASON_MODIFIED_DATE +" VARCHAR(100))";

    public static final String CREATE_PRODUCTION_TOLERANCE_TABLE= "CREATE TABLE " + Constants.PRODUCTION_TOLERANCE_MASTER+" ("
            + Constants.PRODUCTION_TOLERANCE_TOLERANCEID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.PRODUCTION_TOLERANCE_OEMID +" INT ,"
            + Constants.PRODUCTION_TOLERANCE_GRAMMAGE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_TOLERANCE_POSITIVE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_TOLERANCE_NEGATIVE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_ISACTIVE +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_ISDELETED +" VARCHAR(100) ,"
            + Constants.PRODUCTION_TOLERANCE_CREATED_DATE +" VARCHAR(50) ,"
            + Constants.PRODUCTION_TOLERANCE_MODIFIED_DATE +" VARCHAR(50))";

    public static final String CREATE_LIST_MASTER_TABLES= "CREATE TABLE " + Constants.LIST_OF_MASTER_TABLES+" ("
            + Constants.MASTER_TABLES_ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + Constants.MASTER_TABLES_NAME +" VARCHAR(50) ,"
            + Constants.MASTER_TABLES_DISLAY_NAME +" VARCHAR(50))";








    public static final String QUERY_INSERT_TAGTBL_PRODUCTION = "INSERT INTO tags " +
            "(tagid,oinum,targetwt,fillertype,customer,machinetype," +
            "producttype,product,operator,targetwtunit,inspector," +
            "filmtype,productfeed,pouchwidth,pouchlength,agitatormotorfreq," +
            "agitatormode,agitatorod,agitatorid,agitatorpitch,targetacctype," +
            "targetaccval,mcserialnum,prbulkmain,targetspeed,customerproduct," +
            "head,airpressure,qtysetting,bglensetting,fillvalvegap,fillvfdfreq," +
            "horizcutsealsetting,horizbandsealsetting,vertsealsetting,augeragitatorfreq," +
            "augerfillvalve,augurpitch,pouchsealtype,pouchtemphzf,pouchtemphzb,pouchtempvert," +
            "pouchtempvertfl,pouchtempvertfr,pouchtempvertbl,pouchtempvertbr,pouchgusset," +
            "customerfilm,pouchfilmthickness,pouchprbulk,poucheqhorizserrations,pouchfillevent,negWt,fyyear,createddate,shift,uom,totalqty,unitoftotalqty,totalqty1)" +
            "values (?,?,?,?,?,?,?,?,?," +
            "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";*/

    public static final String QUERY_INSERT_TAGTBL = "INSERT INTO tags " +
            "(tagid,oinum,targetwt,fillertype,customer,machinetype," +
            "producttype,product,operator,targetwtunit,inspector," +
            "filmtype,productfeed,pouchwidth,pouchlength,agitatormotorfreq," +
            "agitatormode,agitatorod,agitatorid,agitatorpitch,targetacctype," +
            "targetaccval,mcserialnum,prbulkmain,targetspeed,customerproduct," +
            "head,airpressure,qtysetting,bglensetting,fillvalvegap,fillvfdfreq," +
            "horizcutsealsetting,horizbandsealsetting,vertsealsetting,augeragitatorfreq," +
            "augerfillvalve,augurpitch,pouchsealtype,pouchtemphzf,pouchtemphzb,pouchtempvert," +
            "pouchtempvertfl,pouchtempvertfr,pouchtempvertbl,pouchtempvertbr,pouchgusset," +
            "customerfilm,pouchfilmthickness,pouchprbulk,poucheqhorizserrations,pouchfillevent,negWt,fyyear,createddate)" +
            "values (?,?,?,?,?,?,?,?,?," +
            "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
}
