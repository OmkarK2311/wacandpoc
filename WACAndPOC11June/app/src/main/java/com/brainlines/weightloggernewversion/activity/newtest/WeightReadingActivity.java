package com.brainlines.weightloggernewversion.activity.newtest;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoggerFunctionActivity;
import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.adapter.Weight_Reading_Adapter;
import com.brainlines.weightloggernewversion.broadcastreceiver.NetworkChangeReceiver;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.Tag;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@SuppressLint("Registered")
public class WeightReadingActivity extends BaseActivity implements View.OnClickListener {

    RecyclerView rv_weight_reading;
    Button btn_visual_test,btn_restart,btn_abort,btn_sync;
    public ArrayList<String> weight_reading_list = new ArrayList<>();
    MqttAndroidClient client = null;
    MqttAndroidClient clientObj = null;
    private int DATA_LOGGER_PORT = Constants.DATA_LOGGER_PORT;
    public static final String MQTT_BROKER = "tcp://"+Constants.dbIpAddress+":1883";
    String str_Current_tag_id,dataloggermac_id;
    TextView txt_current_tag_id;
    private String dbIpAddress = Constants.dbIpAddress;
    private String dbUserName = Constants.dbUserName;
    private String dbPassword = Constants.dbPassword;
    private String dbDatabaseName = Constants.dbDatabaseName;
    private String serverport = Constants.serverport;
    private String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
    String dataloggerIp,dataloggerName,current_tag_id,so_oinum;
    DataBaseHelper helper;
    double max_reading_value=0.0,min_reading_value = 0.0,mean_of_readings = 0.0,range = 0.0,average = 0.0,standard_deviation = 0.0,totals_sum =0.0;
    private String target_weight ="",target_accuracy_type="",target_accuracy_value = "",str_tarhetwt_uom = "";
    private Float float_kg_value_from_local_db;
    private NetworkChangeReceiver networkChangeReceiver;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private String status = "FAIL";
    DecimalFormat decimalFormat = new DecimalFormat("0.0");
    private boolean isDone;
    private SQLiteDatabase db;
    private static final String TAG = "WeightReadingActivity";
    private String dbName;
    //if gm then show upto 2 digit  if target weight 50 gm = upto 50.05(0.05 gm extra)
    //if kg then show upto 3 digit  if target weight 1 kg = upto 1.032(32 gm extra weight)

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight_reading);
        initUi();

        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        dataloggerName = getIntent().getStringExtra("dataloggerName");
        dataloggerIp = getIntent().getStringExtra("dataloggerIp");
        dataloggermac_id = getIntent().getStringExtra("mac_id");
        current_tag_id = getIntent().getStringExtra("current_tag_id");
        so_oinum = getIntent().getStringExtra("oinum");
        target_weight = getIntent().getStringExtra("target_wt");
        str_tarhetwt_uom = getIntent().getStringExtra("targetwt_uom");
        float_kg_value_from_local_db = getIntent().getFloatExtra("IntKgValue",0.0f);
        isDone = false;
        str_Current_tag_id = AppPreferences.getCurrentTagId(WeightReadingActivity.this);
        txt_current_tag_id.setText(current_tag_id);
        connectToMQttBroker(true);

    }

    private void initUi() {
        rv_weight_reading = findViewById(R.id.rv_weight_reading);
        btn_visual_test = findViewById(R.id.btn_visual_tests);
        btn_restart = findViewById(R.id.btn_restart);
        btn_abort = findViewById(R.id.btn_abort);
        btn_sync = findViewById(R.id.btn_sync);
        txt_current_tag_id = findViewById(R.id.txt_current_tag_id);

        btn_visual_test.setOnClickListener(this);
        btn_restart.setOnClickListener(this);
        btn_abort.setOnClickListener(this);
        btn_sync.setOnClickListener(this);

        /*weight_reading_list.add("106.7");
        weight_reading_list.add("110.7");
        weight_reading_list.add("108.7");
        weight_reading_list.add("109.7");
        weight_reading_list.add("103.7");
        weight_reading_list.add("100.7");
        weight_reading_list.add("102.7");
        weight_reading_list.add("101.7");*/

        /*weight_reading_list.add("100.0");
        weight_reading_list.add("100.0");
        weight_reading_list.add("100.0");
        weight_reading_list.add("100.0");
        weight_reading_list.add("101.0");
        weight_reading_list.add("99.0");
       weight_reading_list.add("99.0");
       weight_reading_list.add("100.0");
       weight_reading_list.add("101.0");
       weight_reading_list.add("101.0");
        weight_reading_list.add("100.0");
        weight_reading_list.add("100.0");
        weight_reading_list.add("101.0");
        weight_reading_list.add("102.0");
        weight_reading_list.add("103.0");*/

       /* weight_reading_list.add("104.0");
        weight_reading_list.add("105.0");
        weight_reading_list.add("106.0");
        weight_reading_list.add("105.0");
        weight_reading_list.add("104.0");
*/
       /* weight_reading_list.add("104.0");
        weight_reading_list.add("103.0");
        weight_reading_list.add("106.0");
        weight_reading_list.add("104.0");
        weight_reading_list.add("106.0");*/
       /*weight_reading_list.add("150.0");
       weight_reading_list.add("149.0");
       weight_reading_list.add("155.0");
       weight_reading_list.add("152.0");
       weight_reading_list.add("144.0");*/

        /*weight_reading_list.add("0.15");
        weight_reading_list.add("0.149");
        weight_reading_list.add("0.155");
        weight_reading_list.add("0.152");
        weight_reading_list.add("0.144");*/


        /*weight_reading_list.add("150.0");
        weight_reading_list.add("149.0");
        weight_reading_list.add("155.0");
        weight_reading_list.add("152.0");
        weight_reading_list.add("144.0");*/
        /*
        weight_reading_list.add("150.0");
        weight_reading_list.add("151.0");
        weight_reading_list.add("149.0");
        weight_reading_list.add("152.0");
        weight_reading_list.add("150.0");*/


        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerNetworkBroadcastForNougat();

    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==btn_visual_test.getId())
        {
            InsertIntoDb();
            Calculation();
            Intent i = new Intent(WeightReadingActivity.this,NewTestBagLengthActivity.class);
            i.putExtra("LoggerName",dataloggerName);
            i.putExtra("LoggerIP",dataloggerIp);
            i.putExtra("current_tag_id",current_tag_id);
            i.putExtra("so_oinum",so_oinum);
            i.putExtra("weight_test_status",status);
            i.putExtra("IntKgValue", float_kg_value_from_local_db);
            i.putExtra("targetwt_uom",str_tarhetwt_uom);
            i.putExtra("mac_id",dataloggermac_id);
            startActivity(i);
        }
        else if (view.getId()==btn_abort.getId())
        {
            try {
                //abort();
                status = "Abort";
                helper.updateOverallStatusForTag(status,current_tag_id);
                Intent i = new Intent(WeightReadingActivity.this, LoggerFunctionActivity.class);
                i.putExtra("LoggerName",dataloggerName);
                i.putExtra("LoggerIP",dataloggerIp);
                startActivity(i);
                finish();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else if (view.getId()==btn_restart.getId())
        {

        }else if (view.getId() == btn_sync.getId()){
            InsertIntoDb();
            Calculation();

        }
    }

    public void  connectToMQttBroker(final boolean sendReadyToDas){
        String clientId = MqttClient.generateClientId();
        final String tagId = current_tag_id;
        client =  new MqttAndroidClient(this,
                MQTT_BROKER,
                clientId);
        try {

            client.setCallback(new MQttCallbackClass(this, client));
            IMqttToken token = client.connect();
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    try {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(WeightReadingActivity.this);
                        //String dataLoggerName = sharedPreferences.getString(GlobalConstants.ACTIVE_DATA_LOGGER_NAME, "");
                        String topic = "/test/" + dataloggerName;
                        int qos = 2;
                        try {
                            IMqttToken subToken = client.subscribe(topic, qos);
                            subToken.setActionCallback(new IMqttActionListener() {
                                @Override
                                public void onSuccess(IMqttToken asyncActionToken) {
                                    Toast.makeText(WeightReadingActivity.this, "Subscribed succesfully", Toast.LENGTH_SHORT).show();

                                    try {
                                        if(sendReadyToDas) {
                                            sendReadyToDas(tagId,dataloggerIp,dataloggerName);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                @Override
                                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WeightReadingActivity.this);
                                    // set title
                                    alertDialogBuilder.setTitle(R.string.app_name);
                                    // set dialog message
                                    alertDialogBuilder.setMessage("Could not connect to MQTT broker you cannot view readings, please check!!");
                                    alertDialogBuilder.setCancelable(true);
                                    alertDialogBuilder.setNeutralButton(android.R.string.ok,
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = alertDialogBuilder.create();
                                    alert11.show();

                                }
                            });
                        } catch (Exception e) {
                            Toast.makeText(WeightReadingActivity.this, "Exception occured" , Toast.LENGTH_LONG).show();
                        }
                    }catch(Exception ex){
                        Toast.makeText(WeightReadingActivity.this, "Exception occured "+ ex.getMessage() , Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Toast.makeText(WeightReadingActivity.this, "Failed Exception occured" , Toast.LENGTH_LONG).show();

                }
            });

        }catch(MqttException ex){
            Toast.makeText(WeightReadingActivity.this, "Failed with outer exception , " + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    public String sendReadyToDas(String tagId, String dataLoggerIP, String dataLoggerName) throws IOException {
        String tag = tagId;
        HttpClient httpClient = new DefaultHttpClient();
        String responseString="";
        String url = "http://"+dataLoggerIP+":"+DATA_LOGGER_PORT+"/start?tagid="+tagId+"&dataLoggerIP="+dataLoggerIP+"&dataLoggerName="+dataLoggerName;
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();

        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;
    }

    /*private class MQttCallbackClass implements MqttCallback {
        public  MqttAndroidClient client = null;
        public Context actCtx = null;

        public  MQttCallbackClass(Context ctx, MqttAndroidClient client){
            actCtx = ctx;
            clientObj = client;
        }

        public void connectionLost() throws MqttException {
            boolean reconnected = false;
            Toast.makeText(WeightReadingActivity.this, "Connection to MQTT broker lost", Toast.LENGTH_LONG).show();
        }

        @Override
        public void connectionLost(Throwable cause) {
            try {
                connectionLost();
            } catch (MqttException e) {
                // We tried our best to reconnect
                e.printStackTrace();
            }
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            if(message!= null && message.toString().equalsIgnoreCase("done")){
                clientObj.disconnect();
                isDone = true;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WeightReadingActivity.this);
                // set title
                alertDialogBuilder.setTitle(R.string.app_name);
                // set dialog message
                alertDialogBuilder.setMessage("Weight Readings are done !!!");
                alertDialogBuilder.setCancelable(true);
                alertDialogBuilder.setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = alertDialogBuilder.create();
                alert11.show();

            }
            else {
                isDone = false;
                SharedPreferences sharedPrefs = PreferenceManager
                        .getDefaultSharedPreferences(WeightReadingActivity.this);
                String targetWt = sharedPrefs.getString("TargetWt", "0");
                String negwt = sharedPrefs.getString("NegWt", "No");
                Toast.makeText(WeightReadingActivity.this, message.toString(), Toast.LENGTH_SHORT).show();
                //txtMessage.setText(message.toString());
                LinearLayoutManager linearLayoutManager = new GridLayoutManager(WeightReadingActivity.this,4);
                rv_weight_reading.setLayoutManager(linearLayoutManager);
                rv_weight_reading.setAdapter(new Weight_Reading_Adapter(WeightReadingActivity.this,weight_reading_list));

                if(negwt != null && negwt.equalsIgnoreCase("yes")){
                    double targetWtD = Double.parseDouble(targetWt);
                    double reading = Double.parseDouble(message.toString());
                    if(reading < targetWtD){
                        //client.unsubscribe("/test/topic");
                        Toast.makeText(actCtx, "Aborting test", Toast.LENGTH_LONG).show();
                        if(clientObj != null && clientObj.isConnected()) {
                            clientObj.disconnect();
                        }
                    }
                }

                Log.d("TAG", "messageArrived: ");



            }

        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
            String tokenS = token.toString();
            Log.d("TAG", "deliveryComplete: ");
        }
    }*/

    private class MQttCallbackClass implements MqttCallback {
        public  MqttAndroidClient client = null;
        public Context actCtx = null;

        public  MQttCallbackClass(Context ctx, MqttAndroidClient client){
            actCtx = ctx;
            clientObj = client;
        }

        public void connectionLost() throws MqttException {
            boolean reconnected = false;
            Toast.makeText(WeightReadingActivity.this, "Connection to MQTT broker lost", Toast.LENGTH_LONG).show();
        }

        @Override
        public void connectionLost(Throwable cause) {
            try {
                connectionLost();
            } catch (MqttException e) {
                // We tried our best to reconnect
                e.printStackTrace();
            }
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            if(message!= null && message.toString().equalsIgnoreCase("done")){
                clientObj.disconnect();
                isDone = true;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WeightReadingActivity.this);
                // set title
                alertDialogBuilder.setTitle(R.string.app_name);
                // set dialog message
                alertDialogBuilder.setMessage("Weight Readings are done !!!");
                alertDialogBuilder.setCancelable(true);
                alertDialogBuilder.setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = alertDialogBuilder.create();
                alert11.show();

            }else {

                isDone = false;
                SharedPreferences sharedPrefs = PreferenceManager
                        .getDefaultSharedPreferences(WeightReadingActivity.this);
                String targetWt = sharedPrefs.getString("TargetWt", "0");
                String negwt = sharedPrefs.getString("NegWt", "No");
                Toast.makeText(WeightReadingActivity.this, message.toString(), Toast.LENGTH_SHORT).show();


                if(negwt != null && negwt.equalsIgnoreCase("yes")){
                    double targetWtD = Double.parseDouble(targetWt);
                    double reading = Double.parseDouble(message.toString());
                    if(reading < targetWtD){
                        //client.unsubscribe("/test/topic");
                        Toast.makeText(actCtx, "Aborting test", Toast.LENGTH_LONG).show();
                        if(clientObj != null && clientObj.isConnected()) {
                            clientObj.disconnect();
                        }
                        // new DisplayResultsActivity.sendAbortToDas().execute("test", "Negative weight detected, aborting tests!!", "test");
                    }
                }
                String msg = message.toString();
                weight_reading_list.add(msg);
                LinearLayoutManager linearLayoutManager = new GridLayoutManager(WeightReadingActivity.this,4);
                rv_weight_reading.setLayoutManager(linearLayoutManager);
                rv_weight_reading.setAdapter(new Weight_Reading_Adapter(WeightReadingActivity.this,weight_reading_list));
            }
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
            String tokenS = token.toString();
            Log.d(TAG, "deliveryComplete: ");
        }
    }

    public  void InsertIntoDb() {
//        helper = new DataBaseHelper(WeightReadingActivity.this);
        String currentDateandTime = sdf.format(new Date());
        if (str_tarhetwt_uom.equals("gm")){
            for (int i = 0;i<weight_reading_list.size();i++) {
                Float floatvalue = Float.parseFloat(weight_reading_list.get(i));
                ContentValues cv=new ContentValues();
                cv.put(Constants.WEIGHT_TEST_RESULT_TEST_WEIGHT_ID,i+1);
                cv.put(Constants.WEIGHT_TEST_RESULT_OEMID,1234);
                cv.put(Constants.WEIGHT_TEST_RESULT_MAC_ID,dataloggermac_id);
                cv.put(Constants.WEIGHT_TEST_RESULT_LOGGER_ID,dataloggerName);
                cv.put(Constants.WEIGHT_TEST_RESULT_TAG_ID,current_tag_id);
                cv.put(Constants.WEIGHT_TEST_RESULT_WEIGHT,floatvalue);
                cv.put(Constants.WEIGHT_TEST_RESULT_OINUM,so_oinum);
                cv.put(Constants.WEIGHT_TEST_RESULT_MODIFIED_DATE,currentDateandTime);
                cv.put(Constants.WEIGHT_TEST_RESULTS_IS_SYNC,0);
                long d=db.insert(Constants.WEIGHT_TEST_RESULT,null,cv);
                Log.d("Success", String.valueOf(d));
                if (d == -1){
                    Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                    //wakeup();
                }
            }
        }else if (str_tarhetwt_uom.equals("Kg")){
            for (int i = 0;i<weight_reading_list.size();i++)
            {
                ContentValues cv=new ContentValues();
                cv.put(Constants.WEIGHT_TEST_RESULT_TEST_WEIGHT_ID,i+1);
                cv.put(Constants.WEIGHT_TEST_RESULT_OEMID,1234);
                cv.put(Constants.WEIGHT_TEST_RESULT_MAC_ID,dataloggermac_id);
                cv.put(Constants.WEIGHT_TEST_RESULT_LOGGER_ID,dataloggerName);
                cv.put(Constants.WEIGHT_TEST_RESULT_TAG_ID,current_tag_id);
                cv.put(Constants.WEIGHT_TEST_RESULT_WEIGHT,Float.parseFloat(weight_reading_list.get(i))* float_kg_value_from_local_db);
                cv.put(Constants.WEIGHT_TEST_RESULT_OINUM,so_oinum);
                cv.put(Constants.WEIGHT_TEST_RESULT_MODIFIED_DATE,currentDateandTime);
                cv.put(Constants.WEIGHT_TEST_RESULTS_IS_SYNC,0);
                long d=db.insert(Constants.WEIGHT_TEST_RESULT,null,cv);
                Log.d("Success", String.valueOf(d));
                if (d == -1){
                    Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                    //wakeup();
                }

            }
        }
    }

    public void Calculation() {
        getTagInfo();
        double target_weight_of_tag = Double.parseDouble(target_weight) ;

        //maximum reading
        max_reading_value = Double.parseDouble(weight_reading_list.get(0));
        for (int cnt =1;cnt < weight_reading_list.size(); cnt++)
        {
            if (Double.parseDouble(weight_reading_list.get(cnt)) > max_reading_value)
            {
                max_reading_value = Double.parseDouble(weight_reading_list.get(cnt));
            }
        }

        //minimum reading
        min_reading_value = Double.parseDouble(weight_reading_list.get(0));
        for (int cnt =1; cnt < weight_reading_list.size(); cnt++)
        {
            if (Double.parseDouble(weight_reading_list.get(cnt)) < min_reading_value)
            {
                min_reading_value = Double.parseDouble(weight_reading_list.get(cnt));
            }
        }

        //mean calculation
        for (int cnt = 0 ; cnt< weight_reading_list.size() ; cnt++)
        {
            totals_sum += Double.parseDouble(weight_reading_list.get(cnt));
        }

        //mean_of_readings = totals_sum/weight_reading_list.size();
        mean_of_readings = (max_reading_value - min_reading_value)/2 + min_reading_value;

        //range calculation
        //range = max_reading_value - min_reading_value;
        range = (max_reading_value - min_reading_value)/2;

        //standard deviation calculation
        average = mean_of_readings;
        double[] deviations = new double[weight_reading_list.size()];
        //Taking the deviation of mean from each number
        for (int i = 0;i < deviations.length; i++)
        {
            deviations[i] = Double.parseDouble(weight_reading_list.get(i)) - average;
        }

        //getting the squares of deviations
        double[] squares = new double[weight_reading_list.size()];

        for (int i = 0;i < squares.length; i++)
        {
            squares[i] = deviations[i] * deviations[i];
        }

        //Adding all the squares
        double sum_of_squares = 0;
        for (int i =0;i<squares.length;i++)
        {
            sum_of_squares = sum_of_squares + squares[i];
        }
        double result = sum_of_squares / (weight_reading_list.size() - 1);
        standard_deviation = Math.sqrt(result);


        //Formula +/- gm =range/2,  +/-% gm =(range/2*100)/target_weight,
        //Double gm = range / 2;



        if (str_tarhetwt_uom.equals("gm"))
        {
            //Formula +/- gm =range/2,  +/-% gm =(range/2*100)/target_weight,
            //Double gm = range / 2;
            Double gm = range;
            Double gmpercent = (gm * 100) / target_weight_of_tag;
            Double gmsd = standard_deviation;
            Double gmsdpercent = (gmsd * 100) / target_weight_of_tag;
            Double avg_Give_away = mean_of_readings - target_weight_of_tag;

            //Get Test Status

            if (target_accuracy_type != null && target_accuracy_type.equals(Constants.GM))
            {
                if (gm <= Double.parseDouble(target_accuracy_value))
                {
                    status = "PASS";
                }
            }
            else if (target_accuracy_type != null && target_accuracy_type.equals(Constants.GMPERCENT))
            {
                if (gmpercent <= Double.parseDouble(target_accuracy_value))
                {
                    status = "PASS";
                }
            }
            else if (target_accuracy_type != null && target_accuracy_type.equals(Constants.GMSD))
            {
                if (gmsd <= Double.parseDouble(target_accuracy_value))
                {
                    status = "PASS";
                }
            }
            else if (target_accuracy_type != null && target_accuracy_type.equals(Constants.PERCENTSD))
            {
                if (gmsdpercent <= Double.parseDouble(target_accuracy_value))
                {
                    status = "PASS";
                }
            }


//            DataBaseHelper helper=new DataBaseHelper(WeightReadingActivity.this);
//            SQLiteDatabase db = helper.getWritableDatabase();
            String currentDateandTime = sdf.format(new Date());
            decimalFormat= new DecimalFormat("0.000");
            String strrangewithpm = "+/-" +String.valueOf(decimalFormat.format(range));
            ContentValues cv=new ContentValues();
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_ID,1);
            String strstandard_deviation = String.valueOf(standard_deviation);

            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_OEM_ID,1234);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_LOGGER_ID,dataloggerName);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_TAG_ID,current_tag_id);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MIN_READING,Float.parseFloat(String.valueOf(min_reading_value)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MAX_READING,Float.parseFloat(String.valueOf(max_reading_value)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_RANGE_READING,strrangewithpm);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MEAN_READING,Float.parseFloat(String.valueOf(mean_of_readings)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_SD_READING,Float.parseFloat(String.valueOf(strstandard_deviation)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_REMARKS,"Android app remark");
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_RUN_STATUS,status);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM,Float.parseFloat(String.valueOf(gm)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM_PERCENT,Float.parseFloat(String.valueOf(gmpercent)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD,Float.parseFloat(String.valueOf(gmsd)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD_PERCENT,Float.parseFloat(String.valueOf(gmpercent)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_AVERAGE_GIVE_AWAY,Float.parseFloat(String.valueOf(avg_Give_away)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MODIFIED_DATE,currentDateandTime);
            cv.put(Constants.WEIGHT_TEST_RESULTS_IS_SYNC,false);


           /* cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_OEM_ID,1234);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_LOGGER_ID,dataloggerName);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_TAG_ID,current_tag_id);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MIN_READING,Float.parseFloat(String.valueOf(min_reading_value)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MAX_READING,Float.parseFloat(String.valueOf(max_reading_value)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_RANGE_READING,strrangewithpm);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MEAN_READING,Float.parseFloat(String.valueOf(mean_of_readings)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_SD_READING,Float.parseFloat(String.valueOf(standard_deviation)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_REMARKS,"Android app remark");
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_RUN_STATUS,status);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM,Float.parseFloat(String.valueOf(gm)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM_PERCENT,Float.parseFloat(String.valueOf(gmpercent)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD,Float.parseFloat(String.valueOf(gmsd)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD_PERCENT,decimalFormat.format(gmsdpercent));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_AVERAGE_GIVE_AWAY,decimalFormat.format(avg_Give_away) );
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MODIFIED_DATE,currentDateandTime);
            cv.put(Constants.WEIGHT_TEST_RESULTS_IS_SYNC,false);*/

            long d=db.insert(Constants.WEIGHT_RESULTS_ANALYTICS_TABLE,null,cv);
            Log.d("Success", String.valueOf(d));
            if (d == -1){
                Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                //wakeup();
            }
            helper.updateWeightTestStatusTable(status,current_tag_id);
            helper.updateOverallStatusForTag(status,current_tag_id);
        }
        else if (str_tarhetwt_uom.equals("Kg"))
        {
            Double gm = range;
            Double gmpercent = ((gm * 100) /(target_weight_of_tag))/ float_kg_value_from_local_db;
            Double gmsd = standard_deviation;
            Double gmsdpercent = ((gmsd * 100) / (target_weight_of_tag))/ float_kg_value_from_local_db;
            Double avg_Give_away = mean_of_readings - (target_weight_of_tag);
            Double dminreading = min_reading_value* float_kg_value_from_local_db;
            Double dmaxreading = max_reading_value* float_kg_value_from_local_db;
            Double dmeanreading = mean_of_readings* float_kg_value_from_local_db;
            Double dstandarddeviation = standard_deviation* float_kg_value_from_local_db;
            Double dgm = gm* float_kg_value_from_local_db;
            Double dgmpercent = gmpercent* float_kg_value_from_local_db;
            Double dgmsd = gmsd* float_kg_value_from_local_db;
            Double dgmsdpercent = gmsdpercent* float_kg_value_from_local_db;
            Double davggiveaway = avg_Give_away* float_kg_value_from_local_db;

            //Get Test Status

            if (target_accuracy_type != null && target_accuracy_type.equals(Constants.GM))
            {
                if (gm <= Double.parseDouble(target_accuracy_value))
                {
                    status = "PASS";
                }
            }
            else if (target_accuracy_type != null && target_accuracy_type.equals(Constants.GMPERCENT))
            {
                if (gmpercent <= Double.parseDouble(target_accuracy_value))
                {
                    status = "PASS";
                }
            }
            else if (target_accuracy_type != null && target_accuracy_type.equals(Constants.GMSD))
            {
                if (gmsd <= Double.parseDouble(target_accuracy_value))
                {
                    status = "PASS";
                }
            }
            else if (target_accuracy_type != null && target_accuracy_type.equals(Constants.PERCENTSD))
            {
                if (gmsdpercent <= Double.parseDouble(target_accuracy_value))
                {
                    status = "PASS";
                }
            }


//            DataBaseHelper helper=new DataBaseHelper(WeightReadingActivity.this);
//            SQLiteDatabase db = helper.getWritableDatabase();
            String currentDateandTime = sdf.format(new Date());
            decimalFormat= new DecimalFormat("0.000");
            String strrangewithpm = "+/-" +String.valueOf(decimalFormat.format(range * float_kg_value_from_local_db));
            ContentValues cv=new ContentValues();
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_ID,1);

            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_OEM_ID,1234);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_LOGGER_ID,dataloggerName);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_TAG_ID,current_tag_id);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MIN_READING,Float.parseFloat(String.valueOf(dminreading)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MAX_READING,Float.parseFloat(String.valueOf(dmaxreading)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_RANGE_READING,strrangewithpm);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MEAN_READING,Float.parseFloat(String.valueOf(dmeanreading)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_SD_READING,Float.parseFloat(String.valueOf(dstandarddeviation)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_REMARKS,"Android app remark");
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_RUN_STATUS,status);
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM,Float.parseFloat(String.valueOf(dgm)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM_PERCENT,Float.parseFloat(String.valueOf(dgmpercent)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD,Float.parseFloat(String.valueOf(dgmsd)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD_PERCENT,Float.parseFloat(String.valueOf(dgmsdpercent)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_AVERAGE_GIVE_AWAY,Float.parseFloat(String.valueOf(davggiveaway)));
            cv.put(Constants.WEIGHT_RESULTS_ANALYTICS_MODIFIED_DATE,currentDateandTime);
            cv.put(Constants.WEIGHT_TEST_RESULTS_IS_SYNC,false);


            long d=db.insert(Constants.WEIGHT_RESULTS_ANALYTICS_TABLE,null,cv);
            Log.d("Success", String.valueOf(d));
            if (d == -1){
                Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                //wakeup();
            }

            helper.updateWeightTestStatusTable(status,current_tag_id);
            helper.updateOverallStatusForTag(status,current_tag_id);
        }
    }

    public void getTagInfo() {
        Cursor taginfo = helper.getTagInfo(current_tag_id);
        Tag model = new Tag();

        while (taginfo.moveToNext()){

            String tag_id = taginfo.getString(5);
            //target_weight = taginfo.getString(7);
            target_accuracy_type = taginfo.getString(23);
            target_accuracy_value = taginfo.getString(24);

            Log.d("TAG", "getSealTypes: ");
        }
    }

    private String abort() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String tag = current_tag_id;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String dataPort=Constants.DATA_LOGGER_PORT+"";
        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;

    }

    @Override
    protected void dialog(boolean value) {
        super.dialog(value);
//        if(value){
//            InsertIntoDb();
//            Calculation();
//            getAllWeightResultsData();
//            getAllWeighAnalyticsData();
//            Toast.makeText(this, "DATA Pushed", Toast.LENGTH_SHORT).show();
//        }else {
//            Toast.makeText(this, "DATA Not Pushed", Toast.LENGTH_SHORT).show();
//        }
    }
}