package com.brainlines.weightloggernewversion.activity.recalltest;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoggerFunctionActivity;
import com.brainlines.weightloggernewversion.activity.newtest.NewTestBagLengthActivity;
import com.brainlines.weightloggernewversion.activity.newtest.WeightReadingActivity;
import com.brainlines.weightloggernewversion.adapter.Weight_Reading_Adapter;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.Tag;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Recall_weigth_Reading_Activity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView rv_weight_reading;
    Button btn_visual_test,btn_restart,btn_abort;
    Weight_Reading_Adapter weight_reading_adapter;
    public ArrayList<String> weight_reading_list = null;
    MqttAndroidClient client = null;
    MqttAndroidClient clientObj = null;
    private boolean isDone;
    private int DATA_LOGGER_PORT = Constants.DATA_LOGGER_PORT;
    public static final String MQTT_BROKER = "tcp://"+Constants.dbIpAddress+":1883";
    String str_Current_tag_id;
    TextView txt_current_tag_id;
    private String dbIpAddress = Constants.dbIpAddress;
    private String dbUserName = Constants.dbUserName;
    private String dbPassword = Constants.dbPassword;
    private String dbDatabaseName = Constants.dbDatabaseName;
    private String serverport = Constants.serverport;
    private String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
    String dataloggerIp,dataloggerName,current_tag_id,so_oinum;
    DataBaseHelper helper;
    double max_reading_value=0.0;
    double min_reading_value = 0.0;
    double mean_of_readings = 0.0;
    double range = 0.0;
    double average = 0.0;
    double standard_deviation = 0.0;


    double totals_sum =0.0;

    String target_weight ="";
    String target_accuracy_type="";
    String target_accuracy_value = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight_reading);
        initUi();
//        helper = new DataBaseHelper(this);
        dataloggerName = getIntent().getStringExtra("dataloggerName");
        dataloggerIp = getIntent().getStringExtra("dataloggerIp");
        current_tag_id = getIntent().getStringExtra("current_tag_id");
        so_oinum = getIntent().getStringExtra("oinum");
        target_weight = getIntent().getStringExtra("target_wt");
        isDone = false;
        weight_reading_list = new ArrayList<String>();
        str_Current_tag_id = AppPreferences.getCurrentTagId(Recall_weigth_Reading_Activity.this);
        txt_current_tag_id.setText(current_tag_id);
        connectToMQttBroker(true);
    }
    private void initUi()
    {
        rv_weight_reading = findViewById(R.id.rv_weight_reading);
        btn_visual_test = findViewById(R.id.btn_visual_tests);
        btn_restart = findViewById(R.id.btn_restart);
        btn_abort = findViewById(R.id.btn_abort);
        txt_current_tag_id = findViewById(R.id.txt_current_tag_id);

        btn_visual_test.setOnClickListener(this);
        btn_restart.setOnClickListener(this);
        btn_abort.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId()==btn_visual_test.getId())
        {
            InsertIntoDb();
            Calculation();
            Intent i = new Intent(Recall_weigth_Reading_Activity.this, Recall_bag_length_Reading_Activity.class);
            i.putExtra("LoggerName",dataloggerName);
            i.putExtra("LoggerIP",dataloggerIp);
            i.putExtra("current_tag_id",current_tag_id);
            i.putExtra("so_oinum",so_oinum);
            startActivity(i);
        }
        else if (view.getId()==btn_abort.getId())
        {
            try {
                abort();
                Intent i = new Intent(Recall_weigth_Reading_Activity.this, LoggerFunctionActivity.class);
                i.putExtra("LoggerName",dataloggerName);
                i.putExtra("LoggerIP",dataloggerIp);

                startActivity(i);
                finish();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (view.getId()==btn_restart.getId())
        {

        }
    }
    public void  connectToMQttBroker(final boolean sendReadyToDas){
        String clientId = MqttClient.generateClientId();
        MemoryPersistence memPer = new MemoryPersistence();
        /* String tagId = resToMobile;*/

        client =  new MqttAndroidClient(this,
                MQTT_BROKER,
                clientId);
        try {

            client.setCallback(new MQttCallbackClass(this, client));
            IMqttToken token = client.connect();
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    try {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Recall_weigth_Reading_Activity.this);
                        //String dataLoggerName = sharedPreferences.getString(GlobalConstants.ACTIVE_DATA_LOGGER_NAME, "");
                        String topic = "/test/" + dataloggerName;
                        int qos = 2;
                        try {
                            IMqttToken subToken = client.subscribe(topic, qos);
                            subToken.setActionCallback(new IMqttActionListener() {
                                @Override
                                public void onSuccess(IMqttToken asyncActionToken) {
                                    Toast.makeText(Recall_weigth_Reading_Activity.this, "Subscribed succesfully", Toast.LENGTH_SHORT).show();

                                    try {
                                        if(sendReadyToDas) {
                                            sendReadyToDas(current_tag_id,dataloggerIp,dataloggerName);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                @Override
                                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Recall_weigth_Reading_Activity.this);
                                    // set title
                                    alertDialogBuilder.setTitle(R.string.app_name);
                                    // set dialog message
                                    alertDialogBuilder.setMessage("Could not connect to MQTT broker you cannot view readings, please check!!");
                                    alertDialogBuilder.setCancelable(true);
                                    alertDialogBuilder.setNeutralButton(android.R.string.ok,
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = alertDialogBuilder.create();
                                    alert11.show();

                                }
                            });
                        } catch (Exception e) {
                            Toast.makeText(Recall_weigth_Reading_Activity.this, "Exception occured" , Toast.LENGTH_LONG).show();
                        }
                    }catch(Exception ex){
                        Toast.makeText(Recall_weigth_Reading_Activity.this, "Exception occured "+ ex.getMessage() , Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Toast.makeText(Recall_weigth_Reading_Activity.this, "Failed Exception occured" , Toast.LENGTH_LONG).show();

                }
            });

        }catch(MqttException ex){
            Toast.makeText(Recall_weigth_Reading_Activity.this, "Failed with outer exception , " + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    public String sendReadyToDas(String tagId, String dataLoggerIP, String dataLoggerName) throws IOException {
        String tag = tagId;
        HttpClient httpClient = new DefaultHttpClient();
        String responseString="";
        String url = "http://"+dataLoggerIP+":"+DATA_LOGGER_PORT+"/start?tagid="+tagId+"&dataLoggerIP="+dataLoggerIP+"&dataLoggerName="+dataLoggerName;
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();

        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;
    }

    private class MQttCallbackClass implements MqttCallback {
        public  MqttAndroidClient client = null;
        public Context actCtx = null;

        public  MQttCallbackClass(Context ctx, MqttAndroidClient client){
            actCtx = ctx;
            clientObj = client;
        }

        public void connectionLost() throws MqttException {
            boolean reconnected = false;
            Toast.makeText(Recall_weigth_Reading_Activity.this, "Connection to MQTT broker lost", Toast.LENGTH_LONG).show();
        }

        @Override
        public void connectionLost(Throwable cause) {
            try {
                connectionLost();
            } catch (MqttException e) {
                // We tried our best to reconnect
                e.printStackTrace();
            }
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            if(message!= null && message.toString().equalsIgnoreCase("done")){
                clientObj.disconnect();
                isDone = true;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Recall_weigth_Reading_Activity.this);
                // set title
                alertDialogBuilder.setTitle(R.string.app_name);
                // set dialog message
                alertDialogBuilder.setMessage("Weight Readings are done !!!");
                alertDialogBuilder.setCancelable(true);
                alertDialogBuilder.setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = alertDialogBuilder.create();
                alert11.show();

            }
            else {
                isDone = false;
                SharedPreferences sharedPrefs = PreferenceManager
                        .getDefaultSharedPreferences(Recall_weigth_Reading_Activity.this);
                String targetWt = sharedPrefs.getString("TargetWt", "0");
                String negwt = sharedPrefs.getString("NegWt", "No");
                Toast.makeText(Recall_weigth_Reading_Activity.this, message.toString(), Toast.LENGTH_SHORT).show();
                //txtMessage.setText(message.toString());

                if(negwt != null && negwt.equalsIgnoreCase("yes")){
                    double targetWtD = Double.parseDouble(targetWt);
                    double reading = Double.parseDouble(message.toString());
                    if(reading < targetWtD){
                        //client.unsubscribe("/test/topic");
                        Toast.makeText(actCtx, "Aborting test", Toast.LENGTH_LONG).show();
                        if(clientObj != null && clientObj.isConnected()) {
                            clientObj.disconnect();
                        }
                        // new DisplayResultsActivity.sendAbortToDas().execute("test", "Negative weight detected, aborting tests!!", "test");
                    }
                }
                weight_reading_list.add(message.toString());
                LinearLayoutManager linearLayoutManager = new GridLayoutManager(Recall_weigth_Reading_Activity.this,6);
                rv_weight_reading.setLayoutManager(linearLayoutManager);
                rv_weight_reading.setAdapter(new Weight_Reading_Adapter(Recall_weigth_Reading_Activity.this,weight_reading_list));
                //ed_bag_length.setText("");
                Log.d("TAG", "messageArrived: ");

//                gridViewObj.setAdapter(new DataGridViewAdapter(resultsLst, DisplayResultsActivity.this,false));
//                gridViewObj.invalidateViews();

            }

        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
            String tokenS = token.toString();
            Log.d("TAG", "deliveryComplete: ");
        }
    }

    public  void InsertIntoDb()
    {
//        DataBaseHelper helper=new DataBaseHelper(Recall_weigth_Reading_Activity.this);
        SQLiteDatabase db = helper.getWritableDatabase();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());


        for (int i = 0;i<weight_reading_list.size();i++)
        {
            ContentValues cv=new ContentValues();
            cv.put("OEMID","1234");
            cv.put("MacID","18:fe:34:8b:60:75");
            cv.put("LoggerID",dataloggerName);
            cv.put("TagID",current_tag_id);
            cv.put("Weight",weight_reading_list.get(i));
            cv.put("oinum",so_oinum);
            cv.put("ModifiedDate",currentDateandTime);
            cv.put("IsSync","0");
            long d=db.insert(Constants.WEIGHT_TEST_RESULT,null,cv);
            Log.d("Success", String.valueOf(d));
        }

    }

    public void Calculation()
    {
        getTagInfo();
        double target_weight_of_tag = Double.parseDouble(target_weight) ;

        //maximum reading
        max_reading_value = Double.parseDouble(weight_reading_list.get(0));
        for (int cnt =1;cnt < weight_reading_list.size(); cnt++)
        {
            if (Double.parseDouble(weight_reading_list.get(cnt)) > max_reading_value)
            {
                max_reading_value = Double.parseDouble(weight_reading_list.get(cnt));
            }
        }

        //minimum reading
        min_reading_value = Double.parseDouble(weight_reading_list.get(0));
        for (int cnt =1; cnt < weight_reading_list.size(); cnt++)
        {
            if (Double.parseDouble(weight_reading_list.get(cnt)) < min_reading_value)
            {
                min_reading_value = Double.parseDouble(weight_reading_list.get(cnt));
            }
        }

        //mean calculation
        for (int cnt = 0 ; cnt< weight_reading_list.size() ; cnt++)
        {
            totals_sum += Double.parseDouble(weight_reading_list.get(cnt));
        }
        mean_of_readings = totals_sum/weight_reading_list.size();

        //range calculation
        range = max_reading_value - min_reading_value;

        //standard deviation calculation
        average = mean_of_readings;
        double[] deviations = new double[weight_reading_list.size()];

        //Taking the deviation of mean from each number
        for (int i = 0;i < deviations.length; i++)
        {
            deviations[i] = Double.parseDouble(weight_reading_list.get(i)) - average;
        }

        //getting the squares of deviations
        double[] squares = new double[weight_reading_list.size()];

        for (int i = 0;i < squares.length; i++)
        {
            squares[i] = deviations[i] * deviations[i];
        }

        //Adding all the squares
        double sum_of_squares = 0;
        for (int i =0;i<squares.length;i++)
        {
            sum_of_squares = sum_of_squares + squares[i];
        }
        double result = sum_of_squares / (weight_reading_list.size() - 1);
        standard_deviation = Math.sqrt(result);

        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        Double gm = range / 2;

        Double gmpercent = (gm * 100) / target_weight_of_tag;

        Double gmsd = standard_deviation;

        Double gmsdpercent = (gmsd * 100) / target_weight_of_tag;

        Double avg_Give_away = mean_of_readings - target_weight_of_tag;


        //Get Test Status

        String status = "FAIL";
        if (target_accuracy_type != null && target_accuracy_type.equals(Constants.GM))
        {
            if (gm <= Double.parseDouble(target_accuracy_value))
            {
                status = "PASS";
            }
        }
        else if (target_accuracy_type != null && target_accuracy_type.equals(Constants.GMPERCENT))
        {
            if (gmpercent <= Double.parseDouble(target_accuracy_value))
            {
                status = "PASS";
            }
        }
        else if (target_accuracy_type != null && target_accuracy_type.equals(Constants.GMSD))
        {
            if (gmsd <= Double.parseDouble(target_accuracy_value))
            {
                status = "PASS";
            }
        }
        else if (target_accuracy_type != null && target_accuracy_type.equals(Constants.PERCENTSD))
        {
            if (gmsdpercent <= Double.parseDouble(target_accuracy_value))
            {
                status = "PASS";
            }
        }

//        DataBaseHelper helper=new DataBaseHelper(Recall_weigth_Reading_Activity.this);
        SQLiteDatabase db = helper.getWritableDatabase();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());


        ContentValues cv=new ContentValues();
        cv.put("OEMID","1234");
        cv.put("LoggerID","18:fe:34:8b:60:75");
        cv.put("TagID",current_tag_id);
        cv.put("MinReading",min_reading_value);
        cv.put("MaxReading",max_reading_value);
        cv.put("RangeReading",range);
        cv.put("MeanReading",mean_of_readings);
        cv.put("SDReading",standard_deviation);
        cv.put("Remarks","Android app remark");
        cv.put("RunStatus",status);
        cv.put("ModifiedDate",currentDateandTime);
        cv.put("GM",gm);
        cv.put("GMPercent",gmpercent);
        cv.put("GMSD",gmsd);
        cv.put("GMSDPercent",gmsdpercent);
        cv.put("AverageGiveAway",avg_Give_away);
        long d=db.insert(Constants.WEIGHT_RESULTS_ANALYTICS_TABLE,null,cv);
        Log.d("Success", String.valueOf(d));
    }


    public void getTagInfo() {
        Cursor taginfo = helper.getTagInfo(current_tag_id);
        Tag model = new Tag();

        while (taginfo.moveToNext()){

            String tag_id = taginfo.getString(5);
            //target_weight = taginfo.getString(7);
            target_accuracy_type = taginfo.getString(24);
            target_accuracy_value = taginfo.getString(25);


            Log.d("TAG", "getSealTypes: ");
        }


    }

    private String abort() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String tag = current_tag_id;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String dataPort=Constants.DATA_LOGGER_PORT+"";
        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;

    }

}
