package com.brainlines.weightloggernewversion.activity.recalltest;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.Tag;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import java.util.ArrayList;

public class Recall_Main_Activity extends AppCompatActivity implements View.OnClickListener {
    EditText ed_recall_oinum;
    Spinner spin_recall_target_weight,spin_finacial_year,spin_recall_head_type;
    ImageButton btn_recall_Get_target_weight,btn_recall_search;
    ArrayList<String> headtype = new ArrayList<>();
    ArrayList<String> targetweightlist = new ArrayList<>();
    ArrayList<String> fyyearlist = new ArrayList<>();
    DataBaseHelper helper;
    String str_oinum,dataloggerName,dataloggerIp,dataloggerMacId;
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_recall_test_targetweight);
        initUi();

        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        dataloggerName = getIntent().getStringExtra("LoggerName");
        dataloggerIp = getIntent().getStringExtra("LoggerIP");
        dataloggerMacId = getIntent().getStringExtra("LoggerMacId");


    }

    private void initUi()
    {
        ed_recall_oinum = findViewById(R.id.ed_recall_oinum);
        spin_finacial_year = findViewById(R.id.spin_finacial_year);
        spin_recall_head_type = findViewById(R.id.spin_recall_head_type);
        spin_recall_target_weight  =findViewById(R.id.spin_recall_target_weight);
        btn_recall_search = findViewById(R.id.btn_recall_search);
        btn_recall_Get_target_weight = findViewById(R.id.btn_recall_Get_target_weight);
        btn_recall_search.setOnClickListener(this);
        btn_recall_Get_target_weight.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btn_recall_Get_target_weight.getId()) {
            str_oinum = ed_recall_oinum.getText().toString();
            getFyYear(str_oinum);
            ArrayAdapter<String> spFyYearList = new ArrayAdapter<String>(this, R.layout.item_spinner_latyout, fyyearlist);
            spFyYearList.setDropDownViewResource(R.layout.item_spinner_latyout);
            spin_finacial_year.setAdapter(spFyYearList);

            getTargetWeight(str_oinum);
            ArrayAdapter<String> spTargetWeightList = new ArrayAdapter<String>(this, R.layout.item_spinner_latyout, targetweightlist);
            spTargetWeightList.setDropDownViewResource(R.layout.item_spinner_latyout);
            spin_recall_target_weight.setAdapter(spTargetWeightList);

            getHeadFromDB(str_oinum);
            ArrayAdapter<String> spHeadTypeList = new ArrayAdapter<String>(this, R.layout.item_spinner_latyout, headtype);
            spHeadTypeList.setDropDownViewResource(R.layout.item_spinner_latyout);
            spin_recall_head_type.setAdapter(spHeadTypeList);

        }

        else if (view.getId() == btn_recall_search.getId())
        {
            Intent i = new Intent(Recall_Main_Activity.this,RecallTagFillActivity.class);
            i.putExtra("LoggerName",dataloggerName);
            i.putExtra("LoggerIP",dataloggerIp);
            i.putExtra("oinum",str_oinum);
            i.putExtra("LoggerMacId",dataloggerMacId);
            startActivity(i);

        }
    }
    private void getHeadFromDB(String oiNumber) {
        Cursor headType = helper.getHead(oiNumber);
        while (headType.moveToNext()){
            String headTypeValue = headType.getString(0);
            headtype.add(headTypeValue);
            Log.d("TAG", "getHeadFromDB: ");
        }
    }

    private void getTargetWeight(String oiNumber) {
        Cursor targetWeight = helper.getTargetWeight(oiNumber);
        while (targetWeight.moveToNext()){
            String targetWeightValue = targetWeight.getString(0);
            targetweightlist.add(targetWeightValue);
            Log.d("TAG", "getTargetWeight: ");
        }
    }
    private void getFyYear(String oiNumber) {
        Cursor fyYear = helper.getTargetFyyear(oiNumber);
        while (fyYear.moveToNext()){
            String fyYearName = fyYear.getString(0);
            fyyearlist.add(fyYearName);
            Log.d("TAG", "getFyYear: ");
        }

    }








}
