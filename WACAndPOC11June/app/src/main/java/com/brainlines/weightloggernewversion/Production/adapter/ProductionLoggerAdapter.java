package com.brainlines.weightloggernewversion.Production.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;

import java.io.IOException;
import java.util.ArrayList;

public class ProductionLoggerAdapter extends RecyclerView.Adapter<ProductionLoggerAdapter.MyViewHolder> {
    Context context;
    ArrayList<LoggersFromDBModel> loggerList = new ArrayList<>();
    ProductionLoggerListListener listener;

    public ProductionLoggerAdapter(Context context, ArrayList<LoggersFromDBModel> loggerList, ProductionLoggerListListener listener) {
        this.context = context;
        this.loggerList = loggerList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_production_logger_list,parent,false);
        return new ProductionLoggerAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final LoggersFromDBModel model = loggerList.get(position);
        if (model != null){
            holder.txt_Logger_name.setText(model.getSerial_num());
            holder.cv_LoggerName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String s1 = model.getLogger_name();
                        String s2 = model.getIp_address();
                        String s3 = model.getCreate_by_id();
                        String s4 = model.getCreate_timestamp();
                        String s5 = model.getId();
                        String s6 = model.getInuse();
                        String s7 = model.getLogger_location();
                        String s8 = model.getMac_id();
                        String s9 = model.getModified_date();
                        String s10 = model.getOem_id();
                        String s11 = model.getSerial_num();
                        listener.getLogger(model.getLogger_name(),model.getIp_address(),model.getMac_id());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return loggerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_Logger_name;
        CardView cv_LoggerName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_Logger_name = (itemView).findViewById(R.id.txt_pro_Logger_name);
            cv_LoggerName = (itemView).findViewById(R.id.cv_LoggerName);
        }
    }
}
