package com.brainlines.weightloggernewversion.Production.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.Model.PWeighReadingsModel;
import com.brainlines.weightloggernewversion.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class PausedBatchListAdapter extends RecyclerView.Adapter<PausedBatchListAdapter.MyViewHolder> {


    public String TAG = "WorkDetailsListener";
    Context context;
    ArrayList<PWeighReadingsModel> weight_reading = new ArrayList<>();


    public PausedBatchListAdapter(Context context, ArrayList<PWeighReadingsModel> weight_reading) {
        this.context = context;
        this.weight_reading = weight_reading;
    }

    @NonNull
    @Override
    public PausedBatchListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //View view = LayoutInflater.from(context).inflate(R.layout.item_weight_reading, parent, false);
        View view = LayoutInflater.from(context).inflate(R.layout.item_batch_report_details, parent, false);
        return new PausedBatchListAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final PWeighReadingsModel model = weight_reading.get(position);
        DecimalFormat decimalFormat = new DecimalFormat("0.000");


        //String sr_no = model.getSr_no();

        if (model != null)
        {

            String weight = model.getWeight() ;
            Double dwtreading = Double.valueOf(weight);
            String status = model.getStatus();

            final String excess = model.getExcessGiveawayPerPouch();
            String date_time = model.getCreatedDate();
            holder.batch_report_readin_sr_no.setText(model.getPWeightReadingID());
            holder.batch_report_excess.setText(excess);

            holder.batch_report_date_time.setText(date_time);




            if (status.equals("Okay"))
            {
                holder.batch_report_status.setText("Ok");
                holder.batch_report_weight.setText(decimalFormat.format(dwtreading));


            }
            else
            {

                holder.batch_report_weight.setText(decimalFormat.format(dwtreading));
                holder.batch_report_status.setText("Ign");
                holder.batch_report_weight.setTextColor(context.getResources().getColor(R.color.color_2));


            }

        }

      /*  holder.txt_degree.setText(reading);
        if (status.equals("Okay"))
        {
            holder.txt_degree.setText(decimalFormat.format(dwtreading));
            holder.linear_background.setBackground(context.getResources().getDrawable(R.drawable.table_background));

        }
        else
        {

            holder.txt_degree.setText(decimalFormat.format(dwtreading));
            holder.txt_degree.setTextColor(context.getResources().getColor(R.color.color_2));
            holder.linear_background.setBackground(context.getResources().getDrawable(R.drawable.red_background));
        }*/

    }

    @Override
    public int getItemCount() {
        return weight_reading.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        //TextView txt_degree;
        TextView batch_report_readin_sr_no,batch_report_weight,batch_report_excess,batch_report_status,batch_report_date_time;

        LinearLayout linear_background;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            batch_report_readin_sr_no = (itemView).findViewById(R.id.batch_report_readin_sr_no1);
            batch_report_weight = (itemView).findViewById(R.id.batch_report_weight);
            batch_report_excess = (itemView).findViewById(R.id.batch_report_excess);
            batch_report_status = (itemView).findViewById(R.id.batch_report_status);
            batch_report_date_time = (itemView).findViewById(R.id.batch_report_date_time);
            /*txt_degree = (itemView).findViewById(R.id.txt_weight_reading);
            linear_background = (itemView).findViewById(R.id.linear_background);*/
        }
    }
}
