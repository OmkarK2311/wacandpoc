package com.brainlines.weightloggernewversion.activity.recalltest;

import java.util.Date;

public class RecallTestTagInfoModel {

    public String oiNum;
    public String fyyear;
    public String targetWt;
    public String fillerType;
    public Date createdDate;
    public String customer;
    public String machineType;
    public String  productType;
    public String product;
    public String operator;
    public String targetWtUnit;
    public String  inspector;
    public String  filmType;
    public String  productfeed;
    public String  pouchWidth;
    public String tagid;
    public String  pouchLength;
    public String  agitatorMotorFreq;
    public String  agitatorMode;
    public String  agitatorod;
    public String agitatorid;
    public String  agitatorPitch;
    public String targetacctype;
    public String targetaccval;
    public String mcSerialNum;
    public String prBulkMain;
    public String targetSpeed;
    public String customerProduct;
    public String  head;
    public String airPressure;
    public String qtySetting;
    public String bgLenSetting;
    public String fillValveGap;
    public String fillVfdFreq;
    public String horizCutSealSetting;
    public String horizBandSealSetting;
    public String vertSealSetting;
    public String augerAgitatorFreq;
    public String augerFillValve;
    public String augurPitch;
    public String pouchSealType;
    public String pouchTempHZF;
    public String pouchTempHZB;
    public String pouchTempVert;
    public String pouchTempVertFrontLeft;
    public String pouchTempVertFrontRight;
    public String pouchTempVertBackLeft;
    public String pouchTempVertBackRight;
    public String pouchGusset;
    public String customerFilm;
    public String pouchFilmThickness;
    public String pouchPrBulk;
    public String pouchEqHorizSerrations;
    public String pouchFillEvent;
    public String negWt;
    public  long id = 0;
    public String overallStatus = "";
    public String sealStatus = "";
    public String dropStatus = "";
    public String targetSpeedStatus = "";
    public String bgStatus;
    public String pprStatus;
    public String wtTestStatus;
    public String remarks;

    public String getOiNum() {
        return oiNum;
    }

    public void setOiNum(String oiNum) {
        this.oiNum = oiNum;
    }

    public String getFyyear() {
        return fyyear;
    }

    public void setFyyear(String fyyear) {
        this.fyyear = fyyear;
    }

    public String getTargetWt() {
        return targetWt;
    }

    public void setTargetWt(String targetWt) {
        this.targetWt = targetWt;
    }

    public String getFillerType() {
        return fillerType;
    }

    public void setFillerType(String fillerType) {
        this.fillerType = fillerType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getTargetWtUnit() {
        return targetWtUnit;
    }

    public void setTargetWtUnit(String targetWtUnit) {
        this.targetWtUnit = targetWtUnit;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getFilmType() {
        return filmType;
    }

    public void setFilmType(String filmType) {
        this.filmType = filmType;
    }

    public String getProductfeed() {
        return productfeed;
    }

    public void setProductfeed(String productfeed) {
        this.productfeed = productfeed;
    }

    public String getPouchWidth() {
        return pouchWidth;
    }

    public void setPouchWidth(String pouchWidth) {
        this.pouchWidth = pouchWidth;
    }

    public String getTagid() {
        return tagid;
    }

    public void setTagid(String tagid) {
        this.tagid = tagid;
    }

    public String getPouchLength() {
        return pouchLength;
    }

    public void setPouchLength(String pouchLength) {
        this.pouchLength = pouchLength;
    }

    public String getAgitatorMotorFreq() {
        return agitatorMotorFreq;
    }

    public void setAgitatorMotorFreq(String agitatorMotorFreq) {
        this.agitatorMotorFreq = agitatorMotorFreq;
    }

    public String getAgitatorMode() {
        return agitatorMode;
    }

    public void setAgitatorMode(String agitatorMode) {
        this.agitatorMode = agitatorMode;
    }

    public String getAgitatorod() {
        return agitatorod;
    }

    public void setAgitatorod(String agitatorod) {
        this.agitatorod = agitatorod;
    }

    public String getAgitatorid() {
        return agitatorid;
    }

    public void setAgitatorid(String agitatorid) {
        this.agitatorid = agitatorid;
    }

    public String getAgitatorPitch() {
        return agitatorPitch;
    }

    public void setAgitatorPitch(String agitatorPitch) {
        this.agitatorPitch = agitatorPitch;
    }

    public String getTargetacctype() {
        return targetacctype;
    }

    public void setTargetacctype(String targetacctype) {
        this.targetacctype = targetacctype;
    }

    public String getTargetaccval() {
        return targetaccval;
    }

    public void setTargetaccval(String targetaccval) {
        this.targetaccval = targetaccval;
    }

    public String getMcSerialNum() {
        return mcSerialNum;
    }

    public void setMcSerialNum(String mcSerialNum) {
        this.mcSerialNum = mcSerialNum;
    }

    public String getPrBulkMain() {
        return prBulkMain;
    }

    public void setPrBulkMain(String prBulkMain) {
        this.prBulkMain = prBulkMain;
    }

    public String getTargetSpeed() {
        return targetSpeed;
    }

    public void setTargetSpeed(String targetSpeed) {
        this.targetSpeed = targetSpeed;
    }

    public String getCustomerProduct() {
        return customerProduct;
    }

    public void setCustomerProduct(String customerProduct) {
        this.customerProduct = customerProduct;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getAirPressure() {
        return airPressure;
    }

    public void setAirPressure(String airPressure) {
        this.airPressure = airPressure;
    }

    public String getQtySetting() {
        return qtySetting;
    }

    public void setQtySetting(String qtySetting) {
        this.qtySetting = qtySetting;
    }

    public String getBgLenSetting() {
        return bgLenSetting;
    }

    public void setBgLenSetting(String bgLenSetting) {
        this.bgLenSetting = bgLenSetting;
    }

    public String getFillValveGap() {
        return fillValveGap;
    }

    public void setFillValveGap(String fillValveGap) {
        this.fillValveGap = fillValveGap;
    }

    public String getFillVfdFreq() {
        return fillVfdFreq;
    }

    public void setFillVfdFreq(String fillVfdFreq) {
        this.fillVfdFreq = fillVfdFreq;
    }

    public String getHorizCutSealSetting() {
        return horizCutSealSetting;
    }

    public void setHorizCutSealSetting(String horizCutSealSetting) {
        this.horizCutSealSetting = horizCutSealSetting;
    }

    public String getHorizBandSealSetting() {
        return horizBandSealSetting;
    }

    public void setHorizBandSealSetting(String horizBandSealSetting) {
        this.horizBandSealSetting = horizBandSealSetting;
    }

    public String getVertSealSetting() {
        return vertSealSetting;
    }

    public void setVertSealSetting(String vertSealSetting) {
        this.vertSealSetting = vertSealSetting;
    }

    public String getAugerAgitatorFreq() {
        return augerAgitatorFreq;
    }

    public void setAugerAgitatorFreq(String augerAgitatorFreq) {
        this.augerAgitatorFreq = augerAgitatorFreq;
    }

    public String getAugerFillValve() {
        return augerFillValve;
    }

    public void setAugerFillValve(String augerFillValve) {
        this.augerFillValve = augerFillValve;
    }

    public String getAugurPitch() {
        return augurPitch;
    }

    public void setAugurPitch(String augurPitch) {
        this.augurPitch = augurPitch;
    }

    public String getPouchSealType() {
        return pouchSealType;
    }

    public void setPouchSealType(String pouchSealType) {
        this.pouchSealType = pouchSealType;
    }

    public String getPouchTempHZF() {
        return pouchTempHZF;
    }

    public void setPouchTempHZF(String pouchTempHZF) {
        this.pouchTempHZF = pouchTempHZF;
    }

    public String getPouchTempHZB() {
        return pouchTempHZB;
    }

    public void setPouchTempHZB(String pouchTempHZB) {
        this.pouchTempHZB = pouchTempHZB;
    }

    public String getPouchTempVert() {
        return pouchTempVert;
    }

    public void setPouchTempVert(String pouchTempVert) {
        this.pouchTempVert = pouchTempVert;
    }

    public String getPouchTempVertFrontLeft() {
        return pouchTempVertFrontLeft;
    }

    public void setPouchTempVertFrontLeft(String pouchTempVertFrontLeft) {
        this.pouchTempVertFrontLeft = pouchTempVertFrontLeft;
    }

    public String getPouchTempVertFrontRight() {
        return pouchTempVertFrontRight;
    }

    public void setPouchTempVertFrontRight(String pouchTempVertFrontRight) {
        this.pouchTempVertFrontRight = pouchTempVertFrontRight;
    }

    public String getPouchTempVertBackLeft() {
        return pouchTempVertBackLeft;
    }

    public void setPouchTempVertBackLeft(String pouchTempVertBackLeft) {
        this.pouchTempVertBackLeft = pouchTempVertBackLeft;
    }

    public String getPouchTempVertBackRight() {
        return pouchTempVertBackRight;
    }

    public void setPouchTempVertBackRight(String pouchTempVertBackRight) {
        this.pouchTempVertBackRight = pouchTempVertBackRight;
    }

    public String getPouchGusset() {
        return pouchGusset;
    }

    public void setPouchGusset(String pouchGusset) {
        this.pouchGusset = pouchGusset;
    }

    public String getCustomerFilm() {
        return customerFilm;
    }

    public void setCustomerFilm(String customerFilm) {
        this.customerFilm = customerFilm;
    }

    public String getPouchFilmThickness() {
        return pouchFilmThickness;
    }

    public void setPouchFilmThickness(String pouchFilmThickness) {
        this.pouchFilmThickness = pouchFilmThickness;
    }

    public String getPouchPrBulk() {
        return pouchPrBulk;
    }

    public void setPouchPrBulk(String pouchPrBulk) {
        this.pouchPrBulk = pouchPrBulk;
    }

    public String getPouchEqHorizSerrations() {
        return pouchEqHorizSerrations;
    }

    public void setPouchEqHorizSerrations(String pouchEqHorizSerrations) {
        this.pouchEqHorizSerrations = pouchEqHorizSerrations;
    }

    public String getPouchFillEvent() {
        return pouchFillEvent;
    }

    public void setPouchFillEvent(String pouchFillEvent) {
        this.pouchFillEvent = pouchFillEvent;
    }

    public String getNegWt() {
        return negWt;
    }

    public void setNegWt(String negWt) {
        this.negWt = negWt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOverallStatus() {
        return overallStatus;
    }

    public void setOverallStatus(String overallStatus) {
        this.overallStatus = overallStatus;
    }

    public String getSealStatus() {
        return sealStatus;
    }

    public void setSealStatus(String sealStatus) {
        this.sealStatus = sealStatus;
    }

    public String getDropStatus() {
        return dropStatus;
    }

    public void setDropStatus(String dropStatus) {
        this.dropStatus = dropStatus;
    }

    public String getTargetSpeedStatus() {
        return targetSpeedStatus;
    }

    public void setTargetSpeedStatus(String targetSpeedStatus) {
        this.targetSpeedStatus = targetSpeedStatus;
    }

    public String getBgStatus() {
        return bgStatus;
    }

    public void setBgStatus(String bgStatus) {
        this.bgStatus = bgStatus;
    }

    public String getPprStatus() {
        return pprStatus;
    }

    public void setPprStatus(String pprStatus) {
        this.pprStatus = pprStatus;
    }

    public String getWtTestStatus() {
        return wtTestStatus;
    }

    public void setWtTestStatus(String wtTestStatus) {
        this.wtTestStatus = wtTestStatus;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
