package com.brainlines.weightloggernewversion;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.brainlines.weightloggernewversion.Production.activity.AddNewBatchActivity;
import com.brainlines.weightloggernewversion.Production.activity.PauseTestListActivity;
import com.brainlines.weightloggernewversion.Production.activity.ProductionHistoryActvity;

public class ProductionMenuActivity extends AppCompatActivity {
    String dataloggerName,dataloggerIp,dataloggerMacId;
    CardView cv_new_batch,cv_history_test,cv_pause_test;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clone);
        dataloggerName = getIntent().getStringExtra("LoggerName");
        dataloggerIp = getIntent().getStringExtra("LoggerIP");
        dataloggerMacId = getIntent().getStringExtra("mac_id");
        cv_new_batch = findViewById(R.id.cv_new_batch);
        cv_history_test = findViewById(R.id.cv_history_test);
        cv_pause_test = findViewById(R.id.cv_pause_test);

        cv_history_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProductionMenuActivity.this, ProductionHistoryActvity.class);
                startActivity(intent);
            }
        });
        cv_new_batch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProductionMenuActivity.this, AddNewBatchActivity.class);
                intent.putExtra("LoggerName",dataloggerName);
                intent.putExtra("LoggerIP",dataloggerIp);
                intent.putExtra("mac_id",dataloggerMacId);
                startActivity(intent);
            }
        });
        cv_pause_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ProductionMenuActivity.this, PauseTestListActivity.class);
                intent.putExtra("LoggerName",dataloggerName);
                intent.putExtra("LoggerIP",dataloggerIp);
                intent.putExtra("mac_id",dataloggerMacId);
                startActivity(intent);

            }
        });
    }
}
