package com.brainlines.weightloggernewversion.Production.activity;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.brainlines.weightloggernewversion.Production.Model.Production_Tolerance_Model;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.MobileJsonData;
import com.brainlines.weightloggernewversion.model.MobileJsonDataDeserializer;
import com.brainlines.weightloggernewversion.model.Tag;
import com.brainlines.weightloggernewversion.model.TestRun;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@SuppressLint("Registered")
public class AddNewBatchActivity extends AppCompatActivity {

//    private static final String TAG = "AddNewBatchActivity";
//    ImageButton btnStart;
//    Spinner spin_production_product,spin_production_uom,spin_production_uom_for_totalqty,spin_production_shifts,spin_production_operator_name;
//    Spinner spin_production_negative_weight;
//    EditText ed_prod_gm_pouch,ed_production_total_qty,ed_prod_operator_name;
//    TextView txt_production_date;
//    private ArrayList<String> product_list = new ArrayList<>();
//    private ArrayList<String> uomList = new ArrayList<>();
//    ArrayList<String> totaluomList = new ArrayList<>();
//    private ArrayList<String> shiftlist = new ArrayList<>();
//    private ArrayList<String> negative_list = new ArrayList<>();
//    private ArrayList<String> grammage_values_list = new ArrayList<>();
//    private ArrayList<Production_Tolerance_Model> productionTolerance_list = new ArrayList<>();
//    DataBaseHelper helper;
//    String dataloggerName,dataloggerIp,dataloggerMacId;
//    public static final String MQTT_BROKER = "tcp://192.168.10.67:1883";
//    String tagInfoStr="";
//    String tagID = "",rstring,gmtokgcalvatue;
//    private String sp_product_string,sp_uom_str,str_shifts,str_positive_tolerance,str_negative_tolerance,uclgm,lclgm,str_negative_weight_allowed,str_batch_id,strtotalqty;
//    EditText ed_no_of_pouches;
//    TextView ed_positive_tolerance,ed_negative_tolerance,txt_add_new_batch_uom_total_qty;
//    DecimalFormat decimalFormat = new DecimalFormat("0.000");
//    RadioButton rd_no_of_pouches,rd_total_qty;
//    RadioGroup radio_group;
//   // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
//    String currentDateandTime = sdf.format(new Date());
//    int intkgvalue = 0;
//    TextView txtGm;
//    double dintkgvalue =0.0;
//    private String dbName;
//    private SQLiteDatabase db;
//    private String resToMobile;
//
//    private String dbIpAddress = "192.168.10.67";
//    private String dbUserName = "test2";
//    private String dbPassword = "test12345";
//    private String dbDatabaseName = "decitalpool";
//    private String serverport = "1433";
//    private String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
//    private int DATA_LOGGER_PORT = 8080;
//    public static final String QUERY_GET_TEST_RUNS_IN_PROGRESS = "select * from test_run_status where status='" + Constants.RUN_TEST_INPROGRESS_STATUS + "'";
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_add_new_batch);
//        dbName = AppPreferences.getDatabseName(this);
////        helper = new DataBaseHelper(this,dbName);
//        helper = new DataBaseHelper(this);
//        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
//        initUi();
//        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH-mm");
//        masterProductionTolerance();
//        getProductionGrammageValues();
//        final Calendar cal = Calendar.getInstance();
//        txt_production_date.setText(dateFormat.format(cal.getTime()));
//
//        final String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//
//        if (rd_no_of_pouches.isChecked())
//        {
//            rstring = "pouch";
//            ed_production_total_qty.setEnabled(false);
//            ed_production_total_qty.setBackground(getResources().getDrawable(R.drawable.read_only_background));
//            ed_no_of_pouches.setBackground(getResources().getDrawable(R.drawable.table_background));
//            ed_production_total_qty.getText().clear();
//            ed_no_of_pouches.getText().clear();
//            ed_no_of_pouches.setEnabled(true);
//
//            ed_production_total_qty.removeTextChangedListener(textWatcher);
//            ed_no_of_pouches.addTextChangedListener(textWatcher);
//        }
//
//        ed_prod_gm_pouch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                if (s != null && !s.toString().equalsIgnoreCase(""))
//                {
//                    String gm_pouch = s.toString();
//                    if (sp_uom_str.equals("gm"))
//                    {
//                        if (gm_pouch.equals("") || gm_pouch.equals("")){
//                            ed_prod_gm_pouch.setText("");
//                        }else {
//
//                            for (int i = 0;i<productionTolerance_list.size();i++)
//                            {
//                                String grammage = productionTolerance_list.get(i).getGrammage();
//                                if (gm_pouch.equals(grammage))
//                                {
//
//                                    String strpositive = productionTolerance_list.get(i).getTolerancePositive();
//                                    String strnegative = productionTolerance_list.get(i).getToleranceNegetive();
//                                    Double dcalpositivetoleance = Double.parseDouble(strpositive);
//                                    Double dcalnegativetoleance = Double.parseDouble(strnegative);
//
//                                    str_positive_tolerance = strpositive;
//                                    str_negative_tolerance = strnegative;
//                                    ed_positive_tolerance.setText(decimalFormat.format(dcalpositivetoleance));
//                                    ed_negative_tolerance.setText(decimalFormat.format(dcalnegativetoleance));
//                                }
//                            }
//                        }
//                    }
//                    else if (sp_uom_str.equals("Kg"))
//                    {
//                        if (gm_pouch.equals("") || gm_pouch.equals("")){
//                            ed_prod_gm_pouch.setText("");
//
//                        }else {
//                            String strkgvalue = String.valueOf(intkgvalue);
//                            Double gmvalue = Double.parseDouble(gm_pouch) * Double.parseDouble(strkgvalue);
//                            gmtokgcalvatue = String.valueOf(gmvalue);
//
//
//                            gmtokgcalvatue = gmtokgcalvatue.substring(0,gmtokgcalvatue.length()-2);
//                            for (int i = 0;i<productionTolerance_list.size();i++)
//                            {
//                                String grammage = productionTolerance_list.get(i).getGrammage();
//                                if (gmtokgcalvatue.equals(grammage))
//                                {
//                                    String strpositive = productionTolerance_list.get(i).getTolerancePositive();
//                                    String strnegative = productionTolerance_list.get(i).getToleranceNegetive();
//
//
//                                    Double dcalpositivetoleance = Double.parseDouble(strpositive);
//                                    Double dcalnegativetoleance = Double.parseDouble(strnegative);
//
//                                    str_positive_tolerance = String.valueOf(strpositive);
//                                    str_negative_tolerance = String.valueOf(strpositive);
//
//                                    ed_positive_tolerance.setText(String.valueOf(decimalFormat.format(dcalpositivetoleance)));
//                                    ed_negative_tolerance.setText(String.valueOf(decimalFormat.format(dcalnegativetoleance)));
//                                }
//                            }
//                        }
//                    }
//                }
//                else
//                {
//                    ed_positive_tolerance.setText("");
//                    ed_negative_tolerance.setText("");
//                }
//
//
//            }
//        });
//
//        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (checkedId==R.id.rd_no_of_pouches)
//                {
//                    rstring = "pouch";
//                    ed_production_total_qty.setEnabled(false);
//                    ed_production_total_qty.setBackground(getResources().getDrawable(R.drawable.read_only_background));
//                    ed_no_of_pouches.setBackground(getResources().getDrawable(R.drawable.table_background));
//                    ed_production_total_qty.getText().clear();
//                    ed_no_of_pouches.getText().clear();
//                    ed_no_of_pouches.setEnabled(true);
//
//                    ed_production_total_qty.removeTextChangedListener(textWatcher);
//                    ed_no_of_pouches.addTextChangedListener(textWatcher);
//                }
//                else if (checkedId == R.id.rd_total_qty)
//                {
//                    rstring = "totalqty";
//                    ed_no_of_pouches.setEnabled(false);
//                    ed_no_of_pouches.setBackground(getResources().getDrawable(R.drawable.read_only_background));
//                    ed_production_total_qty.setBackground(getResources().getDrawable(R.drawable.table_background));
//                    ed_production_total_qty.setEnabled(true);
//
//                    ed_no_of_pouches.getText().clear();
//                    ed_production_total_qty.getText().clear();
//                    ed_no_of_pouches.removeTextChangedListener(textWatcher);
//                    ed_production_total_qty.addTextChangedListener(textWatcher);
//                }
//
//            }
//
//
//        });
//        spin_production_product.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                sp_product_string = spin_production_product.getSelectedItem().toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        spin_production_shifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                str_shifts = spin_production_shifts.getSelectedItem().toString();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        spin_production_uom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                sp_uom_str = spin_production_uom.getSelectedItem().toString();
//
//                String gm_pouch = ed_prod_gm_pouch.getText().toString();
//                for (int i = 0;i<productionTolerance_list.size();i++)
//                {
//                    String grammage = productionTolerance_list.get(i).getGrammage();
//                    if (gm_pouch.equals(grammage))
//                    {
//                        String strpositive = productionTolerance_list.get(i).getTolerancePositive();
//                        String strnegative = productionTolerance_list.get(i).getToleranceNegetive();
//                        str_positive_tolerance = strpositive;
//                        str_negative_tolerance = strnegative;
//
//                            ed_positive_tolerance.setText(str_positive_tolerance);
//                            ed_negative_tolerance.setText(str_negative_tolerance);
//                    }
//                }
//
//                if (sp_uom_str.equals("Kg")) {
//                    String kgvalue = grammage_values_list.get(0);
//                    intkgvalue = Integer.parseInt(kgvalue);
//                    dintkgvalue = Double.parseDouble(kgvalue);
//                    txtGm.setText("Kg/pouch : ");
//                }
//                else {
//                    txtGm.setText("Gm/pouch : ");
//                }
//                txt_add_new_batch_uom_total_qty.setText(sp_uom_str);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        spin_production_negative_weight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                str_negative_weight_allowed = spin_production_negative_weight.getSelectedItem().toString();
//                if (str_negative_weight_allowed.equals("No"))
//                {
//                    ed_negative_tolerance.setText("0");
//                }
//                else
//                {
//                    ed_negative_tolerance.setText(str_negative_tolerance);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//
//        btnStart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//                DateFormat tagIDDateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
//                String dataLoggerName = dataloggerName;
//                String dataLoggerIP = dataloggerIp;
//                //dataLoggerIP = "localhost";
//                // get current date time with Calendar()
//                if (sp_uom_str.equals("Select")) {
//                    Toast.makeText(AddNewBatchActivity.this, "Please select UOM", Toast.LENGTH_SHORT).show(); }
//                else if (TextUtils.isEmpty(ed_production_total_qty.getText().toString())) {
//                    Toast.makeText(AddNewBatchActivity.this, "Please enter total qty", Toast.LENGTH_SHORT).show();
//                }
//                else if (TextUtils.isEmpty(ed_no_of_pouches.getText().toString())) {
//                    Toast.makeText(AddNewBatchActivity.this, "Please enter number of pouch", Toast.LENGTH_SHORT).show();
//                }
//                else if (TextUtils.isEmpty(ed_production_total_qty.getText().toString())){
//                    Toast.makeText(AddNewBatchActivity.this, "Please enter total quantity", Toast.LENGTH_SHORT).show();
//                }
//                else if (TextUtils.isEmpty(ed_prod_operator_name.getText().toString())) {
//                    Toast.makeText(AddNewBatchActivity.this, "Please enter operator", Toast.LENGTH_SHORT).show();
//                }
//                else if (TextUtils.isEmpty(sp_product_string)) {
//                    Toast.makeText(AddNewBatchActivity.this, "Please select product", Toast.LENGTH_SHORT).show();
//                }
//                else if (TextUtils.isEmpty(ed_prod_gm_pouch.getText().toString())){
//                    Toast.makeText(AddNewBatchActivity.this, "Please enter gm per pouch", Toast.LENGTH_SHORT).show(); }
//                else
//                {
//                    int profile_counts = helper.getproductiontagCount();
//                    helper.close();
//                    int count = profile_counts + 1;
//                    DateFormat dateFormat1 = new SimpleDateFormat("yyyyMMdd HHmm");
//
//                    str_batch_id = "B".concat(m_androidId + "D" +dateFormat.format(cal.getTime()) +"C" +count + "");
//
//                    Calendar cal = Calendar.getInstance();
//                    String tagUniqueStr = "DiA0001" + ":"+ "prod" + ":" + dateFormat.format(cal.getTime());
////                    String tagUniqueStr = "DiA0001" + ":"+ "prod" + ":" + tagIDDateFormat.format(cal.getTime());
//                    //String currentDateandTime = sdf.format(new Date());
//                    String msg = "{\"dataLoggerIP\" : \"192.168.10.69\",  \"dataLoggerName\": \"DiA0AA01\",\"macaddress\" : \"192.168.1.102\",  \"inspector\": \"inspector \",   \"tag\":{\"oiNum\" :\"so1261846\",  \"targetWt\" :\"200\" ,  \"fyyear\" : \"2020-2021\" ,  \"fillerType\" : \"Other\" ,  \"customer\" : \"assdf\", \"machinetype\" : \"Sprint 250\", \"producttype\" : \"Solid\", \"product\" : \"Atta\", \"operator\" : \"xyz\", \"targetWtUnit\" : \"gm\", \"inspector\" : \"inspector\", \"filmtype\" : \"Printed\", \"productfeed\" : \"System\", \"pouchWidth\" : \"100\", \"pouchLength\" : \"50\", \"agitatorMotorFreq\" : \"\", \"agitatormode\" : \"\", \"agitatorod\" : \"\", \"agitatorid\" : \"\", \"agitatorpitch\" : \"\", \"targetacctype\" : \"+/- GM\", \"targetaccval\" : \"0.25\", \"mcserialnum\" : \"1223\", \"prBulkMain\" : \"0.25\", \"targetspeed\" : \"100\", \"customerProduct\" : \"Yes\", \"head\" : \"Right\", \"airPressure\" : \"\", \"qtysetting\" : \"\", \"bglensetting\" : \"\", \"fillValveGap\" : \"\", \"fillvfdFreq\" : \"\", \"horizCutSealSetting\" : \"\", \"horizBandSealSetting\" : \"\", \"vertSealSetting\" : \"\", \"augerAgitatorFreq\" : \"\", \"augerFillValve\" : \"null\", \"augurPitch\" : \"\", \"pouchSealType\" : \"Penta\", \"pouchTempHZF\" : \"\", \"pouchTempHZB\" : \"\", \"pouchTempVert\" : \"\", \"pouchTempVertFrontLeft\" : \"\", \"pouchTempVertFrontRight\" : \"\", \"pouchTempVertBackLeft\" : \"\", \"pouchTempVertBackRight\" : \"\", \"pouchGusset\" : \"\", \"customerFilm\" : \"Yes\", \"pouchFilmThickness\" : \"na\", \"pouchPrBulk\" : \"\", \"pouchEqHorizSerrations\" : \"\", \"pouchFillEvent\" : \"Jaw\", \"negWt\" : \"No\"}}";
//
//                    SaveTagInfo(msg);
//                    if (sp_uom_str.equals("gm"))
//                    {
//                        int  ucl = Integer.parseInt(ed_prod_gm_pouch.getText().toString()) + Integer.parseInt(str_positive_tolerance);
//                        int lcl = Integer.parseInt(ed_prod_gm_pouch.getText().toString()) - Integer.parseInt(str_negative_tolerance);
//                        uclgm = String.valueOf(ucl);
//                        lclgm = String.valueOf(lcl);
//                        double duclgm = Double.parseDouble(uclgm);
//                        double dlclgm = Double.parseDouble(lclgm);
//                        Intent i = new Intent(AddNewBatchActivity.this, ProductionWeightReadingActivity.class);
//                        i.putExtra("dataloggerName",dataloggerName);
//                        i.putExtra("dataloggerIp",dataloggerIp);
//                        i.putExtra("current_tag_id",resToMobile);
//                        i.putExtra("gmperpouch",ed_prod_gm_pouch.getText().toString());
//                        i.putExtra("total_qty",strtotalqty);
//                        i.putExtra("negative_tolerance",str_positive_tolerance);
//                        i.putExtra("positive_tolerance",str_negative_tolerance);
//                        i.putExtra("no_of_pouches",ed_no_of_pouches.getText().toString());
//                        i.putExtra("batch_id",str_batch_id);
//                        i.putExtra("select_date",currentDateandTime);
//                        i.putExtra("product",sp_product_string.trim());
//                        i.putExtra("ucl",uclgm);
//                        i.putExtra("uom",sp_uom_str);
//                        i.putExtra("lcl",lclgm);
//                        i.putExtra("shift",str_shifts);
//                        i.putExtra("mac_id",dataloggerMacId);
//                        startActivity(i);
//                        InsertTagIntoDb();
//                    }
//                    else if (sp_uom_str.equals("Kg"))
//                    {
//                        Double ucl = Double.parseDouble(gmtokgcalvatue) + Double.parseDouble(str_positive_tolerance);
//                        Double lcl = Double.parseDouble(gmtokgcalvatue) - Double.parseDouble(str_negative_tolerance);
//
//                        Double ducl = ucl /dintkgvalue;
//                        Double dlcl = lcl/dintkgvalue;
//                        Intent i = new Intent(AddNewBatchActivity.this, ProductionWeightReadingActivity.class);
//                        i.putExtra("dataloggerName",dataloggerName);
//                        i.putExtra("dataloggerIp",dataloggerIp);
//                        i.putExtra("current_tag_id",tagID);
//                        i.putExtra("gmperpouch",ed_prod_gm_pouch.getText().toString());
//                        i.putExtra("total_qty",ed_production_total_qty.getText().toString());
//                        i.putExtra("negative_tolerance",str_positive_tolerance);
//                        i.putExtra("positive_tolerance",str_negative_tolerance);
//                        i.putExtra("no_of_pouches",ed_no_of_pouches.getText().toString());
//                        i.putExtra("batch_id","B0_1052020_16");
//                        i.putExtra("select_date",currentDateandTime);
//                        i.putExtra("product",sp_product_string.trim());
//                        i.putExtra("ucl",decimalFormat.format(ducl));
//                        i.putExtra("uom",sp_uom_str);
//                        i.putExtra("lcl",decimalFormat.format(dlcl));
//                        i.putExtra("shift",str_shifts);
//                        i.putExtra("intkgvalue",String.valueOf(intkgvalue));
//                        i.putExtra("mac_id",dataloggerMacId);
//                        startActivity(i);
//                        InsertTagIntoDb();
//                    }
//                }
//
//            }
//        });
//
//    }
//
//    private void initUi()
//    {
//        dataloggerName = getIntent().getStringExtra("LoggerName");
//        dataloggerIp = getIntent().getStringExtra("LoggerIP");
//        dataloggerMacId = getIntent().getStringExtra("mac_id");
//
//        radio_group = findViewById(R.id.radio_group);
//        rd_no_of_pouches = findViewById(R.id.rd_no_of_pouches);
//        rd_total_qty = findViewById(R.id.rd_total_qty);
//        txtGm = findViewById(R.id.txtGm);
//
//        ed_prod_operator_name = findViewById(R.id.ed_prod_operator_name);
//        btnStart = findViewById(R.id.btnStart);
//        spin_production_product = findViewById(R.id.spin_production_product);
//        spin_production_uom = findViewById(R.id.spin_production_uom);
//       // spin_production_uom_for_totalqty = findViewById(R.id.spin_production_uom_for_totalqty);
//        spin_production_shifts = findViewById(R.id.spin_production_shifts);
//        spin_production_negative_weight = findViewById(R.id.spin_production_negative_weight);
//        txt_add_new_batch_uom_total_qty = findViewById(R.id.txt_add_new_batch_uom_total_qty);
//
//        ed_prod_gm_pouch = findViewById(R.id.ed_prod_gm_pouch);
//        ed_production_total_qty = findViewById(R.id.ed_production_total_qty);
//        ed_no_of_pouches =findViewById(R.id.ed_no_of_pouches);
//        ed_positive_tolerance = findViewById(R.id.ed_positive_tolerance);
//        ed_negative_tolerance = findViewById(R.id.ed_negative_tolerance);
//        txt_production_date = findViewById(R.id.txt_production_date);
//
//
//        getProductsUsedForTrial();
//        ArrayAdapter<String> spProductUsdForTrialList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, product_list);
//        spProductUsdForTrialList.setDropDownViewResource(R.layout.item_spinner_latyout);
//        spin_production_product.setAdapter(spProductUsdForTrialList);
//
//        //Adding data to the list to show in UoM Spinner
//        getUoMList();
//        ArrayAdapter<String> spUoMList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, uomList);
//        spUoMList.setDropDownViewResource(R.layout.item_spinner_latyout);
//        spin_production_uom.setAdapter(spUoMList);
//
//      /*  ArrayAdapter<String> uom_for_totalqtylist = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, totaluomList);
//        spUoMList.setDropDownViewResource(R.layout.item_spinner_latyout);
//        spin_production_uom_for_totalqty.setAdapter(uom_for_totalqtylist);*/
//
//        shiftlist.add("1");
//        shiftlist.add("2");
//        shiftlist.add("3");
//
//        ArrayAdapter<String> shift_arraylist = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, shiftlist);
//        shift_arraylist.setDropDownViewResource(R.layout.item_spinner_latyout);
//        spin_production_shifts.setAdapter(shift_arraylist);
//
//        negative_list.add("Yes");
//        negative_list.add("No");
//        ArrayAdapter<String> negative_weight_allowed_list = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, negative_list);
//        negative_weight_allowed_list.setDropDownViewResource(R.layout.item_spinner_latyout);
//        spin_production_negative_weight.setAdapter(negative_weight_allowed_list);
//    }
//
//    private void getProductsUsedForTrial() {
//        Cursor productName = helper.getProducts();
//        while (productName.moveToNext()){
//            String product = productName.getString(0);
//            product_list.add(product);
//            Log.d("TAG", "getProductsUsedForTrial: ");
//        }
//
//    }
//
//    private void getProductionGrammageValues() {
//        Cursor grammageValues = helper.getProductionGrammageValues();
//        while (grammageValues.moveToNext()){
//            String kgid = grammageValues.getString(0);
//            String kgname = grammageValues.getString(1);
//            String kgvalues = grammageValues.getString(2);
//
//            grammage_values_list.add(kgvalues);
//            Log.d("TAG", "getProductsUsedForTrial: ");
//        }
//
//    }
//
//    private void masterProductionTolerance()
//    {
//        Cursor tolerancemaster = helper.getMasterProductionTolerance();
//        while (tolerancemaster.moveToNext()){
//            String tolerance_id = tolerancemaster.getString(0);
//            int oem_id = tolerancemaster.getInt(1);
//            String Grammage = tolerancemaster.getString(2);
//            String PositiveTolerance = tolerancemaster.getString(3);
//            String NegativeTolerance = tolerancemaster.getString(4);
//
//            Production_Tolerance_Model model = new Production_Tolerance_Model();
//            model.setProductionToleranceID(tolerance_id);
//            model.setOEMID(oem_id);
//            model.setGrammage(Grammage);
//            model.setTolerancePositive(PositiveTolerance);
//            model.setToleranceNegetive(NegativeTolerance);
//
//            productionTolerance_list.add(model);
//            Log.d("TAG", "getProductsUsedForTrial: ");
//        }
//    }
//
//    private void getUoMList(){
//        uomList.add("Select");
//        Cursor uom = helper.getUoMFromLocalDB();
//        while (uom.moveToNext()){
//            String struom = uom.getString(0);
//            if (struom.equals("gm"))
//            {
//                uomList.add(struom);
//                totaluomList.add(struom);
//            }
//            else if (struom.equals("Kg"))
//            {
//                uomList.add(struom);
//
//            }
//           Log.d("TAG", "getFillerType: ");
//        }
//    }
//
//
//    public void InsertTagIntoDb() {
//        try {
////            SQLiteDatabase sqLiteDatabase = new DataBaseHelper(this).getWritableDatabase();
//            ContentValues contentValues = new ContentValues();
//            if (sp_uom_str.equals("gm"))
//            {
//                Double  ucl = Double.parseDouble(ed_prod_gm_pouch.getText().toString()) + Double.parseDouble(str_positive_tolerance);
//                Double lcl = Double.parseDouble(ed_prod_gm_pouch.getText().toString()) - Double.parseDouble(str_negative_tolerance);
//                uclgm = String.valueOf(ucl);
//                lclgm = String.valueOf(lcl);
//                Double dgmperpouch = Double.valueOf(ed_prod_gm_pouch.getText().toString());
//                Double dtolerance = Double.valueOf(str_positive_tolerance);
//                Double dntolerance = Double.valueOf(str_negative_tolerance);
//
//                contentValues.put(Constants.PRODUCTION_TAG_ID,1);
//                contentValues.put(Constants.PRODUCTION_TAG, resToMobile);
//                contentValues.put(Constants.PRODUCTION_TAG_OEM_ID, 1234);
//                contentValues.put(Constants.PRODUCTION_TAG_MAC_ID, dataloggerMacId);
//                contentValues.put(Constants.PRODUCTION_TAG_BATCH_ID,str_batch_id);
//                contentValues.put(Constants.PRODUCTION_TAG_PRODUCT_ID, sp_product_string.trim());
//                contentValues.put(Constants.PRODUCTION_TAG_GM_PER_POUCH,Float.parseFloat(String.valueOf(dgmperpouch)));
//                contentValues.put(Constants.PRODUCTION_TAG_UNIT, sp_uom_str);
//                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY, ed_production_total_qty.getText().toString());
//                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY_UOM, sp_uom_str);
//                contentValues.put(Constants.PRODUCTION_TAG_TARGET_ACCURACY_TYPE, "Gm");
//                contentValues.put(Constants.PRODUCTION_TAG_TARGET_ACCURACY_VALUE, 0.5);
//                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY_UOM, sp_uom_str);
//               //contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED, str_negative_weight_allowed);
//                if (str_negative_weight_allowed.equals("Yes"))
//                { contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED,0);
//                }
//                else
//                {
//                    contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED,1);
//                }
//
//                contentValues.put(Constants.PRODUCTION_TAG_SHIFT, str_shifts);
//                contentValues.put(Constants.PRODUCTION_TAG_NO_OF_POUCHES, Integer.parseInt(ed_no_of_pouches.getText().toString()));
//                contentValues.put(Constants.PRODUCTION_TAG_SHIFT, str_shifts);
//                contentValues.put(Constants.PRODUCTION_TAG_SESSION_POSITVIE_TOLERANCE,Float.parseFloat(String.valueOf(dtolerance)));
//                contentValues.put(Constants.PRODUCTION_TAG_SESSION_NEAGTIVE_TOLERANCE,Float.parseFloat(String.valueOf(dtolerance)));
//                contentValues.put(Constants.PRODUCTION_TAG_MASTER_TOLERANCE_POSITIVE,Float.parseFloat(String.valueOf(dtolerance)));
//                contentValues.put(Constants.PRODUCTION_TAG_MASTER_TOLERANCE_NEGATIVE,Float.parseFloat(String.valueOf(dtolerance)));
//                contentValues.put(Constants.PRODUCTION_TAG_UCL_GM,Float.parseFloat(String.valueOf(ucl)));
//                contentValues.put(Constants.PRODUCTION_TAG_LCL_GM,Float.parseFloat(String.valueOf(lcl)));
//                contentValues.put(Constants.PRODUCTION_TAG_MODIFIED_DATE,currentDateandTime);
//                contentValues.put(Constants.PRODUCTION_TAG_OPERATOR_NAME,ed_prod_operator_name.getText().toString().trim());
//                contentValues.put(Constants.PRODUCTION_TAG_IS_SYNC, 0);
//
//
//                long result = db.insert(Constants.PRODUCTION_TAG_TABLE, null, contentValues);
//
//                if (result == -1) {
//                    Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                    //wakeup();
//                }
//            }
//            else if (sp_uom_str.equals("Kg"))
//            {
//                Double  ucl = Double.parseDouble(gmtokgcalvatue) + Double.parseDouble(str_positive_tolerance);
//                Double lcl = Double.parseDouble(gmtokgcalvatue) - Double.parseDouble(str_negative_tolerance);
//                uclgm = String.valueOf(ucl);
//                lclgm = String.valueOf(lcl);
//                Double dgmperpouch = Double.valueOf(ed_prod_gm_pouch.getText().toString());
//                Double dtolerance = Double.valueOf(str_positive_tolerance);
//                Double dntolerance = Double.valueOf(str_negative_tolerance);
//                Double dtotalqty = Double.valueOf(ed_production_total_qty.getText().toString());
//                Double gmtotalqty = dtotalqty * dintkgvalue;
//                Double dkgperpouch = dgmperpouch *dintkgvalue;
//
//
//                contentValues.put(Constants.PRODUCTION_TAG_ID,1);
//
//                contentValues.put(Constants.PRODUCTION_TAG, resToMobile);
//                contentValues.put(Constants.PRODUCTION_TAG_OEM_ID, 1234);
//                contentValues.put(Constants.PRODUCTION_TAG_MAC_ID, dataloggerMacId);
//                contentValues.put(Constants.PRODUCTION_TAG_BATCH_ID,str_batch_id);
//                contentValues.put(Constants.PRODUCTION_TAG_PRODUCT_ID, sp_product_string.trim());
//                contentValues.put(Constants.PRODUCTION_TAG_GM_PER_POUCH,Float.parseFloat(String.valueOf(dkgperpouch)));
//                contentValues.put(Constants.PRODUCTION_TAG_UNIT, sp_uom_str);
//                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY,Float.parseFloat(String.valueOf(gmtotalqty)));
//                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY_UOM, sp_uom_str);
//                if (str_negative_weight_allowed.equals("Yes"))
//                {
//                    contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED, 1);
//
//                }
//                else
//                {
//                    contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED, 0);
//
//                }
//                contentValues.put(Constants.PRODUCTION_TAG_SHIFT, str_shifts);
//                contentValues.put(Constants.PRODUCTION_TAG_NO_OF_POUCHES, Integer.parseInt(ed_no_of_pouches.getText().toString()));
//                contentValues.put(Constants.PRODUCTION_TAG_SHIFT, str_shifts);
//                contentValues.put(Constants.PRODUCTION_TAG_SESSION_POSITVIE_TOLERANCE,Float.parseFloat(String.valueOf(dtolerance)));
//                contentValues.put(Constants.PRODUCTION_TAG_SESSION_NEAGTIVE_TOLERANCE,Float.parseFloat(String.valueOf(dtolerance)));
//                contentValues.put(Constants.PRODUCTION_TAG_MASTER_TOLERANCE_POSITIVE,Float.parseFloat(String.valueOf(dtolerance)));
//                contentValues.put(Constants.PRODUCTION_TAG_MASTER_TOLERANCE_NEGATIVE,Float.parseFloat(String.valueOf(dtolerance)));
//                contentValues.put(Constants.PRODUCTION_TAG_UCL_GM,Float.parseFloat(String.valueOf(ucl)));
//                contentValues.put(Constants.PRODUCTION_TAG_LCL_GM,Float.parseFloat(String.valueOf(lcl)));
//                contentValues.put(Constants.PRODUCTION_TAG_MODIFIED_DATE,currentDateandTime);
//                contentValues.put(Constants.PRODUCTION_TAG_OPERATOR_NAME,ed_prod_operator_name.getText().toString().trim());
//                contentValues.put(Constants.PRODUCTION_TAG_IS_SYNC, 0);
//
//                long result = db.insert(Constants.PRODUCTION_TAG_TABLE, null, contentValues);
//
//                if (result == -1) {
//                    Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                    //wakeup();
//                }
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//        }
//
//        @Override
//        public void afterTextChanged(Editable editable) {
//            if (editable != null && !editable.toString().equalsIgnoreCase("")){
//
//                if (rstring.equals("pouch")){
//                    ed_production_total_qty.removeTextChangedListener(textWatcher);
//                    String noOfPouches = editable.toString().trim();
//                    String gmPerPouch = ed_prod_gm_pouch.getText().toString().trim();
//
//                    ed_no_of_pouches.removeTextChangedListener(textWatcher);
//                    if (gmPerPouch.equals("") || noOfPouches.equals("")) {
//                        ed_production_total_qty.setText("");
//                    } else {
//                        if (sp_uom_str.equals("gm"))
//                        {
//                            int totalQty = Integer.parseInt(gmPerPouch) * Integer.parseInt(noOfPouches);
//                            strtotalqty = String.valueOf(totalQty);
//                            Double dtotalqty = Double.valueOf(strtotalqty);
//                            ed_production_total_qty.setText(decimalFormat.format(dtotalqty));
//                        }
//                        else if (sp_uom_str.equals("Kg"))
//                        {
//                            int totalQty = Integer.parseInt(gmtokgcalvatue) * Integer.parseInt(noOfPouches);
//                            strtotalqty = String.valueOf(totalQty);
//                            String strintvalue = String.valueOf(intkgvalue);
//                            Double dintvalue = Double.valueOf(strintvalue);
//                            Double dtotalqty = Double.valueOf(strtotalqty);
//
//                            //int kgtotalqty = totalQty / intkgvalue;
//                            Double dkgtotalqty = dtotalqty / dintvalue;
//                            String strkgtotalqty = String.valueOf(dkgtotalqty);
//                            Double dkgcaltotalqty = Double.valueOf(strkgtotalqty);
//
//                            ed_production_total_qty.setText(decimalFormat.format(dkgcaltotalqty));
//                        }
//                    }
//                    ed_no_of_pouches.addTextChangedListener(textWatcher);
//                }
//
//                else if (rstring.equals("totalqty")){
//
//                    ed_no_of_pouches.removeTextChangedListener(textWatcher);
//                    String totalQty = editable.toString().trim();
//                    String gmPerPouch = ed_prod_gm_pouch.getText().toString().trim();
//                    ed_production_total_qty.removeTextChangedListener(textWatcher);
//                    if (totalQty.equals("") || gmPerPouch.equals("")){
//                        ed_no_of_pouches.setText("");
//                        ed_prod_gm_pouch.setText("");
//                    }else {
//                        int noOfPouches = Integer.parseInt(totalQty) / Integer.parseInt(gmPerPouch);
//                        ed_no_of_pouches.setText(noOfPouches+"");
//                    }
//                    ed_production_total_qty.addTextChangedListener(textWatcher);
//                }
//            }
//
//
//        }
//    };
//
//
//    //Production Tag Save
//    private String SaveTagInfo(String msg) {
//        String resFromDataLogger = "";
//
//        GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.registerTypeAdapter(MobileJsonData.class, new MobileJsonDataDeserializer());
//
//        Gson gson = gsonBuilder.create();
//
//        MobileJsonData outputObj = gson.fromJson(msg, MobileJsonData.class);
//
//        Tag tagTest = outputObj.tagInfo;
//        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
//        String dataLoggerName = outputObj.dataLoggerName;
//        String dataLoggerIP = outputObj.dataLoggerIP;
//
//        // get current date time with Calendar()
//        Calendar cal = Calendar.getInstance();
//        String oiNum = tagTest.getOiNum();
//        String wt = tagTest.getTargetWt();
//        String date = dateFormat.format(cal.getTime());
//
//        String tagUniqueStr = dataLoggerName + ":" + tagTest.getOiNum() + ":" + tagTest.getTargetWt() + ":" + dateFormat.format(cal.getTime());tagTest.tagid = tagUniqueStr.trim();
//        tagTest.tagid = tagUniqueStr.replaceAll(" ", "");
//
//        TestRun testRun = new TestRun();
//        testRun.head = tagTest.head;
//        testRun.oinum = tagTest.oiNum;
//        testRun.targetwt = tagTest.targetWt;
//        testRun.tagid = tagTest.tagid;
//
//        ArrayList<TestRun> existingTestRunObjs = new ArrayList<TestRun>();
//
//        try{
//            deleteTestRunCompleted();
//            existingTestRunObjs = getRunningTestInfo();
//
//            String testStatus = getTestRunAllowedStatus(testRun, existingTestRunObjs);
//            if(testStatus != null && testStatus.equals(Constants.TEST_APPEAL)){
//                insertIntoTestRunsTbl(testRun);
//                System.out.println("In test run appeal sent for ");
//                //return HttpResponse.status(200).entity(Constants.TEST_APPEAL).build();
//                return Constants.TEST_APPEAL;
//            }
//            if(testStatus != null && testStatus.equals(Constants.TEST_NOT_ALLOWED)){
//                //return HttpResponse.status(200).entity(Constants.TEST_NOT_ALLOWED).build();
//                return Constants.TEST_NOT_ALLOWED;
//            }
//        }catch(SQLException sqlEx){
//            sqlEx.printStackTrace();
//            Log.e("", "Exception occured and the stack trace is  "+ sqlEx.getStackTrace().toString(), sqlEx);
//            //return HttpResponse.status(200).entity("").build();
//            return "";
//
//        }
//        resToMobile = submitTagInputs(msg);
//        // return HttpResponse.status(200).entity(resToMobile).build();
//        return resToMobile;
//    }
//
//    public void deleteTestRunCompleted() throws SQLException {
//        Connection conn = null;
//        try {
//
//            conn = getDbConnection();
//            conn.setAutoCommit(false);
//            PreparedStatement deleteTestRunStat = conn.prepareStatement("delete from test_run_status where status=? or status=?");
//            deleteTestRunStat.setString(1, "COMPLETED");
//            deleteTestRunStat.setString(2, "ABORTED");
//
//            deleteTestRunStat.executeUpdate();
//            conn.commit();
//            deleteTestRunStat.close();
//
//        } catch (Exception ne) {
//            ne.printStackTrace();
//            System.out.println("Exception is " + ne.getCause().getMessage());
//            conn.rollback();
//
//        } finally {
//
//            if (conn != null) {
//                conn.close();
//            }
//            conn = null;
//        }
//    }
//
//    public final Connection getDbConnection() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//        Connection conn = null;
//        Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
//        conn = DriverManager.getConnection(dbUrl,dbUserName,dbPassword);
//        if (conn !=null){
//            Log.d(TAG, "getDbConnection: "+"Database is connected to database");
//        }else if (conn == null){
//            Log.d(TAG, "getDbConnection: "+"Database is not connected to database");
//        }
//        return conn;
//    }
//
//    public ArrayList<TestRun> getRunningTestInfo() throws SQLException{
//
//        Connection conn = null;
//        ArrayList<TestRun> testRuns = new ArrayList<TestRun>();
//
//        try{
//            conn = getDbConnection();
//            PreparedStatement getTestRunStat = conn.prepareStatement(QUERY_GET_TEST_RUNS_IN_PROGRESS);
//            ResultSet results =  getTestRunStat.executeQuery();
//            while (results.next()) {
//                TestRun testRunObj = new TestRun();
//                testRunObj.tagid = results.getString("tagid");
//                testRunObj.head = results.getString("head");
//                testRunObj.oinum = results.getString("oinum");
//                testRunObj.targetwt = results.getString("targetwt");
//                testRunObj.status = results.getString("status");
//                testRunObj.isAuthorized = results.getString("isauthorised");
//                testRuns.add(testRunObj);
//            }
//            results.close();
//            getTestRunStat.close();
//
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//            conn = null;
//        }
//        return testRuns;
//    }
//
//    private String getTestRunAllowedStatus(TestRun testRunObj, ArrayList<TestRun> existingTestRuns) {
//        String status = Constants.TEST_ALLOWED;
//        if(existingTestRuns != null && existingTestRuns.size() > 0){
//
//            for(TestRun existingTestRun : existingTestRuns){
//
//                if((existingTestRun.oinum.equals(testRunObj.oinum)) &&
//                        (existingTestRun.targetwt.equals(testRunObj.targetwt))){
//
//                    if(existingTestRun.head.equals(testRunObj.head)){
//                        status = Constants.TEST_NOT_ALLOWED;
//                    }
//
//                    if(existingTestRun.head != null && existingTestRun.head.equals("Single")){
//                        if(testRunObj.head != null && !testRunObj.head.equals("Single")){
//                            status = Constants.TEST_NOT_ALLOWED;
//                        }
//                    }
//                    if(existingTestRun.head != null && !existingTestRun.head.equals("Single")){
//                        if(testRunObj.head != null && testRunObj.head.equals("Single")){
//                            status = Constants.TEST_NOT_ALLOWED;
//                        }
//                    }
//
//                }
//
//                if((existingTestRun.oinum.equals(testRunObj.oinum)) &&
//                        (!existingTestRun.targetwt.equals(testRunObj.targetwt))){
//
//                    if(existingTestRun.head.equals(testRunObj.head)){
//                        status = Constants.TEST_APPEAL;
//                    }
//
//                    if(existingTestRun.head != null && existingTestRun.head.equals("Single")){
//                        if(testRunObj.head != null && !testRunObj.head.equals("Single")){
//                            status = Constants.TEST_NOT_ALLOWED;
//                        }
//                    }
//                    if(existingTestRun.head != null && !existingTestRun.head.equals("Single")){
//                        if(testRunObj.head != null && testRunObj.head.equals("Single")){
//                            status = Constants.TEST_NOT_ALLOWED;
//                        }
//                    }
//                }
//            }
//        }
//        return status;
//    }
//
//    private String submitTagInputs(String tagInfo) {
//        Log.d(TAG, "Submit appealed tags called and message is  "+ tagInfo);
//        String resFromDataLogger = "";
//        String resToMobile = "";
//
//        GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.registerTypeAdapter(MobileJsonData.class,
//                new MobileJsonDataDeserializer());
//        Gson gson = gsonBuilder.create();
//        MobileJsonData outputObj = gson.fromJson(tagInfo, MobileJsonData.class);
//
//        Tag tagTest = outputObj.tagInfo;
//        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
//        String dataLoggerName = outputObj.dataLoggerName;
//        String dataLoggerIP = outputObj.dataLoggerIP;
//        //dataLoggerIP = "localhost";
//        // get current date time with Calendar()
//        Calendar cal = Calendar.getInstance();
//
//        String tagUniqueStr = dataLoggerName + ":" + tagTest.getOiNum() + ":" + tagTest.getTargetWt() + ":" + dateFormat.format(cal.getTime());
////        String tagUniqueStr = tagID;
//        tagTest.tagid = tagUniqueStr.trim();
//        tagTest.tagid = tagUniqueStr.replaceAll(" ", "");
//        TestRun testRunObj = new TestRun();
//        testRunObj.head = tagTest.head;
//        testRunObj.oinum = tagTest.oiNum;
//        testRunObj.targetwt = tagTest.targetWt;
//        testRunObj.status = Constants.RUN_TEST_INPROGRESS_STATUS;
//        testRunObj.tagid = tagTest.tagid;
//        testRunObj.isAuthorized = "Yes";
//
//
//        HttpClient httpClient = new DefaultHttpClient();
//        try {
//            insertIntoTestRunsTbl(testRunObj);
//            String url = "http://"+dataLoggerIP+":"+DATA_LOGGER_PORT+"/wakeup?tagid="+tagUniqueStr.trim()+ "&dataLoggerName=" + dataLoggerName;
////            String url = "http://"+dataLoggerIP+"/wakeup?tagid="+tagUniqueStr.trim()+ "&dataLoggerName=" + dataLoggerName;
//            HttpResponse response1 = httpClient.execute(new HttpGet(url));
//            StatusLine statusLine = response1.getStatusLine();
//            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
//                ByteArrayOutputStream out = new ByteArrayOutputStream();
//                response1.getEntity().writeTo(out);
//                resFromDataLogger = out.toString();
//                Toast.makeText(this, resFromDataLogger, Toast.LENGTH_SHORT).show();
//                out.close();
//            } else{
//                response1.getEntity().getContent().close();
//                throw new IOException(statusLine.getReasonPhrase());
//            }
//        } catch (IOException exception) {
//            exception.printStackTrace();
//        } catch (SQLException exception) {
//            exception.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//            //log.error("Exception occured and the stack trace is  "+ e.getStackTrace().toString(), e);
//        } finally {
//            httpClient.getConnectionManager().shutdown();
//        }
//        try {
//            if (resFromDataLogger != null && resFromDataLogger.contains("ok")) {
//                //log.debug("Got positive response from data logger, inserting record in database");
//
//                insertIntoTagsTbl(tagTest);
//                resToMobile = tagTest.tagid;
//                changeDataLoggerStatus(dataLoggerName, "Yes");
//            } else {
//                resToMobile = "DataLoggerNotReady";
//            }
//        } catch (Exception ex) {
//            System.out.println("The exception occured and the exception is  " + ex.getMessage());
//            //log.error("Exception occured and exception is ", ex);
//            resToMobile = "";
//        }
//        return resToMobile;
//    }
//
//    private void insertIntoTestRunsTbl(TestRun testRunObj) throws SQLException {
//        if (testRunObj != null) {
//            Connection conn = null;
//            try {
//
//                conn = getDbConnection();
//                conn.setAutoCommit(false);
//                PreparedStatement testRunInsertStat = conn.prepareStatement("Insert into test_run_status (tagid, " + "oinum, targetwt, head, status, isauthorised) values (?,?,?,?,?,?)");
//                testRunInsertStat.setString(1, testRunObj.tagid);
//                testRunInsertStat.setString(2,  testRunObj.oinum);
//                testRunInsertStat.setString(3,  testRunObj.targetwt);
//                testRunInsertStat.setString(4,  testRunObj.head);
//                testRunInsertStat.setString(5,  testRunObj.status);
//                testRunInsertStat.setString(6,  testRunObj.isAuthorized);
//                testRunInsertStat.executeUpdate();
//                conn.commit();
//                testRunInsertStat.close();
//
//            } catch (Exception ne) {
//                ne.printStackTrace();
//                System.out.println("Exception is " + ne);
//                conn.rollback();
//
//            } finally {
//
//                if (conn != null) {
//                    conn.close();
//                }
//                conn = null;
//            }
//
//        }
//    }
//
//    private void insertIntoTagsTbl(Tag tagObj) throws SQLException {
//        if (tagObj != null) {
//            Connection conn = null;
//            try {
//
//                conn = getDbConnection();
//                System.out.println("Got the connection");
//                conn.setAutoCommit(false);
//                PreparedStatement tagInsertStat = conn.prepareStatement(Constants.QUERY_INSERT_TAGTBL);
//                tagInsertStat.setString(1, tagObj.tagid);
//                tagInsertStat.setString(2, tagObj.oiNum);
//                tagInsertStat.setString(3, tagObj.targetWt);
//                tagInsertStat.setString(4, tagObj.fillerType);
//                tagInsertStat.setString(5, tagObj.customer);
//                tagInsertStat.setString(6, tagObj.machineType);
//                tagInsertStat.setString(7, tagObj.productType);
//                tagInsertStat.setString(8, tagObj.product);
//                tagInsertStat.setString(9, tagObj.operator);
//                tagInsertStat.setString(10, tagObj.targetWtUnit);
//                tagInsertStat.setString(11, tagObj.inspector);
//                tagInsertStat.setString(12, tagObj.filmType);
//                tagInsertStat.setString(13, tagObj.productfeed);
//                tagInsertStat.setString(14, tagObj.pouchWidth);
//                tagInsertStat.setString(15, tagObj.pouchLength);
//                tagInsertStat.setString(16, tagObj.agitatorMotorFreq);
//                tagInsertStat.setString(17, tagObj.agitatorMode);
//                tagInsertStat.setString(18, tagObj.agitatorod);
//                tagInsertStat.setString(19, tagObj.agitatorid);
//                tagInsertStat.setString(20, tagObj.agitatorPitch);
//                tagInsertStat.setString(21, tagObj.targetacctype);
//                tagInsertStat.setString(22, tagObj.targetaccval);
//                tagInsertStat.setString(23, tagObj.mcSerialNum);
//                tagInsertStat.setString(24, tagObj.prBulkMain);
//                tagInsertStat.setString(25, tagObj.targetSpeed);
//                tagInsertStat.setString(26, tagObj.customerProduct);
//                tagInsertStat.setString(27, tagObj.head);
//                tagInsertStat.setString(28, tagObj.airPressure);
//                tagInsertStat.setString(29, tagObj.qtySetting);
//                tagInsertStat.setString(30, tagObj.bgLenSetting);
//                tagInsertStat.setString(31, tagObj.fillValveGap);
//                tagInsertStat.setString(32, tagObj.fillVfdFreq);
//                tagInsertStat.setString(33, tagObj.horizCutSealSetting);
//                tagInsertStat.setString(34, tagObj.horizBandSealSetting);
//                tagInsertStat.setString(35, tagObj.vertSealSetting);
//                tagInsertStat.setString(36, tagObj.augerAgitatorFreq);
//                tagInsertStat.setString(37, tagObj.augerFillValve);
//                tagInsertStat.setString(38, tagObj.augurPitch);
//                tagInsertStat.setString(39, tagObj.pouchSealType);
//                tagInsertStat.setString(40, tagObj.pouchTempHZF);
//                tagInsertStat.setString(41, tagObj.pouchTempHZB);
//                tagInsertStat.setString(42, tagObj.pouchTempVert);
//                tagInsertStat.setString(43, tagObj.pouchTempVertFrontLeft);
//                tagInsertStat.setString(44, tagObj.pouchTempVertFrontRight);
//                tagInsertStat.setString(45, tagObj.pouchTempVertBackLeft);
//                tagInsertStat.setString(46, tagObj.pouchTempVertBackRight);
//                tagInsertStat.setString(47, tagObj.pouchGusset);
//                tagInsertStat.setString(48, tagObj.customerFilm);
//                tagInsertStat.setString(49, tagObj.pouchFilmThickness);
//                tagInsertStat.setString(50, tagObj.pouchPrBulk);
//                tagInsertStat.setString(51, tagObj.pouchEqHorizSerrations);
//                tagInsertStat.setString(52, tagObj.pouchFillEvent);
//                tagInsertStat.setString(53, tagObj.negWt);
//                tagInsertStat.setString(54, tagObj.fyyear);
//                System.out.println("The created date formed is "+ new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
//                tagInsertStat.setDate(55, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
//
//                tagInsertStat.executeUpdate();
//                conn.commit();
//
//            } catch (Exception ne) {
//                ne.printStackTrace();
//                System.out.println("Exception is " + ne.getCause().getMessage());
//                conn.rollback();
//
//            } finally {
//
//                if (conn != null) {
//                    conn.close();
//                }
//                conn = null;
//            }
//
//        }
//    }
//
//    private void changeDataLoggerStatus(String dataLoggerName, String inuseStatus){
//        Connection conn = null;
//        try {
//            conn = getDbConnection();
//            conn.setAutoCommit(false);
//            PreparedStatement updateDlStat = conn.prepareStatement("update mstr_logger_tbl set inuse=? where logger_name=? and isactive=1 and isdeleted=0");
//            updateDlStat.setString(1,  inuseStatus);
//            updateDlStat.setString(2, dataLoggerName);
//
//            updateDlStat.executeUpdate();
//            conn.commit();
//            updateDlStat.close();
//
//        }  catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } finally {
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//            }
//            conn = null;
//        }
//    }

    private static final String TAG = "AddNewBatchActivity";
    ImageButton btnStart;
    Spinner spin_production_product,spin_production_uom,spin_production_uom_for_totalqty,spin_production_shifts,spin_production_operator_name;
    Spinner spin_production_negative_weight;
    EditText ed_prod_gm_pouch,ed_production_total_qty,ed_prod_operator_name;
    TextView txt_production_date;
    private ArrayList<String> product_list = new ArrayList<>();
    private ArrayList<String> uomList = new ArrayList<>();
    ArrayList<String> totaluomList = new ArrayList<>();
    private ArrayList<String> shiftlist = new ArrayList<>();
    private ArrayList<String> negative_list = new ArrayList<>();
    private ArrayList<String> grammage_values_list = new ArrayList<>();
    private ArrayList<Production_Tolerance_Model> productionTolerance_list = new ArrayList<>();
    DataBaseHelper helper;
    String dataloggerName,dataloggerIp,dataloggerMacId;
    public static final String MQTT_BROKER = "tcp://192.168.10.67:1883";
    String tagInfoStr="";
    String tagID = "",rstring,gmtokgcalvatue;
    private String sp_product_string,sp_uom_str,str_shifts,str_positive_tolerance,str_negative_tolerance,uclgm,lclgm,str_negative_weight_allowed,str_batch_id,strtotalqty;
    EditText ed_no_of_pouches;
    TextView ed_positive_tolerance,ed_negative_tolerance,txt_add_new_batch_uom_total_qty;
    DecimalFormat decimalFormat = new DecimalFormat("0.000");
    RadioButton rd_no_of_pouches,rd_total_qty;
    RadioGroup radio_group;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    String currentDateandTime = sdf.format(new Date());
    int intkgvalue = 0;
    TextView txtGm;
    double dintkgvalue =0.0;
    private String dbName;
    private SQLiteDatabase db;
    private String resToMobile;

    private String dbIpAddress = "192.168.10.67";
    private String dbUserName = "test2";
    private String dbPassword = "test12345";
    private String dbDatabaseName = "decitalpool";
    private String serverport = "1433";
    private String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
    private int DATA_LOGGER_PORT = 8080;
    public static final String QUERY_GET_TEST_RUNS_IN_PROGRESS = "select * from test_run_status where status='" + Constants.RUN_TEST_INPROGRESS_STATUS + "'";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_batch);
        dbName = AppPreferences.getDatabseName(this);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
        initUi();
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH-mm");
        masterProductionTolerance();
        getProductionGrammageValues();
        final Calendar cal = Calendar.getInstance();
        txt_production_date.setText(dateFormat.format(cal.getTime()));

        final String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        if (rd_no_of_pouches.isChecked())
        {
            rstring = "pouch";
            ed_production_total_qty.setEnabled(false);
            ed_production_total_qty.setBackground(getResources().getDrawable(R.drawable.read_only_background));
            ed_no_of_pouches.setBackground(getResources().getDrawable(R.drawable.table_background));
            ed_production_total_qty.getText().clear();
            ed_no_of_pouches.getText().clear();
            ed_no_of_pouches.setEnabled(true);

            ed_production_total_qty.removeTextChangedListener(textWatcher);
            ed_no_of_pouches.addTextChangedListener(textWatcher);
        }

        ed_prod_gm_pouch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {

                if (s != null && !s.toString().equalsIgnoreCase(""))
                {
                    String gm_pouch = s.toString();
                    if (sp_uom_str.equals("gm"))
                    {
                        if (gm_pouch.equals("") || gm_pouch.equals("")){
                            ed_prod_gm_pouch.setText("");
                        }else {

                            for (int i = 0;i<productionTolerance_list.size();i++)
                            {
                                String grammage = productionTolerance_list.get(i).getGrammage();
                                if (gm_pouch.equals(grammage))
                                {

                                    String strpositive = productionTolerance_list.get(i).getTolerancePositive();
                                    String strnegative = productionTolerance_list.get(i).getToleranceNegetive();
                                    Double dcalpositivetoleance = Double.parseDouble(strpositive);
                                    Double dcalnegativetoleance = Double.parseDouble(strnegative);

                                    str_positive_tolerance = strpositive;
                                    str_negative_tolerance = strnegative;
                                    ed_positive_tolerance.setText(decimalFormat.format(dcalpositivetoleance));
                                    ed_negative_tolerance.setText(decimalFormat.format(dcalnegativetoleance));
                                }
                            }
                        }
                    }
                    else if (sp_uom_str.equals("Kg"))
                    {
                        if (gm_pouch.equals("") || gm_pouch.equals("")){
                            ed_prod_gm_pouch.setText("");

                        }else {
                            String strkgvalue = String.valueOf(intkgvalue);
                            Double gmvalue = Double.parseDouble(gm_pouch) * Double.parseDouble(strkgvalue);
                            gmtokgcalvatue = String.valueOf(gmvalue);


                            gmtokgcalvatue = gmtokgcalvatue.substring(0,gmtokgcalvatue.length()-2);
                            for (int i = 0;i<productionTolerance_list.size();i++)
                            {
                                String grammage = productionTolerance_list.get(i).getGrammage();
                                if (gmtokgcalvatue.equals(grammage))
                                {
                                    String strpositive = productionTolerance_list.get(i).getTolerancePositive();
                                    String strnegative = productionTolerance_list.get(i).getToleranceNegetive();


                                    Double dcalpositivetoleance = Double.parseDouble(strpositive);
                                    Double dcalnegativetoleance = Double.parseDouble(strnegative);

                                    str_positive_tolerance = String.valueOf(strpositive);
                                    str_negative_tolerance = String.valueOf(strpositive);

                                    ed_positive_tolerance.setText(String.valueOf(decimalFormat.format(dcalpositivetoleance)));
                                    ed_negative_tolerance.setText(String.valueOf(decimalFormat.format(dcalnegativetoleance)));
                                }
                            }
                        }
                    }
                }
                else
                {
                    ed_positive_tolerance.setText("");
                    ed_negative_tolerance.setText("");
                }


            }
        });

        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId==R.id.rd_no_of_pouches)
                {
                    rstring = "pouch";
                    ed_production_total_qty.setEnabled(false);
                    ed_production_total_qty.setBackground(getResources().getDrawable(R.drawable.read_only_background));
                    ed_no_of_pouches.setBackground(getResources().getDrawable(R.drawable.table_background));
                    ed_production_total_qty.getText().clear();
                    ed_no_of_pouches.getText().clear();
                    ed_no_of_pouches.setEnabled(true);

                    ed_production_total_qty.removeTextChangedListener(textWatcher);
                    ed_no_of_pouches.addTextChangedListener(textWatcher);
                }
                else if (checkedId == R.id.rd_total_qty)
                {
                    rstring = "totalqty";
                    ed_no_of_pouches.setEnabled(false);
                    ed_no_of_pouches.setBackground(getResources().getDrawable(R.drawable.read_only_background));
                    ed_production_total_qty.setBackground(getResources().getDrawable(R.drawable.table_background));
                    ed_production_total_qty.setEnabled(true);

                    ed_no_of_pouches.getText().clear();
                    ed_production_total_qty.getText().clear();
                    ed_no_of_pouches.removeTextChangedListener(textWatcher);
                    ed_production_total_qty.addTextChangedListener(textWatcher);
                }

            }


        });
        spin_production_product.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sp_product_string = spin_production_product.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spin_production_shifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                str_shifts = spin_production_shifts.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spin_production_uom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sp_uom_str = spin_production_uom.getSelectedItem().toString();

                String gm_pouch = ed_prod_gm_pouch.getText().toString();
                for (int i = 0;i<productionTolerance_list.size();i++)
                {
                    String grammage = productionTolerance_list.get(i).getGrammage();
                    if (gm_pouch.equals(grammage))
                    {
                        String strpositive = productionTolerance_list.get(i).getTolerancePositive();
                        String strnegative = productionTolerance_list.get(i).getToleranceNegetive();
                        str_positive_tolerance = strpositive;
                        str_negative_tolerance = strnegative;

                        ed_positive_tolerance.setText(str_positive_tolerance);
                        ed_negative_tolerance.setText(str_negative_tolerance);
                    }
                }

                if (sp_uom_str.equals("Kg")) {
                    String kgvalue = grammage_values_list.get(0);
                    intkgvalue = Integer.parseInt(kgvalue);
                    dintkgvalue = Double.parseDouble(kgvalue);
                    txtGm.setText("Kg/pouch : ");
                }
                else {
                    txtGm.setText("Gm/pouch : ");
                }
                txt_add_new_batch_uom_total_qty.setText(sp_uom_str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spin_production_negative_weight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                str_negative_weight_allowed = spin_production_negative_weight.getSelectedItem().toString();
                if (str_negative_weight_allowed.equals("No"))
                {
                    ed_negative_tolerance.setText("0");
                }
                else
                {
                    ed_negative_tolerance.setText(str_negative_tolerance);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                // get current date time with Calendar()
                if (sp_uom_str.equals("Select")) {
                    Toast.makeText(AddNewBatchActivity.this, "Please select UOM", Toast.LENGTH_SHORT).show(); }
                else if (TextUtils.isEmpty(ed_production_total_qty.getText().toString())) {
                    Toast.makeText(AddNewBatchActivity.this, "Please enter total qty", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(ed_no_of_pouches.getText().toString())) {
                    Toast.makeText(AddNewBatchActivity.this, "Please enter number of pouch", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(ed_production_total_qty.getText().toString())){
                    Toast.makeText(AddNewBatchActivity.this, "Please enter total quantity", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(ed_prod_operator_name.getText().toString())) {
                    Toast.makeText(AddNewBatchActivity.this, "Please enter operator", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(sp_product_string)) {
                    Toast.makeText(AddNewBatchActivity.this, "Please select product", Toast.LENGTH_SHORT).show();
                }
                else if (TextUtils.isEmpty(ed_prod_gm_pouch.getText().toString())){
                    Toast.makeText(AddNewBatchActivity.this, "Please enter gm per pouch", Toast.LENGTH_SHORT).show(); }
                else
                {
                    int profile_counts = helper.getproductiontagCount();
                    helper.close();
                    int count = profile_counts + 1;

                    str_batch_id = "B".concat(m_androidId + "D" +dateFormat.format(cal.getTime()) +"C" +count + "");


                    String msg = "{\"dataLoggerIP\" : \"192.168.10.69\",  \"dataLoggerName\": \"DiA0AA01\",\"macaddress\" : \"192.168.1.102\",  \"inspector\": \"inspector \",   \"tag\":{\"oiNum\" :\"so1561745\",  \"targetWt\" :\"20000\" ,  \"fyyear\" : \"2020-2021\" ,  \"fillerType\" : \"Other\" ,  \"customer\" : \"assdf\", \"machinetype\" : \"Sprint 250\", \"producttype\" : \"Solid\", \"product\" : \"Atta\", \"operator\" : \"xyz\", \"targetWtUnit\" : \"gm\", \"inspector\" : \"inspector\", \"filmtype\" : \"Printed\", \"productfeed\" : \"System\", \"pouchWidth\" : \"100\", \"pouchLength\" : \"50\", \"agitatorMotorFreq\" : \"\", \"agitatormode\" : \"\", \"agitatorod\" : \"\", \"agitatorid\" : \"\", \"agitatorpitch\" : \"\", \"targetacctype\" : \"+/- GM\", \"targetaccval\" : \"0.65\", \"mcserialnum\" : \"1223\", \"prBulkMain\" : \"0.25\", \"targetspeed\" : \"100\", \"customerProduct\" : \"Yes\", \"head\" : \"Right\", \"airPressure\" : \"\", \"qtysetting\" : \"\", \"bglensetting\" : \"\", \"fillValveGap\" : \"\", \"fillvfdFreq\" : \"\", \"horizCutSealSetting\" : \"\", \"horizBandSealSetting\" : \"\", \"vertSealSetting\" : \"\", \"augerAgitatorFreq\" : \"\", \"augerFillValve\" : \"null\", \"augurPitch\" : \"\", \"pouchSealType\" : \"Penta\", \"pouchTempHZF\" : \"\", \"pouchTempHZB\" : \"\", \"pouchTempVert\" : \"\", \"pouchTempVertFrontLeft\" : \"\", \"pouchTempVertFrontRight\" : \"\", \"pouchTempVertBackLeft\" : \"\", \"pouchTempVertBackRight\" : \"\", \"pouchGusset\" : \"\", \"customerFilm\" : \"Yes\", \"pouchFilmThickness\" : \"na\", \"pouchPrBulk\" : \"\", \"pouchEqHorizSerrations\" : \"\", \"pouchFillEvent\" : \"Jaw\", \"negWt\" : \"No\"}}";

                    SaveTagInfo(msg);
                    if (sp_uom_str.equals("gm"))
                    {
                        int  ucl = Integer.parseInt(ed_prod_gm_pouch.getText().toString()) + Integer.parseInt(str_positive_tolerance);
                        int lcl = Integer.parseInt(ed_prod_gm_pouch.getText().toString()) - Integer.parseInt(str_negative_tolerance);
                        uclgm = String.valueOf(ucl);
                        lclgm = String.valueOf(lcl);
                        double duclgm = Double.parseDouble(uclgm);
                        double dlclgm = Double.parseDouble(lclgm);
                        Intent i = new Intent(AddNewBatchActivity.this, ProductionWeightReadingActivity.class);
                        i.putExtra("dataloggerName",dataloggerName);
                        i.putExtra("dataloggerIp",dataloggerIp);
                        i.putExtra("current_tag_id",resToMobile);
                        i.putExtra("gmperpouch",ed_prod_gm_pouch.getText().toString());
                        i.putExtra("total_qty",strtotalqty);
                        i.putExtra("negative_tolerance",str_positive_tolerance);
                        i.putExtra("positive_tolerance",str_negative_tolerance);
                        i.putExtra("no_of_pouches",ed_no_of_pouches.getText().toString());
                        i.putExtra("batch_id",str_batch_id);
                        i.putExtra("select_date",currentDateandTime);
                        i.putExtra("product",sp_product_string.trim());
                        i.putExtra("ucl",uclgm);
                        i.putExtra("uom",sp_uom_str);
                        i.putExtra("lcl",lclgm);
                        i.putExtra("shift",str_shifts);
                        i.putExtra("mac_id",dataloggerMacId);
                        startActivity(i);
                        InsertTagIntoDb();
                    }
                    else if (sp_uom_str.equals("Kg"))
                    {
                        Double ucl = Double.parseDouble(gmtokgcalvatue) + Double.parseDouble(str_positive_tolerance);
                        Double lcl = Double.parseDouble(gmtokgcalvatue) - Double.parseDouble(str_negative_tolerance);

                        Double ducl = ucl /dintkgvalue;
                        Double dlcl = lcl/dintkgvalue;
                        Intent i = new Intent(AddNewBatchActivity.this, ProductionWeightReadingActivity.class);
                        i.putExtra("dataloggerName",dataloggerName);
                        i.putExtra("dataloggerIp",dataloggerIp);
                        i.putExtra("current_tag_id",tagID);
                        i.putExtra("gmperpouch",ed_prod_gm_pouch.getText().toString());
                        i.putExtra("total_qty",ed_production_total_qty.getText().toString());
                        i.putExtra("negative_tolerance",str_positive_tolerance);
                        i.putExtra("positive_tolerance",str_negative_tolerance);
                        i.putExtra("no_of_pouches",ed_no_of_pouches.getText().toString());
                        i.putExtra("batch_id","B0_1052020_16");
                        i.putExtra("select_date",currentDateandTime);
                        i.putExtra("product",sp_product_string.trim());
                        i.putExtra("ucl",decimalFormat.format(ducl));
                        i.putExtra("uom",sp_uom_str);
                        i.putExtra("lcl",decimalFormat.format(dlcl));
                        i.putExtra("shift",str_shifts);
                        i.putExtra("intkgvalue",String.valueOf(intkgvalue));
                        i.putExtra("mac_id",dataloggerMacId);
                        startActivity(i);
                        InsertTagIntoDb();
                    }
                }

            }
        });

    }

    private void initUi()
    {
        dataloggerName = getIntent().getStringExtra("LoggerName");
        dataloggerIp = getIntent().getStringExtra("LoggerIP");
        dataloggerMacId = getIntent().getStringExtra("mac_id");

        radio_group = findViewById(R.id.radio_group);
        rd_no_of_pouches = findViewById(R.id.rd_no_of_pouches);
        rd_total_qty = findViewById(R.id.rd_total_qty);
        txtGm = findViewById(R.id.txtGm);

        ed_prod_operator_name = findViewById(R.id.ed_prod_operator_name);
        btnStart = findViewById(R.id.btnStart);
        spin_production_product = findViewById(R.id.spin_production_product);
        spin_production_uom = findViewById(R.id.spin_production_uom);
        spin_production_shifts = findViewById(R.id.spin_production_shifts);
        spin_production_negative_weight = findViewById(R.id.spin_production_negative_weight);
        txt_add_new_batch_uom_total_qty = findViewById(R.id.txt_add_new_batch_uom_total_qty);

        ed_prod_gm_pouch = findViewById(R.id.ed_prod_gm_pouch);
        ed_production_total_qty = findViewById(R.id.ed_production_total_qty);
        ed_no_of_pouches =findViewById(R.id.ed_no_of_pouches);
        ed_positive_tolerance = findViewById(R.id.ed_positive_tolerance);
        ed_negative_tolerance = findViewById(R.id.ed_negative_tolerance);
        txt_production_date = findViewById(R.id.txt_production_date);

        getProductsUsedForTrial();
        ArrayAdapter<String> spProductUsdForTrialList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, product_list);
        spProductUsdForTrialList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spin_production_product.setAdapter(spProductUsdForTrialList);

        //Adding data to the list to show in UoM Spinner
        getUoMList();
        ArrayAdapter<String> spUoMList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, uomList);
        spUoMList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spin_production_uom.setAdapter(spUoMList);

      /*  ArrayAdapter<String> uom_for_totalqtylist = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, totaluomList);
        spUoMList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spin_production_uom_for_totalqty.setAdapter(uom_for_totalqtylist);*/

        shiftlist.add("1");
        shiftlist.add("2");
        shiftlist.add("3");

        ArrayAdapter<String> shift_arraylist = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, shiftlist);
        shift_arraylist.setDropDownViewResource(R.layout.item_spinner_latyout);
        spin_production_shifts.setAdapter(shift_arraylist);

        negative_list.add("Yes");
        negative_list.add("No");
        ArrayAdapter<String> negative_weight_allowed_list = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, negative_list);
        negative_weight_allowed_list.setDropDownViewResource(R.layout.item_spinner_latyout);
        spin_production_negative_weight.setAdapter(negative_weight_allowed_list);
    }

    private void getProductsUsedForTrial() {
        Cursor productName = helper.getProducts1(db);
        while (productName.moveToNext()){
            String product = productName.getString(0);
            product_list.add(product);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }

    }

    private void getProductionGrammageValues() {
        Cursor grammageValues = helper.getProductionGrammageValues1(db);
        while (grammageValues.moveToNext()){
            String kgid = grammageValues.getString(0);
            String kgname = grammageValues.getString(1);
            String kgvalues = grammageValues.getString(2);

            grammage_values_list.add(kgvalues);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }

    }

    private void masterProductionTolerance()
    {
        Cursor tolerancemaster = helper.getMasterProductionTolerance();
        while (tolerancemaster.moveToNext()){
            String tolerance_id = tolerancemaster.getString(0);
            int oem_id = tolerancemaster.getInt(1);
            String Grammage = tolerancemaster.getString(2);
            String PositiveTolerance = tolerancemaster.getString(3);
            String NegativeTolerance = tolerancemaster.getString(4);

            Production_Tolerance_Model model = new Production_Tolerance_Model();
            model.setProductionToleranceID(tolerance_id);
            model.setOEMID(oem_id);
            model.setGrammage(Grammage);
            model.setTolerancePositive(PositiveTolerance);
            model.setToleranceNegetive(NegativeTolerance);

            productionTolerance_list.add(model);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }
    }

    private void getUoMList(){
        uomList.add("Select");
        Cursor uom = helper.getUoMFromLocalDB1(db);
        while (uom.moveToNext()){
            String struom = uom.getString(0);
            if (struom.equals("gm"))
            {
                uomList.add(struom);
                totaluomList.add(struom);
            }
            else if (struom.equals("Kg"))
            {
                uomList.add(struom);

            }
            Log.d("TAG", "getFillerType: ");
        }
    }


    public void InsertTagIntoDb() {
        try {
//            SQLiteDatabase sqLiteDatabase = new DataBaseHelper(this).getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            if (sp_uom_str.equals("gm"))
            {
                Double  ucl = Double.parseDouble(ed_prod_gm_pouch.getText().toString()) + Double.parseDouble(str_positive_tolerance);
                Double lcl = Double.parseDouble(ed_prod_gm_pouch.getText().toString()) - Double.parseDouble(str_negative_tolerance);
                uclgm = String.valueOf(ucl);
                lclgm = String.valueOf(lcl);
                Double dgmperpouch = Double.valueOf(ed_prod_gm_pouch.getText().toString());
                Double dtolerance = Double.valueOf(str_positive_tolerance);
                Double dntolerance = Double.valueOf(str_negative_tolerance);

                contentValues.put(Constants.PRODUCTION_TAG_ID,1);
                contentValues.put(Constants.PRODUCTION_TAG, resToMobile);
                contentValues.put(Constants.PRODUCTION_TAG_OEM_ID, 1234);
                contentValues.put(Constants.PRODUCTION_TAG_MAC_ID, dataloggerMacId);
                contentValues.put(Constants.PRODUCTION_TAG_BATCH_ID,str_batch_id);
                contentValues.put(Constants.PRODUCTION_TAG_PRODUCT_ID, sp_product_string.trim());
                contentValues.put(Constants.PRODUCTION_TAG_GM_PER_POUCH,Float.parseFloat(String.valueOf(dgmperpouch)));
                contentValues.put(Constants.PRODUCTION_TAG_UNIT, sp_uom_str);
                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY, ed_production_total_qty.getText().toString());
                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY_UOM, sp_uom_str);
                contentValues.put(Constants.PRODUCTION_TAG_TARGET_ACCURACY_TYPE, "Gm");
                contentValues.put(Constants.PRODUCTION_TAG_TARGET_ACCURACY_VALUE, 0.5);
                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY_UOM, sp_uom_str);
                //contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED, str_negative_weight_allowed);
                if (str_negative_weight_allowed.equals("Yes"))
                { contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED,0);
                }
                else
                {
                    contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED,1);
                }

                contentValues.put(Constants.PRODUCTION_TAG_SHIFT, str_shifts);
                contentValues.put(Constants.PRODUCTION_TAG_NO_OF_POUCHES, Integer.parseInt(ed_no_of_pouches.getText().toString()));
                contentValues.put(Constants.PRODUCTION_TAG_SHIFT, str_shifts);
                contentValues.put(Constants.PRODUCTION_TAG_SESSION_POSITVIE_TOLERANCE,Float.parseFloat(String.valueOf(dtolerance)));
                contentValues.put(Constants.PRODUCTION_TAG_SESSION_NEAGTIVE_TOLERANCE,Float.parseFloat(String.valueOf(dtolerance)));
                contentValues.put(Constants.PRODUCTION_TAG_MASTER_TOLERANCE_POSITIVE,Float.parseFloat(String.valueOf(dtolerance)));
                contentValues.put(Constants.PRODUCTION_TAG_MASTER_TOLERANCE_NEGATIVE,Float.parseFloat(String.valueOf(dtolerance)));
                contentValues.put(Constants.PRODUCTION_TAG_UCL_GM,Float.parseFloat(String.valueOf(ucl)));
                contentValues.put(Constants.PRODUCTION_TAG_LCL_GM,Float.parseFloat(String.valueOf(lcl)));
                contentValues.put(Constants.PRODUCTION_TAG_MODIFIED_DATE,currentDateandTime);
                contentValues.put(Constants.PRODUCTION_TAG_OPERATOR_NAME,ed_prod_operator_name.getText().toString().trim());
                contentValues.put(Constants.PRODUCTION_TAG_IS_SYNC, 0);


                long result = db.insert(Constants.PRODUCTION_TAG_TABLE, null, contentValues);

                if (result == -1) {
                    Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                    //wakeup();
                }
            }
            else if (sp_uom_str.equals("Kg"))
            {
                Double  ucl = Double.parseDouble(gmtokgcalvatue) + Double.parseDouble(str_positive_tolerance);
                Double lcl = Double.parseDouble(gmtokgcalvatue) - Double.parseDouble(str_negative_tolerance);
                uclgm = String.valueOf(ucl);
                lclgm = String.valueOf(lcl);
                Double dgmperpouch = Double.valueOf(ed_prod_gm_pouch.getText().toString());
                Double dtolerance = Double.valueOf(str_positive_tolerance);
                Double dntolerance = Double.valueOf(str_negative_tolerance);
                Double dtotalqty = Double.valueOf(ed_production_total_qty.getText().toString());
                Double gmtotalqty = dtotalqty * dintkgvalue;
                Double dkgperpouch = dgmperpouch *dintkgvalue;


                contentValues.put(Constants.PRODUCTION_TAG_ID,1);

                contentValues.put(Constants.PRODUCTION_TAG, resToMobile);
                contentValues.put(Constants.PRODUCTION_TAG_OEM_ID, 1234);
                contentValues.put(Constants.PRODUCTION_TAG_MAC_ID, dataloggerMacId);
                contentValues.put(Constants.PRODUCTION_TAG_BATCH_ID,str_batch_id);
                contentValues.put(Constants.PRODUCTION_TAG_PRODUCT_ID, sp_product_string.trim());
                contentValues.put(Constants.PRODUCTION_TAG_GM_PER_POUCH,Float.parseFloat(String.valueOf(dkgperpouch)));
                contentValues.put(Constants.PRODUCTION_TAG_UNIT, sp_uom_str);
                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY,Float.parseFloat(String.valueOf(gmtotalqty)));
                contentValues.put(Constants.PRODUCTION_TAG_TOTAL_QTY_UOM, sp_uom_str);
                if (str_negative_weight_allowed.equals("Yes"))
                {
                    contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED, 1);

                }
                else
                {
                    contentValues.put(Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED, 0);

                }
                contentValues.put(Constants.PRODUCTION_TAG_SHIFT, str_shifts);
                contentValues.put(Constants.PRODUCTION_TAG_NO_OF_POUCHES, Integer.parseInt(ed_no_of_pouches.getText().toString()));
                contentValues.put(Constants.PRODUCTION_TAG_SHIFT, str_shifts);
                contentValues.put(Constants.PRODUCTION_TAG_SESSION_POSITVIE_TOLERANCE,Float.parseFloat(String.valueOf(dtolerance)));
                contentValues.put(Constants.PRODUCTION_TAG_SESSION_NEAGTIVE_TOLERANCE,Float.parseFloat(String.valueOf(dtolerance)));
                contentValues.put(Constants.PRODUCTION_TAG_MASTER_TOLERANCE_POSITIVE,Float.parseFloat(String.valueOf(dtolerance)));
                contentValues.put(Constants.PRODUCTION_TAG_MASTER_TOLERANCE_NEGATIVE,Float.parseFloat(String.valueOf(dtolerance)));
                contentValues.put(Constants.PRODUCTION_TAG_UCL_GM,Float.parseFloat(String.valueOf(ucl)));
                contentValues.put(Constants.PRODUCTION_TAG_LCL_GM,Float.parseFloat(String.valueOf(lcl)));
                contentValues.put(Constants.PRODUCTION_TAG_MODIFIED_DATE,currentDateandTime);
                contentValues.put(Constants.PRODUCTION_TAG_OPERATOR_NAME,ed_prod_operator_name.getText().toString().trim());
                contentValues.put(Constants.PRODUCTION_TAG_IS_SYNC, 0);

                long result = db.insert(Constants.PRODUCTION_TAG_TABLE, null, contentValues);

                if (result == -1) {
                    Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                    //wakeup();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable != null && !editable.toString().equalsIgnoreCase("")){

                if (rstring.equals("pouch")){
                    ed_production_total_qty.removeTextChangedListener(textWatcher);
                    String noOfPouches = editable.toString().trim();
                    String gmPerPouch = ed_prod_gm_pouch.getText().toString().trim();

                    ed_no_of_pouches.removeTextChangedListener(textWatcher);
                    if (gmPerPouch.equals("") || noOfPouches.equals("")) {
                        ed_production_total_qty.setText("");
                    } else {
                        if (sp_uom_str.equals("gm"))
                        {
                            int totalQty = Integer.parseInt(gmPerPouch) * Integer.parseInt(noOfPouches);
                            strtotalqty = String.valueOf(totalQty);
                            Double dtotalqty = Double.valueOf(strtotalqty);
                            ed_production_total_qty.setText(decimalFormat.format(dtotalqty));
                        }
                        else if (sp_uom_str.equals("Kg"))
                        {
                            int totalQty = Integer.parseInt(gmtokgcalvatue) * Integer.parseInt(noOfPouches);
                            strtotalqty = String.valueOf(totalQty);
                            String strintvalue = String.valueOf(intkgvalue);
                            Double dintvalue = Double.valueOf(strintvalue);
                            Double dtotalqty = Double.valueOf(strtotalqty);

                            //int kgtotalqty = totalQty / intkgvalue;
                            Double dkgtotalqty = dtotalqty / dintvalue;
                            String strkgtotalqty = String.valueOf(dkgtotalqty);
                            Double dkgcaltotalqty = Double.valueOf(strkgtotalqty);

                            ed_production_total_qty.setText(decimalFormat.format(dkgcaltotalqty));
                        }
                    }
                    ed_no_of_pouches.addTextChangedListener(textWatcher);
                }

                else if (rstring.equals("totalqty")){

                    ed_no_of_pouches.removeTextChangedListener(textWatcher);
                    String totalQty = editable.toString().trim();
                    String gmPerPouch = ed_prod_gm_pouch.getText().toString().trim();
                    ed_production_total_qty.removeTextChangedListener(textWatcher);
                    if (totalQty.equals("") || gmPerPouch.equals("")){
                        ed_no_of_pouches.setText("");
                        ed_prod_gm_pouch.setText("");
                    }else {
                        int noOfPouches = Integer.parseInt(totalQty) / Integer.parseInt(gmPerPouch);
                        ed_no_of_pouches.setText(noOfPouches+"");
                    }
                    ed_production_total_qty.addTextChangedListener(textWatcher);
                }
            }


        }
    };


    //Production Tag Save
    private String SaveTagInfo(String msg) {
        String resFromDataLogger = "";

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(MobileJsonData.class, new MobileJsonDataDeserializer());

        Gson gson = gsonBuilder.create();

        MobileJsonData outputObj = gson.fromJson(msg, MobileJsonData.class);

        Tag tagTest = outputObj.tagInfo;
        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        String dataLoggerName = outputObj.dataLoggerName;
        String dataLoggerIP = outputObj.dataLoggerIP;

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String oiNum = tagTest.getOiNum();
        String wt = tagTest.getTargetWt();
        String date = dateFormat.format(cal.getTime());

        String tagUniqueStr = dataLoggerName + ":" + tagTest.getOiNum() + ":" + tagTest.getTargetWt() + ":" + dateFormat.format(cal.getTime());tagTest.tagid = tagUniqueStr.trim();
        tagTest.tagid = tagUniqueStr.replaceAll(" ", "");

        TestRun testRun = new TestRun();
        testRun.head = tagTest.head;
        testRun.oinum = tagTest.oiNum;
        testRun.targetwt = tagTest.targetWt;
        testRun.tagid = tagTest.tagid;

        ArrayList<TestRun> existingTestRunObjs = new ArrayList<TestRun>();

        try{
            deleteTestRunCompleted();
            existingTestRunObjs = getRunningTestInfo();

            String testStatus = getTestRunAllowedStatus(testRun, existingTestRunObjs);
            if(testStatus != null && testStatus.equals(Constants.TEST_APPEAL)){
                insertIntoTestRunsTbl(testRun);
                System.out.println("In test run appeal sent for ");
                //return HttpResponse.status(200).entity(Constants.TEST_APPEAL).build();
                return Constants.TEST_APPEAL;
            }
            if(testStatus != null && testStatus.equals(Constants.TEST_NOT_ALLOWED)){
                //return HttpResponse.status(200).entity(Constants.TEST_NOT_ALLOWED).build();
                return Constants.TEST_NOT_ALLOWED;
            }
        }catch(SQLException sqlEx){
            sqlEx.printStackTrace();
            Log.e("", "Exception occured and the stack trace is  "+ sqlEx.getStackTrace().toString(), sqlEx);
            //return HttpResponse.status(200).entity("").build();
            return "";

        }
        resToMobile = submitTagInputs(msg);
        // return HttpResponse.status(200).entity(resToMobile).build();
        return resToMobile;
    }

    public void deleteTestRunCompleted() throws SQLException {
        Connection conn = null;
        try {

            conn = getDbConnection();
            conn.setAutoCommit(false);
            PreparedStatement deleteTestRunStat = conn.prepareStatement("delete from test_run_status where status=? or status=?");
            deleteTestRunStat.setString(1, "COMPLETED");
            deleteTestRunStat.setString(2, "ABORTED");

            deleteTestRunStat.executeUpdate();
            conn.commit();
            deleteTestRunStat.close();

        } catch (Exception ne) {
            ne.printStackTrace();
            System.out.println("Exception is " + ne.getCause().getMessage());
            conn.rollback();

        } finally {

            if (conn != null) {
                conn.close();
            }
            conn = null;
        }
    }

    public final Connection getDbConnection() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
        conn = DriverManager.getConnection(dbUrl,dbUserName,dbPassword);
        if (conn !=null){
            Log.d(TAG, "getDbConnection: "+"Database is connected to database");
        }else if (conn == null){
            Log.d(TAG, "getDbConnection: "+"Database is not connected to database");
        }
        return conn;
    }

    public ArrayList<TestRun> getRunningTestInfo() throws SQLException{

        Connection conn = null;
        ArrayList<TestRun> testRuns = new ArrayList<TestRun>();

        try{
            conn = getDbConnection();
            PreparedStatement getTestRunStat = conn.prepareStatement(QUERY_GET_TEST_RUNS_IN_PROGRESS);
            ResultSet results =  getTestRunStat.executeQuery();
            while (results.next()) {
                TestRun testRunObj = new TestRun();
                testRunObj.tagid = results.getString("tagid");
                testRunObj.head = results.getString("head");
                testRunObj.oinum = results.getString("oinum");
                testRunObj.targetwt = results.getString("targetwt");
                testRunObj.status = results.getString("status");
                testRunObj.isAuthorized = results.getString("isauthorised");
                testRuns.add(testRunObj);
            }
            results.close();
            getTestRunStat.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            conn = null;
        }
        return testRuns;
    }

    private String getTestRunAllowedStatus(TestRun testRunObj, ArrayList<TestRun> existingTestRuns) {
        String status = Constants.TEST_ALLOWED;
        if(existingTestRuns != null && existingTestRuns.size() > 0){

            for(TestRun existingTestRun : existingTestRuns){

                if((existingTestRun.oinum.equals(testRunObj.oinum)) &&
                        (existingTestRun.targetwt.equals(testRunObj.targetwt))){

                    if(existingTestRun.head.equals(testRunObj.head)){
                        status = Constants.TEST_NOT_ALLOWED;
                    }

                    if(existingTestRun.head != null && existingTestRun.head.equals("Single")){
                        if(testRunObj.head != null && !testRunObj.head.equals("Single")){
                            status = Constants.TEST_NOT_ALLOWED;
                        }
                    }
                    if(existingTestRun.head != null && !existingTestRun.head.equals("Single")){
                        if(testRunObj.head != null && testRunObj.head.equals("Single")){
                            status = Constants.TEST_NOT_ALLOWED;
                        }
                    }

                }

                if((existingTestRun.oinum.equals(testRunObj.oinum)) &&
                        (!existingTestRun.targetwt.equals(testRunObj.targetwt))){

                    if(existingTestRun.head.equals(testRunObj.head)){
                        status = Constants.TEST_APPEAL;
                    }

                    if(existingTestRun.head != null && existingTestRun.head.equals("Single")){
                        if(testRunObj.head != null && !testRunObj.head.equals("Single")){
                            status = Constants.TEST_NOT_ALLOWED;
                        }
                    }
                    if(existingTestRun.head != null && !existingTestRun.head.equals("Single")){
                        if(testRunObj.head != null && testRunObj.head.equals("Single")){
                            status = Constants.TEST_NOT_ALLOWED;
                        }
                    }
                }
            }
        }
        return status;
    }

    private String submitTagInputs(String tagInfo) {
        Log.d(TAG, "Submit appealed tags called and message is  "+ tagInfo);
        String resFromDataLogger = "";
        String resToMobile = "";

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(MobileJsonData.class,
                new MobileJsonDataDeserializer());
        Gson gson = gsonBuilder.create();
        MobileJsonData outputObj = gson.fromJson(tagInfo, MobileJsonData.class);

        Tag tagTest = outputObj.tagInfo;
        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        String dataLoggerName = outputObj.dataLoggerName;
        String dataLoggerIP = outputObj.dataLoggerIP;
        //dataLoggerIP = "localhost";
        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();

        String tagUniqueStr = dataLoggerName + ":" + tagTest.getOiNum() + ":" + tagTest.getTargetWt() + ":" + dateFormat.format(cal.getTime());
//        String tagUniqueStr = tagID;
        tagTest.tagid = tagUniqueStr.trim();
        tagTest.tagid = tagUniqueStr.replaceAll(" ", "");
        TestRun testRunObj = new TestRun();
        testRunObj.head = tagTest.head;
        testRunObj.oinum = tagTest.oiNum;
        testRunObj.targetwt = tagTest.targetWt;
        testRunObj.status = Constants.RUN_TEST_INPROGRESS_STATUS;
        testRunObj.tagid = tagTest.tagid;
        testRunObj.isAuthorized = "Yes";


        HttpClient httpClient = new DefaultHttpClient();
        try {
            insertIntoTestRunsTbl(testRunObj);
            String url = "http://"+dataLoggerIP+":"+DATA_LOGGER_PORT+"/wakeup?tagid="+tagUniqueStr.trim()+ "&dataLoggerName=" + dataLoggerName;
//            String url = "http://"+dataLoggerIP+"/wakeup?tagid="+tagUniqueStr.trim()+ "&dataLoggerName=" + dataLoggerName;
            HttpResponse response1 = httpClient.execute(new HttpGet(url));
            StatusLine statusLine = response1.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response1.getEntity().writeTo(out);
                resFromDataLogger = out.toString();
                Toast.makeText(this, resFromDataLogger, Toast.LENGTH_SHORT).show();
                out.close();
            } else{
                response1.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            //log.error("Exception occured and the stack trace is  "+ e.getStackTrace().toString(), e);
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        try {
            if (resFromDataLogger != null && resFromDataLogger.contains("ok")) {
                //log.debug("Got positive response from data logger, inserting record in database");

                insertIntoTagsTbl(tagTest);
                resToMobile = tagTest.tagid;
                changeDataLoggerStatus(dataLoggerName, "Yes");
            } else {
                resToMobile = "DataLoggerNotReady";
            }
        } catch (Exception ex) {
            System.out.println("The exception occured and the exception is  " + ex.getMessage());
            //log.error("Exception occured and exception is ", ex);
            resToMobile = "";
        }
        return resToMobile;
    }

    private void insertIntoTestRunsTbl(TestRun testRunObj) throws SQLException {
        if (testRunObj != null) {
            Connection conn = null;
            try {

                conn = getDbConnection();
                conn.setAutoCommit(false);
                PreparedStatement testRunInsertStat = conn.prepareStatement("Insert into test_run_status (tagid, " + "oinum, targetwt, head, status, isauthorised) values (?,?,?,?,?,?)");
                testRunInsertStat.setString(1, testRunObj.tagid);
                testRunInsertStat.setString(2,  testRunObj.oinum);
                testRunInsertStat.setString(3,  testRunObj.targetwt);
                testRunInsertStat.setString(4,  testRunObj.head);
                testRunInsertStat.setString(5,  testRunObj.status);
                testRunInsertStat.setString(6,  testRunObj.isAuthorized);
                testRunInsertStat.executeUpdate();
                conn.commit();
                testRunInsertStat.close();

            } catch (Exception ne) {
                ne.printStackTrace();
                System.out.println("Exception is " + ne);
                conn.rollback();

            } finally {

                if (conn != null) {
                    conn.close();
                }
                conn = null;
            }

        }
    }

    private void insertIntoTagsTbl(Tag tagObj) throws SQLException {
        if (tagObj != null) {
            Connection conn = null;
            try {

                conn = getDbConnection();
                System.out.println("Got the connection");
                conn.setAutoCommit(false);
                PreparedStatement tagInsertStat = conn.prepareStatement(Constants.QUERY_INSERT_TAGTBL);
                tagInsertStat.setString(1, tagObj.tagid);
                tagInsertStat.setString(2, tagObj.oiNum);
                tagInsertStat.setString(3, tagObj.targetWt);
                tagInsertStat.setString(4, tagObj.fillerType);
                tagInsertStat.setString(5, tagObj.customer);
                tagInsertStat.setString(6, tagObj.machineType);
                tagInsertStat.setString(7, tagObj.productType);
                tagInsertStat.setString(8, tagObj.product);
                tagInsertStat.setString(9, tagObj.operator);
                tagInsertStat.setString(10, tagObj.targetWtUnit);
                tagInsertStat.setString(11, tagObj.inspector);
                tagInsertStat.setString(12, tagObj.filmType);
                tagInsertStat.setString(13, tagObj.productfeed);
                tagInsertStat.setString(14, tagObj.pouchWidth);
                tagInsertStat.setString(15, tagObj.pouchLength);
                tagInsertStat.setString(16, tagObj.agitatorMotorFreq);
                tagInsertStat.setString(17, tagObj.agitatorMode);
                tagInsertStat.setString(18, tagObj.agitatorod);
                tagInsertStat.setString(19, tagObj.agitatorid);
                tagInsertStat.setString(20, tagObj.agitatorPitch);
                tagInsertStat.setString(21, tagObj.targetacctype);
                tagInsertStat.setString(22, tagObj.targetaccval);
                tagInsertStat.setString(23, tagObj.mcSerialNum);
                tagInsertStat.setString(24, tagObj.prBulkMain);
                tagInsertStat.setString(25, tagObj.targetSpeed);
                tagInsertStat.setString(26, tagObj.customerProduct);
                tagInsertStat.setString(27, tagObj.head);
                tagInsertStat.setString(28, tagObj.airPressure);
                tagInsertStat.setString(29, tagObj.qtySetting);
                tagInsertStat.setString(30, tagObj.bgLenSetting);
                tagInsertStat.setString(31, tagObj.fillValveGap);
                tagInsertStat.setString(32, tagObj.fillVfdFreq);
                tagInsertStat.setString(33, tagObj.horizCutSealSetting);
                tagInsertStat.setString(34, tagObj.horizBandSealSetting);
                tagInsertStat.setString(35, tagObj.vertSealSetting);
                tagInsertStat.setString(36, tagObj.augerAgitatorFreq);
                tagInsertStat.setString(37, tagObj.augerFillValve);
                tagInsertStat.setString(38, tagObj.augurPitch);
                tagInsertStat.setString(39, tagObj.pouchSealType);
                tagInsertStat.setString(40, tagObj.pouchTempHZF);
                tagInsertStat.setString(41, tagObj.pouchTempHZB);
                tagInsertStat.setString(42, tagObj.pouchTempVert);
                tagInsertStat.setString(43, tagObj.pouchTempVertFrontLeft);
                tagInsertStat.setString(44, tagObj.pouchTempVertFrontRight);
                tagInsertStat.setString(45, tagObj.pouchTempVertBackLeft);
                tagInsertStat.setString(46, tagObj.pouchTempVertBackRight);
                tagInsertStat.setString(47, tagObj.pouchGusset);
                tagInsertStat.setString(48, tagObj.customerFilm);
                tagInsertStat.setString(49, tagObj.pouchFilmThickness);
                tagInsertStat.setString(50, tagObj.pouchPrBulk);
                tagInsertStat.setString(51, tagObj.pouchEqHorizSerrations);
                tagInsertStat.setString(52, tagObj.pouchFillEvent);
                tagInsertStat.setString(53, tagObj.negWt);
                tagInsertStat.setString(54, tagObj.fyyear);
                System.out.println("The created date formed is "+ new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
                tagInsertStat.setDate(55, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

                tagInsertStat.executeUpdate();
                conn.commit();

            } catch (Exception ne) {
                ne.printStackTrace();
                System.out.println("Exception is " + ne.getCause().getMessage());
                conn.rollback();

            } finally {

                if (conn != null) {
                    conn.close();
                }
                conn = null;
            }

        }
    }

    private void changeDataLoggerStatus(String dataLoggerName, String inuseStatus){
        Connection conn = null;
        try {
            conn = getDbConnection();
            conn.setAutoCommit(false);
            PreparedStatement updateDlStat = conn.prepareStatement("update mstr_logger_tbl set inuse=? where logger_name=? and isactive=1 and isdeleted=0");
            updateDlStat.setString(1,  inuseStatus);
            updateDlStat.setString(2, dataLoggerName);

            updateDlStat.executeUpdate();
            conn.commit();
            updateDlStat.close();

        }  catch (InstantiationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            conn = null;
        }
    }
}
