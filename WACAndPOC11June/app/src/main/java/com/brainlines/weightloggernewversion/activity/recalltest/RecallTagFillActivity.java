package com.brainlines.weightloggernewversion.activity.recalltest;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.newtest.WeightReadingActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.C_TagsModel;
import com.brainlines.weightloggernewversion.model.MobileJsonData;
import com.brainlines.weightloggernewversion.model.MobileJsonDataDeserializer;
import com.brainlines.weightloggernewversion.model.SpinnerMachinesModel;
import com.brainlines.weightloggernewversion.model.Tag;
import com.brainlines.weightloggernewversion.model.TestRun;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class RecallTagFillActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "NewTestTagFillActivity";
    String resToMobile = "";

    CardView cvMainDetails,cvLiquidDetails,cvAugerDetails,cvPouchAndSealDetails;
    LinearLayout llMainDetailForm,llLiquidDetailForm,llAugerDetailForm,llPouchAndSealDetailForm;

    Spinner spFinancialYear,spMachineModel,spProductUsedForTrials,spTargetAccuracyType,spFillerType,spHead,spTargetWtUOM,spSealType;
    private DataBaseHelper helper;
    private String dbIpAddress = Constants.dbIpAddress;
    private String dbUserName = Constants.dbUserName;
    private String dbPassword = Constants.dbPassword;
    private String dbDatabaseName = Constants.dbDatabaseName;
    private String serverport = Constants.serverport;
    private String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
    private int DATA_LOGGER_PORT = Constants.DATA_LOGGER_PORT;
    public static final String MQTT_BROKER = "tcp://"+Constants.dbIpAddress+":1883";
    private Float floatkgvalue;

    DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss.SSS");

    ArrayList<String> financialYearList = new ArrayList<>();
    ArrayList<SpinnerMachinesModel> spMachineModelsList = new ArrayList<>();
    ArrayList<String> spProductsUsedForTrialList = new ArrayList<>();
    ArrayList<String> spTargetAccuracyList = new ArrayList<>();
    ArrayList<String> spFillerTypeList = new ArrayList<>();
    ArrayList<String> spHeadTypeList = new ArrayList<>();
    ArrayList<String> sealTypeList = new ArrayList<>();
    ArrayList<String> uomList = new ArrayList<>();
    ArrayList<String> grammage_values_list = new ArrayList<>();

    public static final String QUERY_GET_TEST_RUNS_IN_PROGRESS = "select * from test_run_status where status='" + Constants.RUN_TEST_INPROGRESS_STATUS + "'";

    //main details
    EditText ed_operator_name,ed_customer_name,ed_oinum_no,ed_machine_sr_no,ed_bulk_density
            ,ed_target_weight,ed_target_accuracy_value,ed_target_speed_ppm;

    //liquid machine details
    EditText ed_air_pressure_bar,ed_quantity_setting,ed_bag_length_setting,ed_fill_valve_gap,ed_fill_vfd_frequency,ed_horiz_seal_settings,ed_horiz_band_seal_setting,ed_vert_seal_setting;

    //Auger details
    EditText ed_agitator_freq_hz,ed_agitator_mode,ed_auger_tube_od_mm,ed_auger_tube_id_mm,ed_auger_pitch_mm;

    //pouch and seal details
    EditText ed_temp_vert_c,ed_temp_hz_c,ed_temp_hz_back_c,ed_temp_vert_front_left,ed_temp_vert_front_right,ed_film_thickness_microns,ed_equal_horiz_serrations,
            ed_temp_vert_back_left,ed_temp_vert_back_right,ed_pouch_width_mm,ed_pouch_seal_length_mm,ed_pouch_guest_size;

    String str_operator_name,str_customer_name,str_oinum_no,str_machine_sr_no,str_bulk_Density,str_target_weight,str_target_accuracy_value,str_ed_target_speed_ppm;
    String str_air_pressure_bar,str_quantity_setting,str_bag_length_setting,str_fill_valve_gap,str_fill_vfd_frequency,str_horiz_seal_settings,str_horiz_band_seal_setting,str_vert_seal_setting;
    String str_temp_vert_c,str_temp_hz_c,str_temp_hz_back_c,str_temp_vert_front_left,str_temp_vert_front_right,
            str_temp_vert_back_left,str_temp_vert_back_right,str_pouch_width_mm,str_pouch_seal_length_mm;

    String str_agitator_freq_hz,str_agitator_mode,str_auger_tube_od_mm,str_auger_tubeid_mm,str_auger_pitch_mm;
    String strFinancialYear,strMachineModel,strProductsUsedForTrail,strTargetAccuracy,strFillerType,strHeadType,strSealType;
    String tagID,dataloggerName,dataloggerIp,dataloggerMacId;
    ImageButton btn_submit,btn_cancel;
    Button btn_sync;

    String tagInfoStr="";
    SwitchCompat switch_product_type,switch_negative_weight,switch_customer_product,switch_product_feed;
    SwitchCompat switch_film_type,switch_customer_film,switch_film_event;

    String str_switch_product_type,str_switch_negative_wt,str_cust_product,str_product_feed;
    String str_switch_film_type,str_customer_film,str_switch_film_event;
    String tag_so_oinum;
    private String str_film_thickness;
    private ArrayList<C_TagsModel> c_tagsModelArrayList = new ArrayList<>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss.SSS");
    String strTargetWtUOM;
    private double intkgvalue;
    DecimalFormat decimalFormat = new DecimalFormat("0.000");
    private SQLiteDatabase db;
    private String dbName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_test_tag_fill);
        initUI();

        dataloggerIp =getIntent().getStringExtra("LoggerIP");
        dataloggerName=getIntent().getStringExtra("LoggerName");
        tag_so_oinum = getIntent().getStringExtra("oinum");
        dataloggerMacId = getIntent().getStringExtra("LoggerMacId");

        getTagInfoAgainstOinum();
        SwitchValues();
        getProductionGrammageValues();

        spFinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strFinancialYear = spFinancialYear.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spMachineModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strMachineModel = spMachineModel.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spProductUsedForTrials.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strProductsUsedForTrail = spProductUsedForTrials.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spTargetAccuracyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strTargetAccuracy = spTargetAccuracyType.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spFillerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strFillerType = spFillerType.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spHead.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strHeadType = spHeadTypeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spSealType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strSealType = spSealType.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spTargetWtUOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                strTargetWtUOM = spTargetWtUOM.getSelectedItem().toString();
                if (strTargetWtUOM.equals("Kg")) {
                    String kgvalue = grammage_values_list.get(0);
                    floatkgvalue = Float.parseFloat(kgvalue);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                strTargetWtUOM = "";
            }
        });
    }

    private void SwitchValues()
    {
        if (!switch_product_type.isChecked())
        {
            str_switch_product_type = (String) switch_product_type.getTextOff();
        }
        else
        {
            str_switch_product_type = (String) switch_product_type.getTextOn();
        }
        if (!switch_negative_weight.isChecked())
        {
            str_switch_negative_wt = (String) switch_negative_weight.getTextOff();
        }
        else
        {
            str_switch_negative_wt = (String) switch_negative_weight.getTextOn();
        }
        if (!switch_customer_product.isChecked())
        {
            str_cust_product = (String) switch_customer_product.getTextOff();
        }
        else
        {
            str_cust_product = (String) switch_customer_product.getTextOn();

        }
        if (!switch_product_feed.isChecked())
        {
            str_product_feed = (String) switch_product_feed.getTextOff();
        }
        else
        {
            str_product_feed = (String) switch_product_feed.getTextOn();
        }
        if (!switch_film_type.isChecked())
        {
            str_switch_film_type = (String) switch_film_type.getTextOff();
        }
        else
        {
            str_switch_film_type = (String) switch_film_type.getTextOn();
        }
        if (!switch_customer_film.isChecked())
        {
            str_customer_film = (String) switch_customer_film.getTextOff();
        }
        else
        {
            str_customer_film = (String) switch_customer_film.getTextOn();

        }
        if (!switch_film_event.isChecked())
        {
            str_switch_film_event = (String) switch_film_event.getTextOff();
        }
        else
        {
            str_switch_film_event = (String) switch_film_event.getTextOn();
        }


        switch_product_type.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked)
                {
                    str_switch_product_type = (String) switch_product_type.getTextOn();

                }
                else
                {
                    str_switch_product_type = (String) switch_product_type.getTextOff();

                }
            }
        });
        switch_negative_weight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked)
                {
                    str_switch_negative_wt = (String) switch_negative_weight.getTextOn();

                }
                else
                {
                    str_switch_negative_wt = (String) switch_negative_weight.getTextOff();

                }
            }
        });
        switch_customer_product.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked)
                {
                    str_cust_product = (String) switch_customer_product.getTextOn();

                }
                else
                {
                    str_cust_product = (String) switch_customer_product.getTextOff();

                }
            }
        });
        switch_product_feed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked)
                {
                    str_product_feed = (String) switch_product_feed.getTextOn();

                }
                else
                {
                    str_product_feed = (String) switch_product_feed.getTextOff();

                }
            }
        });
        switch_film_type.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked)
                {
                    str_switch_film_type = (String) switch_film_type.getTextOn();

                }
                else
                {
                    str_switch_film_type = (String) switch_film_type.getTextOff();

                }
            }
        });
        switch_customer_film.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked)
                {
                    str_customer_film = (String) switch_customer_film.getTextOn();

                }
                else
                {
                    str_customer_film = (String) switch_customer_film.getTextOff();

                }
            }
        });
        switch_film_event.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked)
                {
                    str_switch_film_event = (String) switch_film_event.getTextOn();

                }
                else
                {
                    str_switch_film_event = (String) switch_film_event.getTextOff();

                }
            }
        });

    }

    private void initUI() {

        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

//        dataloggerName = getIntent().getStringExtra("LoggerName");
//        dataloggerIp = getIntent().getStringExtra("LoggerIP");

        switch_product_type = findViewById(R.id.switch_product_type);
        switch_negative_weight = findViewById(R.id.switch_negative_weight);
        switch_customer_product = findViewById(R.id.switch_customer_product);
        switch_product_feed = findViewById(R.id.switch_product_feed);
        switch_film_type = findViewById(R.id.switch_film_type);
        switch_customer_film = findViewById(R.id.switch_customer_film);
        switch_film_event = findViewById(R.id.switch_film_event);

        cvMainDetails = findViewById(R.id.cvMainDetails);
        cvLiquidDetails = findViewById(R.id.cvLiquidDetails);
        cvAugerDetails = findViewById(R.id.cvAugerDetails);
        cvPouchAndSealDetails = findViewById(R.id.cvPouchAndSealDetails);
        llMainDetailForm = findViewById(R.id.llMainDetailForm);
        llLiquidDetailForm = findViewById(R.id.llLiquidDetailForm);
        llAugerDetailForm = findViewById(R.id.llAugerDetailForm);
        llPouchAndSealDetailForm = findViewById(R.id.llPouchAndSealDetailForm);
        spFinancialYear = findViewById(R.id.spFinancialYear);
        spMachineModel = findViewById(R.id.spMachineModel);
        spProductUsedForTrials = findViewById(R.id.spProductUsedForTrials);
        spTargetAccuracyType = findViewById(R.id.spTargetAccuracyType);
        spFillerType = findViewById(R.id.spFillerType);
        spHead = findViewById(R.id.spHead);
        spTargetWtUOM = findViewById(R.id.spTargetWtUOM);
        spSealType = findViewById(R.id.spSealType);

        //main details
        ed_operator_name = findViewById(R.id.ed_operator_name);
        ed_customer_name = findViewById(R.id.ed_customer_name);
        ed_oinum_no = findViewById(R.id.ed_oinum_no);
        ed_machine_sr_no = findViewById(R.id.ed_machine_sr_no);
        ed_bulk_density = findViewById(R.id.ed_bulk_density);
        ed_target_weight = findViewById(R.id.ed_target_weight);
        ed_target_accuracy_value = findViewById(R.id.ed_target_accuracy_value);
        ed_target_speed_ppm = findViewById(R.id.ed_target_speed_ppm);

        //liquid machine details
        ed_air_pressure_bar = findViewById(R.id.ed_air_pressure_bar);
        ed_quantity_setting = findViewById(R.id.ed_quantity_setting);
        ed_bag_length_setting = findViewById(R.id.ed_bag_length_setting);
        ed_fill_valve_gap = findViewById(R.id.ed_fill_valve_gap);
        ed_fill_vfd_frequency = findViewById(R.id.ed_fill_vfd_frequency);
        ed_horiz_seal_settings = findViewById(R.id.ed_horiz_seal_settings);
        ed_horiz_band_seal_setting = findViewById(R.id.ed_horiz_band_seal_setting);
        ed_vert_seal_setting = findViewById(R.id.ed_vert_seal_setting);

        //Auger details
        ed_agitator_freq_hz = findViewById(R.id.ed_agitator_freq_hz);
        ed_agitator_mode = findViewById(R.id.ed_agitator_mode);
        ed_auger_tube_od_mm = findViewById(R.id.ed_auger_tube_od_mm);
        ed_auger_tube_id_mm = findViewById(R.id.ed_auger_tube_id_mm);
        ed_auger_pitch_mm = findViewById(R.id.ed_auger_pitch_mm);

        //pouch and seal details

        ed_temp_vert_c = findViewById(R.id.ed_temp_vert_c);
        ed_temp_hz_c = findViewById(R.id.ed_temp_hz_c);
        ed_temp_hz_back_c = findViewById(R.id.ed_temp_hz_back_c);
        ed_temp_vert_front_left = findViewById(R.id.ed_temp_vert_front_left);
        ed_temp_vert_front_right = findViewById(R.id.ed_temp_vert_front_right);
        ed_temp_vert_back_left = findViewById(R.id.ed_temp_vert_back_left);
        ed_temp_vert_back_right = findViewById(R.id.ed_temp_vert_back_right);
        ed_pouch_width_mm = findViewById(R.id.ed_pouch_width_mm);
        ed_pouch_seal_length_mm = findViewById(R.id.ed_pouch_seal_length_mm);
        ed_pouch_guest_size = findViewById(R.id.ed_pouch_guest_size);
        ed_film_thickness_microns = findViewById(R.id.ed_film_thickness_microns);
        ed_equal_horiz_serrations = findViewById(R.id.ed_equal_horiz_serrations);


        btn_submit = findViewById(R.id.btn_submit);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_sync = findViewById(R.id.btn_sync);


        cvMainDetails.setOnClickListener(this);
        cvLiquidDetails.setOnClickListener(this);
        cvAugerDetails.setOnClickListener(this);
        cvPouchAndSealDetails.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        btn_sync.setOnClickListener(this);

        //Adding data to the list to show in financial year spinner
        financialYearList.add("2018-2019");
        financialYearList.add("2019-2020");
        financialYearList.add("2020-2021");
        ArrayAdapter<String> spFinancialYears = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, financialYearList);
        spFinancialYears.setDropDownViewResource(R.layout.item_spinner_latyout);
        spFinancialYear.setAdapter(spFinancialYears);

        //Adding data to the list to show in Machine Model spinner
        getMachineModel();
        ArrayAdapter<SpinnerMachinesModel> spMachineModelList = new ArrayAdapter<SpinnerMachinesModel>(this,R.layout.item_spinner_latyout, spMachineModelsList);
        spMachineModelList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spMachineModel.setAdapter(spMachineModelList);

        //Adding data to the list to show in Product used For Trial spinner
        getProductsUsedForTrial();
        ArrayAdapter<String> spProductUsdForTrialList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, spProductsUsedForTrialList);
        spProductUsdForTrialList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spProductUsedForTrials.setAdapter(spProductUsdForTrialList);

        //Adding data to the list to show in Target Accuracy Type spinner
        getTargetAccuracyType();
        ArrayAdapter<String> spTargetAccuracy = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, spTargetAccuracyList);
        spTargetAccuracy.setDropDownViewResource(R.layout.item_spinner_latyout);
        spTargetAccuracyType.setAdapter(spTargetAccuracy);

        //Adding data to the list to show in Filler Type Type spinner
        getFillerType();
        ArrayAdapter<String> spFillerTy = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, spFillerTypeList);
        spFillerTy.setDropDownViewResource(R.layout.item_spinner_latyout);
        spFillerType.setAdapter(spFillerTy);

        //Adding data to the list to show in Head Type Type spinner
        getHeadType();
        ArrayAdapter<String> spHead1 = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, spHeadTypeList);
        spHead1.setDropDownViewResource(R.layout.item_spinner_latyout);
        spHead.setAdapter(spHead1);

        //Adding data to the list to show in Seal Type Type spinner
        getSealTypes();
        ArrayAdapter<String> spSealTypeS = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, sealTypeList);
        spSealTypeS.setDropDownViewResource(R.layout.item_spinner_latyout);
        spSealType.setAdapter(spSealTypeS);

        //Adding data to the list to show in UoM Spinner
        getUoMList();
        ArrayAdapter<String> spUoMList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, uomList);
        spUoMList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spTargetWtUOM.setAdapter(spUoMList);

    }

    private void getSealTypes() {
        Cursor sealType = helper.getSealType();
        while (sealType.moveToNext()){
            String sealTypeName = sealType.getString(0);
            sealTypeList.add(sealTypeName);
            Log.d(TAG, "getSealTypes: ");
        }
    }

    private void getHeadType() {
        Cursor headType = helper.getHead();
        while (headType.moveToNext()){
            String head = headType.getString(0);
            spHeadTypeList.add(head);
            Log.d(TAG, "getHeadType: ");
        }
    }

    private void getTargetAccuracyType() {
        Cursor targetAccuracy = helper.getTargetAccuracyType();
        while (targetAccuracy.moveToNext()){
            String targetAcc = targetAccuracy.getString(0);
            spTargetAccuracyList.add(targetAcc);
            Log.d(TAG, "getTargetAccuracyType: ");
        }
    }

    private void getMachineModel() {
        Cursor machineModel = helper.getMachineModel();
        while (machineModel.moveToNext()){
            String machineName = machineModel.getString(0);
            String productTypeId = machineModel.getString(1);
            spMachineModelsList.add(new SpinnerMachinesModel(machineName,productTypeId));
            Log.d(TAG, "getMachineModel: ");
        }
    }

    private void getProductsUsedForTrial() {
        Cursor productName = helper.getProducts();
        while (productName.moveToNext()){
            String product = productName.getString(0);
            spProductsUsedForTrialList.add(product);
            Log.d(TAG, "getProductsUsedForTrial: ");
        }
    }

    private void getFillerType() {
        Cursor fillerTypeName = helper.getMasterFillersType();
        while (fillerTypeName.moveToNext()){
            String fillerType = fillerTypeName.getString(0);
            spFillerTypeList.add(fillerType);
            Log.d(TAG, "getFillerType: ");
        }
    }

    private void getUoMList(){
        Cursor uom = helper.getUoMFromLocalDB();
        while (uom.moveToNext()){
            String fillerType = uom.getString(0);
            uomList.add(fillerType);
            Log.d(TAG, "getFillerType: ");
        }
    }
    private void getData() {

        str_operator_name = ed_operator_name.getText().toString();
        str_customer_name = ed_customer_name.getText().toString();
        str_oinum_no = ed_oinum_no.getText().toString();
        str_machine_sr_no = ed_machine_sr_no.getText().toString();
        str_bulk_Density = ed_bulk_density.getText().toString();
        str_target_weight = ed_target_weight.getText().toString();
        str_target_accuracy_value = ed_target_accuracy_value.getText().toString();
        str_air_pressure_bar = ed_air_pressure_bar.getText().toString();
        str_quantity_setting = ed_quantity_setting.getText().toString();
        str_bag_length_setting = ed_bag_length_setting.getText().toString();
        str_fill_valve_gap = ed_fill_valve_gap.getText().toString();
        str_fill_vfd_frequency = ed_fill_vfd_frequency.getText().toString();
        str_horiz_seal_settings = ed_horiz_seal_settings.getText().toString();
        str_vert_seal_setting = ed_vert_seal_setting.getText().toString();
        str_ed_target_speed_ppm = ed_target_speed_ppm.getText().toString();

        str_temp_vert_c = ed_temp_vert_c.getText().toString();
        str_temp_hz_c = ed_temp_hz_c.getText().toString();
        str_temp_hz_back_c = ed_temp_hz_back_c.getText().toString();
        str_temp_vert_front_left = ed_temp_vert_front_left.getText().toString();
        str_temp_vert_front_right = ed_temp_vert_front_right.getText().toString();
        str_temp_vert_back_left = ed_temp_vert_back_left.getText().toString();
        str_temp_vert_back_right = ed_temp_vert_back_right.getText().toString();

        str_pouch_width_mm = ed_pouch_width_mm.getText().toString();
        str_pouch_seal_length_mm = ed_pouch_seal_length_mm.getText().toString();

        str_agitator_freq_hz = ed_agitator_freq_hz.getText().toString();
        str_agitator_mode = ed_agitator_mode.getText().toString();
        str_auger_tube_od_mm = ed_auger_tube_od_mm.getText().toString();
        str_auger_tubeid_mm = ed_auger_tube_id_mm.getText().toString();
        str_auger_pitch_mm = ed_auger_pitch_mm.getText().toString();


        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        String currentDateandTime = dateFormat.format(new Date());
        tagID = dataloggerName + ":" + str_oinum_no + ":" + str_target_weight + ":" + currentDateandTime;
        SaveTagInfo();

    }

    private String SaveTagInfo() {

        tagInfoStr = "{" +  "\"dataLoggerIP\" : \""+ dataloggerIp + "\", " +
                " \"dataLoggerName\": \"" + dataloggerName + "\"," +
                "\"macaddress\" : \"192.168.1.102\", " +
                " \"inspector\": \"" + "inspector" + " \"," +
                "   \"tag\":{\"oiNum\" :\"" + str_oinum_no + "\"," +
                "  \"targetWt\" :\"" + str_target_weight + "\" , " +
                " \"fyyear\" : \"" + strFinancialYear + "\" , " +
                " \"fillerType\" : \"" + strFillerType + "\" , " +
                " \"customer\" : \"" + str_customer_name + "\"," +
                " \"machinetype\" : \"" + strMachineModel + "\"," +
                " \"producttype\" : \"" + strProductsUsedForTrail + "\"," +
                " \"product\" : \"" + "Solid" + "\"," +
                " \"operator\" : \"" + str_operator_name + "\"," +
                " \"targetWtUnit\" : \"" + "gm" + "\"," +
                " \"inspector\" : \"" + "inspector" + "\"," +
                " \"filmtype\" : \"" + "" + "\"," +
                " \"productfeed\" : \"" + "" + "\"," +
                " \"pouchWidth\" : \"" + str_pouch_width_mm + "\"," +
                " \"pouchLength\" : \"" + str_pouch_seal_length_mm + "\"," +
                " \"agitatorMotorFreq\" : \"" + str_agitator_freq_hz + "\"," +
                " \"agitatormode\" : \"" + str_agitator_mode + "\"," +
                " \"agitatorod\" : \"" + str_auger_tube_od_mm + "\"," +
                " \"agitatorid\" : \"" + str_auger_tubeid_mm + "\"," +
                " \"agitatorpitch\" : \"" + str_auger_pitch_mm + "\"," +
                " \"targetacctype\" : \"" + strTargetAccuracy + "\"," +
                " \"targetaccval\" : \"" + str_target_accuracy_value + "\"," +
                " \"mcserialnum\" : \"" + str_machine_sr_no + "\"," +
                " \"prBulkMain\" : \"" + "0.5" + "\"," +
                " \"targetspeed\" : \"" + "100" + "\"," +
                " \"customerProduct\" : \"" + "No" + "\"," +
                " \"head\" : \"" + strHeadType + "\"," +
                " \"airPressure\" : \"" + str_air_pressure_bar + "\"," +
                " \"qtysetting\" : \"" + str_quantity_setting + "\"," +
                " \"bglensetting\" : \"" + str_bag_length_setting + "\"," +
                " \"fillValveGap\" : \"" + str_fill_valve_gap + "\"," +
                " \"fillvfdFreq\" : \"" + str_fill_vfd_frequency + "\"," +
                " \"horizCutSealSetting\" : \"" + "10" + "\"," +
                " \"horizBandSealSetting\" : \"" + str_horiz_band_seal_setting + "\"," +
                " \"vertSealSetting\" : \"" + str_vert_seal_setting + "\"," +
                " \"augerAgitatorFreq\" : \"" + str_agitator_freq_hz + "\"," +
                " \"augerFillValve\" : \"" + str_fill_valve_gap + "\"," +
                " \"augurPitch\" : \"" + str_auger_pitch_mm + "\"," +
                " \"pouchSealType\" : \"" + strSealType + "\"," +
                " \"pouchTempHZF\" : \"" + str_temp_hz_c + "\"," +
                " \"pouchTempHZB\" : \"" + str_temp_hz_back_c + "\"," +
                " \"pouchTempVert\" : \"" + str_temp_vert_c + "\"," +
                " \"pouchTempVertFrontLeft\" : \"" + str_temp_vert_front_left + "\"," +
                " \"pouchTempVertFrontRight\" : \"" + str_temp_vert_front_right + "\"," +
                " \"pouchTempVertBackLeft\" : \"" + str_temp_vert_back_left + "\"," +
                " \"pouchTempVertBackRight\" : \"" + str_temp_vert_back_right + "\"," +
                " \"pouchGusset\" : \"" + "10" + "\"," +
                " \"customerFilm\" : \"" + "No" + "\"," +
                " \"pouchFilmThickness\" : \"" + "10" + "\"," +
                " \"pouchPrBulk\" : \"" + "0.25" + "\"," +
                " \"pouchEqHorizSerrations\" : \"" + "na" + "\"," +
                " \"pouchFillEvent\" : \"" + "Yes" + "\"," +
                " \"negWt\" : \"" + "No" + "\"" + "}"
                + "}";
        String resFromDataLogger = "";

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(MobileJsonData.class, new MobileJsonDataDeserializer());

        Gson gson = gsonBuilder.create();

        MobileJsonData outputObj = gson.fromJson(tagInfoStr, MobileJsonData.class);

        Tag tagTest = outputObj.tagInfo;
        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        String dataLoggerName = outputObj.dataLoggerName;
        String dataLoggerIP = outputObj.dataLoggerIP;

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();

        String tagUniqueStr = dataLoggerName + ":" + tagTest.getOiNum() + ":" + tagTest.getTargetWt() + ":" + dateFormat.format(cal.getTime());
        tagTest.tagid = tagUniqueStr.trim();
        tagTest.tagid = tagUniqueStr.replaceAll(" ", "");

        TestRun testRun = new TestRun();
        testRun.head = tagTest.head;
        testRun.oinum = tagTest.oiNum;
        testRun.targetwt = tagTest.targetWt;
        testRun.tagid = tagTest.tagid;

        ArrayList<TestRun> existingTestRunObjs = new ArrayList<TestRun>();

/*        try{
           // deleteTestRunCompleted();
            existingTestRunObjs = getRunningTestInfo();

            String testStatus = getTestRunAllowedStatus(testRun, existingTestRunObjs);
            if(testStatus != null && testStatus.equals(Constants.TEST_APPEAL)){
                insertIntoTestRunsTbl(testRun);
                System.out.println("In test run appeal sent for ");
                //return HttpResponse.status(200).entity(Constants.TEST_APPEAL).build();
                return Constants.TEST_APPEAL;
            }
            if(testStatus != null && testStatus.equals(Constants.TEST_NOT_ALLOWED)){
                //return HttpResponse.status(200).entity(Constants.TEST_NOT_ALLOWED).build();
                return Constants.TEST_NOT_ALLOWED;
            }
        }catch(SQLException sqlEx){
            sqlEx.printStackTrace();
            Log.e("", "Exception occured and the stack trace is  "+ sqlEx.getStackTrace().toString(), sqlEx);
            //return HttpResponse.status(200).entity("").build();
            return "";

        }*/

        Log.d("Test", tagInfoStr);

        resToMobile = submitTagInputs(tagInfoStr);
        Log.d(TAG, "SaveTagInfo: ");
        return resToMobile;
    }

    private String submitTagInputs(String tagInfo) {
        Log.d(TAG, "Submit appealed tags called and message is  "+ tagInfo);

        String resFromDataLogger = "";
        String resToMobile = "";

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(MobileJsonData.class,
                new MobileJsonDataDeserializer());
        Gson gson = gsonBuilder.create();
        MobileJsonData outputObj = gson.fromJson(tagInfo, MobileJsonData.class);

        Tag tagTest = outputObj.tagInfo;
        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        String dataLoggerName = outputObj.dataLoggerName;
        String dataLoggerIP = outputObj.dataLoggerIP;
        //dataLoggerIP = "localhost";
        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();

        String tagUniqueStr = dataLoggerName + ":" + tagTest.getOiNum() + ":" + tagTest.getTargetWt() + ":" + dateFormat.format(cal.getTime());
        tagTest.tagid = tagUniqueStr.trim();
        tagTest.tagid = tagUniqueStr.replaceAll(" ", "");
        TestRun testRunObj = new TestRun();
        testRunObj.head = tagTest.head;
        testRunObj.oinum = tagTest.oiNum;
        testRunObj.targetwt = tagTest.targetWt;
        testRunObj.status = Constants.RUN_TEST_INPROGRESS_STATUS;
        testRunObj.tagid = tagTest.tagid;
        testRunObj.isAuthorized = "Yes";

//        //wakeup call
//        HttpClient httpClient = new DefaultHttpClient();
//        try {
//            insertIntoTestRunsTbl(testRunObj);
//
//            String url = "http://"+dataLoggerIP+":"+DATA_LOGGER_PORT+"/wakeup?tagid="+tagUniqueStr.trim()+ "&dataLoggerName=" + dataLoggerName;
//
//            HttpResponse response1 = httpClient.execute(new HttpGet(url));
//            StatusLine statusLine = response1.getStatusLine();
//            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
//                ByteArrayOutputStream out = new ByteArrayOutputStream();
//                response1.getEntity().writeTo(out);
//                resFromDataLogger = out.toString();
//                Toast.makeText(this, resFromDataLogger, Toast.LENGTH_SHORT).show();
//                out.close();
//            } else{
//                response1.getEntity().getContent().close();
//                throw new IOException(statusLine.getReasonPhrase());
//            }
//        } catch (IOException exception) {
//            exception.printStackTrace();
//        } catch (SQLException exception) {
//            exception.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//            //log.error("Exception occured and the stack trace is  "+ e.getStackTrace().toString(), e);
//        } finally {
//            httpClient.getConnectionManager().shutdown();
//        }//wakeup call

        try {
/*
            if (resFromDataLogger != null && resFromDataLogger.contains("ok"))
*/
            //log.debug("Got positive response from data logger, inserting record in database");
            //insertIntoTagsTbl(tagTest);
            resToMobile = tagTest.tagid;
            tagID = resToMobile;
            InsertTagIntoDb();
            //changeDataLoggerStatus(dataLoggerName, "Yes");

            Intent i = new Intent(RecallTagFillActivity.this,WeightReadingActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("dataloggerName",dataloggerName);
            bundle.putString("dataloggerIp",dataloggerIp);
            bundle.putString("current_tag_id",resToMobile);
            bundle.putString("oinum",str_oinum_no);
            bundle.putString("target_wt",str_target_weight);
            bundle.putString("targetwt_uom",strTargetWtUOM);
            i.putExtra("IntKgValue",intkgvalue);
            i.putExtra("LoggerMacId",dataloggerMacId);
            i.putExtras(bundle);
            startActivity(i);


        } catch (Exception ex) {
            System.out.println("The exception occured and the exception is  " + ex.getMessage());
            //log.error("Exception occured and exception is ", ex);
            resToMobile = "";
        }
        return resToMobile;
    }

  /*  private void insertIntoTagsTbl(Tag tagObj) throws SQLException {
        if (tagObj != null) {
            Connection conn = null;
            try {
                conn = getDbConnection();
                System.out.println("Got the connection");
                conn.setAutoCommit(false);
                PreparedStatement tagInsertStat = conn.prepareStatement(Queries.QUERY_INSERT_TAGTBL);
                tagInsertStat.setString(1, tagObj.tagid);
                tagInsertStat.setString(2, tagObj.oiNum);
                tagInsertStat.setString(3, tagObj.targetWt);
                tagInsertStat.setString(4, tagObj.fillerType);
                tagInsertStat.setString(5, tagObj.customer);
                tagInsertStat.setString(6, tagObj.machineType);
                tagInsertStat.setString(7, tagObj.productType);
                tagInsertStat.setString(8, tagObj.product);
                tagInsertStat.setString(9, tagObj.operator);
                tagInsertStat.setString(10, tagObj.targetWtUnit);
                tagInsertStat.setString(11, tagObj.inspector);
                tagInsertStat.setString(12, tagObj.filmType);
                tagInsertStat.setString(13, tagObj.productfeed);
                tagInsertStat.setString(14, tagObj.pouchWidth);
                tagInsertStat.setString(15, tagObj.pouchLength);
                tagInsertStat.setString(16, tagObj.agitatorMotorFreq);
                tagInsertStat.setString(17, tagObj.agitatorMode);
                tagInsertStat.setString(18, tagObj.agitatorod);
                tagInsertStat.setString(19, tagObj.agitatorid);
                tagInsertStat.setString(20, tagObj.agitatorPitch);
                tagInsertStat.setString(21, tagObj.targetacctype);
                tagInsertStat.setString(22, tagObj.targetaccval);
                tagInsertStat.setString(23, tagObj.mcSerialNum);
                tagInsertStat.setString(24, tagObj.prBulkMain);
                tagInsertStat.setString(25, tagObj.targetSpeed);
                tagInsertStat.setString(26, tagObj.customerProduct);
                tagInsertStat.setString(27, tagObj.head);
                tagInsertStat.setString(28, tagObj.airPressure);
                tagInsertStat.setString(29, tagObj.qtySetting);
                tagInsertStat.setString(30, tagObj.bgLenSetting);
                tagInsertStat.setString(31, tagObj.fillValveGap);
                tagInsertStat.setString(32, tagObj.fillVfdFreq);
                tagInsertStat.setString(33, tagObj.horizCutSealSetting);
                tagInsertStat.setString(34, tagObj.horizBandSealSetting);
                tagInsertStat.setString(35, tagObj.vertSealSetting);
                tagInsertStat.setString(36, tagObj.augerAgitatorFreq);
                tagInsertStat.setString(37, tagObj.augerFillValve);
                tagInsertStat.setString(38, tagObj.augurPitch);
                tagInsertStat.setString(39, tagObj.pouchSealType);
                tagInsertStat.setString(40, tagObj.pouchTempHZF);
                tagInsertStat.setString(41, tagObj.pouchTempHZB);
                tagInsertStat.setString(42, tagObj.pouchTempVert);
                tagInsertStat.setString(43, tagObj.pouchTempVertFrontLeft);
                tagInsertStat.setString(44, tagObj.pouchTempVertFrontRight);
                tagInsertStat.setString(45, tagObj.pouchTempVertBackLeft);
                tagInsertStat.setString(46, tagObj.pouchTempVertBackRight);
                tagInsertStat.setString(47, tagObj.pouchGusset);
                tagInsertStat.setString(48, tagObj.customerFilm);
                tagInsertStat.setString(49, tagObj.pouchFilmThickness);
                tagInsertStat.setString(50, tagObj.pouchPrBulk);
                tagInsertStat.setString(51, tagObj.pouchEqHorizSerrations);
                tagInsertStat.setString(52, tagObj.pouchFillEvent);
                tagInsertStat.setString(53, tagObj.negWt);
                tagInsertStat.setString(54, tagObj.fyyear);
                System.out.println("The created date formed is "+ new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
                tagInsertStat.setDate(55, new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

                tagInsertStat.executeUpdate();
                conn.commit();

            } catch (Exception ne) {
                ne.printStackTrace();
                System.out.println("Exception is " + ne.getCause().getMessage());
                conn.rollback();

            } finally {

                if (conn != null) {
                    conn.close();
                }
                conn = null;
            }

        }
    }

    private void insertIntoTestRunsTbl(TestRun testRunObj) throws SQLException {
        if (testRunObj != null) {
            Connection conn = null;
            try {

                conn = getDbConnection();
                conn.setAutoCommit(false);
                PreparedStatement testRunInsertStat = conn.prepareStatement("Insert into test_run_status (tagid, " + "oinum, targetwt, head, status, isauthorised) values (?,?,?,?,?,?)");
                testRunInsertStat.setString(1, testRunObj.tagid);
                testRunInsertStat.setString(2,  testRunObj.oinum);
                testRunInsertStat.setString(3,  testRunObj.targetwt);
                testRunInsertStat.setString(4,  testRunObj.head);
                testRunInsertStat.setString(5,  testRunObj.status);
                testRunInsertStat.setString(6,  testRunObj.isAuthorized);
                testRunInsertStat.executeUpdate();
                conn.commit();
                testRunInsertStat.close();

            } catch (Exception ne) {
                ne.printStackTrace();
                System.out.println("Exception is " + ne);
                conn.rollback();

            } finally {

                if (conn != null) {
                    conn.close();
                }
                conn = null;
            }

        }
    }

    private String getTestRunAllowedStatus(TestRun testRunObj, ArrayList<TestRun> existingTestRuns) {
        String status = Constants.TEST_ALLOWED;
        if(existingTestRuns != null && existingTestRuns.size() > 0){

            for(TestRun existingTestRun : existingTestRuns){

                if((existingTestRun.oinum.equals(testRunObj.oinum)) &&
                        (existingTestRun.targetwt.equals(testRunObj.targetwt))){

                    if(existingTestRun.head.equals(testRunObj.head)){
                        status = Constants.TEST_NOT_ALLOWED;
                    }

                    if(existingTestRun.head != null && existingTestRun.head.equals("Single")){
                        if(testRunObj.head != null && !testRunObj.head.equals("Single")){
                            status = Constants.TEST_NOT_ALLOWED;
                        }
                    }
                    if(existingTestRun.head != null && !existingTestRun.head.equals("Single")){
                        if(testRunObj.head != null && testRunObj.head.equals("Single")){
                            status = Constants.TEST_NOT_ALLOWED;
                        }
                    }

                }

                if((existingTestRun.oinum.equals(testRunObj.oinum)) &&
                        (!existingTestRun.targetwt.equals(testRunObj.targetwt))){

                    if(existingTestRun.head.equals(testRunObj.head)){
                        status = Constants.TEST_APPEAL;
                    }

                    if(existingTestRun.head != null && existingTestRun.head.equals("Single")){
                        if(testRunObj.head != null && !testRunObj.head.equals("Single")){
                            status = Constants.TEST_NOT_ALLOWED;
                        }
                    }
                    if(existingTestRun.head != null && !existingTestRun.head.equals("Single")){
                        if(testRunObj.head != null && testRunObj.head.equals("Single")){
                            status = Constants.TEST_NOT_ALLOWED;
                        }
                    }
                }
            }
        }
        return status;
    }

    public void deleteTestRunCompleted() throws SQLException {
        Connection conn = null;
        try {

            conn = getDbConnection();
            conn.setAutoCommit(false);
            PreparedStatement deleteTestRunStat = conn.prepareStatement("delete from test_run_status where status=? or status=?");
            deleteTestRunStat.setString(1, "COMPLETED");
            deleteTestRunStat.setString(2, "ABORTED");

            deleteTestRunStat.executeUpdate();
            conn.commit();
            deleteTestRunStat.close();

        } catch (Exception ne) {
            ne.printStackTrace();
            System.out.println("Exception is " + ne.getCause().getMessage());
            conn.rollback();

        } finally {

            if (conn != null) {
                conn.close();
            }
            conn = null;
        }
    }

    public ArrayList<TestRun> getRunningTestInfo() throws SQLException{

        Connection conn = null;
        ArrayList<TestRun> testRuns = new ArrayList<TestRun>();

        try{
            conn = getDbConnection();
            PreparedStatement getTestRunStat = conn.prepareStatement(QUERY_GET_TEST_RUNS_IN_PROGRESS);
            ResultSet results =  getTestRunStat.executeQuery();
            while (results.next()) {
                TestRun testRunObj = new TestRun();
                testRunObj.tagid = results.getString("tagid");
                testRunObj.head = results.getString("head");
                testRunObj.oinum = results.getString("oinum");
                testRunObj.targetwt = results.getString("targetwt");
                testRunObj.status = results.getString("status");
                testRunObj.isAuthorized = results.getString("isauthorised");
                testRuns.add(testRunObj);
            }
            results.close();
            getTestRunStat.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            conn = null;
        }
        return testRuns;
    }
*/
    public final Connection getDbConnection() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
        conn = DriverManager.getConnection(dbUrl,dbUserName,dbPassword);
        if (conn !=null){
            Log.d(TAG, "getDbConnection: "+"Database is connected to database");
        }else if (conn == null){
            Log.d(TAG, "getDbConnection: "+"Database is not connected to database");
        }
        return conn;
    }

    private void changeDataLoggerStatus(String dataLoggerName, String inuseStatus){
        Connection conn = null;
        try {
            conn = getDbConnection();
            conn.setAutoCommit(false);
            PreparedStatement updateDlStat = conn.prepareStatement("update mstr_logger_tbl set inuse=? where logger_name=? and isactive=1 and isdeleted=0");
            updateDlStat.setString(1,  inuseStatus);
            updateDlStat.setString(2, dataLoggerName);

            updateDlStat.executeUpdate();
            conn.commit();
            updateDlStat.close();

        }  catch (InstantiationException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            conn = null;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.cvMainDetails:
                if (llMainDetailForm.getVisibility() == View.GONE){
                    llMainDetailForm.setVisibility(View.VISIBLE);
                }else if (llMainDetailForm.getVisibility() == View.VISIBLE){
                    llMainDetailForm.setVisibility(View.GONE);
                }
                break;
            case R.id.cvLiquidDetails:
                strFillerType = spFillerType.getSelectedItem().toString();
                if (llLiquidDetailForm.getVisibility() == View.GONE){
                    llLiquidDetailForm.setVisibility(View.VISIBLE);
                }else if (llLiquidDetailForm.getVisibility() == View.VISIBLE){
                    llLiquidDetailForm.setVisibility(View.GONE);
                }
                break;
            case R.id.cvAugerDetails:
                strFillerType = spFillerType.getSelectedItem().toString();
                if (llAugerDetailForm.getVisibility() == View.GONE && strFillerType.equals("Auger")){
                    llAugerDetailForm.setVisibility(View.VISIBLE);
                }else if (llAugerDetailForm.getVisibility() == View.VISIBLE){
                    llAugerDetailForm.setVisibility(View.GONE);
                }
                break;
            case R.id.cvPouchAndSealDetails:
                if (llPouchAndSealDetailForm.getVisibility() == View.GONE){
                    llPouchAndSealDetailForm.setVisibility(View.VISIBLE);
                }else if (llPouchAndSealDetailForm.getVisibility() == View.VISIBLE){
                    llPouchAndSealDetailForm.setVisibility(View.GONE);
                }
                break;
            case R.id.btn_submit:
                getData();
                //getAllDataFromCTags();
                break;
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_sync:
                getData();
                break;
        }
    }

    public void InsertTagIntoDb() {

        String currentDateandTime = sdf.format(new Date());
//        SQLiteDatabase sqLiteDatabase = new DataBaseHelper(this).getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        double target_wt;

        contentValues.put(Constants.C_TAG_ID,1);
        contentValues.put(Constants.C_TAG_OEM_ID,1234);
        contentValues.put(Constants.C_TAG_MACID,dataloggerMacId);
        contentValues.put(Constants.C_TAG_TAGS_ID,tagID);
        contentValues.put(Constants.C_TAG_LOGGER_ID,dataloggerName);
        contentValues.put(Constants.C_TAG_OEM_NUMBER,str_oinum_no);
        if (strTargetWtUOM.equals("Kg")){
            target_wt = Double.parseDouble(str_target_weight) * floatkgvalue;
        }else {
            target_wt = Double.parseDouble(str_target_weight);
        }
        contentValues.put(Constants.C_TAG_TARGET_WIGHT,Float.parseFloat(String.valueOf(target_wt)));
        contentValues.put(Constants.C_TAG_CUSTOMER_NAME,str_customer_name);
        contentValues.put(Constants.C_TAG_MACHINE_TYPE_ID,strMachineModel.trim());
        contentValues.put(Constants.C_TAG_PRODUCT_TYPE_ID,str_switch_product_type);
        contentValues.put(Constants.C_TAG_MACHINE_TYPE_NAME,strMachineModel.trim());
        contentValues.put(Constants.C_TAG_PRODUCT_TYPE_NAME,str_switch_product_type);
        contentValues.put(Constants.C_TAG_FILLER_TYPE,strFillerType);
        contentValues.put(Constants.C_TAG_PRODUCT,strProductsUsedForTrail.trim());
        contentValues.put(Constants.C_TAG_OPERATOR,str_operator_name);
        contentValues.put(Constants.C_TAG_TARGET_WEIGHT_UNIT,strTargetWtUOM);
        contentValues.put(Constants.C_TAG_INSPECTOR,"Inspector");
        contentValues.put(Constants.C_TAG_FILEM_TYPE,str_switch_film_type);
        contentValues.put(Constants.C_TAG_PRODUCT_FEED,str_product_feed);
        contentValues.put(Constants.C_TAG_AGITATOR_MOTOR_FREQ,1);
        contentValues.put(Constants.C_TAG_AGITATOR_MODE,str_agitator_mode);
        contentValues.put(Constants.C_TAG_AGITATOR_OD,str_auger_tube_od_mm);
        contentValues.put(Constants.C_TAG_AGITATOR_ID,str_auger_tubeid_mm);
        contentValues.put(Constants.C_TAG_AGITATOR_PITCH,str_auger_pitch_mm);
        contentValues.put(Constants.C_TAG_TARGET_ACCURACY_TYPE,strTargetAccuracy.trim());
        contentValues.put(Constants.C_TAG_TARGET_ACCURACY_VALUE,Float.parseFloat(str_target_accuracy_value.trim()));
        contentValues.put(Constants.C_TAG_MACHINE_SERIAL_NUMBER,str_machine_sr_no);
        contentValues.put(Constants.C_TAG_PRODUCT_BULK_MAIN,str_bulk_Density);
        contentValues.put(Constants.C_TAG_TARGET_SPEED,Float.parseFloat(str_ed_target_speed_ppm));
        contentValues.put(Constants.C_TAG_HEAD,strHeadType);
        contentValues.put(Constants.C_TAG_AIR_PRESSURE,str_air_pressure_bar);
        contentValues.put(Constants.C_TAG_QUANTITY_SETTING,str_quantity_setting);
        contentValues.put(Constants.C_TAG_BAG_LENGTH_SETTING,str_bag_length_setting);
        contentValues.put(Constants.C_TAG_FILLVALVE_GAP,str_fill_valve_gap);
        contentValues.put(Constants.C_TAG_FILL_VFD_FREQUENCY,str_fill_vfd_frequency);
        contentValues.put(Constants.C_TAG_HORIZONTAL_CUT_SEAL_SETTING,str_horiz_seal_settings);
        contentValues.put(Constants.C_TAG_HORIZONTAL_BAND_SEAL_SETTING,str_horiz_band_seal_setting);
        contentValues.put(Constants.C_TAG_VERTICAL_SEAL_SETTING,str_vert_seal_setting);
        contentValues.put(Constants.C_TAG_AUGER_AGITATOR_FREQUENCY,str_agitator_freq_hz);
        contentValues.put(Constants.C_TAG_AUGER_FILL_VALVE,str_fill_valve_gap);
        contentValues.put(Constants.C_TAG_AUGER_PITCH,str_auger_pitch_mm);

        contentValues.put(Constants.C_TAG_POUCH_WIDTH,Float.parseFloat(str_pouch_width_mm));
        contentValues.put(Constants.C_TAG_POUCH_LENGTH,Float.parseFloat(str_pouch_seal_length_mm));
        contentValues.put(Constants.C_TAG_POUCH_SEAL_TYPE,strSealType.trim());

        contentValues.put(Constants.C_TAG_POUCH_TEMP_HORIZONTAL_FRONT,Float.parseFloat(str_temp_hz_c));
        contentValues.put(Constants.C_TAG_POUCH_TEMP_HORIZONTAL_BACK,Float.parseFloat(str_temp_hz_back_c));
        contentValues.put(Constants.C_TAG_POUCH_TEMP_VERTICAL,Float.parseFloat(str_temp_vert_c));
        contentValues.put(Constants.C_TAG_POUCH_TEMP_VERT_FRONT_LEFT,Float.parseFloat(str_temp_vert_front_left));
        contentValues.put(Constants.C_TAG_POUCH_TEMP_VERTICAL_FRONT_RIGHT,Float.parseFloat(str_temp_vert_front_right));
        contentValues.put(Constants.C_TAG_POUCH_TEMP_VERT_BACK_LEFT,Float.parseFloat(str_temp_vert_back_left));
        contentValues.put(Constants.C_TAG_POUCH_TEMP_VERT_BACK_RIGHT,Float.parseFloat(str_temp_vert_back_right));
        contentValues.put(Constants.C_TAG_POUCH_GUSSET,ed_pouch_guest_size.getText().toString());
        contentValues.put(Constants.C_TAG_POUCH_FILM_THICKNESS,Float.parseFloat(ed_film_thickness_microns.getText().toString()));
        contentValues.put(Constants.C_TAG_PUCH_PRODUCT_BULK,str_bulk_Density);
        contentValues.put(Constants.C_TAG_POUCH_EQ_HORIZ_SERRATIONS,ed_equal_horiz_serrations.getText().toString());

        contentValues.put(Constants.C_TAG_MODIFIED_DATE,currentDateandTime);
        contentValues.put(Constants.C_TAG_OVERALL_STATUS,"");
        contentValues.put(Constants.C_TAG_REMARK,"");
        contentValues.put(Constants.C_TAG_SEAL_STATUS,"");
        contentValues.put(Constants.C_TAG_DROP_STATUS,"");
        contentValues.put(Constants.C_TAG_TARGET_SPEED_STATUS,"");
        contentValues.put(Constants.C_TAG_WEIGHT_TEST_STATUS,"");
        contentValues.put(Constants.C_TAG_BAG_STATUS,"");
        contentValues.put(Constants.C_TAG_PAPER_STATUS,"");
        contentValues.put(Constants.C_TAG_FY_YEAR,strFinancialYear);
        contentValues.put(Constants.C_TAG_CREATED_DATE,currentDateandTime);
        //contentValues.put(Constants.C_TAG_SEAL_REJECTION_REASON,"");

        if (str_cust_product.equals("Yes")) {
            contentValues.put(Constants.C_TAG_CUSTOMER_PRODUCT,0);
        }
        else {
            contentValues.put(Constants.C_TAG_CUSTOMER_PRODUCT,1);

        }

        if (str_switch_negative_wt.equals("Yes"))
        {
            contentValues.put(Constants.C_TAG_NEGATIVE_WEIGHT,0);

        }
        else
        {
            contentValues.put(Constants.C_TAG_NEGATIVE_WEIGHT,1);

        }
        if (str_switch_film_event.equals("Web"))
        {
            contentValues.put(Constants.C_TAG_POUCH_FILL_eVENT,1);

        }
        else
        {
            contentValues.put(Constants.C_TAG_POUCH_FILL_eVENT,0);

        }

        contentValues.put(Constants.C_TAG_LOGGER_CREATE_TIMESTAMP,currentDateandTime);

        contentValues.put(Constants.C_TAGS_IS_SYNC,0);
        long result = db.insert(Constants.C_TAGS_TABLE,null,contentValues);
        if (result == -1){
            Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
            //wakeup();
        }
    }
    public void getTagInfoAgainstOinum()
    {
        Cursor taginfo = helper.getTagInfoAgainstoinum(tag_so_oinum);
        RecallTestTagInfoModel model = new RecallTestTagInfoModel();

        while (taginfo.moveToNext()){

            String tagid= taginfo.getString(0);
            String oemid= taginfo.getString(1);
            String macid= taginfo.getString(2);
            String loggerid= taginfo.getString(3);
            String oinumber= taginfo.getString(5);
            String targetweight= taginfo.getString(6);
            String customername= taginfo.getString(8);
            String producttype= taginfo.getString(10);
            String product= taginfo.getString(11);
            String operator= taginfo.getString(12);
            String targetweightunit= taginfo.getString(13);
            String targetaccuracytype= taginfo.getString(24);
            String targetaccuracyvalue= taginfo.getString(25);
            String machinesrno= taginfo.getString(26);
            String productbulk_main= taginfo.getString(27);
            String targetspeed= taginfo.getString(28);
            String customer_product= taginfo.getString(29);
            String head= taginfo.getString(30);

            ed_target_weight.setText(targetweight);
            ed_customer_name.setText(customername);
            ed_operator_name.setText(operator);
            ed_target_accuracy_value.setText(targetaccuracyvalue);
            ed_machine_sr_no.setText(machinesrno);
            ed_bulk_density.setText(productbulk_main);
            ed_target_speed_ppm.setText(targetspeed);
//            ed_agitator_freq_hz.setText(taginfo.getString());
//            ed_air_pressure_bar.setText(taginfo.getString());





            Log.d("TAG", "getSealTypes: ");
        }
    }

    private void getProductionGrammageValues() {
        Cursor grammageValues = helper.getProductionGrammageValues();
        while (grammageValues.moveToNext()){
            String kgid = grammageValues.getString(0);
            String kgname = grammageValues.getString(1);
            String kgvalues = grammageValues.getString(2);

            grammage_values_list.add(kgvalues);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }
    }
}
