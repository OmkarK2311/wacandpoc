package com.brainlines.weightloggernewversion.Production.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductionTagModel implements Parcelable {

    String ProductionTagID;
    //String ProductionTagVal;
    String ProductionTagVal;
    String OEMID;
    String MacID;
    String SO;
    String ProductID;
    String GmPerPouch;
    String Unit;
    String TotalQTY;
    String TargetAccurancyType;
    String TargetAccurancyValue;
    String IsNegAllowed;
    String TagModifiedDate,TagShift,TagNumberOfPouches,TagPositiveTolerance,NegativeTolerance;
    String IsSync;


    public ProductionTagModel(String productionTag, String OEMID, String macID, String SO, String productID, String gmPerPouch, String unit, String totalQTY, String targetAccuracyType, String targetAccuracyValue, String isNegAllowed, String tagModifiedDate, String tagShift, String tagNumberOfPouches, String tagPositiveTolerance, String negativeTolerance) {
        //ProductionTagID = productionTagID;
        ProductionTagVal = productionTag;
        this.OEMID = OEMID;
        MacID = macID;
        this.SO = SO;
        ProductID = productID;
        GmPerPouch = gmPerPouch;
        Unit = unit;
        TotalQTY = totalQTY;
        TargetAccurancyType = targetAccuracyType;
        TargetAccurancyType = targetAccuracyValue;
        IsNegAllowed = isNegAllowed;
        TagModifiedDate = tagModifiedDate;
        TagShift = tagShift;
        TagNumberOfPouches = tagNumberOfPouches;
        TagPositiveTolerance = tagPositiveTolerance;
        NegativeTolerance = negativeTolerance;
        //IsSync = isSync;
    }

    protected ProductionTagModel(Parcel in) {
        ProductionTagID = in.readString();
        ProductionTagVal = in.readString();
        OEMID = in.readString();
        MacID = in.readString();
        SO = in.readString();
        ProductID = in.readString();
        GmPerPouch = in.readString();
        Unit = in.readString();
        TotalQTY = in.readString();
        TargetAccurancyType = in.readString();
        TargetAccurancyType = in.readString();
        IsNegAllowed = in.readString();
        TagModifiedDate = in.readString();
        TagShift = in.readString();
        TagNumberOfPouches = in.readString();
        TagPositiveTolerance = in.readString();
        NegativeTolerance = in.readString();
        IsSync = in.readString();
    }

    public static final Creator<ProductionTagModel> CREATOR = new Creator<ProductionTagModel>() {
        @Override
        public ProductionTagModel createFromParcel(Parcel in) {
            return new ProductionTagModel(in);
        }

        @Override
        public ProductionTagModel[] newArray(int size) {
            return new ProductionTagModel[size];
        }
    };

    public String getProductionTagID() {
        return ProductionTagID;
    }

    public void setProductionTagID(String productionTagID) {
        ProductionTagID = productionTagID;
    }

    public String getProductionTag() {
        return ProductionTagVal;
    }

    public void setProductionTag(String productionTag) {
        ProductionTagVal = productionTag;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getSO() {
        return SO;
    }

    public void setSO(String SO) {
        this.SO = SO;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getGmPerPouch() {
        return GmPerPouch;
    }

    public void setGmPerPouch(String gmPerPouch) {
        GmPerPouch = gmPerPouch;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getTotalQTY() {
        return TotalQTY;
    }

    public void setTotalQTY(String totalQTY) {
        TotalQTY = totalQTY;
    }

    public String getTargetAccuracyType() {
        return TargetAccurancyType;
    }

    public void setTargetAccuracyType(String targetAccuracyType) {
        TargetAccurancyType = targetAccuracyType;
    }

    public String getTargetAccuracyValue() {
        return TargetAccurancyType;
    }

    public void setTargetAccuracyValue(String targetAccuracyValue) {
        TargetAccurancyType = targetAccuracyValue;
    }

    public String getIsNegAllowed() {
        return IsNegAllowed;
    }

    public void setIsNegAllowed(String isNegAllowed) {
        IsNegAllowed = isNegAllowed;
    }

    public String getTagModifiedDate() {
        return TagModifiedDate;
    }

    public void setTagModifiedDate(String tagModifiedDate) {
        TagModifiedDate = tagModifiedDate;
    }

    public String getTagShift() {
        return TagShift;
    }

    public void setTagShift(String tagShift) {
        TagShift = tagShift;
    }

    public String getTagNumberOfPouches() {
        return TagNumberOfPouches;
    }

    public void setTagNumberOfPouches(String tagNumberOfPouches) {
        TagNumberOfPouches = tagNumberOfPouches;
    }

    public String getTagPositiveTolerance() {
        return TagPositiveTolerance;
    }

    public void setTagPositiveTolerance(String tagPositiveTolerance) {
        TagPositiveTolerance = tagPositiveTolerance;
    }

    public String getNegativeTolerance() {
        return NegativeTolerance;
    }

    public void setNegativeTolerance(String negativeTolerance) {
        NegativeTolerance = negativeTolerance;
    }

    public String getIsSync() {
        return IsSync;
    }

    public void setIsSync(String isSync) {
        IsSync = isSync;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ProductionTagID);
        dest.writeString(ProductionTagVal);
        dest.writeString(OEMID);
        dest.writeString(MacID);
        dest.writeString(SO);
        dest.writeString(ProductID);
        dest.writeString(GmPerPouch);
        dest.writeString(Unit);
        dest.writeString(TotalQTY);
        dest.writeString(TargetAccurancyType);
        dest.writeString(TargetAccurancyValue);
        dest.writeString(IsNegAllowed);
        dest.writeString(TagModifiedDate);
        dest.writeString(TagShift);
        dest.writeString(TagNumberOfPouches);
        dest.writeString(TagPositiveTolerance);
        dest.writeString(NegativeTolerance);
        dest.writeString(IsSync);
    }
}
