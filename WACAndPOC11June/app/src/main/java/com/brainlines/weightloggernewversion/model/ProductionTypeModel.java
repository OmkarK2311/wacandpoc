package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductionTypeModel implements Parcelable {

    int OEMID,ProductTypeID,UOMID;
    boolean IsActive,IsDeleted;
    String CreatedBy,ModifiedBy,UOM,CreatedDate,ModifiedDate,ProductType;

    public ProductionTypeModel(int OEMID, int productTypeID, int UOMID, boolean isActive, boolean isDeleted, String createdBy, String modifiedBy, String UOM, String createdDate, String modifiedDate, String productType) {
        this.OEMID = OEMID;
        ProductTypeID = productTypeID;
        this.UOMID = UOMID;
        IsActive = isActive;
        IsDeleted = isDeleted;
        CreatedBy = createdBy;
        ModifiedBy = modifiedBy;
        this.UOM = UOM;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
        ProductType = productType;
    }

    protected ProductionTypeModel(Parcel in) {
        OEMID = in.readInt();
        ProductTypeID = in.readInt();
        UOMID = in.readInt();
        IsActive = in.readByte() != 0;
        IsDeleted = in.readByte() != 0;
        CreatedBy = in.readString();
        ModifiedBy = in.readString();
        UOM = in.readString();
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
        ProductType = in.readString();
    }

    public static final Creator<ProductionTypeModel> CREATOR = new Creator<ProductionTypeModel>() {
        @Override
        public ProductionTypeModel createFromParcel(Parcel in) {
            return new ProductionTypeModel(in);
        }

        @Override
        public ProductionTypeModel[] newArray(int size) {
            return new ProductionTypeModel[size];
        }
    };

    public int getOEMID() {
        return OEMID;
    }

    public void setOEMID(int OEMID) {
        this.OEMID = OEMID;
    }

    public int getProductTypeID() {
        return ProductTypeID;
    }

    public void setProductTypeID(int productTypeID) {
        ProductTypeID = productTypeID;
    }

    public int getUOMID() {
        return UOMID;
    }

    public void setUOMID(int UOMID) {
        this.UOMID = UOMID;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getUOM() {
        return UOM;
    }

    public void setUOM(String UOM) {
        this.UOM = UOM;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(OEMID);
        dest.writeInt(ProductTypeID);
        dest.writeInt(UOMID);
        dest.writeByte((byte) (IsActive ? 1 : 0));
        dest.writeByte((byte) (IsDeleted ? 1 : 0));
        dest.writeString(CreatedBy);
        dest.writeString(ModifiedBy);
        dest.writeString(UOM);
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
        dest.writeString(ProductType);
    }
}
