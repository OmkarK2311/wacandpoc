package com.brainlines.weightloggernewversion.Production.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class PWeighReadingsModel implements Parcelable {
    String PWeightReadingID,OEMID,MacID,LoggerID,BatchId,Weight,Shift,TargetGmPerPouch,ActualGmPerPouch,ExcessGiveawayPerPouch,CumilativeExcessGiveaway;
    String CumilativeQuantity,CumilativePouches,ModifiedDate,NegetiveGigiveawayFromTag;
    String Status,CreatedDate,CreatedBy,ModifiedBy,srno;


    public PWeighReadingsModel() {
    }


    public String getPWeightReadingID() {
        return PWeightReadingID;
    }

    public void setPWeightReadingID(String PWeightReadingID) {
        this.PWeightReadingID = PWeightReadingID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getBatchId() {
        return BatchId;
    }

    public String getSrno() {
        return srno;
    }

    public void setSrno(String srno) {
        this.srno = srno;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }





    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }






    public void setBatchId(String batchId) {
        BatchId = batchId;
    }





    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getShift() {
        return Shift;
    }

    public void setShift(String shift) {
        Shift = shift;
    }

    public String getTargetGmPerPouch() {
        return TargetGmPerPouch;
    }

    public void setTargetGmPerPouch(String targetGmPerPouch) {
        TargetGmPerPouch = targetGmPerPouch;
    }

    public String getActualGmPerPouch() {
        return ActualGmPerPouch;
    }

    public void setActualGmPerPouch(String actualGmPerPouch) {
        ActualGmPerPouch = actualGmPerPouch;
    }

    public String getExcessGiveawayPerPouch() {
        return ExcessGiveawayPerPouch;
    }

    public void setExcessGiveawayPerPouch(String excessGiveawayPerPouch) {
        ExcessGiveawayPerPouch = excessGiveawayPerPouch;
    }

    public String getCumilativeExcessGiveaway() {
        return CumilativeExcessGiveaway;
    }

    public void setCumilativeExcessGiveaway(String cumilativeExcessGiveaway) {
        CumilativeExcessGiveaway = cumilativeExcessGiveaway;
    }

    public String getCumilativeQuantity() {
        return CumilativeQuantity;
    }

    public void setCumilativeQuantity(String cumilativeQuantity) {
        CumilativeQuantity = cumilativeQuantity;
    }

    public String getCumilativePouches() {
        return CumilativePouches;
    }

    public void setCumilativePouches(String cumilativePouches) {
        CumilativePouches = cumilativePouches;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }




    public String getNegetiveGigiveawayFromTag() {
        return NegetiveGigiveawayFromTag;
    }

    public void setNegetiveGigiveawayFromTag(String negetiveGigiveawayFromTag) {
        NegetiveGigiveawayFromTag = negetiveGigiveawayFromTag;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public static Creator<PWeighReadingsModel> getCREATOR() {
        return CREATOR;
    }

    public PWeighReadingsModel(Parcel in) {
        PWeightReadingID = in.readString();
        OEMID = in.readString();
        MacID = in.readString();
        LoggerID = in.readString();
        BatchId = in.readString();
        Weight = in.readString();
        Shift = in.readString();
        TargetGmPerPouch = in.readString();
        ActualGmPerPouch = in.readString();
        ExcessGiveawayPerPouch = in.readString();
        CumilativeExcessGiveaway = in.readString();
        CumilativeQuantity = in.readString();
        CumilativePouches = in.readString();
        ModifiedDate = in.readString();
        NegetiveGigiveawayFromTag = in.readString();
        Status = in.readString();
        CreatedDate = in.readString();
        CreatedBy = in.readString();
        ModifiedBy = in.readString();
    }

    public static final Creator<PWeighReadingsModel> CREATOR = new Creator<PWeighReadingsModel>() {
        @Override
        public PWeighReadingsModel createFromParcel(Parcel in) {
            return new PWeighReadingsModel(in);
        }

        @Override
        public PWeighReadingsModel[] newArray(int size) {
            return new PWeighReadingsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(PWeightReadingID);
        dest.writeString(OEMID);
        dest.writeString(MacID);
        dest.writeString(LoggerID);
        dest.writeString(BatchId);
        dest.writeString(Weight);
        dest.writeString(Shift);
        dest.writeString(TargetGmPerPouch);
        dest.writeString(ActualGmPerPouch);
        dest.writeString(ExcessGiveawayPerPouch);
        dest.writeString(CumilativeExcessGiveaway);
        dest.writeString(CumilativeQuantity);
        dest.writeString(CumilativePouches);
        dest.writeString(ModifiedDate);
        dest.writeString(NegetiveGigiveawayFromTag);
        dest.writeString(Status);
        dest.writeString(CreatedDate);
        dest.writeString(CreatedBy);
        dest.writeString(ModifiedBy);
    }
}
