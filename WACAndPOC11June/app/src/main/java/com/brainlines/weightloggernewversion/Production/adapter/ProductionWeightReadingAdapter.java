package com.brainlines.weightloggernewversion.Production.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.Model.PWeighReadingsModel;
import com.brainlines.weightloggernewversion.Production.Model.ProductionTagHistoryListModel;
import com.brainlines.weightloggernewversion.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ProductionWeightReadingAdapter extends RecyclerView.Adapter<ProductionWeightReadingAdapter.MyViewHolder> {

    public String TAG = "WorkDetailsListener";
    Context context;
    ArrayList<PWeighReadingsModel> weight_reading = new ArrayList<>();


    public ProductionWeightReadingAdapter(Context context, ArrayList<PWeighReadingsModel> weight_reading) {
        this.context = context;
        this.weight_reading = weight_reading;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //View view = LayoutInflater.from(context).inflate(R.layout.item_weight_reading, parent, false);
        View view = LayoutInflater.from(context).inflate(R.layout.item_batch_report_details, parent, false);
        return new ProductionWeightReadingAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
       PWeighReadingsModel model = weight_reading.get(position);
        DecimalFormat decimalFormat = new DecimalFormat("0.000");

        String weight = model.getWeight() ;
            Double dwtreading = Double.valueOf(weight);
            String status = model.getStatus();
            String sr_no = model.getPWeightReadingID();

            String excess = model.getExcessGiveawayPerPouch();
            Double dexcess = Double.parseDouble(excess);
            String date_time = model.getCreatedDate();
            String shift = model.getShift();
            holder.batch_report_excess.setText(decimalFormat.format(dexcess));
            holder.batch_report_date_time.setText(date_time);
            holder.batch_report_readin_sr_no.setText(sr_no);

            //int kgweight = Integer.parseInt(weight)/1000;
            //int kgexcess = Integer.parseInt(excess)/1000;

            if (status.equals("Okay"))
            {

                holder.batch_report_weight.setText(decimalFormat.format(dwtreading));
                holder.batch_report_status.setText("Ok");

            }
            else
            {
                holder.batch_report_weight.setText(decimalFormat.format(dwtreading));
                holder.batch_report_status.setText("Ign");
                holder.batch_report_weight.setTextColor(context.getResources().getColor(R.color.color_2));
            }




    }

    @Override
    public int getItemCount() {
        return weight_reading.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        //TextView txt_degree;
        TextView batch_report_readin_sr_no,batch_report_weight,batch_report_excess,batch_report_status,batch_report_date_time;

        LinearLayout linear_background;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            batch_report_readin_sr_no = (itemView).findViewById(R.id.batch_report_readin_sr_no1);
            batch_report_weight = (itemView).findViewById(R.id.batch_report_weight);
            batch_report_excess = (itemView).findViewById(R.id.batch_report_excess);
            batch_report_status = (itemView).findViewById(R.id.batch_report_status);
            batch_report_date_time = (itemView).findViewById(R.id.batch_report_date_time);
            /*txt_degree = (itemView).findViewById(R.id.txt_weight_reading);
            linear_background = (itemView).findViewById(R.id.linear_background);*/
        }
    }
}
