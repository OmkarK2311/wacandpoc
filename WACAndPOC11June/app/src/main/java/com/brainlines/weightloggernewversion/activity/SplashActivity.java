package com.brainlines.weightloggernewversion.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.brainlines.weightloggernewversion.CalibrationOrProductionActivity;
import com.brainlines.weightloggernewversion.NewTestActivity;
import com.brainlines.weightloggernewversion.Permission.PermissionResultCallback;
import com.brainlines.weightloggernewversion.Permission.PermissionUtils;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.model.UserLoginModel;
import com.brainlines.weightloggernewversion.utils.AppPreferences;

import java.util.ArrayList;

@SuppressLint("Registered")
public class SplashActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback, PermissionResultCallback {

    private final int SPLASH_DISPLAY_LENGHT = 1000;
    PermissionUtils permissionUtils;
    public ArrayList<UserLoginModel> users_details = new ArrayList<UserLoginModel>();


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash_screen);
        //initUi();
        lauchActivity();

        //custom code goes here
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // redirects to utils
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    // Callback functions
    @Override
    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION", "GRANTED");
        lauchActivity();
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY", "GRANTED");
        lauchActivity();
    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION", "DENIED");
        lauchActivity();
    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION", "NEVER ASK AGAIN");
        lauchActivity();
    }

    private void lauchActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String user_name = "", user_token = "";
                user_name= AppPreferences.getUser(SplashActivity.this);
                user_token=AppPreferences.getPassword(SplashActivity.this);


                if (!user_name.equals("null") && !user_token.equals("")) {

                    Intent mainintent = new Intent(SplashActivity.this, CalibrationOrProductionActivity.class);
                    Bundle bun=new Bundle();
                    bun.putString("username",user_name);
                    bun.putString("userpassword",user_token);

                    mainintent.putExtras(bun);
                    startActivity(mainintent);
                }
                else {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(mainIntent);
                }
            }
        }, SPLASH_DISPLAY_LENGHT);
    }


   /* public void setData() {
        DBHelper dbHelper = new DBHelper(launch_activity.this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        Cursor cursor = database.query(Table_Constants.TABLE_USER_DETAILS, null, null, null, null, null, null);

        if (cursor.getCount() > 0) {
            if (!cursor.isAfterLast()) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    User_Details_Model model = new User_Details_Model();

                    String user_email = cursor.getString(cursor.getColumnIndex(Table_Constants.USER_EMAILID));
                    String user_token = cursor.getString(cursor.getColumnIndex(Table_Constants.USER_TOKEN));
                    model.setAcess_token(user_token);
                    model.setUser_name(user_email);

                    users_details.add(model);
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
    }*/
}
