package com.brainlines.weightloggernewversion.Production.activity;

import android.app.AlertDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.adapter.MatchCloneTagListener;
import com.brainlines.weightloggernewversion.Production.adapter.MatchCloneTagModel;
import com.brainlines.weightloggernewversion.Production.adapter.Match_Clone_Tag_Adapter;
import com.brainlines.weightloggernewversion.Production.adapter.ProductionTagListAdapter;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;

import java.util.ArrayList;

public class BatchHistoryDetailsActivity extends AppCompatActivity implements MatchCloneTagListener {

    DataBaseHelper helper;
    String selected_date,str_shift;
    TextView txt_history_product,txt_history_negative_wt_allowed,txt_history_operator_name,txt_history_shift,txt_history_date,txt_history_total_qty_uom;
    TextView txt_history_uom,txt_history_gm_pouch,txt_history_total_qty;
    ImageButton btnhistorydetails,btnhistoryCancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_information);
//        helper = new DataBaseHelper(BatchHistoryDetailsActivity.this);
        initUi();
        selected_date = getIntent().getStringExtra("selected_date");
        str_shift = getIntent().getStringExtra("shift");
        getSelectedProductionTagDetails(selected_date,str_shift);

    }
    private void initUi()
    {
        txt_history_product = findViewById(R.id.txt_history_product);
        txt_history_negative_wt_allowed = findViewById(R.id.txt_history_negative_wt_allowed);
        txt_history_operator_name = findViewById(R.id.txt_history_operator_name);
        txt_history_shift = findViewById(R.id.txt_history_shift);
        txt_history_date = findViewById(R.id.txt_history_date);
        txt_history_total_qty_uom = findViewById(R.id.txt_history_total_qty_uom);
        txt_history_uom = findViewById(R.id.txt_history_uom);
        txt_history_gm_pouch = findViewById(R.id.txt_history_gm_pouch);
        txt_history_total_qty = findViewById(R.id.txt_history_total_qty);
        btnhistorydetails = findViewById(R.id.btnhistorydetails);
        btnhistoryCancel = findViewById(R.id.btnhistoryCancel);

    }

    private void getSelectedProductionTagDetails(String str_selected_date, String str_shift) {
        Cursor p_tag = helper.getSelectedProductionTagDetails(str_selected_date, str_shift);
        while (p_tag.moveToNext()){
            String p_tag_string = p_tag.getString(0);
            String product_name= p_tag.getString(1);
            String gm_pouch = p_tag.getString(2);
            String uom = p_tag.getString(3);
            String total_qty = p_tag.getString(4);
            String date = p_tag.getString(5);
            String shift = p_tag.getString(6);
            String negative_allowed = p_tag.getString(7);

            txt_history_product.setText(product_name); txt_history_gm_pouch.setText(gm_pouch);
            txt_history_uom.setText(uom); txt_history_total_qty.setText(total_qty);
            txt_history_date.setText(date); txt_history_shift.setText(shift);
            txt_history_negative_wt_allowed.setText(negative_allowed);

            Log.d("TAG", "p_tag: ");
        }
    }

    @Override
    public void tagClick(MatchCloneTagModel model) {

        if (model != null)
        {
            ViewGroup viewGroup = findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(BatchHistoryDetailsActivity.this).inflate(R.layout.log_information, viewGroup, false);
            AlertDialog.Builder builder = new AlertDialog.Builder(BatchHistoryDetailsActivity.this);
            builder.setView(dialogView);
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();

        }

    }
}
