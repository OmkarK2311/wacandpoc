package com.brainlines.weightloggernewversion.listener;

import com.brainlines.weightloggernewversion.model.UserModel;

public interface UserDataListener {
    void sendUserData(UserModel model);
}
