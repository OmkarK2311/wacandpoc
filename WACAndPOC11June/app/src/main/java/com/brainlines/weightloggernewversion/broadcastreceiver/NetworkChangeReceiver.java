package com.brainlines.weightloggernewversion.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.listener.ConnectionCheckingInterface;
import com.brainlines.weightloggernewversion.utils.CommonMethods;


public class NetworkChangeReceiver extends BroadcastReceiver {
    private ConnectionCheckingInterface listener;

    public NetworkChangeReceiver(ConnectionCheckingInterface listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try
        {
            if (checkInternet(context)) {
                //dialog(true);
                listener.connectionLost(true);
                Log.e("keshav", "Online Connect Intenet ");
            } else {
                //dialog(false);
                listener.connectionLost(false);
                Log.e("keshav", "Conectivity Failure !!! ");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private boolean checkInternet(Context context) {
        CommonMethods serviceManager = new CommonMethods(context);
        if (serviceManager.isNetworkAvailable()) {
            return true;
        } else {
            return false;
        }
    }
}
