package com.brainlines.weightloggernewversion.activity.newtest;

import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoggerFunctionActivity;
import com.brainlines.weightloggernewversion.broadcastreceiver.NetworkChangeReceiver;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class NewTestObservationsActivity extends BaseActivity implements View.OnClickListener {
    EditText ed_auger_count,ed_auger_speed_rpm,ed_pre_auger_used,ed_no_of_agitator,ed_vertical_overlap_mm,ed_observed_speed,ed_wire_cup;
    EditText ed_hz_jaw_movement,ed_vertical_assly_teflon_tape_shift,ed_hz_jaw_opening,ed_hz_cut_seal_Setting;
    Spinner spin_pouch_symmetry,spin_seal_failure_reasons;

    SwitchCompat switch_vert_jaw_movement,switch_vertical_alignment_centre,switch_empty_pouches,switch_perfortation,switch_drop_test,switch_seal_quality;
    String str_auger_count,str_auger_speed_rpm,str_pre_auger_used,str_no_of_agitator,str_vertical_overlap_mm,str_observed_speed,str_wire_cup;
    String str_hz_jaw_movement,str_vertical_assly_teflon_tape_shift,str_hz_jaw_opening,str_hz_cut_seal_Setting;
    String str_vert_jaw_movement,str_vertical_alignment_centre,str_empty_pouches,str_perfortation,str_drop_test,str_seal_quality,so_oinum="",rejectionReason;
    ImageButton btn_enter_comments,btn_observation_abort;
    Button btn_sync;
    String dataloggerName,dataloggerIp,current_tag_id,dataloggerMacId,str_pouch_symmetry;
    LinearLayout llAugerDetailForm,llMainDetailForm,llLiquidDetailForm;
    CardView cvMainDetails,cvLiquidDetails,cvAugerDetails;

    ArrayList<String> rejectReasonList = new ArrayList<>();
    DataBaseHelper helper;
    private NetworkChangeReceiver networkChangeReceiver;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private String status = "",weight_test_status,str_drop_test_status,str_seal_test_status,str_test_speed_status,str_target_speed,str_tarhetwt_uom = "";
    private ArrayList<String> pouchSymmetryList = new ArrayList<>();
    double str_kg_value_from_local_db;
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_test_observations);

        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        initUi();
        dataloggerName = getIntent().getStringExtra("LoggerName");
        dataloggerIp = getIntent().getStringExtra("LoggerIP");
        dataloggerMacId = getIntent().getStringExtra("mac_id");
        current_tag_id = getIntent().getStringExtra("current_tag_id");
        so_oinum = getIntent().getStringExtra("so_oinum");
        weight_test_status = getIntent().getStringExtra("weight_test_status");
        str_tarhetwt_uom = getIntent().getStringExtra("targetwt_uom");
        str_kg_value_from_local_db = getIntent().getDoubleExtra("IntKgValue",0.0d);


        spin_seal_failure_reasons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rejectionReason = spin_seal_failure_reasons.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                rejectionReason = "";
            }
        });

        Cursor cursor = helper.getTargerSpeedFromC_TagTable(current_tag_id);
        while (cursor.moveToNext())
        {
            str_target_speed = cursor.getString(0);
        }

    }

    private void initUi() {
        spin_pouch_symmetry = findViewById(R.id.spin_pouch_symmetry);
        spin_seal_failure_reasons = findViewById(R.id.spin_seal_failure_reasons);

        switch_vert_jaw_movement = findViewById(R.id.switch_vert_jaw_movement);
        switch_vertical_alignment_centre = findViewById(R.id.switch_vertical_alignment_centre);
        switch_empty_pouches = findViewById(R.id.switch_empty_pouches);
        switch_perfortation = findViewById(R.id.switch_perfortation);
        switch_drop_test = findViewById(R.id.switch_drop_test);
        switch_seal_quality = findViewById(R.id.switch_seal_quality);
        btn_sync = findViewById(R.id.btn_sync);

        ed_auger_count = findViewById(R.id.ed_auger_count);
        ed_auger_speed_rpm = findViewById(R.id.ed_auger_speed_rpm);
        ed_pre_auger_used = findViewById(R.id.ed_pre_auger_used);
        ed_no_of_agitator = findViewById(R.id.ed_no_of_agitator);
        ed_vertical_overlap_mm = findViewById(R.id.ed_vertical_overlap_mm);
        ed_observed_speed = findViewById(R.id.ed_observed_speed);
        ed_wire_cup = findViewById(R.id.ed_wire_cup);
        ed_hz_jaw_movement = findViewById(R.id.ed_hz_jaw_movement);
        ed_vertical_assly_teflon_tape_shift = findViewById(R.id.ed_vertical_assly_teflon_tape_shift);
        ed_hz_jaw_opening = findViewById(R.id.ed_hz_jaw_opening);
        ed_hz_cut_seal_Setting = findViewById(R.id.ed_hz_cut_seal_Setting);

        llAugerDetailForm = findViewById(R.id.llAugerDetailForm);
        llMainDetailForm = findViewById(R.id.llMainDetailForm);
        llLiquidDetailForm = findViewById(R.id.llLiquidDetailForm);
        cvMainDetails = findViewById(R.id.cvMainDetails);
        cvLiquidDetails = findViewById(R.id.cvLiquidDetails);
        cvAugerDetails = findViewById(R.id.cvAugerDetails);

        btn_enter_comments = findViewById(R.id.btn_enter_comments);
        btn_observation_abort = findViewById(R.id.btn_observation_abort);
        btn_enter_comments.setOnClickListener(this);
        btn_observation_abort.setOnClickListener(this);
        btn_sync.setOnClickListener(this);

        cvMainDetails.setOnClickListener(this);
        cvLiquidDetails.setOnClickListener(this);
        cvAugerDetails.setOnClickListener(this);

        getSealRejectionList();
        ArrayAdapter<String> spRejectReasonList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, rejectReasonList);
        spRejectReasonList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spin_seal_failure_reasons.setAdapter(spRejectReasonList);

        getPouchSymmetry();
        final ArrayAdapter<String> spPouchSymmetryList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, pouchSymmetryList);
        spPouchSymmetryList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spin_pouch_symmetry.setAdapter(spPouchSymmetryList);

        spin_pouch_symmetry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                str_pouch_symmetry = pouchSymmetryList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerNetworkBroadcastForNougat();

    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    private void getData() {
        str_auger_count = ed_auger_count.getText().toString();
        str_auger_speed_rpm = ed_auger_speed_rpm.getText().toString();
        str_pre_auger_used = ed_pre_auger_used.getText().toString();
        str_no_of_agitator = ed_no_of_agitator.getText().toString();
        str_vertical_overlap_mm = ed_vertical_overlap_mm.getText().toString();
        str_observed_speed = ed_observed_speed.getText().toString();
        str_wire_cup = ed_wire_cup.getText().toString();
        str_hz_jaw_movement = ed_hz_jaw_movement.getText().toString();
        str_vertical_assly_teflon_tape_shift = ed_vertical_assly_teflon_tape_shift.getText().toString();
        str_hz_jaw_opening = ed_hz_jaw_opening.getText().toString();
        str_hz_cut_seal_Setting = ed_hz_cut_seal_Setting.getText().toString();

        if (!switch_vert_jaw_movement.isChecked()){
            str_vert_jaw_movement = (String)switch_vert_jaw_movement.getTextOff();
        }else {
            str_vert_jaw_movement = (String)switch_vert_jaw_movement.getTextOn();
        }

        if (!switch_drop_test.isChecked()){
            str_drop_test = (String)switch_drop_test.getTextOff();
            str_drop_test_status = "PASS";

        }else {
            str_drop_test = (String)switch_drop_test.getTextOn();
            str_drop_test_status = "FAIL";
        }

        if (!switch_vertical_alignment_centre.isChecked()){
            str_vertical_alignment_centre = (String)switch_vertical_alignment_centre.getTextOff();
        }else {
            str_vertical_alignment_centre = (String)switch_vertical_alignment_centre.getTextOn();
        }

        if (!switch_empty_pouches.isChecked()){
            str_empty_pouches = (String)switch_empty_pouches.getTextOff();
        }else {
            str_empty_pouches = (String)switch_empty_pouches.getTextOn();
        }

        if(!switch_perfortation.isChecked()){
            str_perfortation = (String) switch_perfortation.getTextOff();
        }else {
            str_perfortation = (String) switch_perfortation.getTextOn();
        }

        if (!switch_seal_quality.isChecked()){
            str_seal_quality = (String) switch_seal_quality.getTextOff();
            str_seal_test_status= "PASS";


        }else {
            str_seal_quality = (String) switch_seal_quality.getTextOn();
            str_seal_test_status= "FAIL";
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_enter_comments:
                getData();
                InsertIntoDb();
                Intent i = new Intent(NewTestObservationsActivity.this,NewTestCommentsActivity.class);
                i.putExtra("LoggerName",dataloggerName);
                i.putExtra("LoggerIP",dataloggerIp);
                i.putExtra("current_tag_id",current_tag_id);
                i.putExtra("so_oinum",so_oinum);
                i.putExtra("IntKgValue",str_kg_value_from_local_db);
                i.putExtra("targetwt_uom",str_tarhetwt_uom);
                i.putExtra("mac_id",dataloggerMacId);

                startActivity(i);
                break;
            case R.id.cvMainDetails:
                if (llAugerDetailForm.getVisibility() == View.VISIBLE){
                    llAugerDetailForm.setVisibility(View.GONE);
                }else if (llAugerDetailForm.getVisibility() == View.GONE){
                    llAugerDetailForm.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.cvLiquidDetails:
                if (llMainDetailForm.getVisibility() == View.VISIBLE){
                    llMainDetailForm.setVisibility(View.GONE);
                }else if (llMainDetailForm.getVisibility() == View.GONE){
                    llMainDetailForm.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.cvAugerDetails:
                if (llLiquidDetailForm.getVisibility() == View.VISIBLE){
                    llLiquidDetailForm.setVisibility(View.GONE);
                }else if (llLiquidDetailForm.getVisibility() == View.GONE){
                    llLiquidDetailForm.setVisibility(View.VISIBLE);
                }
                break;
        }
        if (view.getId() == btn_observation_abort.getId()) {
            //abort();
            status = "Abort";
            helper.updateOverallStatusForTag(status,current_tag_id);
            Intent i = new Intent(NewTestObservationsActivity.this, LoggerFunctionActivity.class);
            i.putExtra("LoggerName",dataloggerName);
            i.putExtra("LoggerIP",dataloggerIp);
            startActivity(i);
            finish();

        }else if (view.getId() == btn_sync.getId()){
            getData();
            InsertIntoDb();

        }
    }



    public  void InsertIntoDb()     {
        try {
//            DataBaseHelper helper=new DataBaseHelper(NewTestObservationsActivity.this);
//            SQLiteDatabase db = helper.getWritableDatabase();

            if (str_target_speed.equals(str_observed_speed)) { str_test_speed_status="PASS";
            }
            else
            {
                str_test_speed_status="FAIL";
            }
            ContentValues cv=new ContentValues();
            cv.put(Constants.OBSERVATIONS_ID,1);
            cv.put(Constants.OBSERVATIONS_OEM_ID,1234);
            cv.put(Constants.OBSERVATIONS_MAC_ID,dataloggerMacId);
            cv.put(Constants.OBSERVATIONS_LOGGER_ID,dataloggerName);
            cv.put(Constants.OBSERVATIONS_TAG_ID,current_tag_id);
            cv.put(Constants.OBSERVATIONS_AUGER_COUNT,Integer.parseInt(str_auger_count));
            cv.put(Constants.OBSERVATIONS_AUGER_SPEED,Float.parseFloat(str_auger_speed_rpm));
            cv.put(Constants.OBSERVATIONS_AUGER_PRE_AUGER,str_pre_auger_used);
            cv.put(Constants.OBSERVATIONS_AUGER_NO_OF_AGITATORS,Integer.parseInt(str_no_of_agitator));
            cv.put(Constants.OBSERVATIONS_MACHINE_VERTICAL_TEFLON,str_vertical_assly_teflon_tape_shift);
            cv.put(Constants.OBSERVATIONS_MACHINE_HORIZONTAL_LAW_OPENING,str_hz_jaw_opening);
            cv.put(Constants.OBSERVATIONS_MACHINE_HORIZONTAL_LAW_MOVEMENT,str_hz_jaw_movement);
            cv.put(Constants.OBSERVATIONS_MACHINE_VERTICAL_ALIGN_CENTER,str_vertical_alignment_centre);
            cv.put(Constants.OBSERVATIONS_MACHINE_VERTICAL_LAW_MOVEMENT,str_vert_jaw_movement);
            cv.put(Constants.OBSERVATIONS_PACKAGE_WIRE_CUP,str_wire_cup);
            cv.put(Constants.OBSERVATIONS_PACKAGE_EMPTY_POUCHES,str_empty_pouches);
            cv.put(Constants.OBSERVATIONS_PACKAGE_BAG_LENGTH_VARIATION,"No");
            cv.put(Constants.OBSERVATIONS_PACKAGE_PAPER_SHIFTING,"No");
            cv.put(Constants.OBSERVATIONS_PACKAGE_VERTICAL_OVERLAP,str_vertical_overlap_mm);
            cv.put(Constants.OBSERVATIONS_PACKAGE_POUCH_SYMMETRY,str_pouch_symmetry);
            cv.put(Constants.OBSERVATIONS_PACKAGE_OBSERV_SPEED,Float.parseFloat(str_observed_speed));
            if (str_seal_test_status.equals("PASS"))
            {
                cv.put(Constants.OBSERVATIONS_SEAL_REJECTION_REASON,"null");

            }
            else
            {
                cv.put(Constants.OBSERVATIONS_SEAL_REJECTION_REASON,rejectionReason);

            }
            cv.put(Constants.OBSERVATIONS_PACKAGE_IS_SYNC,0);

            long d=db.insert(Constants.OBSERVATIONS_TABLE,null,cv);
            Log.d("Success", String.valueOf(d));
            if (d == -1){
                Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                //wakeup();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        helper.updateCtagsTableFromNewTestObservationPage(current_tag_id,str_drop_test,str_seal_quality,str_test_speed_status);

        if (weight_test_status.equals("PASS")) {
            if (str_drop_test_status.equals("FAIL") || str_seal_test_status.equals("FAIL") || str_test_speed_status.equals("FAIL"))
            {
                helper.updateOverallStatusForTag("RESUME",current_tag_id);
            }
        }
        else {

            helper.updateOverallStatusForTag("FAIL",current_tag_id);

        }
    }

    private String abort() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String tag = current_tag_id;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String dataPort=Constants.DATA_LOGGER_PORT+"";
        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;

    }

    private void getSealRejectionList(){

        Cursor rejectReason = helper.getRejectionReasonsFromTable();
        while (rejectReason.moveToNext()){
            String reason = rejectReason.getString(0);
            rejectReasonList.add(reason);
            Log.d("TAG", "getMachineModel: ");
        }
    }

    private void getPouchSymmetry(){
        Cursor cursor = helper.getPouchSymmetry();
        while (cursor.moveToNext()){
            String pouchDesc = cursor.getString(0);
            pouchSymmetryList.add(pouchDesc);
        }
    }

    @Override
    protected void dialog(boolean value) {
        super.dialog(value);
//        if (value){
//            getData();
//            InsertIntoDb();
//            getAllDataForObservationsData();
//            Toast.makeText(this, "DATA Pushed", Toast.LENGTH_SHORT).show();
//        }else {
//            Toast.makeText(this, "DATA Not Pushed", Toast.LENGTH_SHORT).show();
//        }
    }
}
