package com.brainlines.weightloggernewversion.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class MobileJsonDataDeserializer implements JsonDeserializer<MobileJsonData> {
    @Override
    public MobileJsonData deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
        MobileJsonData outputObj = new MobileJsonData();
        JsonObject jsonObj = (JsonObject)arg0;
        outputObj.dataLoggerIP= jsonObj.get("dataLoggerIP").getAsString();
        outputObj.dataLoggerName = jsonObj.get("dataLoggerName").getAsString();
        Tag tagObj  = new Tag();
        JsonObject tagJsonObj = jsonObj.get("tag").getAsJsonObject();
        tagObj.oiNum = tagJsonObj.get("oiNum").getAsString();
        tagObj.fyyear = tagJsonObj.get("fyyear").getAsString();
        tagObj.targetWt = tagJsonObj.get("targetWt").getAsString();
        tagObj.agitatorMotorFreq = tagJsonObj.get("agitatorMotorFreq").getAsString();
        tagObj.customer = tagJsonObj.get("customer").getAsString();
        tagObj.inspector = tagJsonObj.get("inspector").getAsString();
        tagObj.negWt = tagJsonObj.get("negWt").getAsString();
        tagObj.pouchLength = tagJsonObj.get("pouchLength").getAsString();
        tagObj.pouchWidth = tagJsonObj.get("pouchWidth").getAsString();
        tagObj.mcSerialNum = tagJsonObj.get("mcserialnum").getAsString();
        tagObj.product = tagJsonObj.get("product").getAsString();
        tagObj.fillerType = tagJsonObj.get("fillerType").getAsString();
        tagObj.machineType = tagJsonObj.get("machinetype").getAsString();
        tagObj.productType = tagJsonObj.get("producttype").getAsString();
        tagObj.operator = tagJsonObj.get("operator").getAsString();
        tagObj.targetWtUnit = tagJsonObj.get("targetWtUnit").getAsString();
        tagObj.filmType = tagJsonObj.get("filmtype").getAsString();
        tagObj.productfeed = tagJsonObj.get("productfeed").getAsString();
        tagObj.agitatorMode = tagJsonObj.get("agitatormode").getAsString();
        tagObj.agitatorod = tagJsonObj.get("agitatorod").getAsString();
        tagObj.agitatorid = tagJsonObj.get("agitatorid").getAsString();
        tagObj.agitatorPitch = tagJsonObj.get("agitatorpitch").getAsString();
        tagObj.targetacctype = tagJsonObj.get("targetacctype").getAsString();
        tagObj.targetaccval = tagJsonObj.get("targetaccval").getAsString();
        tagObj.prBulkMain = tagJsonObj.get("prBulkMain").getAsString();
        tagObj.targetSpeed = tagJsonObj.get("targetspeed").getAsString();
        tagObj.customerProduct = tagJsonObj.get("customerProduct").getAsString();
        tagObj.head = tagJsonObj.get("head").getAsString();
        tagObj.airPressure = tagJsonObj.get("airPressure").getAsString();
        tagObj.qtySetting = tagJsonObj.get("qtysetting").getAsString();
        tagObj.bgLenSetting = tagJsonObj.get("bglensetting").getAsString();
        tagObj.fillValveGap = tagJsonObj.get("fillValveGap").getAsString();
        tagObj.fillVfdFreq = tagJsonObj.get("fillvfdFreq").getAsString();
        tagObj.horizCutSealSetting = tagJsonObj.get("horizCutSealSetting").getAsString();
        tagObj.horizBandSealSetting = tagJsonObj.get("horizBandSealSetting").getAsString();
        tagObj.vertSealSetting = tagJsonObj.get("vertSealSetting").getAsString();
        tagObj.augerAgitatorFreq = tagJsonObj.get("augerAgitatorFreq").getAsString();
        tagObj.augerFillValve = tagJsonObj.get("augerFillValve").getAsString();
        tagObj.augurPitch = tagJsonObj.get("augurPitch").getAsString();
        tagObj.pouchSealType = tagJsonObj.get("pouchSealType").getAsString();
        tagObj.pouchTempHZF = tagJsonObj.get("pouchTempHZF").getAsString();
        tagObj.pouchTempHZB = tagJsonObj.get("pouchTempHZB").getAsString();
        tagObj.pouchTempVert = tagJsonObj.get("pouchTempVert").getAsString();
        tagObj.pouchTempVertFrontLeft = tagJsonObj.get("pouchTempVertFrontLeft").getAsString();
        tagObj.pouchTempVertFrontRight = tagJsonObj.get("pouchTempVertFrontRight").getAsString();
        tagObj.pouchTempVertBackLeft = tagJsonObj.get("pouchTempVertBackLeft").getAsString();
        tagObj.pouchTempVertBackRight = tagJsonObj.get("pouchTempVertBackRight").getAsString();
        tagObj.pouchGusset = tagJsonObj.get("pouchGusset").getAsString();
        tagObj.customerFilm = tagJsonObj.get("customerFilm").getAsString();
        tagObj.pouchFilmThickness = tagJsonObj.get("pouchFilmThickness").getAsString();
        tagObj.pouchPrBulk = tagJsonObj.get("pouchPrBulk").getAsString();
        tagObj.pouchEqHorizSerrations = tagJsonObj.get("pouchEqHorizSerrations").getAsString();
        tagObj.pouchFillEvent = tagJsonObj.get("pouchFillEvent").getAsString();
        tagObj.negWt = tagJsonObj.get("negWt").getAsString();





        tagObj.tagid = "";

        outputObj.tagInfo = tagObj;

        return outputObj;

    }
}
