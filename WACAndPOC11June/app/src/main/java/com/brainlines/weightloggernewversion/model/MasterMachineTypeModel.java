package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterMachineTypeModel implements Parcelable {

    String MachineTypeID;
    int oemId;
    String MachineID;
    String ProductTypeID;
    String MachineName;
    String MachineDescription;
    boolean isActive;
    boolean IsDeleted;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;

    public MasterMachineTypeModel(String machineTypeID, int oemId, String machineID, String productTypeID, String machineName, String machineDescription, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        MachineTypeID = machineTypeID;
        this.oemId = oemId;
        MachineID = machineID;
        ProductTypeID = productTypeID;
        MachineName = machineName;
        MachineDescription = machineDescription;
        this.isActive = isActive;
        IsDeleted = isDeleted;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
    }

    protected MasterMachineTypeModel(Parcel in) {
        MachineTypeID = in.readString();
        oemId = in.readInt();
        MachineID = in.readString();
        ProductTypeID = in.readString();
        MachineName = in.readString();
        MachineDescription = in.readString();
        isActive = in.readByte() != 0;
        IsDeleted = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
    }

    public static final Creator<MasterMachineTypeModel> CREATOR = new Creator<MasterMachineTypeModel>() {
        @Override
        public MasterMachineTypeModel createFromParcel(Parcel in) {
            return new MasterMachineTypeModel(in);
        }

        @Override
        public MasterMachineTypeModel[] newArray(int size) {
            return new MasterMachineTypeModel[size];
        }
    };

    public String getMachineTypeID() {
        return MachineTypeID;
    }

    public void setMachineTypeID(String machineTypeID) {
        MachineTypeID = machineTypeID;
    }

    public int getOemId() {
        return oemId;
    }

    public void setOemId(int oemId) {
        this.oemId = oemId;
    }

    public String getMachineID() {
        return MachineID;
    }

    public void setMachineID(String machineID) {
        MachineID = machineID;
    }

    public String getProductTypeID() {
        return ProductTypeID;
    }

    public void setProductTypeID(String productTypeID) {
        ProductTypeID = productTypeID;
    }

    public String getMachineName() {
        return MachineName;
    }

    public void setMachineName(String machineName) {
        MachineName = machineName;
    }

    public String getMachineDescription() {
        return MachineDescription;
    }

    public void setMachineDescription(String machineDescription) {
        MachineDescription = machineDescription;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(MachineTypeID);
        dest.writeInt(oemId);
        dest.writeString(MachineID);
        dest.writeString(ProductTypeID);
        dest.writeString(MachineName);
        dest.writeString(MachineDescription);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (IsDeleted ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
    }
}
