package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterSealTypeModel implements Parcelable {

    String SealTypeID;
    int OEMID;
    String seal_type;
    String ProductTypeID;
    String CreatedDate;
    String ModifiedDate;
    String CreatedBy;
    String ModifiedBy;
    boolean IsActive;
    boolean IsDeleted;

    public MasterSealTypeModel(String sealTypeID, int OEMID, String seal_type, String productTypeID, String createdDate, String modifiedDate, String createdBy, String modifiedBy, boolean isActive, boolean isDeleted) {
        SealTypeID = sealTypeID;
        this.OEMID = OEMID;
        this.seal_type = seal_type;
        ProductTypeID = productTypeID;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
        CreatedBy = createdBy;
        ModifiedBy = modifiedBy;
        IsActive = isActive;
        IsDeleted = isDeleted;
    }

    protected MasterSealTypeModel(Parcel in) {
        SealTypeID = in.readString();
        OEMID = in.readInt();
        seal_type = in.readString();
        ProductTypeID = in.readString();
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
        CreatedBy = in.readString();
        ModifiedBy = in.readString();
        IsActive = in.readByte() != 0;
        IsDeleted = in.readByte() != 0;
    }

    public static final Creator<MasterSealTypeModel> CREATOR = new Creator<MasterSealTypeModel>() {
        @Override
        public MasterSealTypeModel createFromParcel(Parcel in) {
            return new MasterSealTypeModel(in);
        }

        @Override
        public MasterSealTypeModel[] newArray(int size) {
            return new MasterSealTypeModel[size];
        }
    };

    public String getSealTypeID() {
        return SealTypeID;
    }

    public void setSealTypeID(String sealTypeID) {
        SealTypeID = sealTypeID;
    }

    public int getOEMID() {
        return OEMID;
    }

    public void setOEMID(int OEMID) {
        this.OEMID = OEMID;
    }

    public String getSeal_type() {
        return seal_type;
    }

    public void setSeal_type(String seal_type) {
        this.seal_type = seal_type;
    }

    public String getProductTypeID() {
        return ProductTypeID;
    }

    public void setProductTypeID(String productTypeID) {
        ProductTypeID = productTypeID;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(SealTypeID);
        dest.writeInt(OEMID);
        dest.writeString(seal_type);
        dest.writeString(ProductTypeID);
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
        dest.writeString(CreatedBy);
        dest.writeString(ModifiedBy);
        dest.writeByte((byte) (IsActive ? 1 : 0));
        dest.writeByte((byte) (IsDeleted ? 1 : 0));
    }
}
