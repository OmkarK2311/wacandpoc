package com.brainlines.weightloggernewversion.activity.newtest;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;

import java.text.DecimalFormat;

public class Report_Activity extends AppCompatActivity {

    TextView txt_report_fy,txt_report_customer,txt_report_oi_no,txt_report_pro_type,txt_report_product,txt_report_targetweight,txt_report_machine_model,txt_machine_sr_no,txt_report_target_wt_uom;
    DataBaseHelper helper;
    String loggerName,loggerIp,tagId,oinum;
    TextView txt_report_weight_min,txt_report_weight_max,txt_report_weight_mean,txt_report_weight_range,txt_report_weight_gm,txt_report_weight_sd,txt_report_weight_gmsd,txt_report_weight_status;
    TextView txt_report_bag_min,txt_report_bag_max,txt_report_bag_mean,txt_report_bag_range,txt_report_bag_status;
    TextView txt_report_paper_min,txt_report_paper_max,txt_report_paper_mean,txt_report_paper_range,txt_report_paper_status1;
    TextView txt_final_speedtest_status,txt_final_droptest_status,txt_final_sealquality_status,txt_final_papershift_status,txt_final_baglength_status;
    TextView txt_final_weight_status,txt_report_drop_test,txt_report_seal_quality,txt_headline_for_final_Result;
    TextView txtWeightReadingHeader,txt_header_papershift_calculation,txt_report_header_baglength;
    double str_kg_value_from_local_db;
    private String str_tarhetwt_uom = "";
    DecimalFormat decimalFormat = new DecimalFormat("0.000");
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_report);
        initUi();
        getTagInfoAgainstOinum();
        getCalculatedValuesFromWeightTestAnalyticsTable(tagId);
        getCalculatedValuesFromBagLengthAnalyticsTable(tagId);
        getCalculatedValuesFromPaperShiftAnalyticsTable(tagId);
    }
    private void initUi() {

        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        tagId = getIntent().getStringExtra("TagId");
        loggerName = getIntent().getStringExtra("LoggerName");
        loggerIp = getIntent().getStringExtra("LoggerIP");
        oinum = getIntent().getStringExtra("oinum");
        str_kg_value_from_local_db = getIntent().getDoubleExtra("IntKgValue",0.0d);
        str_tarhetwt_uom = getIntent().getStringExtra("targetwt_uom");
        //Final result status
        txt_final_speedtest_status = findViewById(R.id.txt_final_speedtest_status);
        txt_final_droptest_status = findViewById(R.id.txt_final_droptest_status);
        txt_final_sealquality_status = findViewById(R.id.txt_final_sealquality_status);
        txt_final_papershift_status = findViewById(R.id.txt_final_papershift_status);
        txt_final_baglength_status = findViewById(R.id.txt_final_baglength_status);
        txt_final_weight_status = findViewById(R.id.txt_final_weight_status);
        txt_report_seal_quality = findViewById(R.id.txt_report_seal_quality);
        txt_report_drop_test = findViewById(R.id.txt_report_drop_test);
        txt_headline_for_final_Result = findViewById(R.id.txt_headline_for_final_Result);


        //batch details
        txt_report_fy = findViewById(R.id.txt_report_fy);
        txt_report_customer = findViewById(R.id.txt_report_customer);
        txt_report_oi_no = findViewById(R.id.txt_report_oi_no);
        txt_report_pro_type = findViewById(R.id.txt_report_pro_type);
        txt_report_product = findViewById(R.id.txt_report_product);
        txt_report_targetweight = findViewById(R.id.txt_report_targetweight);
        txt_report_machine_model = findViewById(R.id.txt_report_machine_model);
        txt_machine_sr_no = findViewById(R.id.txt_machine_sr_no);
        txt_report_target_wt_uom = findViewById(R.id.txt_report_target_wt_uom);

        //weight_reading_analytics;
        txt_report_weight_min = findViewById(R.id.txt_report_weight_min);
        txt_report_weight_max = findViewById(R.id.txt_report_weight_max);
        txt_report_weight_mean = findViewById(R.id.txt_report_weight_mean);
        txt_report_weight_range = findViewById(R.id.txt_report_weight_range);
        txt_report_weight_gm = findViewById(R.id.txt_report_weight_gm);
        txt_report_weight_sd = findViewById(R.id.txt_report_weight_sd);
        txt_report_weight_gmsd = findViewById(R.id.txt_report_weight_gmsd);
        txt_report_weight_status = findViewById(R.id.txt_report_weight_status);

        //bag_length_reading
        txt_report_bag_min = findViewById(R.id.txt_report_bag_min);
        txt_report_bag_max = findViewById(R.id.txt_report_bag_max);
        txt_report_bag_mean = findViewById(R.id.txt_report_bag_mean);
        txt_report_bag_range = findViewById(R.id.txt_report_bag_range);
        txt_report_bag_status = findViewById(R.id.txt_report_bag_status);

        //paper shift reading
        txt_report_paper_min = findViewById(R.id.txt_report_paper_min);
        txt_report_paper_max = findViewById(R.id.txt_report_paper_max);
        txt_report_paper_mean = findViewById(R.id.txt_report_paper_mean);
        txt_report_paper_range = findViewById(R.id.txt_report_paper_range);
        txt_report_paper_status1 = findViewById(R.id.txt_report_paper_status1);

        txtWeightReadingHeader = findViewById(R.id.txtWeightReadingHeader);
        txt_header_papershift_calculation = findViewById(R.id.txt_header_papershift_calculation);
        txt_report_header_baglength = findViewById(R.id.txt_report_header_baglength);

        txt_report_header_baglength.setText("Bag Length Reading Calculations"+ " (" + "mm" + ")");
        txt_header_papershift_calculation.setText("Paper Shift Reading Calculations" + " (" + "mm" + ")");
        txt_report_target_wt_uom.setText(str_tarhetwt_uom);


    }

    public void getTagInfoAgainstOinum() {
        Cursor taginfo = helper.getTagInfoAgainstoinum(oinum);
        while (taginfo.moveToNext()){

            String TagID = taginfo.getString(0);
            String OEMID  = taginfo.getString(1);
            String MacID = taginfo.getString(2);
            String LoggerID  = taginfo.getString(3);
            String CalTagsID  = taginfo.getString(4);
            String OINumber  = taginfo.getString(5);
            String TargetWeight  = taginfo.getString(6);
            String CustomerName  = taginfo.getString(7);
            String MachineTypeID  = taginfo.getString(8);
            String ProductTypeID  = taginfo.getString(9);
            String ProductTypeName    = taginfo.getString(8);
            String MachineTypeName   = taginfo.getString(9);
            String ProductName  = taginfo.getString(12);
            String Operator  = taginfo.getString(13);
            String TargetWeightUnit  = taginfo.getString(14);
            String Inspector  = taginfo.getString(15);
            String FilmType  = taginfo.getString(16);
            String ProductFeed  = taginfo.getString(17);
            String AgitatorMotorFrequency  = taginfo.getString(18);
            String AgitatorMode  = taginfo.getString(19);
            String AgitatorOD  = taginfo.getString(20);
            String AgitatorID  = taginfo.getString(21);
            String AgitatorPitch  = taginfo.getString(22);
            String TargetAccuracyType  = taginfo.getString(23);
            String TargetAccuracyValue  = taginfo.getString(24);
            String MachineSerialNumber  = taginfo.getString(25);
            String PrBulkMain  = taginfo.getString(26);
            String TargetSpeed  = taginfo.getString(27);
            String Head  = taginfo.getString(28);
            String AirPressure = taginfo.getString(29);
            String QuantitySetting  = taginfo.getString(30);
            String BaglengthSetting  = taginfo.getString(31);
            String FillValveGap  = taginfo.getString(32);
            String FillVfdFreq  = taginfo.getString(33);
            String HorizCutSealSetting  = taginfo.getString(34);
            String HorizBandSealSetting  = taginfo.getString(35);
            String VertSealSetting  = taginfo.getString(36);
            String AugerAgitatorFreq  = taginfo.getString(37);
            String AugerFillValve  = taginfo.getString(38);
            String AugurPitch  = taginfo.getString(39);
            String PouchWidth  = taginfo.getString(40);
            String PouchLength  = taginfo.getString(41);
            String PouchSealType  = taginfo.getString(42);
            String PouchTempHorizFront  = taginfo.getString(43);
            String PouchTempHorizBack  = taginfo.getString(44);
            String PouchTempVert  = taginfo.getString(45);
            String PouchTempVertFrontLeft  = taginfo.getString(46);
            String PouchTempVertFrontRight  = taginfo.getString(47);
            String PouchTempVertBackLeft  = taginfo.getString(48);
            String PouchTempVertBackRight  = taginfo.getString(49);
            String PouchGusset  = taginfo.getString(50);
            String PouchFilmThickness  = taginfo.getString(51);
            String PouchProductBulk  = taginfo.getString(52);
            String PouchEqHorizSerrations  = taginfo.getString(53);
            String PouchFillEvent  = taginfo.getString(54);
            String OverallStatus  = taginfo.getString(55);
            String Remarks  = taginfo.getString(56);
            String SealStatus  = taginfo.getString(57);
            String DropStatus  = taginfo.getString(58);
            String TargetSpeedStatus  = taginfo.getString(59);
            String WeightTestStatus  = taginfo.getString(60);
            String BagStatus  = taginfo.getString(61);
            String PaperStatus  = taginfo.getString(62);
            String FinancialYear  = taginfo.getString(63);
            String FillerType  = taginfo.getString(64);

            String IsCustomerProduct  = taginfo.getString(65);
            String IsCustomerFilm  = taginfo.getString(66);
            String IsNegativeWeight  = taginfo.getString(67);
            String LoggerCreateTimestamp  = taginfo.getString(68);
            String CreatedDate  = taginfo.getString(69);
            String ModifiedDate  = taginfo.getString(70);

            txtWeightReadingHeader.setText("Weight Reading Calculations"+ " (" + str_tarhetwt_uom + ")");

            txt_report_drop_test.setText(DropStatus);
            txt_report_seal_quality.setText(SealStatus);

            txt_final_speedtest_status.setText(TargetSpeedStatus);
            txt_final_baglength_status.setText(BagStatus);
            txt_final_weight_status.setText(WeightTestStatus);
            txt_final_papershift_status.setText(PaperStatus);
            if (SealStatus.equals("OK"))
            {
                txt_final_sealquality_status.setText("PASS");
            }
            else
            {
                txt_final_sealquality_status.setText("FAIL");

            }
            if (SealStatus.equals("OK"))
            {
                txt_final_droptest_status.setText("PASS");
            }
            else
            {
                txt_final_droptest_status.setText("FAIL");

            }
            txt_headline_for_final_Result.setText("Report for SO number " + OINumber + " is" + " " + OverallStatus);

            txt_report_customer.setText(CustomerName);
            txt_report_oi_no.setText(OINumber);
            txt_report_pro_type.setText(ProductTypeID);
            txt_report_product.setText(ProductName);
            txt_report_target_wt_uom.setText(TargetWeightUnit);
            Double dweight= Double.parseDouble(TargetWeight);
            if (str_tarhetwt_uom.equals("gm")) {
                txt_report_targetweight.setText(decimalFormat.format(dweight));
            }
            else
            {
                dweight = Double.parseDouble(TargetWeight)/str_kg_value_from_local_db;
                txt_report_targetweight.setText(decimalFormat.format(dweight));

            }

            txt_report_machine_model.setText(MachineTypeID);
            txt_machine_sr_no.setText(MachineSerialNumber);

            Log.d("TAG", "getSealTypes: ");
        }
    }

    private void getCalculatedValuesFromWeightTestAnalyticsTable(String tagId) {
        if (str_tarhetwt_uom.equals("gm")){
            Cursor calculatedValues = helper.getCalculatedValuesFromWeightResultsAnalytics(tagId);
            while (calculatedValues.moveToNext()){
                String minWeightReading = calculatedValues.getString(0);
                String maxWeightReading = calculatedValues.getString(1);
                String rangeWeightReading = calculatedValues.getString(2);
                String meanWeightReading = calculatedValues.getString(3);
                String sdWeightReading = calculatedValues.getString(4);
                String runWeightStatus = calculatedValues.getString(5);
                String gmWeight = calculatedValues.getString(6);
                String gmsdPercentWeight = calculatedValues.getString(9);
                String srangeWeightReading= rangeWeightReading.replaceAll("[^0-9]", "");


                Double dmin = Double.parseDouble(minWeightReading);
                Double dmax = Double.parseDouble(maxWeightReading);
                Double drange = Double.parseDouble(srangeWeightReading);
                Double dmean = Double.parseDouble(meanWeightReading);
                Double dsd = Double.parseDouble(sdWeightReading);
                Double dgmweight = Double.parseDouble(gmWeight);
                Double dgmsdpercent = Double.parseDouble(gmsdPercentWeight);
                String strweightrange = "+/- " + decimalFormat.format(drange);

                txt_report_weight_min.setText(decimalFormat.format(dmin));
                txt_report_weight_max.setText(decimalFormat.format(dmax));
                txt_report_weight_range.setText(strweightrange);
                txt_report_weight_mean.setText(decimalFormat.format(dmean));
                txt_report_weight_sd.setText(decimalFormat.format(dsd));
                txt_report_weight_status.setText(runWeightStatus);
                txt_report_weight_gm.setText(decimalFormat.format(dgmweight));
                txt_report_weight_gmsd.setText(decimalFormat.format(dgmsdpercent));

            }
        }
        else if(str_tarhetwt_uom.equals("Kg")){
            Cursor calculatedValues = helper.getCalculatedValuesFromWeightResultsAnalytics(tagId);
            while (calculatedValues.moveToNext()){
                String minWeightReading = calculatedValues.getString(0);
                String maxWeightReading = calculatedValues.getString(1);

                String meanWeightReading = calculatedValues.getString(3);
                String sdWeightReading = calculatedValues.getString(4);
                String runWeightStatus = calculatedValues.getString(5);
                String gmWeight = calculatedValues.getString(6);
                String gmsdPercentWeight = calculatedValues.getString(9);

                String rangeWeightReading = calculatedValues.getString(2);
                //rangeWeightReading = rangeWeightReading.substring(3);
                String srangeWeightReading= rangeWeightReading.replaceAll("[^0-9]", "");


                Double dmin = (Double.parseDouble(minWeightReading))/str_kg_value_from_local_db;
                Double dmax = (Double.parseDouble(maxWeightReading))/str_kg_value_from_local_db;

                Double drange = (Double.parseDouble(srangeWeightReading))/str_kg_value_from_local_db;
                Double dmean = (Double.parseDouble(meanWeightReading))/str_kg_value_from_local_db;
                Double dsd = (Double.parseDouble(sdWeightReading))/str_kg_value_from_local_db;
                Double dgm = (Double.parseDouble(gmWeight))/str_kg_value_from_local_db;
                Double dgmsd = (Double.parseDouble(gmsdPercentWeight))/str_kg_value_from_local_db;
                String strweightrange = "+/- " + decimalFormat.format(drange);

                txt_report_weight_min.setText(decimalFormat.format(dmin));
                txt_report_weight_max.setText(decimalFormat.format(dmax));
                txt_report_weight_range.setText(strweightrange);
                txt_report_weight_mean.setText(decimalFormat.format(dmean));
                txt_report_weight_sd.setText(decimalFormat.format(dsd));
                txt_report_weight_status.setText(runWeightStatus);
                txt_report_weight_gm.setText(decimalFormat.format(dgm));
                txt_report_weight_gmsd.setText(decimalFormat.format(dgmsd));
            }
        }
    }

    private void getCalculatedValuesFromBagLengthAnalyticsTable(String tagId) {
        Cursor calculatedValues = helper.getCalculatedValuesFromBaglengthAnalytics(tagId);
        while (calculatedValues.moveToNext()) {
            String minWeightReading = calculatedValues.getString(0);
            String maxWeightReading = calculatedValues.getString(1);
            String rangeWeightReading = calculatedValues.getString(2);
            String meanWeightReading = calculatedValues.getString(4);
            String runWeightStatus = calculatedValues.getString(3);

            txt_report_bag_min.setText(minWeightReading);
            txt_report_bag_max.setText(maxWeightReading);
            txt_report_bag_range.setText(rangeWeightReading);
            txt_report_bag_mean.setText(meanWeightReading);
            txt_report_bag_status.setText(runWeightStatus);
        }
    }

    private void getCalculatedValuesFromPaperShiftAnalyticsTable(String tagId) {
        Cursor calculatedValues = helper.getCalculatedValuesFromPaperShiftAnalytics(tagId);
        while (calculatedValues.moveToNext()){
            String minWeightReading = calculatedValues.getString(0);
            String maxWeightReading = calculatedValues.getString(1);
            String rangeWeightReading = calculatedValues.getString(2);
            String meanWeightReading = calculatedValues.getString(4);
            String runWeightStatus = calculatedValues.getString(3);

            txt_report_paper_min.setText(minWeightReading);
            txt_report_paper_max.setText(maxWeightReading);
            txt_report_paper_mean.setText(meanWeightReading);
            txt_report_paper_range.setText(rangeWeightReading);
            txt_report_paper_status1.setText(runWeightStatus);
        }
    }
}
