package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class C_TagsModel implements Parcelable {

    String  CalTagsID,OEMID ,MacID ,LoggerID ,TagID ,OINumber ,TargetWeight ,CustomerName ,MachineTypeID ,ProductTypeID ,
            MachineTypeName,ProductTypeName, FillerType ,ProductName ,Operator ,TargetWeightUnit ,Inspector ,FilmType ,
            ProductFeed ,AgitatorMotorFrequency ,AgitatorMode ,AgitatorOD ,AgitatorID , AgitatorPitch ,TargetAccuracyType ,
            TargetAccuracyValue ,MachineSerialNumber ,PrBulkMain ,TargetSpeed ,Head ,AirPressure,QuantitySetting,BaglengthSetting ,FillValveGap ,
            FillVfdFreq ,HorizCutSealSetting ,HorizBandSealSetting ,VertSealSetting ,AugerAgitatorFreq ,AugerFillValve ,AugurPitch ,
            PouchWidth ,PouchLength,PouchSealType,PouchTempHorizFront ,PouchTempHorizBack ,PouchTempVert ,PouchTempVertFrontLeft ,
            PouchTempVertFrontRight ,PouchTempVertBackLeft ,PouchTempVertBackRight ,PouchGusset , PouchFilmThickness,PouchProductBulk ,
            PouchEqHorizSerrations ,PouchFillEvent ,OverallStatus ,Remarks ,SealStatus , DropStatus ,TargetSpeedStatus ,WeightTestStatus ,
            BagStatus ,PaperStatus,FinancialYear ,IsCustomerProduct ,IsCustomerFilm ,IsNegativeWeight , LoggerCreateTimestamp ,CreatedDate ,
            ModifiedDate ,sealRejectionRsn;

    public C_TagsModel(String calTagsID, String OEMID, String macID, String loggerID, String tagID, String OINumber, String targetWeight, String customerName, String machineTypeID, String productTypeID, String machineTypeName, String productTypeName, String fillerType, String productName, String operator, String targetWeightUnit, String inspector, String filmType, String productFeed, String agitatorMotorFrequency, String agitatorMode, String agitatorOD, String agitatorID, String agitatorPitch, String targetAccuracyType, String targetAccuracyValue, String machineSerialNumber, String prBulkMain, String targetSpeed, String head, String airPressure, String quantitySetting, String baglengthSetting, String fillValveGap, String fillVfdFreq, String horizCutSealSetting, String horizBandSealSetting, String vertSealSetting, String augerAgitatorFreq, String augerFillValve, String augurPitch, String pouchWidth, String pouchLength, String pouchSealType, String pouchTempHorizFront, String pouchTempHorizBack, String pouchTempVert, String pouchTempVertFrontLeft, String pouchTempVertFrontRight, String pouchTempVertBackLeft, String pouchTempVertBackRight, String pouchGusset, String pouchFilmThickness, String pouchProductBulk, String pouchEqHorizSerrations, String pouchFillEvent, String overallStatus, String remarks, String sealStatus, String dropStatus, String targetSpeedStatus, String weightTestStatus, String bagStatus, String paperStatus, String financialYear, String isCustomerProduct, String isCustomerFilm, String isNegativeWeight, String loggerCreateTimestamp, String createdDate, String modifiedDate) {
        CalTagsID = calTagsID;
        this.OEMID = OEMID;
        MacID = macID;
        LoggerID = loggerID;
        TagID = tagID;
        this.OINumber = OINumber;
        TargetWeight = targetWeight;
        CustomerName = customerName;
        MachineTypeID = machineTypeID;
        ProductTypeID = productTypeID;
        MachineTypeName = machineTypeName;
        ProductTypeName = productTypeName;
        FillerType = fillerType;
        ProductName = productName;
        Operator = operator;
        TargetWeightUnit = targetWeightUnit;
        Inspector = inspector;
        FilmType = filmType;
        ProductFeed = productFeed;
        AgitatorMotorFrequency = agitatorMotorFrequency;
        AgitatorMode = agitatorMode;
        AgitatorOD = agitatorOD;
        AgitatorID = agitatorID;
        AgitatorPitch = agitatorPitch;
        TargetAccuracyType = targetAccuracyType;
        TargetAccuracyValue = targetAccuracyValue;
        MachineSerialNumber = machineSerialNumber;
        PrBulkMain = prBulkMain;
        TargetSpeed = targetSpeed;
        Head = head;
        AirPressure = airPressure;
        QuantitySetting = quantitySetting;
        BaglengthSetting = baglengthSetting;
        FillValveGap = fillValveGap;
        FillVfdFreq = fillVfdFreq;
        HorizCutSealSetting = horizCutSealSetting;
        HorizBandSealSetting = horizBandSealSetting;
        VertSealSetting = vertSealSetting;
        AugerAgitatorFreq = augerAgitatorFreq;
        AugerFillValve = augerFillValve;
        AugurPitch = augurPitch;
        PouchWidth = pouchWidth;
        PouchLength = pouchLength;
        PouchSealType = pouchSealType;
        PouchTempHorizFront = pouchTempHorizFront;
        PouchTempHorizBack = pouchTempHorizBack;
        PouchTempVert = pouchTempVert;
        PouchTempVertFrontLeft = pouchTempVertFrontLeft;
        PouchTempVertFrontRight = pouchTempVertFrontRight;
        PouchTempVertBackLeft = pouchTempVertBackLeft;
        PouchTempVertBackRight = pouchTempVertBackRight;
        PouchGusset = pouchGusset;
        PouchFilmThickness = pouchFilmThickness;
        PouchProductBulk = pouchProductBulk;
        PouchEqHorizSerrations = pouchEqHorizSerrations;
        PouchFillEvent = pouchFillEvent;
        OverallStatus = overallStatus;
        Remarks = remarks;
        SealStatus = sealStatus;
        DropStatus = dropStatus;
        TargetSpeedStatus = targetSpeedStatus;
        WeightTestStatus = weightTestStatus;
        BagStatus = bagStatus;
        PaperStatus = paperStatus;
        FinancialYear = financialYear;
        IsCustomerProduct = isCustomerProduct;
        IsCustomerFilm = isCustomerFilm;
        IsNegativeWeight = isNegativeWeight;
        LoggerCreateTimestamp = loggerCreateTimestamp;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
    }

    protected C_TagsModel(Parcel in) {
        CalTagsID = in.readString();
        OEMID = in.readString();
        MacID = in.readString();
        LoggerID = in.readString();
        TagID = in.readString();
        OINumber = in.readString();
        TargetWeight = in.readString();
        CustomerName = in.readString();
        MachineTypeID = in.readString();
        ProductTypeID = in.readString();
        MachineTypeName = in.readString();
        ProductTypeName = in.readString();
        FillerType = in.readString();
        ProductName = in.readString();
        Operator = in.readString();
        TargetWeightUnit = in.readString();
        Inspector = in.readString();
        FilmType = in.readString();
        ProductFeed = in.readString();
        AgitatorMotorFrequency = in.readString();
        AgitatorMode = in.readString();
        AgitatorOD = in.readString();
        AgitatorID = in.readString();
        AgitatorPitch = in.readString();
        TargetAccuracyType = in.readString();
        TargetAccuracyValue = in.readString();
        MachineSerialNumber = in.readString();
        PrBulkMain = in.readString();
        TargetSpeed = in.readString();
        Head = in.readString();
        AirPressure = in.readString();
        QuantitySetting = in.readString();
        BaglengthSetting = in.readString();
        FillValveGap = in.readString();
        FillVfdFreq = in.readString();
        HorizCutSealSetting = in.readString();
        HorizBandSealSetting = in.readString();
        VertSealSetting = in.readString();
        AugerAgitatorFreq = in.readString();
        AugerFillValve = in.readString();
        AugurPitch = in.readString();
        PouchWidth = in.readString();
        PouchLength = in.readString();
        PouchSealType = in.readString();
        PouchTempHorizFront = in.readString();
        PouchTempHorizBack = in.readString();
        PouchTempVert = in.readString();
        PouchTempVertFrontLeft = in.readString();
        PouchTempVertFrontRight = in.readString();
        PouchTempVertBackLeft = in.readString();
        PouchTempVertBackRight = in.readString();
        PouchGusset = in.readString();
        PouchFilmThickness = in.readString();
        PouchProductBulk = in.readString();
        PouchEqHorizSerrations = in.readString();
        PouchFillEvent = in.readString();
        OverallStatus = in.readString();
        Remarks = in.readString();
        SealStatus = in.readString();
        DropStatus = in.readString();
        TargetSpeedStatus = in.readString();
        WeightTestStatus = in.readString();
        BagStatus = in.readString();
        PaperStatus = in.readString();
        FinancialYear = in.readString();
        IsCustomerProduct = in.readString();
        IsCustomerFilm = in.readString();
        IsNegativeWeight = in.readString();
        LoggerCreateTimestamp = in.readString();
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
        sealRejectionRsn = in.readString();
    }

    public static final Creator<C_TagsModel> CREATOR = new Creator<C_TagsModel>() {
        @Override
        public C_TagsModel createFromParcel(Parcel in) {
            return new C_TagsModel(in);
        }

        @Override
        public C_TagsModel[] newArray(int size) {
            return new C_TagsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CalTagsID);
        dest.writeString(OEMID);
        dest.writeString(MacID);
        dest.writeString(LoggerID);
        dest.writeString(TagID);
        dest.writeString(OINumber);
        dest.writeString(TargetWeight);
        dest.writeString(CustomerName);
        dest.writeString(MachineTypeID);
        dest.writeString(ProductTypeID);
        dest.writeString(MachineTypeName);
        dest.writeString(ProductTypeName);
        dest.writeString(FillerType);
        dest.writeString(ProductName);
        dest.writeString(Operator);
        dest.writeString(TargetWeightUnit);
        dest.writeString(Inspector);
        dest.writeString(FilmType);
        dest.writeString(ProductFeed);
        dest.writeString(AgitatorMotorFrequency);
        dest.writeString(AgitatorMode);
        dest.writeString(AgitatorOD);
        dest.writeString(AgitatorID);
        dest.writeString(AgitatorPitch);
        dest.writeString(TargetAccuracyType);
        dest.writeString(TargetAccuracyValue);
        dest.writeString(MachineSerialNumber);
        dest.writeString(PrBulkMain);
        dest.writeString(TargetSpeed);
        dest.writeString(Head);
        dest.writeString(AirPressure);
        dest.writeString(QuantitySetting);
        dest.writeString(BaglengthSetting);
        dest.writeString(FillValveGap);
        dest.writeString(FillVfdFreq);
        dest.writeString(HorizCutSealSetting);
        dest.writeString(HorizBandSealSetting);
        dest.writeString(VertSealSetting);
        dest.writeString(AugerAgitatorFreq);
        dest.writeString(AugerFillValve);
        dest.writeString(AugurPitch);
        dest.writeString(PouchWidth);
        dest.writeString(PouchLength);
        dest.writeString(PouchSealType);
        dest.writeString(PouchTempHorizFront);
        dest.writeString(PouchTempHorizBack);
        dest.writeString(PouchTempVert);
        dest.writeString(PouchTempVertFrontLeft);
        dest.writeString(PouchTempVertFrontRight);
        dest.writeString(PouchTempVertBackLeft);
        dest.writeString(PouchTempVertBackRight);
        dest.writeString(PouchGusset);
        dest.writeString(PouchFilmThickness);
        dest.writeString(PouchProductBulk);
        dest.writeString(PouchEqHorizSerrations);
        dest.writeString(PouchFillEvent);
        dest.writeString(OverallStatus);
        dest.writeString(Remarks);
        dest.writeString(SealStatus);
        dest.writeString(DropStatus);
        dest.writeString(TargetSpeedStatus);
        dest.writeString(WeightTestStatus);
        dest.writeString(BagStatus);
        dest.writeString(PaperStatus);
        dest.writeString(FinancialYear);
        dest.writeString(IsCustomerProduct);
        dest.writeString(IsCustomerFilm);
        dest.writeString(IsNegativeWeight);
        dest.writeString(LoggerCreateTimestamp);
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
        dest.writeString(sealRejectionRsn);
    }
}
