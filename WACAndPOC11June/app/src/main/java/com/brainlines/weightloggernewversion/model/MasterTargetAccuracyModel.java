package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterTargetAccuracyModel implements Parcelable {

    String targetAccuracyId;
    int oemId;
    int targetId;
    String targetAccuracy;
    String targetAccuracy_desc;
    boolean isActive;
    boolean isDeleted;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;

    public MasterTargetAccuracyModel(String targetAccuracyId, int oemId, int targetId, String targetAccuracy, String targetAccuracy_desc, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        this.targetAccuracyId = targetAccuracyId;
        this.oemId = oemId;
        this.targetId = targetId;
        this.targetAccuracy = targetAccuracy;
        this.targetAccuracy_desc = targetAccuracy_desc;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
    }

    protected MasterTargetAccuracyModel(Parcel in) {
        targetAccuracyId = in.readString();
        oemId = in.readInt();
        targetId = in.readInt();
        targetAccuracy = in.readString();
        targetAccuracy_desc = in.readString();
        isActive = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
    }

    public static final Creator<MasterTargetAccuracyModel> CREATOR = new Creator<MasterTargetAccuracyModel>() {
        @Override
        public MasterTargetAccuracyModel createFromParcel(Parcel in) {
            return new MasterTargetAccuracyModel(in);
        }

        @Override
        public MasterTargetAccuracyModel[] newArray(int size) {
            return new MasterTargetAccuracyModel[size];
        }
    };

    public String  getTargetAccuracyId() {
        return targetAccuracyId;
    }

    public void setTargetAccuracyId(String targetAccuracyId) {
        this.targetAccuracyId = targetAccuracyId;
    }

    public int getOemId() {
        return oemId;
    }

    public void setOemId(int oemId) {
        this.oemId = oemId;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public String getTargetAccuracy() {
        return targetAccuracy;
    }

    public void setTargetAccuracy(String targetAccuracy) {
        this.targetAccuracy = targetAccuracy;
    }

    public String getTargetAccuracy_desc() {
        return targetAccuracy_desc;
    }

    public void setTargetAccuracy_desc(String targetAccuracy_desc) {
        this.targetAccuracy_desc = targetAccuracy_desc;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(targetAccuracyId);
        dest.writeInt(oemId);
        dest.writeInt(targetId);
        dest.writeString(targetAccuracy);
        dest.writeString(targetAccuracy_desc);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
    }
}
