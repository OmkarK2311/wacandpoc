package com.brainlines.weightloggernewversion;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.brainlines.weightloggernewversion.Production.activity.ProductionLoggerList;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;

import com.brainlines.weightloggernewversion.broadcastreceiver.NetworkChangeReceiver;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.TableListModelForInsertion;
import com.brainlines.weightloggernewversion.model.UserModel;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.brainlines.weightloggernewversion.utils.GlobalClass;
import com.brainlines.weightloggernewversion.utils.Queries;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CalibrationOrProductionActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "CalibrationOrProduction";
    RelativeLayout btn_Calibration, btn_Production;
    ImageButton btn_sync;
    TextView txt_calibration,txt_production;
    private NetworkChangeReceiver networkChangeReceiver;
    CardView cv_calibration,cv_production;
    DataBaseHelper helper;
    GlobalClass globalClass;
    SQLiteDatabase db;
    ProgressDialog dialog;
    private ArrayList<TableListModelForInsertion> tableListModelForInsertions = new ArrayList<>();
    String dbName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_cal_pro_selection);
        initUI();

//        UserModel model = getIntent().getParcelableExtra("UserDataModel");
//        final String dbName = "FG_"+model.getOEMID()+"_"+model.getModuleID()+".db";
//
//        File dbFile= helper.dbFilePath(CalibrationOrProductionActivity.this,DataBaseHelper.DATABASE_NAME);
//        String dbFilePath = dbFile.getAbsolutePath();
//
//        db = SQLiteDatabase.openDatabase(dbFilePath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
//        helper = new DataBaseHelper(CalibrationOrProductionActivity.this);
//        globalClass = new GlobalClass(CalibrationOrProductionActivity.this,helper);
        UserModel model = getIntent().getParcelableExtra("UserDataModel");
        dbName = GlobalClass.db_global_name;
        if (model != null){
            //dbName = "FG_"+model.getOEMID()+"_"+model.getModuleID()+".db";
//            AppPreferences.setDatabseName(this,dbName);
//            helper = new DataBaseHelper(this,dbName);
            helper = new DataBaseHelper(this);
            db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
        }else {
            //dbName = AppPreferences.getDatabseName(this);
            helper = new DataBaseHelper(this);
//            helper = new DataBaseHelper(this,dbName);
            db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
            Log.d(TAG, "onCreate: ");
        }

//        try {
//            if(helper.checkDataBase(helper.dbAbsolutePath(this,dbName))){
//                Log.e(TAG,"Data Base Already Exists");
//            }else {
//                Log.e(TAG,"Not Exists");
//            }
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        try {
//            helper.openDataBase(helper.dbAbsolutePath(this,dbName));
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        globalClass = new GlobalClass(this,helper);

        int c = helper.getTableCount();
        if (c == 0){
            Log.d(TAG, "onCreate: Table Count -> " + c);
            db.execSQL(Queries.DROP_PRODUCTION_KG_VALUES_MASTER_TABLES);
            db.execSQL(Queries.CREATE_PRODUCTION_KG_VALUES_MASTER_TABLES);
            InsertIntoGrammageTable();
            getCreateStatementsForDB();
        }else {
            Log.d(TAG, "onCreate: Table Count -> " + c );

        }
    }

    private void getCreateStatementsForDB() {
        dialog = new ProgressDialog(this);
        dialog.setMessage("wait");
        dialog.setCancelable(false);
        dialog.show();
        Log.d(TAG, "getCreateStatementsForDB: Inside");
        final API api = Retroconfig.baseMethod().create(API.class);
        Call<ResponseBody> call = api.createTableForModule(1234,3);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null){
                    try {
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String statusCode = jsonObject.getString("statusCode");
                        if (statusCode.equals("200")) {
                            JSONObject dataObject = jsonObject.getJSONObject("Data");
                            JSONArray tableDataArray = dataObject.getJSONArray("TableData");
                            if (tableDataArray.length() != 0){
                                helper.createTableFromServerData(tableDataArray);
                                getAllMasterData();
                            }
                        }
                        dialog.dismiss();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: Create Table API Failed");
            }
        });

    }

    private void getAllMasterData() {
        createTableList();
        Cursor cursor = helper.getFGMobileSQLLiteTableList();
        if (cursor!= null && cursor.getCount() > 0 ){
            while (cursor.moveToNext()){
                globalClass.syncSingleTable(cursor.getString(0), cursor.getString(1),dialog);
            }
        }
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1,1234,"mstMachineType","Machine Type","MachineTypeID","ModifiedDate","SyncMachineTypeDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(2,1234,"mstWireCup","WireCup","WireCupID","ModifiedDate","SyncWireCupDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(3,1234,"mstTargetAccuracy","Target Accuracy","TargetAccuracyID","ModifiedDate","SyncTargetAccuracyDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(4,1234,"mstProductionTolerance","Production Tolerance","ProductionToleranceID","ModifiedDate","SyncProductionToleranceDetails"));
////        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(5,1234,"mstProductionResonMaster","Production Reson","ReasonID","ModifiedDate","SyncProductionreasonDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(6,1234,"mstProductType","Product Type","ProductTypeID","ModifiedDate","SyncProductionTypeDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(7,1234,"mstFillerType","Filler Type","FillerTypeID","ModifiedDate","SyncFillerTypeDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(8,1234,"mstHead","Head","HeadID","ModifiedDate","SyncHeadDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(9,1234,"mstLogger","Logger","LoggerID","ModifiedDate","SyncLoggerDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(10,1234,"mstProducts","Product","ProductID","ModifiedDate","SyncProductDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(11,1234,"mstSealType","Seal Type","SealID","ModifiedDate","SyncSealTypeDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(12,1234,"mstUomTable","Uom","UOMId","ModifiedDate","SyncUOMDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(13,1234,"mstPouchAttributes","Pouch Attributes","PouchAttributesID","ModifiedDate","SyncPouchAttributesDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(14,1234,"mstPouchSymmetry","Pouch Symmetry","PouchSymmetryID","ModifiedDate","SyncPouchSymmetryDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(15,1234,"mstSealRejectionReasons","Seal Rejection Reason","rsn_id","ModifiedDate","SyncPouchSymmetryDetails"));
//        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(16,1234,"UserLogin","User login","LoginID","ModifiedDate","SyncPouchSymmetryDetails"));
//        for (int i = 0 ; i < getDataFromMasterTablesModels.size() ; i++){
//            globalMethodCall.syncSingleTable(getDataFromMasterTablesModels.get(i).getMasterTableName(),
//                    getDataFromMasterTablesModels.get(i).getKeyIDColumnName(),progressDialog);
//        }
    }

    private void createTableList() {
        tableListModelForInsertions.add(new TableListModelForInsertion( 1 ,"CalibrationBagLengthAnalytics","BagLengthAnalyticsID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 2 ,"CalibrationBagLengthVariationResults",    "BagLengthID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 3 ,"CalibrationComments","CommentsID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 4 ,"CalibrationObservations","ObservationsID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 5 ,"CalibrationPaperShiftAnalytics","PaperShiftAnalyticsID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 6 ,"CalibrationPaperShiftResults","PaperShiftID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 7 ,"CalibrationSealRejectReasons","SealRejectRsnID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 8 ,"CalibrationTags","CalTagsID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 9 ,"CalibrationWeightTestResults","TestWeightID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 10,"CalibrationWeightTestResultstAnalytics","WeightResultsAnalyticsID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 11,"FGMobileSQLLiteSyncTableList","ID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 12,"mstCalibrationTolerance","CalibrationToleranceID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 13,"mstFillerType","FillerTypeID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 14,"mstHead","HeadID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 15,"mstLogger","LoggerID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 16,"mstMachineType","MachineTypeID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 17,"mstPouchAttributes","PouchAttributesID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 18,"mstPouchSymmetry","PouchSymmetryID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 19,"mstProductionReasonMaster","ReasonID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 20,"mstProductionTolerance","ProductionToleranceID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 21,"mstProducts","ProductID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 22,"mstProductType","ProductTypeID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 23,"mstSealRejectionReasons","rsn_id")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 24,"mstSealType","SealID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 25,"mstTargetAccuracy","TargetAccuracyID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 26,"mstUomTable","UOMId")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 27,"mstWireCup","WireCupID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 28,"ProductionTag","ProductionTagID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 29,"ProductionTagResults","PTagResultsID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 30,"ProductionWeightReadings","PWeightReadingID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 31,"ProductionWeightReport","ReportID")) ;
        tableListModelForInsertions.add(new TableListModelForInsertion( 32,"UserLogin","LoginID")) ;
        for (int i = 0 ; i < tableListModelForInsertions.size() ; i++){
            String tableName = tableListModelForInsertions.get(i).getTableName().trim();
            String pkColumnName = tableListModelForInsertions.get(i).getPKColumnName().trim();
            String strIdentifier = tableName.substring(0,3);
            if (strIdentifier.equals("mst") || tableName.equals("UserLogin")){
                helper.addMasterTableDataToLocalDb1(i+1,tableName,pkColumnName,
                        "29 May 2020",false,db);
            }
        }
    }

    private void initUI() {
        btn_Calibration = findViewById(R.id.relative_calibration);
        btn_Production = findViewById(R.id.relative_production);
        btn_sync = findViewById(R.id.btn_sync);
        txt_calibration = findViewById(R.id.txt_calibration);
        txt_production = findViewById(R.id.txt_production);
        cv_production = findViewById(R.id.cv_production);
        cv_calibration = findViewById(R.id.cv_calibration);

        btn_Production.setOnClickListener(this);
        btn_Calibration.setOnClickListener(this);
        txt_calibration.setOnClickListener(this);
        txt_production.setOnClickListener(this);
        btn_sync.setOnClickListener(this);
        cv_calibration.setOnClickListener(this);
        cv_production.setOnClickListener(this);
        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerNetworkBroadcastForNougat();
//        startTimer();
    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_calibration:
                Intent cal = new Intent(this, NewTestActivity.class);
                startActivity(cal);
                break;

            case R.id.txt_production:
                Intent pro = new Intent(this, ProductionLoggerList.class);
                startActivity(pro);
                break;

            case R.id.cv_calibration:
                Intent cv_calibration = new Intent(this, NewTestActivity.class);
                startActivity(cv_calibration);
                break;

                case R.id.cv_production:
                Intent cv_production = new Intent(this, ProductionLoggerList.class);
                startActivity(cv_production);
                break;
            case R.id.btn_sync:
                try {
                    syncAllAppData();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void syncAllAppData() throws JSONException {
        getAllDatafromTableIntoJson();
    }

    public ArrayList<String> createTXNTableList(){
        ArrayList<String> tableList = new ArrayList<>();
        tableList.add(Constants.C_TAGS_TABLE);
        tableList.add(Constants.BAG_LENGTH_ANALYTICS_TABLE);
        tableList.add(Constants.BAG_LENGTH_VARIATION_RESULT);
        tableList.add(Constants.PAPER_SHIFT_RESULTS);
        tableList.add(Constants.PAPER_SHIFT_ANALYTICS_TABLE);
        tableList.add(Constants.WEIGHT_RESULTS_ANALYTICS_TABLE);
        tableList.add(Constants.WEIGHT_TEST_RESULT);
        tableList.add(Constants.OBSERVATIONS_TABLE);
        tableList.add(Constants.COMMENTS_TABLE);
        tableList.add(Constants.PRODUCTION_TAG_TABLE);
        tableList.add(Constants.P_TAG_RESULT_TABLE);
        tableList.add(Constants.P_WEIGHT_READING_TABLE);
        tableList.add(Constants.PRODUCTION_WEIGHT_REPORT);
        return tableList;
    }

    private void getAllDatafromTableIntoJson() throws JSONException {
        //Cursor cursor = helper.getCursorfromTableWhereIsSyncZero(Constants.BAG_LENGTH_ANALYTICS_TABLE);
        for (String tableName : createTXNTableList()){
            //helper.updateTableToSetIsSyncEqualtoOne(tableName);
            Cursor cursor = helper.getCursorfromTableWhereIsSyncZero(tableName);
            int count = cursor.getCount();
            String oem_id = "1234";
            final String table_name = tableName;
            String module_id = "3";
            JsonArray array = globalClass.getTableDataToJSon(cursor);
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("Data",array);

            API api = Retroconfig.baseMethod().create(API.class);
            retrofit2.Call<ResponseBody> call = api.SyncTXNDataForThisTable(jsonObject,oem_id,table_name,module_id);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.body() != null) {
                        try {
                            String res = response.body().string();
                            JSONObject object = new JSONObject(res);
                            String statusCode = object.getString("statusCode");
                            int Data=object.getInt("Data");
                            if (statusCode.equals("200") && Data == 1) {
                                helper.updateTableToSetIsSyncEqualtoOne1(table_name,db);
                                Log.d(TAG, "onResponse: Data is Inserted");
                            }

                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }

    @Override
    protected void dialog(boolean value) {
        super.dialog(value);
        if (value) {
            //syncAllAppData();
            // Toast.makeText(this, "Data Inserted", Toast.LENGTH_SHORT).show();
        } else {
            // Toast.makeText(this, "Data Not Inserted", Toast.LENGTH_SHORT).show();
        }
    }

    public void InsertIntoGrammageTable() {
//        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(Constants.PRODUCTION_GRAMMAGE,"Kg");
        cv.put(Constants.PRODUCTION_KG_VALUES,"1000");
        long d=db.insert(Constants.PRODUCTION_KG_VALUES_MASTER_TABLES,null,cv);
        Log.d("Success", String.valueOf(d));
    }

//    private void startTimer(){
//        Timer timerObj = new Timer();
//        TimerTask timerTaskObj = new TimerTask() {
//            int i = 0;
//            public void run() {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            syncAllAppData();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//            }};
//        timerObj.schedule(timerTaskObj, 0, 1800000);
//
//    }
}