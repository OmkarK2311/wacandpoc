package com.brainlines.weightloggernewversion.activity;

import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.brainlines.weightloggernewversion.broadcastreceiver.NetworkChangeReceiver;
import com.brainlines.weightloggernewversion.listener.ConnectionCheckingInterface;

public abstract class BaseActivity extends AppCompatActivity implements ConnectionCheckingInterface {

    private NetworkChangeReceiver networkChangeReceiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerNetworkBroadcastForNougat();
    }

    protected void dialog(boolean value){}

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    public void connectionLost(boolean value) {
        boolean v = value;
        dialog(value);
    }
}
