package com.brainlines.weightloggernewversion;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.activity.LoggerFunctionActivity;
import com.brainlines.weightloggernewversion.adapter.LoggerListAdapter;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;

import com.brainlines.weightloggernewversion.listener.GoToNewTestTagFillAcitivity;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;
import com.brainlines.weightloggernewversion.model.MasterFillerTypeModel;
import com.brainlines.weightloggernewversion.model.MasterHeadModel;
import com.brainlines.weightloggernewversion.model.MasterMachineTypeModel;
import com.brainlines.weightloggernewversion.model.MasterPouchSymmetryModel;
import com.brainlines.weightloggernewversion.model.MasterProduct;
import com.brainlines.weightloggernewversion.model.MasterSealTypeModel;
import com.brainlines.weightloggernewversion.model.MasterTargetAccuracyModel;
import com.brainlines.weightloggernewversion.model.MasterWireCupModel;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.AsyncTasks;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class NewTestActivity extends AppCompatActivity implements GoToNewTestTagFillAcitivity {

    private static final String TAG = "NewTestActivity";

    RecyclerView rv_Logger_List;
    LoggerListAdapter adapter;
    ArrayList<LoggersFromDBModel> loggerList = new ArrayList<>();
    ArrayList<MasterFillerTypeModel> masterFillerTypeList = new ArrayList<>();
    ArrayList<MasterHeadModel> masterHeadModelList = new ArrayList<>();
    ArrayList<MasterPouchSymmetryModel> masterPouchSymmetryList = new ArrayList<>();
    ArrayList<MasterTargetAccuracyModel> masterTargetAccuracyModels = new ArrayList<>();
    ArrayList<MasterProduct> masterProductTypeArrayList = new ArrayList<>();
    ArrayList<MasterMachineTypeModel> machineTypeModels = new ArrayList<>();
    ArrayList<MasterSealTypeModel> sealTypeModels = new ArrayList<>();
    ArrayList<MasterWireCupModel> masterWireCupModels = new ArrayList<>();
    DataBaseHelper helper;
    private String dataPort = Constants.DATA_LOGGER_PORT+"";
    private String dbName;
    private SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_test);
        try {
            initUI();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initUI() throws ExecutionException, InterruptedException {
        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
        rv_Logger_List = findViewById(R.id.rv_Logger_List);
        ArrayList<LoggersFromDBModel> loggers = getLoggerListFromDB();

        LinearLayoutManager manager = new LinearLayoutManager(this);
        rv_Logger_List.setLayoutManager(manager);
        adapter = new LoggerListAdapter(this,loggers,this);
        rv_Logger_List.setAdapter(adapter);

    }

    private ArrayList<LoggersFromDBModel> getLoggerListFromDB() throws ExecutionException, InterruptedException {
        loggerList.clear();
//        loggerList = new AsyncTasks.GetLoggerListFromDB(this).execute().get();
        Cursor data = helper.getLoggerList();
        while (data.moveToNext()){
            String loggerId = data.getString(0);
            String oemId = data.getString(1);
            String srNum = data.getString(2);
            String loggerName = data.getString(3);
            String ip = data.getString(4);
            String mac = data.getString(5);
            String location = data.getString(6);
            String createTimeStamp = data.getString(7);
            String createdBy = data.getString(8);
            String uptimestamp = data.getString(9);
            String modifiedBy = data.getString(10);
            boolean isActive = data.getInt(11) > 0;
            boolean inUse = data.getInt(12) > 0;
            loggerList.add(new LoggersFromDBModel(loggerId,oemId,srNum,loggerName,mac,ip,location,uptimestamp,modifiedBy,createTimeStamp,
                    createdBy,uptimestamp,isActive,inUse+""));
        }
        return loggerList;
    }


    @Override
    public void getLogger(String loggerName, String loggerIpAddress,String loggerMacId) {
        try {
            String loggerStatus = getDataLoggerStatus(loggerName);
            String status1 = "false";
            if (loggerStatus.equals(status1)){
                String ifReady = checkIfIsReady(loggerIpAddress,loggerName);
                String status = ifReady;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
//        Intent intent = new Intent(this, LoggerFunctionActivity.class);
//        intent.putExtra("LoggerName",loggerName);
//        intent.putExtra("LoggerIP",loggerIpAddress);
//        intent.putExtra("LoggerMacId",loggerMacId);
//        startActivity(intent);
    }

    private String getDataLoggerStatus(String loggerName) {
        Cursor loggerStatus = helper.getLoggerStatusFromLoggerTable1(loggerName,db);
        String loggerStatusYesOrNo = "";
        while (loggerStatus.moveToNext()){
            loggerStatusYesOrNo = loggerStatus.getString(0);
            Log.d(TAG, "getFillerType: ");
        }
        return loggerStatusYesOrNo;
    }

    private String checkIfIsReady(String dataLoggerIP, String dataLoggerName) throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpClient httpClient = new DefaultHttpClient();
        String responseString="";
        String url = "http://"+dataLoggerIP+":"+dataPort+"/hello?loggername="+dataLoggerName;
        //String url = "http://"+dataLoggerIP+":"+dataPort+"/hello?loggername=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            if (responseString.equals("ok")){
                changeDataLoggerStatus(dataLoggerName,"Yes");
                Intent intent = new Intent(this, LoggerFunctionActivity.class);
                intent.putExtra("LoggerName",dataLoggerName);
                intent.putExtra("LoggerIP",dataLoggerIP);
                startActivity(intent);
            }
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;
    }

    private void changeDataLoggerStatus(String dataLoggerName, String yes) {
        helper.updateMstLoggerTableForLoggerStatus1(dataLoggerName,db);
    }
}