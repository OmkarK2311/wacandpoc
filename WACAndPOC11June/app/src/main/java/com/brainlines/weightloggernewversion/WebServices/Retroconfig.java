package com.brainlines.weightloggernewversion.WebServices;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retroconfig {

    static String SYNC_DATA_BASEURL = "http://apps.brainlines.net/WeightLoggerApi/api/WeightLogger/";
    static String MASTER_DATA_BASEURL = "http://apps.brainlines.net/WeightLoggerMasterSyncAPIPublish/api/WeightLogger/";
    static String TESTING_DATA_BASEURL = "https://fgmobileapi.decintell.co.in/api/FGMobileAPI/";
    static String BASE_URL = "https://fgmobileapi.decintell.co.in/WAC/api/FGMobileAPI/";
    //static String MASTER_DATA_BASEURL1 = "https://fgmobileapi.decintell.co.in/api/FGMobileAPI/";

    public static Retrofit baseMethod(){
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(5000, TimeUnit.SECONDS);
        b.writeTimeout(5000, TimeUnit.SECONDS);
        OkHttpClient client = b.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }

    public static Retrofit syncDataRetrofit(){
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(5000, TimeUnit.SECONDS);
        b.writeTimeout(5000, TimeUnit.SECONDS);
        OkHttpClient client = b.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SYNC_DATA_BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }
    public static Retrofit masterDataRetrofit(){
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.readTimeout(5000, TimeUnit.SECONDS);
        b.writeTimeout(5000, TimeUnit.SECONDS);
        OkHttpClient client = b.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MASTER_DATA_BASEURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }
}
