package com.brainlines.weightloggernewversion.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.brainlines.weightloggernewversion.Production.Model.ProductionTagModel;
import com.brainlines.weightloggernewversion.Production.Model.ProductionTagSyncDataModel;
import com.brainlines.weightloggernewversion.Production.Model.Production_Reason_Table;
import com.brainlines.weightloggernewversion.Production.Model.Production_Tolerance_Model;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;
import com.brainlines.weightloggernewversion.model.MasterFillerTypeModel;
import com.brainlines.weightloggernewversion.model.MasterHeadModel;
import com.brainlines.weightloggernewversion.model.MasterMachineTypeModel;
import com.brainlines.weightloggernewversion.model.MasterProduct;
import com.brainlines.weightloggernewversion.model.MasterSealTypeModel;
import com.brainlines.weightloggernewversion.model.MasterSealTypeRejectionReasonsModel;
import com.brainlines.weightloggernewversion.model.MasterTargetAccuracyModel;
import com.brainlines.weightloggernewversion.model.MasterToleranceTable;
import com.brainlines.weightloggernewversion.model.MasterUoMTableModel;
import com.brainlines.weightloggernewversion.model.MasterWireCupModel;
import com.brainlines.weightloggernewversion.model.UserLoginModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AsyncTasks {

    private static final String TAG = "AsyncTasks";

    //Get User's list from database
    public static class GetLoginUsersFromDB extends AsyncTask<String,Void, ArrayList<UserLoginModel>>{

        Context context;
        ArrayList<UserLoginModel> usersList = new ArrayList<>();

        public GetLoginUsersFromDB(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "onPreExecute: ");
        }

        @Override
        protected ArrayList<UserLoginModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getUsersListFromDB = conn.prepareStatement("SELECT * FROM UserLogin WHERE IsActive=1 AND IsDeleted=0");
                    ResultSet rs = getUsersListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    usersList.clear();
                    while (rs.next()) {
                        String loginId = rs.getString("LoginID");
                        String userId = rs.getString("UserID");
                        String roleId = rs.getString("RoleID");
                        String userName = rs.getString("UserName");
                        String password = rs.getString("Password");
                        boolean isActive = rs.getBoolean("IsActive");
                        boolean isDeleted = rs.getBoolean("IsDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        usersList.add(new UserLoginModel(loginId, userId, roleId, userName, password, isActive, isDeleted, createdDate, modifiedDate));
                    }
                    conn.commit();
                    getUsersListFromDB.close();

                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | SQLException | ClassNotFoundException | InstantiationException e) {
                e.printStackTrace();
            }
            return usersList;
        }

            @Override
        protected void onPostExecute(ArrayList<UserLoginModel> usersFromDBModels) {
            super.onPostExecute(usersFromDBModels);
        }
    }
    //**Get User's list from database

    //Get Logger's list from database
    public static class GetLoggerListFromDB extends AsyncTask<String,Void,ArrayList<LoggersFromDBModel>>{

        Context context;
        ArrayList<LoggersFromDBModel> loggersList = new ArrayList<>();

        public GetLoggerListFromDB(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<LoggersFromDBModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("SELECT * FROM mstLogger WHERE IsActive=1");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    loggersList.clear();
                    while (rs.next()) {
                        String id = rs.getString("LoggerID");
                        String oem_id = rs.getString("OEMID");
                        String serial_num = rs.getString("SerialNumber");
                        String logger_name = rs.getString("LoggerName");
                        String mac_id = rs.getString("logger_mac_id");
                        String ip_address = rs.getString("LocalIpAddress");
                        String logger_location = rs.getString("LoggerLocation");
                        //String create_tmstmp = rs.getString("CreateTimestamp");
                        String create_by_id = rs.getString("CreatedBy");
                        //String update_tmstmp = rs.getString("UpdateTimestamp");
                        String update_by_id = rs.getString("ModifiedBy");
                        boolean isactive = rs.getBoolean("IsActive");
                        //String inuse = rs.getString("IsInUse");
                        String modified_date = "null";
                        loggersList.add(new LoggersFromDBModel(id,oem_id,serial_num,logger_name,mac_id,ip_address,logger_location,modified_date,update_by_id,"create_tmstmp",create_by_id,"update_tmstmp",isactive,"1"));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | ClassNotFoundException | SQLException | InstantiationException e) {
                e.printStackTrace();
            }
            return loggersList;
        }

        @Override
        protected void onPostExecute(ArrayList<LoggersFromDBModel> usersFromDBModels) {
            super.onPostExecute(usersFromDBModels);
        }
    }
    //**Get Logger's list from database

    //Get Master Filler Type
    public static class GetMasterFillerTypeFromDB extends AsyncTask<String,Void,ArrayList<MasterFillerTypeModel>>{

        Context context;
        ArrayList<MasterFillerTypeModel> fillerList = new ArrayList<>();

        public GetMasterFillerTypeFromDB(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterFillerTypeModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from mstFillerType where IsActive=1 and IsDeleted=0");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    fillerList.clear();
                    while (rs.next()) {
                        String id = rs.getString("FillerTypeID");
                        String oem_id = "1234";
                        String productTYpe = rs.getString("ProductTypeID");
                        String fillerType = rs.getString("FillerType");
                        boolean isactive = rs.getBoolean("IsActive");
                        boolean isdeleted = rs.getBoolean("IsDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        fillerList.add(new MasterFillerTypeModel(id,oem_id,productTYpe,fillerType,createdDate,modifiedDate,createdBy,modifiedby,isactive,isdeleted));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | ClassNotFoundException | SQLException | InstantiationException e) {
                e.printStackTrace();
            }
            return fillerList;
        }

        @Override
        protected void onPostExecute(ArrayList<MasterFillerTypeModel> models) {
            super.onPostExecute(models);
        }
    }
    //**Get Master Filler Type

    //Get Master Head Type
    public static class GetMasterHeadTable extends AsyncTask<String,Void,ArrayList<MasterHeadModel>>{

        Context context;
        ArrayList<MasterHeadModel> masterHeadModelList = new ArrayList<>();

        public GetMasterHeadTable(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterHeadModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from mstHead where IsActive=1");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    masterHeadModelList.clear();
                    while (rs.next()) {
                        String id = rs.getString("HeadID");
                        String oem_id = "1234";
                        String headType = rs.getString("HeadType");
                        boolean isactive = rs.getBoolean("IsActive");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        masterHeadModelList.add(new MasterHeadModel(id,oem_id,headType,isactive,createdDate,modifiedDate,createdBy,modifiedby));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | SQLException | ClassNotFoundException | InstantiationException e) {
                e.printStackTrace();
            }
            return masterHeadModelList;
        }

        @Override
        protected void onPostExecute(ArrayList<MasterHeadModel> masterHeadModels) {
            super.onPostExecute(masterHeadModels);
        }
    }
    //**Get Master Head Type

    //Get Target Accuracy
    public static class GetTargetAccuracy extends AsyncTask<String,Void,ArrayList<MasterTargetAccuracyModel>>{

        Context context;
        ArrayList<MasterTargetAccuracyModel> masterTargetAccuracyModels = new ArrayList<>();

        public GetTargetAccuracy(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterTargetAccuracyModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from mstTargetAccuracy");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    masterTargetAccuracyModels.clear();
                    while (rs.next()) {
                        String target_id = rs.getString("TargetAccuracyID");
                        int oem_id = 1234;
                        String target_accuracy = rs.getString("TargetAccuracy");
                        String target_accuracy_desc = rs.getString("TargetAccuracy_Desc");
                        boolean isactive = rs.getBoolean("IsActive");
                        boolean isDeleted = rs.getBoolean("IsDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        masterTargetAccuracyModels.add(new MasterTargetAccuracyModel(target_id,oem_id,0,target_accuracy,target_accuracy_desc,isactive,isDeleted,createdDate,modifiedDate,createdBy,modifiedby));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return masterTargetAccuracyModels;
        }

        @Override
        protected void onPostExecute(ArrayList<MasterTargetAccuracyModel> masterPouchSymmetryModels) {
            super.onPostExecute(masterPouchSymmetryModels);
        }
    }
    //**Get Target Accuracy

    //Get Product master
    public static class GetProductMaster extends AsyncTask<String,Void,ArrayList<MasterProduct>>{

        Context context;
        ArrayList<MasterProduct> masterproductlist = new ArrayList<>();

        public GetProductMaster(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterProduct> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("  select * from mstProduct where isactive='1' and isdeleted='0'");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    masterproductlist.clear();
                    while (rs.next()) {
                        String product_id = rs.getString("ProductID");
                        int oem_id = 1234;
                        String product_type = rs.getString("ProductTypeID");
                        String product = rs.getString("Product");
                        //String product = rs.getString("product");
                        boolean isactive = rs.getBoolean("IsActive");
                        boolean isDeleted = rs.getBoolean("IsDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        masterproductlist.add(new MasterProduct(product_id,oem_id,product_type,product,isactive,isDeleted,createdDate,modifiedDate,createdBy,modifiedby));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | ClassNotFoundException | SQLException | InstantiationException e) {
                e.printStackTrace();
            }
            return masterproductlist;
        }

        @Override
        protected void onPostExecute(ArrayList<MasterProduct> masterPouchSymmetryModels) {
            super.onPostExecute(masterPouchSymmetryModels);
        }
    }
    //**Get Product master

    //Get Machine Type master
    public static class GetMachineTypeMaster extends AsyncTask<String,Void,ArrayList<MasterMachineTypeModel>>{

        Context context;
        ArrayList<MasterMachineTypeModel> machinetypelist = new ArrayList<>();

        public GetMachineTypeMaster(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterMachineTypeModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from mstMachineType");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    machinetypelist.clear();
                    while (rs.next()) {
                        String product_id = rs.getString("MachineID");
                        int oem_id = 1234;
                        String product_type = rs.getString("ProductTypeID");
                        String product = rs.getString("ProductTypeID");
                        String machine_name = rs.getString("MachineName");
                        String machine_description = rs.getString("MachineDescription");
                        boolean isactive = rs.getBoolean("IsActive");
                        boolean isDeleted = rs.getBoolean("IsDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        machinetypelist.add(new MasterMachineTypeModel(product_id,oem_id,product_type,product,machine_name,machine_description,isactive,isDeleted,createdDate,modifiedDate,createdBy,modifiedby));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return machinetypelist;
        }

        @Override
        protected void onPostExecute(ArrayList<MasterMachineTypeModel> masterPouchSymmetryModels) {
            super.onPostExecute(masterPouchSymmetryModels);
        }
    }
    //**Get Machine Type master

//    //Get Pouch Attributes master  (it is not used in existing project)
//    public static class GetPouchAttributesMaster extends AsyncTask<String,Void,ArrayList<MasterPouchAttributesModel>>{
//
//        Context context;
//        ArrayList<MasterPouchAttributesModel> pouchattributeslist = new ArrayList<>();
//
//        public GetPouchAttributesMaster(Context context) {
//            this.context = context;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected ArrayList<MasterPouchAttributesModel> doInBackground(String... strings) {
//            Connection conn = null;
//            try {
//                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
//                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
//                if (conn != null) {
//                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
//                    conn.setAutoCommit(false);
//                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select machine_name,product_type from Mstr_machine_type_tbl where isactive=1 and isdeleted=0");
//                    ResultSet rs = getLoggerListFromDB.executeQuery();
//                    String resultSet = rs.toString();
//                    pouchattributeslist.clear();
//                    while (rs.next()) {
//                        String product_id = rs.getString("machine_id");
//                        int oem_id = 1234;
//                        String product_type = rs.getString("product_id");
//                        String product = rs.getString("product_type");
//                        String machine_name = rs.getString("machine_name");
//                        String machine_description = rs.getString("machine_description");
//                        boolean isactive = rs.getBoolean("isactive");
//                        boolean isDeleted = rs.getBoolean("isdeleted");
//                        String createdDate = rs.getString("createddate");
//                        String modifiedDate = rs.getString("modifieddate");
//                        String createdBy = rs.getString("createdby");
//                        String modifiedby = rs.getString("modifiedby");
//                       // pouchattributeslist.add(new MasterPouchAttributesModel(product_id,oem_id,product_type,product,machine_name,machine_description,isactive,isDeleted,createdDate,modifiedDate,createdBy,modifiedby));
//                    }
//                    conn.commit();
//                    getLoggerListFromDB.close();
//                } else if (conn == null) {
//                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
//                }
//            } catch (IllegalAccessException e) {
//                e.printStackTrace();
//            } catch (InstantiationException e) {
//                e.printStackTrace();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
//            return pouchattributeslist;
//        }
//
//        @Override
//        protected void onPostExecute(ArrayList<MasterPouchAttributesModel> masterPouchSymmetryModels) {
//            super.onPostExecute(masterPouchSymmetryModels);
//        }
//    }

    //Get Machine seal type master
    public static class GetSealTypeMaster extends AsyncTask<String,Void,ArrayList<MasterSealTypeModel>>{

        Context context;
        ArrayList<MasterSealTypeModel> sealtypelist = new ArrayList<>();

        public GetSealTypeMaster(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterSealTypeModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from mstSealType");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    sealtypelist.clear();
                    while (rs.next()) {
                        String seal_id = rs.getString("SealID");
                        int oem_id = 1234;
                        String seal_type = rs.getString("SealType");
                        String product_type = rs.getString("ProductTypeID");
                        boolean isactive = rs.getBoolean("IsActive");
                        boolean isDeleted = rs.getBoolean("IsDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        sealtypelist.add(new MasterSealTypeModel(seal_id,oem_id,seal_type,product_type,createdDate,modifiedDate,createdBy,modifiedby,isactive,isDeleted));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return sealtypelist;
        }

        @Override
        protected void onPostExecute(ArrayList<MasterSealTypeModel> masterPouchSymmetryModels) {
            super.onPostExecute(masterPouchSymmetryModels);
        }
    }
    //**Get Machine seal type master

    //Get Product Type MAster
//    public static class GetProductTypeMAsterTable extends AsyncTask<String,Void,ArrayList<MasterProductTypeModel>>{
//
//        Context context;
//        ArrayList<MasterProductTypeModel> productTypeModels = new ArrayList<>();
//
//        public GetProductTypeMAsterTable(Context context) {
//            this.context = context;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected ArrayList<MasterProductTypeModel> doInBackground(String... strings) {
//            Connection conn = null;
//            try {
//                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
//                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
//                if (conn != null) {
//                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
//                    conn.setAutoCommit(false);
//                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from Mstr_product_type_tbl where isactive='1' and isdeleted='0'");
//                    ResultSet rs = getLoggerListFromDB.executeQuery();
//                    String resultSet = rs.toString();
//                    productTypeModels.clear();
//                    while (rs.next()) {
//                        String seal_id = rs.getString("seal_id");
//                        int oem_id = 1234;
//                        String seal_type = rs.getString("seal_type");
//                        String product_type = rs.getString("product_type");
//                        boolean isactive = rs.getBoolean("isactive");
//                        boolean isDeleted = rs.getBoolean("isdeleted");
//                        String createdDate = rs.getString("createddate");
//                        String modifiedDate = rs.getString("modifieddate");
//                        String createdBy = rs.getString("createdby");
//                        String modifiedby = rs.getString("modifiedby");
//                        productTypeModels.add(new MasterSealTypeModel(seal_id,oem_id,seal_type,product_type,createdDate,modifiedDate,createdBy,modifiedby,isactive,isDeleted));
//                    }
//                    conn.commit();
//                    getLoggerListFromDB.close();
//                } else if (conn == null) {
//                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
//                }
//            } catch (IllegalAccessException e) {
//                e.printStackTrace();
//            } catch (InstantiationException e) {
//                e.printStackTrace();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            } catch (ClassNotFoundException e) {
//                e.printStackTrace();
//            }
//            return productTypeModels;
//        }
//
//        @Override
//        protected void onPostExecute(ArrayList<MasterProductTypeModel> masterPouchSymmetryModels) {
//            super.onPostExecute(masterPouchSymmetryModels);
//        }
//    }

    //Get Wire Cup master
    public static class GetWireCupMaster extends AsyncTask<String,Void,ArrayList<MasterWireCupModel>>{

        Context context;
        ArrayList<MasterWireCupModel> masterWireCupModels = new ArrayList<>();

        public GetWireCupMaster(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterWireCupModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from mstWireCup");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    masterWireCupModels.clear();
                    while (rs.next()) {
                        String wire_cup_id  = rs.getString("WireCupID");
                        int oem_id = 1234;
                        String wire_cup = rs.getString("WireCup");
                        boolean isactive = rs.getBoolean("IsActive");
                        boolean isDeleted = rs.getBoolean("IsDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        masterWireCupModels.add(new MasterWireCupModel(wire_cup_id,oem_id,wire_cup,isactive,isDeleted,createdDate,modifiedDate,createdBy,modifiedby));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return masterWireCupModels;
        }

        @Override
        protected void onPostExecute(ArrayList<MasterWireCupModel> masterWireCupModels) {
            super.onPostExecute(masterWireCupModels);
        }
    }

    //Get Master Tolerance table
    public static class GetMasterToleranceTable extends AsyncTask<String,Void,ArrayList<MasterToleranceTable>>{

        Context context;
        ArrayList<MasterToleranceTable> masterToleranceTableList = new ArrayList<>();

        public GetMasterToleranceTable(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterToleranceTable> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from Tolerance");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    masterToleranceTableList.clear();
                    while (rs.next()) {
                        String tolerance_id  = rs.getString("ToleranceID");
                        int oem_id = 1234;
                        String product_type = rs.getString("ProductType");
                        int target_weight = rs.getInt("TargetWeight");
                        int min_target_weight = rs.getInt("MinTargetWeight");
                        int max_target_weight = rs.getInt("MaxTargetWeight");
                        int min_weight_samples = rs.getInt("MinWeightSamples");
                        int min_ppr_shift_samples = rs.getInt("MinPaperShiftSamples");
                        int ppr_shift_tolerance = rs.getInt("PaperShiftTolerance");
                        int min_bag_length_samples = rs.getInt("MinBagLengthSamples");
                        int bag_length_tolerance = rs.getInt("BagLengthTolerance");
                        boolean isactive = rs.getBoolean("IsActive");
                        boolean isdeleted = rs.getBoolean("IsDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        //String NegativeTolerance = rs.getString("NegativeTolerance");
                        //String PositiveTolerance = rs.getString("PositiveTolerance");

                        masterToleranceTableList.add(new MasterToleranceTable(tolerance_id,oem_id,product_type,target_weight,min_target_weight,
                                max_target_weight,min_weight_samples,min_ppr_shift_samples,ppr_shift_tolerance,min_bag_length_samples,
                                bag_length_tolerance,isactive,isdeleted,createdDate,modifiedDate,createdBy,modifiedby,0,0,0,"",""));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
            return masterToleranceTableList;
        }
        @Override
        protected void onPostExecute(ArrayList<MasterToleranceTable> masterToleranceTables) {
            super.onPostExecute(masterToleranceTables);
        }
    }
    //**Get Master Tolerance table

    //Get Master UoM Table
    public static class GetMasterUoMTable extends AsyncTask<String,Void,ArrayList<MasterUoMTableModel>>{

        Context context;
        ArrayList<MasterUoMTableModel> masterUoMTableModes = new ArrayList<>();

        public GetMasterUoMTable(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterUoMTableModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from mstUomTable");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    masterUoMTableModes.clear();
                    while (rs.next()) {
                        String uomId  = rs.getString("UOMId");
                        int oem_id = 1234;
                        String product_type = rs.getString("ProductType");
                        String product_uom = rs.getString("ProductUOM");
                        boolean isactive = rs.getBoolean("isActive");
                        boolean isdeleted = rs.getBoolean("isDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        masterUoMTableModes.add(new MasterUoMTableModel(uomId,oem_id,product_type,product_uom,isactive,isdeleted,createdDate,modifiedDate,createdBy,modifiedby));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return masterUoMTableModes;
        }

        @Override
        protected void onPostExecute(ArrayList<MasterUoMTableModel> masterUoMTableModels) {
            super.onPostExecute(masterUoMTableModels);
        }
    }
    //**Get Master UoM Table

    //Get Master Seal Rejection Reason Table
    public static class GetSealRejectionReasons extends AsyncTask<String,Void,ArrayList<MasterSealTypeRejectionReasonsModel>>{

        Context context;

        ArrayList<MasterSealTypeRejectionReasonsModel> rejectionReasonsModels = new ArrayList<>();

        public GetSealRejectionReasons(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<MasterSealTypeRejectionReasonsModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from SealRejectionReasonMasterTable");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    rejectionReasonsModels.clear();
                    while (rs.next()) {
                        String rsn_id  = rs.getString("rsn_id");
                        int oem_id = 1234;
                        String product_type = rs.getString("product_type");
                        String reason_desc = rs.getString("reason_desc");
                        boolean isactive = rs.getBoolean("IsActive");
                        boolean isdeleted = rs.getBoolean("IsDeleted");
                        String createdDate = rs.getString("CreatedDate");
                        String modifiedDate = rs.getString("ModifiedDate");
                        String createdBy = rs.getString("CreatedBy");
                        String modifiedby = rs.getString("ModifiedBy");
                        rejectionReasonsModels.add(new MasterSealTypeRejectionReasonsModel(rsn_id,oem_id,product_type,reason_desc,isactive,isdeleted,createdDate,modifiedDate,createdBy,modifiedby));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return rejectionReasonsModels;
        }

        @Override
        protected void onPostExecute(ArrayList<MasterSealTypeRejectionReasonsModel> masterSealTypeRejectionReasonsModels) {
            super.onPostExecute(masterSealTypeRejectionReasonsModels);
        }
    }

    public static class GetMasterProductionToleranceTable extends AsyncTask<String, Void, ArrayList<Production_Tolerance_Model>> {

        Context context;
        ArrayList<Production_Tolerance_Model> masterProductionToleranceModel = new ArrayList<>();

        public GetMasterProductionToleranceTable(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Production_Tolerance_Model> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from mstProductionTolerance");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    masterProductionToleranceModel.clear();
                    while (rs.next()) {
                        String ProductionToleranceID  = rs.getString("ProductionToleranceID");
                        int oem_id = 1234;
                        //String product_type = rs.getString("OEMID");
                        String Grammage = rs.getString("Grammage");
                        String TolerancePositive = rs.getString("TolerancePositive");
                        String ToleranceNegetive = rs.getString("ToleranceNegetive");
                        boolean Isactive = rs.getBoolean("Isactive");
                        boolean IsDeleted = rs.getBoolean("IsDeleted");
                        String CreatedDate = rs.getString("CreatedDate");
                        String ModifiedDate = rs.getString("ModifiedDate");
                        masterProductionToleranceModel.add(new Production_Tolerance_Model(ProductionToleranceID,oem_id,Grammage,TolerancePositive,ToleranceNegetive,Isactive,IsDeleted,CreatedDate,ModifiedDate));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return masterProductionToleranceModel;
        }

        @Override
        protected void onPostExecute(ArrayList<Production_Tolerance_Model> masterUoMTableModels) {
            super.onPostExecute(masterUoMTableModels);
        }
    }

    public static class GetMasterProductionReasonTable extends AsyncTask<String, Void, ArrayList<Production_Reason_Table>> {

        Context context;
        ArrayList<Production_Reason_Table> masterProductionReasonModel = new ArrayList<>();

        public GetMasterProductionReasonTable(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Production_Reason_Table> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from mstProductionResonMaster");

                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    masterProductionReasonModel.clear();
                    while (rs.next()) {
                        String ReasonID = rs.getString("ReasonID");
                        String ReasonDesc = rs.getString("ReasonDesc");
                        String CreatedDate = rs.getString("CreatedDate");
                        String ModifiedDate = rs.getString("ModifiedDate");

                        masterProductionReasonModel.add(new Production_Reason_Table(ReasonID,ReasonDesc,CreatedDate,ModifiedDate));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return masterProductionReasonModel;
        }

        @Override
        protected void onPostExecute(ArrayList<Production_Reason_Table> masterProductionReasonModel) {
            super.onPostExecute(masterProductionReasonModel);
        }
    }

   /* public static class GetProductionTagTransactionData extends AsyncTask<String, Void, ArrayList<ProductionTagSyncDataModel>> {

        Context context;
        ArrayList<ProductionTagSyncDataModel> transactionproductionDataModel = new ArrayList<>();

        public GetProductionTagTransactionData(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<ProductionTagSyncDataModel> doInBackground(String... strings) {
            Connection conn = null;
            try {
                Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(Constants.dbUrl, Constants.dbUserName, Constants.dbPassword);
                if (conn != null) {
                    Log.d(TAG, "getDbConnection: " + "Database is connected to database");
                    conn.setAutoCommit(false);
                    PreparedStatement getLoggerListFromDB = conn.prepareStatement("select * from ProductionTag");
                    ResultSet rs = getLoggerListFromDB.executeQuery();
                    String resultSet = rs.toString();
                    transactionproductionDataModel.clear();
                    while (rs.next()) {
                        String ProductionTag = rs.getString("ProductionTag");
                        String OEMID = rs.getString("OEMID");
                        String MacID = rs.getString("MacID");
                        String BatchID = rs.getString("BatchID");
                        String ProductID = rs.getString("ProductID");
                        String GmPerPouch = rs.getString("GmPerPouch");
                        String Unit = rs.getString("Unit");
                        String TotalQTY = rs.getString("TotalQTY");
                        String TotalQuantityUOM = rs.getString("TotalQuantityUOM");
                        String IsNegAllowed = rs.getString("IsNegAllowed");
                        String TagModifiedDate = rs.getString("TagModifiedDate");
                        String TagShift = rs.getString("TagShift");
                        String TagNumberOfPouches = rs.getString("TagNumberOfPouches");
                        String SessionPositiveTolerance = rs.getString("SessionPositiveTolerance");
                        String SessionNegativeTolerance = rs.getString("SessionNegativeTolerance");
                        String MasterTolerancePositive = rs.getString("MasterTolerancePositive");
                        String MasterToleranceNegetive = rs.getString("MasterToleranceNegetive");
                        String UCLgm = rs.getString("UCLgm");
                        String LCLgm = rs.getString("LCLgm");
                        String Operator = rs.getString("Operator");
                        String issync = "1";

                        transactionproductionDataModel.add(new ProductionTagSyncDataModel(ProductionTag,OEMID,MacID,BatchID,ProductID,GmPerPouch,Unit,TotalQTY,
                                TotalQuantityUOM,IsNegAllowed,TagModifiedDate,TagModifiedDate,TagShift,TagNumberOfPouches,SessionPositiveTolerance,
                                SessionNegativeTolerance,MasterTolerancePositive,MasterToleranceNegetive,UCLgm,LCLgm,Operator,issync));
                    }
                    conn.commit();
                    getLoggerListFromDB.close();
                } else if (conn == null) {
                    Log.d(TAG, "getDbConnection: " + "Database is not connected to database");
                }
            } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return transactionproductionDataModel;
        }

        @Override
        protected void onPostExecute(ArrayList<ProductionTagSyncDataModel> masterProductionReasonModel) {
            super.onPostExecute(transactionproductionDataModel);
        }
    }*/


}

