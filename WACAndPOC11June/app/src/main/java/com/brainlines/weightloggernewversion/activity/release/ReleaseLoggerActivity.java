package com.brainlines.weightloggernewversion.activity.release;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.adapter.LoggerListAdapter;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.listener.GoToNewTestTagFillAcitivity;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.AsyncTasks;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class ReleaseLoggerActivity extends AppCompatActivity implements GoToNewTestTagFillAcitivity {

    private static final String TAG = "NewTestActivity";
    RecyclerView rv_Logger_List;
    LoggerListAdapter adapter;
    ArrayList<LoggersFromDBModel> loggerList = new ArrayList<>();
    DataBaseHelper helper;
    private String dataPort = Constants.DATA_LOGGER_PORT+"";
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_release_logger);
        try {
            initUI();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initUI() throws ExecutionException, InterruptedException {

        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        rv_Logger_List = findViewById(R.id.rv_Logger_List);

        ArrayList<LoggersFromDBModel> loggers = getLoggerListFromDB();

        LinearLayoutManager manager = new LinearLayoutManager(this);
        rv_Logger_List.setLayoutManager(manager);
        adapter = new LoggerListAdapter(this,loggers,this);
        rv_Logger_List.setAdapter(adapter);

        for (int i = 0 ; i < loggers.size() ; i++){
            LoggersFromDBModel model = loggers.get(i);
            helper.addDataLoggersToLocalDB(i+1+"",model.getOem_id(),model.getSerial_num(),
                    model.getLogger_name(),model.getIp_address(),model.getMac_id(),
                    model.getLogger_location(),model.getCreate_timestamp(),model.getCreate_by_id(),
                    model.getUpdate_timestamp(),model.getUpdate_by_id(),model.isActive(),model.getInuse());
            Log.d(TAG, "initUI: ");
        }

        Cursor data = helper.getDataLoggers();
        while (data.moveToNext()){
            String loggerId = data.getString(0);
            String oemId = data.getString(1);
            String srNum = data.getString(2);
            String loggerName = data.getString(3);
            String ip = data.getString(4);
            String mac = data.getString(5);
            String location = data.getString(6);
            String createTimeStamp = data.getString(7);
            String createdBy = data.getString(8);
            String uptimestamp = data.getString(9);
            String modifiedBy = data.getString(10);
            boolean isActive = data.getInt(11) > 0;
            boolean inUse = data.getInt(12) > 0;
            Log.d(TAG, "initUI: ");
            //usersListFromLocalDb.add(new UsersFromDBModel(loginID,userID,roleID,userName,password,isActive,isDeleted,createdDate,modifiedDate));
        }
    }

    private ArrayList<LoggersFromDBModel> getLoggerListFromDB() throws ExecutionException, InterruptedException {
        loggerList.clear();
        loggerList = new AsyncTasks.GetLoggerListFromDB(this).execute().get();
        return loggerList;
    }



    private String releaseLogger(String dataLoggerIp) throws IOException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String url = "http://"+dataLoggerIp+":"+dataPort+"/release";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            if (responseString.equals("ok")){
                //changeDataLoggerStatus(dataLoggerName,"Yes");
            }
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;
    }

    @Override
    public void getLogger(String loggerName, String loggerIpAddress, String loggerMacId) throws IOException {

        releaseLogger(loggerIpAddress);

    }
}
