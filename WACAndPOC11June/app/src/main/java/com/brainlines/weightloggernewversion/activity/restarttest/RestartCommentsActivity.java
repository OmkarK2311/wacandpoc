package com.brainlines.weightloggernewversion.activity.restarttest;

import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoggerFunctionActivity;
import com.brainlines.weightloggernewversion.activity.newtest.Report_Activity;
import com.brainlines.weightloggernewversion.broadcastreceiver.NetworkChangeReceiver;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.CommentsModel;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class RestartCommentsActivity extends BaseActivity implements View.OnClickListener{

    EditText ed_main_comment;
    EditText ed_comment_function1,ed_comment_function2,ed_comment_function3,ed_comment_function4,ed_comment_function5;
    ImageButton btn_test_complete,btn_abort,btn_sync;
    TextView alert_message;
    Button alert_button_ok,alert_button_cancel;
    AlertDialog alertDialog;
    String str_comment,str_fun_comment1,str_fun_comment2,str_fun_comment3,str_fun_comment4,str_fun_comment5;
    String dataloggerName,dataloggerIp,current_tag_id,so_oinum;
    DataBaseHelper helper;
    private ArrayList<CommentsModel> commentsModelArrayList = new ArrayList<>();
    private NetworkChangeReceiver networkChangeReceiver;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss.SSS");
    private String status="";
    double str_kg_value_from_local_db;
    private String str_tarhetwt_uom = "";
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_test_comments);
        initUi();

        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        dataloggerName = getIntent().getStringExtra("LoggerName");
        dataloggerIp = getIntent().getStringExtra("LoggerIP");
        current_tag_id = getIntent().getStringExtra("current_tag_id");
        so_oinum = getIntent().getStringExtra("so_oinum");
        str_kg_value_from_local_db = getIntent().getDoubleExtra("IntKgValue",0.0d);
        str_tarhetwt_uom = getIntent().getStringExtra("targetwt_uom");
    }

    private void initUi()
    {
        ed_main_comment = findViewById(R.id.ed_main_comment);
        ed_comment_function1 = findViewById(R.id.ed_comment_function1);
        ed_comment_function2 = findViewById(R.id.ed_comment_function2);
        ed_comment_function3 = findViewById(R.id.ed_comment_function3);
        ed_comment_function4 = findViewById(R.id.ed_comment_function4);
        ed_comment_function5 = findViewById(R.id.ed_comment_function5);

        btn_test_complete = findViewById(R.id.btn_test_complete);
        btn_abort = findViewById(R.id.btn_comment_abort);
        btn_sync = findViewById(R.id.btn_sync);

        btn_test_complete.setOnClickListener(this);
        btn_abort.setOnClickListener(this);
        btn_sync.setOnClickListener(this);

        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerNetworkBroadcastForNougat();

    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }


    public void alertMessage()
    {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.layout_alert_dialog, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        alert_button_ok= dialogView.findViewById(R.id.alert_btn_ok);
        //alert_button_cancel = dialogView.findViewById(R.id.alert_btn_cancel);
        alert_message= dialogView.findViewById(R.id.alert_message);
        alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btn_test_complete.getId())
        {
            str_comment = ed_main_comment.getText().toString();
            str_fun_comment1 = "1: ".concat(ed_comment_function1.getText().toString());
            str_fun_comment2 = "2: .".concat(ed_comment_function2.getText().toString());
            str_fun_comment3 = "3: ".concat(ed_comment_function3.getText().toString());
            str_fun_comment4 = "4: ".concat(ed_comment_function4.getText().toString());
            str_fun_comment5 = "5: ".concat(ed_comment_function5.getText().toString());

            InsertIntoDb();
            changeDataLoggerStatus(dataloggerName,"No");
            Intent i = new Intent(RestartCommentsActivity.this, Report_Activity.class);
            i.putExtra("LoggerName",dataloggerName);
            i.putExtra("LoggerIP",dataloggerIp);
            i.putExtra("TagId",current_tag_id);
            i.putExtra("oinum",so_oinum);
            i.putExtra("IntKgValue",str_kg_value_from_local_db);
            i.putExtra("targetwt_uom",str_tarhetwt_uom);
            startActivity(i);

        }
        else if (view.getId() == btn_abort.getId())
        {
            try {
                abort();
                status = "Abort";
                helper.updateOverallStatusForTag(status,current_tag_id);
                Intent i = new Intent(RestartCommentsActivity.this, LoggerFunctionActivity.class);
                i.putExtra("LoggerName",dataloggerName);
                i.putExtra("LoggerIP",dataloggerIp);
                startActivity(i);
                finish();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if (view.getId() == btn_sync.getId()){
            str_comment = ed_main_comment.getText().toString();
            str_fun_comment1 = "1: ".concat(ed_comment_function1.getText().toString());
            str_fun_comment2 = "2: .".concat(ed_comment_function2.getText().toString());
            str_fun_comment3 = "3: ".concat(ed_comment_function3.getText().toString());
            str_fun_comment4 = "4: ".concat(ed_comment_function4.getText().toString());
            str_fun_comment5 = "5: ".concat(ed_comment_function5.getText().toString());
            InsertIntoDb();
//            changeDataLoggerStatus(dataloggerName,"No");
//            getAllCommentsfromLocalDB();
        }
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            alertMessage();
            alert_message.setText("Back functionality not allowed!");
            alert_button_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.cancel();
                }
            });

            return false;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    public  void InsertIntoDb() {
//        DataBaseHelper helper=new DataBaseHelper(RestartCommentsActivity.this);
//        SQLiteDatabase db = helper.getWritableDatabase();
        String currentDateandTime = sdf.format(new Date());

        ContentValues cv=new ContentValues();
        cv.put("CommentsID","1");
        cv.put("OEMID","1234");
        cv.put("MacID","18:fe:34:8b:60:75");
        cv.put("LoggerID",dataloggerName);
        cv.put("TagID",current_tag_id);
        cv.put("Comments",str_comment);
        cv.put("FunComment1",str_fun_comment1);
        cv.put("FunComment2",str_fun_comment2);
        cv.put("FunComment3",str_fun_comment3);
        cv.put("FunComment4",str_fun_comment4);
        cv.put("FunComment5",str_fun_comment5);
        cv.put("ModifiedDate",currentDateandTime);
        cv.put("IsSync","0");

        long d=db.insert(Constants.COMMENTS_TABLE,null,cv);
        Log.d("Success", String.valueOf(d));

    }

    private void changeDataLoggerStatus(String dataLoggerName, String no) {
        helper.updateMstLoggerTableForLoggerStatus(dataLoggerName);
    }

    private String abort() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String tag = current_tag_id;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String dataPort=Constants.DATA_LOGGER_PORT+"";
        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;

    }

    @Override
    protected void dialog(boolean value) {
        super.dialog(value);
//        if (value){
//            str_comment = ed_main_comment.getText().toString();
//            str_fun_comment1 = "1: ".concat(ed_comment_function1.getText().toString());
//            str_fun_comment2 = "2: .".concat(ed_comment_function2.getText().toString());
//            str_fun_comment3 = "3: ".concat(ed_comment_function3.getText().toString());
//            str_fun_comment4 = "4: ".concat(ed_comment_function4.getText().toString());
//            str_fun_comment5 = "5: ".concat(ed_comment_function5.getText().toString());
//            InsertIntoDb();
//            changeDataLoggerStatus(dataloggerName,"No");
//            getAllCommentsfromLocalDB();
//            Toast.makeText(this, "DATA Pushed", Toast.LENGTH_SHORT).show();
//        }else {
//            Toast.makeText(this, "DATA Not Pushed", Toast.LENGTH_SHORT).show();
//        }
    }
}
