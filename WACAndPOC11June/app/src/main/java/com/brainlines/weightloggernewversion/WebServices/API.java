package com.brainlines.weightloggernewversion.WebServices;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    /*@POST("InsertBagLengthAnalyticsData")
    Call<ResponseBody> syncBagLengthAnalyticsData(@Query("bagLengthAnalyticsStr") String response);*/

    @Headers({"Content-Type: application/json","Accept: application/json"})
    @POST("InsertTXNDataIntoTable")
    Call<ResponseBody> SyncTXNDataForThisTable(@Body JsonObject object,
                                               @Query("oemId") String oemId,
                                               @Query("tableName") String tableName,
                                               @Query("moduleId") String moduleId);

   /* @Headers({"Content-Type: application/json","Accept: application/json"})
    @POST("InsertTXNDataIntoTable")
    Call<ResponseBody> SyncTXNDataForThisTable(@Body JsonObject object,
                                               @Query("oemId") String oemId,
                                               @Query("tableName") String tableName,
                                               @Query("moduleId") String moduleId);*/

    @POST("InsertBagLengthVariationResultsDvcata")
    Call<ResponseBody> syncBagLengthVariationResultsData(@Query("bagLengthVariationResultsStr") String response);

    @POST("InsertPaperShiftAnalyticsData")
    Call<ResponseBody> syncPaperShiftAnalyticsData(@Query("paperShiftAnalyticsStr") String response);

    @POST("InsertPaperShiftResultsData")
    Call<ResponseBody> syncPaperShiftResultsData(@Query("paperShiftResultsStr") String response);

    @POST("InsertWeightTestResultsData")
    Call<ResponseBody> syncWeightTestResultsData(@Query("weightTestResultsStr") String response);

    @POST("InsertWeightResultstAnalyticsData")
    Call<ResponseBody> syncWeightResultstAnalyticsData(@Query("weightResultstAnalyticsStr") String response);

    @POST("InsertObservationsData")
    Call<ResponseBody> syncObservationsData(@Query("observationsStr") String response);

    @POST("InsertCommentsData")
    Call<ResponseBody> syncCommentsData(@Query("commentsStr") String response);

    @POST("InsertCTagsData")
    Call<ResponseBody> syncCTagsData(@Query("cTagResultsStr") String response);

    @POST("InsertProductionTagData")
    Call<ResponseBody> syncProductionTagData(@Query("productionTagStr") String response);

    @POST("InsertPWeightReadingsData")
    Call<ResponseBody> syncInsertPWeightReadingsData(@Query("pWeightReadingsStr") String response);

    @POST("InsertWeightReportTagData")
    Call<ResponseBody> syncInsertWeightReportData(@Query("weightResultStr") String response);

    @POST("InsertPTagResultsTagData")
    Call<ResponseBody> syncInsertPTagResultsTagData(@Query("pTagResultstr") String response);


    //Syncing Master Data to SQLITE DB
    @POST("SyncUOMDetails")
    Call<ResponseBody> SyncUOMDetails();

    @POST("SyncFillerTypeDetails")
    Call<ResponseBody> SyncFillerTypeDetails();

    @POST("SyncHeadDetails")
    Call<ResponseBody> SyncHeadDetails();

    @POST("SyncLoggerDetails")
    Call<ResponseBody> SyncLoggerDetails();

    @POST("SyncMachineTypeDetails")
    Call<ResponseBody> SyncMachineTypeDetails();

    @POST("SyncPouchSymmetryDetails")
    Call<ResponseBody> SyncPouchSymmetryDetails ();

    @POST("SyncProductionreasonDetails")
    Call<ResponseBody> SyncProductionreasonDetails ();

    @POST("SyncProductionToleranceDetails")
    Call<ResponseBody> SyncProductionToleranceDetails ();

    @POST("SyncProductionTypeDetails")
    Call<ResponseBody> SyncProductionTypeDetails ();

    @POST("SyncSealTypeDetails")
    Call<ResponseBody> SyncSealTypeDetails ();

    @POST("SyncTargetAccuracyDetails")
    Call<ResponseBody> SyncTargetAccuracyDetails ();

    @POST("SyncWireCupDetails")
    Call<ResponseBody> SyncWireCupDetails ();

    @POST("SyncProductDetails")
    Call<ResponseBody> SyncProductDetails ();

    @POST("SyncPouchAttributesDetails")
    Call<ResponseBody> SyncPouchAttributesDetails ();

    @POST("SealRejectionReasonMasterDetails")
    Call<ResponseBody> SealRejectionReasonMasterDetails ();

    @POST("GetMasterDataFromSQLTable")
    Call<ResponseBody> syncThisMasterTable(@Query("oemId") int oemId,@Query("tableName") String tableName);

    @GET("GetScriptForCreateTables")
    Call<ResponseBody> createTableForModule(@Query("oemId") int oemId , @Query("moduleId") int moduleId);

    @GET("GetUserEntitlement")
    Call<ResponseBody> syncUserLoginData(@Query("userName") String userName,@Query("password") String password);

}
