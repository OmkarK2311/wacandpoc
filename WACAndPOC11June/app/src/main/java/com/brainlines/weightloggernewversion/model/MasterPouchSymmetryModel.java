package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterPouchSymmetryModel implements Parcelable {

    String pouchSymmetryId;
    int oemId;
    int PouchID;
    String PouchTypeID;
    String pouchDesc;
    boolean isActive;
    boolean isDeleted;
    String createdDate;
    String modifiedDate;
    String createdBy;
    String modifiedBy;

    public MasterPouchSymmetryModel(String pouchSymmetryId, int oemId, int pouchID, String PouchTypeID, String pouchDesc, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy) {
        this.pouchSymmetryId = pouchSymmetryId;
        this.oemId = oemId;
        PouchID = pouchID;
        this.PouchTypeID = PouchTypeID;
        this.pouchDesc = pouchDesc;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
    }

    protected MasterPouchSymmetryModel(Parcel in) {
        pouchSymmetryId = in.readString();
        oemId = in.readInt();
        PouchID = in.readInt();
        PouchTypeID = in.readString();
        pouchDesc = in.readString();
        isActive = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
        createdBy = in.readString();
        modifiedBy = in.readString();
    }

    public static final Creator<MasterPouchSymmetryModel> CREATOR = new Creator<MasterPouchSymmetryModel>() {
        @Override
        public MasterPouchSymmetryModel createFromParcel(Parcel in) {
            return new MasterPouchSymmetryModel(in);
        }

        @Override
        public MasterPouchSymmetryModel[] newArray(int size) {
            return new MasterPouchSymmetryModel[size];
        }
    };

    public String getPouchSymmetryId() {
        return pouchSymmetryId;
    }

    public void setPouchSymmetryId(String pouchSymmetryId) {
        this.pouchSymmetryId = pouchSymmetryId;
    }

    public int getOemId() {
        return oemId;
    }

    public void setOemId(int oemId) {
        this.oemId = oemId;
    }

    public int getPouchID() {
        return PouchID;
    }

    public void setPouchID(int pouchID) {
        PouchID = pouchID;
    }

    public String getPouchTypeID() {
        return PouchTypeID;
    }

    public void setPouchTypeID(String PouchTypeID) {
        this.PouchTypeID = PouchTypeID;
    }

    public String getPouchDesc() {
        return pouchDesc;
    }

    public void setPouchDesc(String pouchDesc) {
        this.pouchDesc = pouchDesc;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pouchSymmetryId);
        dest.writeInt(oemId);
        dest.writeInt(PouchID);
        dest.writeString(PouchTypeID);
        dest.writeString(pouchDesc);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
        dest.writeString(createdBy);
        dest.writeString(modifiedBy);
    }
}
