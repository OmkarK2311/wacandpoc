package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MasterProductModel implements Parcelable {

    int ProductID;
    int OEMID;
    String Product;
    int ProductTypeID;
    boolean IsActive;
    boolean IsDeleted;
    String CreatedDate;
    String ModifiedDate;
    String CreatedBy;
    String ModifiedBy;
    String PricePerKG;
    String Currency;

    public MasterProductModel(int productID, int OEMID, String product, int productTypeID, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate, String createdBy, String modifiedBy, String pricePerKG, String currency) {
        ProductID = productID;
        this.OEMID = OEMID;
        Product = product;
        ProductTypeID = productTypeID;
        IsActive = isActive;
        IsDeleted = isDeleted;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
        CreatedBy = createdBy;
        ModifiedBy = modifiedBy;
        PricePerKG = pricePerKG;
        Currency = currency;
    }

    protected MasterProductModel(Parcel in) {
        ProductID = in.readInt();
        OEMID = in.readInt();
        Product = in.readString();
        ProductTypeID = in.readInt();
        IsActive = in.readByte() != 0;
        IsDeleted = in.readByte() != 0;
        CreatedDate = in.readString();
        ModifiedDate = in.readString();
        CreatedBy = in.readString();
        ModifiedBy = in.readString();
        PricePerKG = in.readString();
        Currency = in.readString();
    }

    public static final Creator<MasterProductModel> CREATOR = new Creator<MasterProductModel>() {
        @Override
        public MasterProductModel createFromParcel(Parcel in) {
            return new MasterProductModel(in);
        }

        @Override
        public MasterProductModel[] newArray(int size) {
            return new MasterProductModel[size];
        }
    };

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int productID) {
        ProductID = productID;
    }

    public int getOEMID() {
        return OEMID;
    }

    public void setOEMID(int OEMID) {
        this.OEMID = OEMID;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public int getProductTypeID() {
        return ProductTypeID;
    }

    public void setProductTypeID(int productTypeID) {
        ProductTypeID = productTypeID;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        ModifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getPricePerKG() {
        return PricePerKG;
    }

    public void setPricePerKG(String pricePerKG) {
        PricePerKG = pricePerKG;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ProductID);
        dest.writeInt(OEMID);
        dest.writeString(Product);
        dest.writeInt(ProductTypeID);
        dest.writeByte((byte) (IsActive ? 1 : 0));
        dest.writeByte((byte) (IsDeleted ? 1 : 0));
        dest.writeString(CreatedDate);
        dest.writeString(ModifiedDate);
        dest.writeString(CreatedBy);
        dest.writeString(ModifiedBy);
        dest.writeString(PricePerKG);
        dest.writeString(Currency);
    }
}
