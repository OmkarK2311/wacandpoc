package com.brainlines.weightloggernewversion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.adapter.UserDataListAdapter;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.listener.UserDataListener;
import com.brainlines.weightloggernewversion.model.UserModel;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.GlobalClass;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ModuleSelectionActivity extends AppCompatActivity implements UserDataListener {
    private ArrayList<UserModel> userModelList = new ArrayList<>();
    private RecyclerView rvUserDataList;
    private UserDataListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_selection);

        rvUserDataList = findViewById(R.id.rvUserDataList);

        userModelList = getIntent().getParcelableArrayListExtra("UserDataList");
        ArrayList<UserModel> uniqueItemList = unique(userModelList);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvUserDataList.setLayoutManager(manager);
        adapter = new UserDataListAdapter(this,uniqueItemList,this);
        rvUserDataList.setAdapter(adapter);
    }

    @Override
    public void sendUserData(UserModel model) {
        if (model != null){
            GlobalClass.getDbNameBasedOnModuleSelected(model,this);
            switch (model.getModuleID()){
                case 1:
                    //WAC Module Flow
                    Intent moduleIdOne = new Intent(this, CalibrationOrProductionActivity.class);
                    moduleIdOne.putExtra("UserDataModel",model);
                    startActivity(moduleIdOne);
                    break;
                case 2:
                    //SWAS Module Flow
//                    Intent moduleIdTwo = new Intent(this,SecondActivity.class);
//                    moduleIdTwo.putExtra("UserDataModel",model);
//                    startActivity(moduleIdTwo);
                    break;
                case 3:
                    //JK Planning Module Flow
                    break;
            }
        }
    }

    private ArrayList<UserModel> unique(ArrayList<UserModel> list) {
        ArrayList<UserModel> uniqueList = new ArrayList<>();
        Set<UserModel> uniqueSet = new HashSet<>();
        for (UserModel obj : list) {
            if (uniqueSet.add(obj)) {
                uniqueList.add(obj);
            }
        }
        return uniqueList;
    }
}
