package com.brainlines.weightloggernewversion.Production.activity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.brainlines.weightloggernewversion.Production.Model.P_WeightReading_History_Model;
import com.brainlines.weightloggernewversion.Production.adapter.ProductionHistoryWeightReadingAdapter;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class BatchReportDetails extends AppCompatActivity implements View.OnClickListener {

    TextView txt_history_product,txt_history_gm_pouch,txt_history_uom,txt_history_total_qty,txt_history_total_qty_uom,txt_history_date,txt_history_shift;
    TextView txt_history_positive_tolerance,txt_history_negative_tolerance,txt_history_no_of_pouches;
    TextView txt_history_operator_name,txt_history_negative_wt_allowed,txt_history_max_weight,txt_history_min_weight,txt_history_range,txt_history_avg_weight;
    TextView txt_history_excess_weight,txt_history_unfilled_quantity,txt_history_filled_quantity,txt_history_unfilled_pouches,txt_history_filled_pouches;
    String selected_date,str_product,str_batch_id;
    TextView txt_history_standard_Deviation;
    DataBaseHelper helper;
    LinearLayout llExpandWeightReadingDetails,llExpandReportDetails,llExpandBatchDetails;
    ImageView img_weight_reading_details_expand,img_report_details_expand,img_batch_details_expand;
    CardView cv_weight_reading_details,cv_report_details,cv_batch_details;
    TextView txt_history_batchid;
    ArrayList<P_WeightReading_History_Model> p_weightReading_history_models = new ArrayList<>();
    RecyclerView rv_history_weight_Reading;
    TextView txt_Report_min_heading,txt_Report_max_heading,txt_Report_range_heading,txt_Report_average_heading,txt_Report_excess_giveaway_heading;
    TextView txt_Report_unfilled_qty_heading,txt_Report_filled_qty_heading,txt_report_negative_tolerance,txt_report_positive_tole,txt_head_gmperpouch;
    String str_unit;
    DecimalFormat decimalFormat = new DecimalFormat("0.000");
    ArrayList<String> grammage_values_list = new ArrayList<>();
    Double dintkgvalue =0.0;
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_production_report_details);
        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        initUi();
        selected_date = getIntent().getStringExtra("selected_date");
        str_product = getIntent().getStringExtra("product");
        str_batch_id = getIntent().getStringExtra("batch_id");
        rv_history_weight_Reading = findViewById(R.id.rv_history_weight_Reading);
        getProductionGrammageValues();
        getProductionTags(selected_date,str_product.trim());
        getWeightReportData(str_batch_id);
        getWeightReadingData(str_batch_id);
    }

    private void initUi()
    {
        //heading with unit of measurement
        txt_head_gmperpouch = findViewById(R.id.txt_head_gmperpouch);
        txt_Report_min_heading = findViewById(R.id.txt_Report_min_heading);
        txt_Report_max_heading = findViewById(R.id.txt_Report_max_heading);
        txt_Report_range_heading = findViewById(R.id.txt_Report_range_heading);
        txt_Report_average_heading = findViewById(R.id.txt_Report_average_heading);
        txt_Report_excess_giveaway_heading = findViewById(R.id.txt_Report_excess_giveaway_heading);
        txt_Report_unfilled_qty_heading = findViewById(R.id.txt_Report_unfilled_qty_heading);
        txt_Report_filled_qty_heading = findViewById(R.id.txt_Report_filled_qty_heading);
        txt_report_negative_tolerance = findViewById(R.id.txt_report_negative_tolerance);
        txt_report_positive_tole = findViewById(R.id.txt_report_positive_tole);


        llExpandWeightReadingDetails = findViewById(R.id.llExpandWeightReadingDetails);
        llExpandReportDetails = findViewById(R.id.llExpandReportDetails);
        llExpandBatchDetails = findViewById(R.id.llExpandBatchDetails);

        cv_batch_details = findViewById(R.id.cv_batch_details);
        cv_report_details = findViewById(R.id.cv_report_details);
        cv_weight_reading_details = findViewById(R.id.cv_weight_reading_details);

        img_weight_reading_details_expand = findViewById(R.id.img_weight_reading_details_expand);
        img_report_details_expand = findViewById(R.id.img_report_details_expand);
        img_batch_details_expand = findViewById(R.id.img_batch_details_expand);

        cv_batch_details.setOnClickListener(this);
        cv_report_details.setOnClickListener(this);
        cv_weight_reading_details.setOnClickListener(this);

        img_weight_reading_details_expand.setOnClickListener(this);
        img_report_details_expand.setOnClickListener(this);
        img_batch_details_expand.setOnClickListener(this);


        txt_history_product = findViewById(R.id.txt_history_product);
        txt_history_gm_pouch = findViewById(R.id.txt_history_gm_pouch);
        txt_history_uom = findViewById(R.id.txt_history_uom);
        txt_history_total_qty = findViewById(R.id.txt_history_total_qty);
        txt_history_total_qty_uom = findViewById(R.id.txt_history_total_qty_uom);
        txt_history_date = findViewById(R.id.txt_history_date);
        txt_history_shift = findViewById(R.id.txt_history_shift);
        txt_history_operator_name = findViewById(R.id.txt_history_operator_name);
        txt_history_negative_wt_allowed = findViewById(R.id.txt_history_negative_wt_allowed);
        txt_history_positive_tolerance = findViewById(R.id.txt_history_positive_tolerance);
        txt_history_negative_tolerance = findViewById(R.id.txt_history_negative_tolerance);
        txt_history_no_of_pouches = findViewById(R.id.txt_history_no_of_pouches);
        txt_history_batchid = findViewById(R.id.txt_history_batchid);


        txt_history_max_weight = findViewById(R.id.txt_history_max_weight);
        txt_history_min_weight = findViewById(R.id.txt_history_min_weight);
        txt_history_range = findViewById(R.id.txt_history_range);
        txt_history_avg_weight = findViewById(R.id.txt_history_avg_weight);
        txt_history_excess_weight = findViewById(R.id.txt_history_excess_weight);
        txt_history_unfilled_quantity = findViewById(R.id.txt_history_unfilled_quantity);
        txt_history_filled_quantity = findViewById(R.id.txt_history_filled_quantity);
        txt_history_unfilled_pouches = findViewById(R.id.txt_history_unfilled_pouches);
        txt_history_filled_pouches = findViewById(R.id.txt_history_filled_pouches);
        txt_history_standard_Deviation = findViewById(R.id.txt_history_standard_Deviation);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_batch_details:
                if (llExpandBatchDetails.getVisibility() == View.GONE) {
                    llExpandBatchDetails.setVisibility(View.VISIBLE);
                } else if (llExpandBatchDetails.getVisibility() == View.VISIBLE) {
                    llExpandBatchDetails.setVisibility(View.GONE);
                }
                break;

            case R.id.cv_report_details:
                if (llExpandReportDetails.getVisibility() == View.GONE) {
                    llExpandReportDetails.setVisibility(View.VISIBLE);
                } else if (llExpandReportDetails.getVisibility() == View.VISIBLE) {
                    llExpandReportDetails.setVisibility(View.GONE);
                }
                break;

            case R.id.cv_weight_reading_details:
                if (llExpandWeightReadingDetails.getVisibility() == View.GONE) {
                    llExpandWeightReadingDetails.setVisibility(View.VISIBLE);
                } else if (llExpandWeightReadingDetails.getVisibility() == View.VISIBLE) {
                    llExpandWeightReadingDetails.setVisibility(View.GONE);
                }
                break;

        }
    }

    private void getProductionTags(String str_selected_date, String str_product) {
        Cursor p_tag = helper.getProductionBatchDetails(str_selected_date, str_product);
        while (p_tag.moveToNext()){
            try {

                String batchid = p_tag.getString(4);
                String product = p_tag.getString(5);
                String gm_pouch = p_tag.getString(6);
                str_unit = p_tag.getString(7);
                String total_weight = p_tag.getString(8);
                String total_qty_uom = p_tag.getString(21);
                String negative_weight_allowed = p_tag.getString(11);
                String date = p_tag.getString(12);
                String shift = p_tag.getString(13);
                String no_of_pouches = p_tag.getString(14);
                String positive_tolerance = p_tag.getString(15);
                String negative_tolerance = p_tag.getString(16);
                String operator = p_tag.getString(22);
                if (str_unit.equals("Kg"))
                {
                    String kgvalue = grammage_values_list.get(0);
                    dintkgvalue = Double.parseDouble(kgvalue);
                    txt_head_gmperpouch.setText("Kg/Pouch");

                }
                else
                {
                    txt_head_gmperpouch.setText("Gm/Pouch");
                }


                if (str_unit.equals("Kg"))
                {  Double dpositivetolerance = Double.parseDouble(positive_tolerance) / dintkgvalue;
                    Double dnegativetolerance = Double.parseDouble(negative_tolerance) / dintkgvalue;
                    Double dtotaqty = Double.parseDouble(total_weight) / dintkgvalue;
                    Double dgmperpouch = Double.parseDouble(gm_pouch) / dintkgvalue;

                    txt_history_batchid.setText(batchid);
                    txt_history_product.setText(product);
                    txt_history_gm_pouch.setText(decimalFormat.format(dgmperpouch));
                    txt_history_uom.setText(str_unit);
                    txt_history_total_qty.setText(decimalFormat.format(dtotaqty));
                    txt_history_total_qty_uom.setText(total_qty_uom);
                    txt_history_negative_wt_allowed.setText(negative_weight_allowed);
                    txt_history_shift.setText(shift);
                    txt_history_no_of_pouches.setText(no_of_pouches);
                    txt_history_positive_tolerance.setText(decimalFormat.format(dpositivetolerance));
                    txt_history_negative_tolerance.setText(decimalFormat.format(dnegativetolerance));
                    txt_history_date.setText(date);
                    txt_history_operator_name.setText(operator);


                }
                else
                {
                    Double dpositivetolerance = Double.valueOf(positive_tolerance);
                    Double dnegativetolerance = Double.valueOf(negative_tolerance);
                    txt_history_batchid.setText(batchid);
                    txt_history_product.setText(product);
                    txt_history_gm_pouch.setText(gm_pouch);
                    txt_history_uom.setText(str_unit);
                    txt_history_total_qty.setText(total_weight);
                    txt_history_total_qty_uom.setText(total_qty_uom);
                    txt_history_negative_wt_allowed.setText(negative_weight_allowed);
                    txt_history_shift.setText(shift);
                    txt_history_no_of_pouches.setText(no_of_pouches);
                    txt_history_positive_tolerance.setText(decimalFormat.format(dpositivetolerance));
                    txt_history_negative_tolerance.setText(decimalFormat.format(dnegativetolerance));
                    txt_history_date.setText(date);
                    txt_history_operator_name.setText(operator);
                }


                txt_Report_min_heading.setText("Min Weight" + " (" + str_unit + ")" );
                txt_Report_max_heading.setText("Max Weight" + " (" + str_unit + ")" );
                txt_Report_range_heading.setText("Range" + " (" + str_unit + ")" );
                txt_Report_average_heading.setText("Average Weight" + " (" + str_unit + ")" );
                txt_Report_excess_giveaway_heading.setText("Excess" + " (" + str_unit + ")" );
                txt_Report_unfilled_qty_heading.setText("Unfilled Quantity" + " (" + str_unit + ")" );
                txt_Report_filled_qty_heading.setText("Filled Quantity" + " (" + str_unit + ")" );
                txt_report_negative_tolerance.setText("-ve Tolerance" + " (" + str_unit + ")" );
                txt_report_positive_tole.setText("+ve Tolerance" + " (" + str_unit + ")" );

                Log.d("TAG", "p_tag: ");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }

    private void getWeightReportData(String Batchid) {
        Cursor report = helper.getProductionBatchWeightReport(Batchid);
        while (report.moveToNext()){
            try {

                String max = report.getString(4);
                String average = report.getString(5);
                String range= report.getString(6);
                String sd = report.getString(7);
                String unfilled_qty = report.getString(8);
                String filled_qty = report.getString(9);
                String unfilled_pouches = report.getString(11);
                String filled_pouches = report.getString(12);
                String excess_give_away_price = report.getString(13);
                String excess_give_away_gm = report.getString(14);
                String min = report.getString(15);
                String mean =report.getString(16);


                String reason = report.getString(16);
                String remark = report.getString(17);


                if (str_unit.equals("Kg"))
                {
                    Double dmin = Double.parseDouble(min) / dintkgvalue;
                    Double dmax = Double.parseDouble(max) / dintkgvalue;
                    Double daverage = Double.parseDouble(average) / dintkgvalue;
                    Double drangee = Double.parseDouble(range) / dintkgvalue;
                    Double dsd = Double.parseDouble(sd) / dintkgvalue;
                    Double dunfilelledqty = Double.parseDouble(unfilled_qty) / dintkgvalue;
                    Double dfilelledqty = Double.parseDouble(filled_qty) / dintkgvalue;
                    Double dexcessgiveaway = Double.parseDouble(excess_give_away_gm) /dintkgvalue;

                    double drange = drangee/2;
                    String a = "+/-";
                    String str_range = a.concat(" " + String.valueOf(drange));
                    txt_history_min_weight.setText(String.valueOf(decimalFormat.format(dmin)));
                    txt_history_max_weight.setText(String.valueOf(decimalFormat.format(dmax)));
                    txt_history_avg_weight.setText(String.valueOf(decimalFormat.format(daverage)));
                    txt_history_range.setText(str_range);
                    txt_history_standard_Deviation.setText(String.valueOf(decimalFormat.format(dsd)));
                    txt_history_unfilled_quantity.setText(String.valueOf(decimalFormat.format(dunfilelledqty)));
                    txt_history_filled_quantity.setText(String.valueOf(decimalFormat.format(dfilelledqty)));
                    txt_history_filled_pouches.setText(filled_pouches);
                    txt_history_unfilled_pouches.setText(unfilled_pouches);
                    txt_history_excess_weight.setText(String.valueOf(decimalFormat.format(dexcessgiveaway)));

                }
                else
                {
                    txt_history_min_weight.setText(min);  txt_history_max_weight.setText(max);
                    double drange = Double.parseDouble(range)/2;
                    String a = "+/-";
                    String str_range = a.concat(" " + String.valueOf(drange));
                    txt_history_range.setText(str_range);
                    txt_history_avg_weight.setText(average); txt_history_standard_Deviation.setText(sd);
                    txt_history_filled_quantity.setText(String.valueOf(filled_qty)); txt_history_filled_pouches.setText(filled_pouches);
                    txt_history_unfilled_quantity.setText(String.valueOf(unfilled_qty)); txt_history_unfilled_pouches.setText(unfilled_pouches);
                    txt_history_excess_weight.setText(excess_give_away_gm);

                }

                Log.d("TAG", "p_tag: ");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            } }
    }

    private void getWeightReadingData(String Batchid) {
        Cursor reading = helper.getProductionweightReading(Batchid);
        while (reading.moveToNext()){
            try {
            String weight = reading.getString(7);
            String status = reading.getString(18);
            String shift = reading.getString(8);
            String excess = reading.getString(10);
            String date_time = reading.getString(19);
            String weight_Id = reading.getString(4);

            if (str_unit.equals("Kg"))
            {
                Double kgweight = Double.parseDouble(weight)/dintkgvalue;
                Double kgexcess = Double.parseDouble(excess)/dintkgvalue;
                P_WeightReading_History_Model model = new P_WeightReading_History_Model();
                model.setWeight(String.valueOf(kgweight));
                model.setStatus(status);
                model.setDate_time(date_time);
                model.setExcess(String.valueOf(kgexcess));
                model.setShift(shift);
                model.setSr_no(String.valueOf(weight_Id));
                p_weightReading_history_models.add(model);
            }
            else
            {
                P_WeightReading_History_Model model = new P_WeightReading_History_Model();
                model.setWeight(String.valueOf(weight));
                model.setStatus(status);
                model.setDate_time(date_time);
                model.setExcess(excess);
                model.setShift(shift);
                model.setSr_no(String.valueOf(weight_Id));
                p_weightReading_history_models.add(model);
            }


            Log.d("TAG", "baglengthReadingList: ");


            // production_tag_list.add(p_tag_string);
                Log.d("TAG", "p_tag: ");
            }

            catch (Exception e)
            {
                e.printStackTrace();
            }
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(BatchReportDetails.this);
            rv_history_weight_Reading.setLayoutManager(linearLayoutManager);
            rv_history_weight_Reading.setAdapter(new ProductionHistoryWeightReadingAdapter(BatchReportDetails.this, p_weightReading_history_models));


        } }

    private void getProductionGrammageValues() {
        Cursor grammageValues = helper.getProductionGrammageValues();
        while (grammageValues.moveToNext()){
            String kgid = grammageValues.getString(0);
            String kgname = grammageValues.getString(1);
            String kgvalues = grammageValues.getString(2);

            grammage_values_list.add(kgvalues);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }

    }



}
