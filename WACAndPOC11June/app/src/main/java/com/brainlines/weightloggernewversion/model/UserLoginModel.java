package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class UserLoginModel implements Parcelable {

    private String loginId;
    private String userId;
    private String roleId;
    private String userName;
    private String password;
    private boolean isActive;
    private boolean isDeleted;
    private String createdDate;
    private String modifiedDate;

    public UserLoginModel(String loginId, String userId, String roleId, String userName, String password, boolean isActive, boolean isDeleted, String createdDate, String modifiedDate) {
        this.loginId = loginId;
        this.userId = userId;
        this.roleId = roleId;
        this.userName = userName;
        this.password = password;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }

    protected UserLoginModel(Parcel in) {
        loginId = in.readString();
        userId = in.readString();
        roleId = in.readString();
        userName = in.readString();
        password = in.readString();
        isActive = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
        createdDate = in.readString();
        modifiedDate = in.readString();
    }

    public static final Creator<UserLoginModel> CREATOR = new Creator<UserLoginModel>() {
        @Override
        public UserLoginModel createFromParcel(Parcel in) {
            return new UserLoginModel(in);
        }

        @Override
        public UserLoginModel[] newArray(int size) {
            return new UserLoginModel[size];
        }
    };

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(loginId);
        dest.writeString(userId);
        dest.writeString(roleId);
        dest.writeString(userName);
        dest.writeString(password);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
        dest.writeString(createdDate);
        dest.writeString(modifiedDate);
    }
}
