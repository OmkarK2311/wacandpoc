package com.brainlines.weightloggernewversion.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.history.History_Main_Activity;
import com.brainlines.weightloggernewversion.activity.newtest.NewTestTagFillActvity;
import com.brainlines.weightloggernewversion.activity.newtest.WeightReadingActivity;
import com.brainlines.weightloggernewversion.activity.recalltest.Recall_Main_Activity;
import com.brainlines.weightloggernewversion.activity.release.ReleaseLoggerActivity;
import com.brainlines.weightloggernewversion.activity.release.ReleaseLoggerListActivity;
import com.brainlines.weightloggernewversion.activity.restarttest.RestartTestActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static com.brainlines.weightloggernewversion.utils.Constants.DATA_LOGGER_PORT;

@SuppressLint("Registered")
public class LoggerFunctionActivity extends AppCompatActivity implements View.OnClickListener {

    private CardView cv_newTest,cv_recall,cv_restart,cv_history,cv_release;
    private String dataLoggerName,dataLoggerIP,dataloggerMacId,dbName;
    private String DATA_LOGGER_PORT = Constants.DATA_LOGGER_PORT+"";
    private DataBaseHelper helper;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logger_function);
        initUI();

        dataLoggerIP =getIntent().getStringExtra("LoggerIP");
        dataLoggerName=getIntent().getStringExtra("LoggerName");
        dataloggerMacId=getIntent().getStringExtra("LoggerMacId");
        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

    }
    private void initUI() {
        cv_newTest = findViewById(R.id.cv_newTest);
        cv_recall = findViewById(R.id.cv_recall);
        cv_restart = findViewById(R.id.cv_restart);
        cv_history = findViewById(R.id.cv_history);
        cv_release = findViewById(R.id.cv_release);

        cv_newTest.setOnClickListener(this);
        cv_recall.setOnClickListener(this);
        cv_restart.setOnClickListener(this);
        cv_history.setOnClickListener(this);
        cv_release.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cv_newTest:
                helper.updateMstLoggerTableForLoggerStatus1(dataLoggerName,db);
                Intent intent = new Intent(this, NewTestTagFillActvity.class);
                intent.putExtra("LoggerName",dataLoggerName);
                intent.putExtra("LoggerIP",dataLoggerIP);
                intent.putExtra("LoggerMacId",dataloggerMacId);
                startActivity(intent);
                break;
            case R.id.cv_recall:
                Intent recallTestIntent = new Intent(this, Recall_Main_Activity.class);
                recallTestIntent.putExtra("LoggerName",dataLoggerName);
                recallTestIntent.putExtra("LoggerIP",dataLoggerIP);
                recallTestIntent.putExtra("LoggerMacId",dataloggerMacId);
                startActivity(recallTestIntent);
                break;
            case R.id.cv_restart:
                Intent restart_intent = new Intent(this, RestartTestActivity.class);
                restart_intent.putExtra("LoggerName",dataLoggerName);
                restart_intent.putExtra("LoggerIP",dataLoggerIP);
                restart_intent.putExtra("LoggerMacId",dataloggerMacId);
                startActivity(restart_intent);
                break;
            case R.id.cv_history:
                Toast.makeText(this, "History", Toast.LENGTH_SHORT).show();
                Intent historyintent = new Intent(this, History_Main_Activity.class);
                startActivity(historyintent);
                break;
            case R.id.cv_release:
                Intent release_intent = new Intent(this, ReleaseLoggerActivity.class);
                startActivity(release_intent);
                break;
        }
    }

    private String releaseLogger(String dataLoggerIP, String dataLoggerName) throws IOException {
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String url = "http://"+dataLoggerIP+":"+DATA_LOGGER_PORT+"/release";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            if (responseString.equals("ok")){
                //changeDataLoggerStatus(dataLoggerName,"Yes");
            }
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;
    }
}
