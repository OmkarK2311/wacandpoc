package com.brainlines.weightloggernewversion.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.brainlines.weightloggernewversion.BaseActivity;
import com.brainlines.weightloggernewversion.CalibrationOrProductionActivity;
import com.brainlines.weightloggernewversion.ModuleSelectionActivity;
import com.brainlines.weightloggernewversion.Production.activity.ForgotPasswordActivity;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.WebServices.API;
import com.brainlines.weightloggernewversion.WebServices.Retroconfig;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.GetDataFromMasterTablesModel;
import com.brainlines.weightloggernewversion.model.LoggersFromDBModel;
import com.brainlines.weightloggernewversion.model.MasterFillerTypeModel;
import com.brainlines.weightloggernewversion.model.MasterHeadModel;
import com.brainlines.weightloggernewversion.model.MasterMachineTypeModel;
import com.brainlines.weightloggernewversion.model.MasterPouchAttributes;
import com.brainlines.weightloggernewversion.model.MasterPouchSymmetryModel;
import com.brainlines.weightloggernewversion.model.MasterProductModel;
import com.brainlines.weightloggernewversion.model.MasterSealTypeModel;
import com.brainlines.weightloggernewversion.model.MasterSealTypeRejectionReasonsModel;
import com.brainlines.weightloggernewversion.model.MasterTablesModels;
import com.brainlines.weightloggernewversion.model.MasterTargetAccuracyModel;
import com.brainlines.weightloggernewversion.model.MasterUoMTableModel;
import com.brainlines.weightloggernewversion.model.MasterWireCupModel;
import com.brainlines.weightloggernewversion.model.ProductionReasonTable;
import com.brainlines.weightloggernewversion.model.ProductionToleranceModel;
import com.brainlines.weightloggernewversion.model.ProductionTypeModel;
import com.brainlines.weightloggernewversion.model.UserLoginModel;
import com.brainlines.weightloggernewversion.model.UserModel;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.AsyncTasks;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.brainlines.weightloggernewversion.utils.GlobalClass;
import com.brainlines.weightloggernewversion.utils.Queries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("Registered")
public class LoginActivity extends BaseActivity implements View.OnClickListener{

    private static final String TAG = "LoginActivity";
    Button btn_login;
    EditText edt_enter_user_name,edt_enter_password;
    public static TextView tv_check_connection;
    DataBaseHelper helper;
    ArrayList<UserLoginModel> usersListFromDb = new ArrayList<>();
    ArrayList<UserLoginModel> usersListFromLocalDb = new ArrayList<>();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int MY_PERMISSION = 21;
    //    private NetworkChangeReceiver networkChangeReceiver;
    Intent mServiceIntent;
    TextView txt_forgot_password;
    int i = 0;

    SQLiteDatabase db;
    GlobalClass globalMethodCall;
    private ProgressDialog progressDialog;
    private ArrayList<GetDataFromMasterTablesModel> getDataFromMasterTablesModels = new ArrayList<>();
    private ArrayList<UserModel> userModelList = new ArrayList<>();


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initUI();
        /*
         * Check if DB exists
         *
         * */

//        File dbFile= dbFilePath(LoginActivity.this,DataBaseHelper.DATABASE_NAME);
//        String dbFilePath = dbFile.getAbsolutePath();
//        db = SQLiteDatabase.openDatabase(dbFilePath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
//        helper = new DataBaseHelper(LoginActivity.this);
//        globalMethodCall = new GlobalClass(this,helper);
//        int c = helper.getTableCount();
//        if (c == 0){
//            Log.d(TAG, "onCreate: Table Count -> " + c);
//            db.execSQL(Queries.CREATE_PRODUCTION_KG_VALUES_MASTER_TABLES);
//            InsertIntoGrammageTable();
//            getCreateStatementsForDB();
//        }else {
//            Log.d(TAG, "onCreate: Table Count -> " + c );
//
//        }

        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(i);
            }
        });

    }


    private static File dbFilePath(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile;
    }
    private void getAllMasterData() {
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstCalibrationTolerance","CalibrationToleranceID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstMachineType","MachineTypeID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstWireCup","WireCupID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstTargetAccuracy","TargetAccuracyID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstProductionTolerance","ProductionToleranceID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstProductionReasonMaster","ReasonID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstProductType","ProductTypeID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstFillerType","FillerTypeID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstHead","HeadID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstLogger","LoggerID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstProducts","ProductID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstSealType","SealID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstUomTable","UOMId"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstPouchAttributes","PouchAttributesID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstPouchSymmetry","PouchSymmetryID"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"mstSealRejectionReasons","rsn_id"));
        getDataFromMasterTablesModels.add(new GetDataFromMasterTablesModel(1234,"UserLogin","LoginID"));

        for (int i = 0 ; i < getDataFromMasterTablesModels.size() ; i++){
            globalMethodCall.syncSingleTable(getDataFromMasterTablesModels.get(i).getMasterTableName(),
                    getDataFromMasterTablesModels.get(i).getKeyIDColumnName(),progressDialog);
        }
    }

    private void initUI() {

        btn_login = findViewById(R.id.btn_login);
        edt_enter_user_name = findViewById(R.id.edt_enter_user_name);
        edt_enter_password = findViewById(R.id.edt_enter_password);
        tv_check_connection = findViewById(R.id.tv_check_connection);
        txt_forgot_password = findViewById(R.id.txt_forgot_password);

        btn_login.setOnClickListener(this);
        txt_forgot_password.setOnClickListener(this);
//        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerNetworkBroadcastForNougat();

//        final long period = 5000;
//        new Timer().schedule(new TimerTask() {
//            @Override
//            public void run() {
//                // do your task here
//                TimerMethod();
//            }
//        }, 0, period);
    }

    private void TimerMethod() {
        this.runOnUiThread(Timer_Tick);
    }

    private Runnable Timer_Tick = new Runnable() {

        public void run() {

            Toast.makeText(LoginActivity.this, i+++"", Toast.LENGTH_SHORT).show();

        }
    };

    private void registerNetworkBroadcastForNougat() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
    }

    protected void unregisterNetworkChanges() {
//        try {
//            unregisterReceiver(networkChangeReceiver);
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
//                Cursor data = helper.getUserName();
//                while (data.moveToNext()){
//                    String loginID = data.getString(0);
//                    String userID = data.getString(2);
//                    String roleID = data.getString(3);
//                    String userName = data.getString(4);
//                    String password = data.getString(5);
//                    boolean isActive = data.getInt(6) > 0;
//                    boolean isDeleted = data.getInt(7) > 0;
//                    String createdDate = data.getString(8);
//                    String modifiedDate = data.getString(9);
//                    usersListFromLocalDb.add(new UserLoginModel(loginID,userID,roleID,userName,password,isActive,isDeleted,createdDate,modifiedDate));
//                }
//                ArrayList<UserLoginModel> list = usersListFromLocalDb;
//                for (UserLoginModel model : list){
//                    String username = model.getUserName();
//                    String password = model.getPassword();
//                    if (edt_enter_user_name.getText().toString().equals(model.getUserName()) && edt_enter_password.getText().toString().equals(model.getPassword())){
//                        AppPreferences.setUser(LoginActivity.this,username);
//                        AppPreferences.setPassword(LoginActivity.this,password);
//                        Intent intent = new Intent(LoginActivity.this, CalibrationOrProductionActivity.class);
//                        intent.putExtra("UserModel",model);
//                        startActivity(intent);
////                        try {
//////                            getInitialMasterDataFromServer();
//////                            getCreateStatementsForDB(model);
////                        } catch (Exception e) {
////                            e.printStackTrace();
////                        }
//                    }else {
//                        Toast.makeText(this, "Please insert valid Username and Password", Toast.LENGTH_SHORT).show();
//                    }
//                }
                login();
                break;
        }
    }

    private void login() {
        Log.d(TAG, "login: Inside Login Method");
        final API api = Retroconfig.baseMethod().create(API.class);
        Call<ResponseBody> call = api.syncUserLoginData("sameekshazaware@gmail.com","telviwljaF84HGULDZ4tdQ==");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body()!=null){
                    try {
                        String res = response.body().string();
                        if (res != null) {
                            JSONObject object = new JSONObject(res);
                            JSONObject data = object.getJSONObject("Data");
                            JSONArray array = data.getJSONArray("UserData");
                            JSONArray array1 = object.getJSONObject("Data").getJSONArray("UserData");
                            Log.d(TAG, "onResponse: ");
                            if (array.length() != 0){
                                for (int i = 0 ; i < array.length() ; i++){
                                    JSONObject singleUserDataObject = array.getJSONObject(i);
                                    userModelList.add(new UserModel(
                                            singleUserDataObject.getInt("UserID"),
                                            singleUserDataObject.getInt("OEMID"),
                                            singleUserDataObject.getInt("RoleID"),
                                            singleUserDataObject.getString("RoleName"),
                                            singleUserDataObject.getInt("ModuleID"),
                                            singleUserDataObject.getString("ModuleName")
                                    ));
                                }
                                AppPreferences.setUser(LoginActivity.this,"sameekshazaware@gmail.com");
                                AppPreferences.setPassword(LoginActivity.this,"telviwljaF84HGULDZ4tdQ==");
                                Intent intent = new Intent(LoginActivity.this, ModuleSelectionActivity.class);
                                intent.putParcelableArrayListExtra("UserDataList",userModelList);
                                startActivity(intent);
                            }else {

                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });
    }

    private void getCreateStatementsForDB() {
        Log.d(TAG, "getCreateStatementsForDB: Inside");
        final API api = Retroconfig.baseMethod().create(API.class);
        Call<ResponseBody> call = api.createTableForModule(1234,3);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("wait");
        progressDialog.setCancelable(false);
        progressDialog.show();


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null){
                    try {
                        progressDialog.dismiss();
                        String res = response.body().string();
                        JSONObject jsonObject = new JSONObject(res);
                        String statusCode = jsonObject.getString("statusCode");
                        if (statusCode.equals("200")) {
                            JSONObject dataObject = jsonObject.getJSONObject("Data");
                            JSONArray tableDataArray = dataObject.getJSONArray("TableData");
                            if (tableDataArray.length() != 0){

//                                helper.createTableFromServerData(db,tableDataArray);
                                getAllMasterData();
                            }
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: Create Table API Failed");
            }
        });

    }

    private static boolean isDBExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    private void deleteThisDatabase(String oemID,String moduleID){
        this.deleteDatabase("FG_"+oemID+"_"+moduleID);
        Log.d(TAG, "deleteThisDatabase: Database is Deleted");
    }

    private ArrayList<UserLoginModel> getUserFromDB() throws SQLException, ExecutionException, InterruptedException {
        usersListFromDb.clear();
        usersListFromDb = new AsyncTasks.GetLoginUsersFromDB(LoginActivity.this).execute().get();
        return usersListFromDb;
    }
    @Override
    protected void dialog(boolean value) {
        if(value){
            //tv_check_connection.setText("We are back !!!");
            //tv_check_connection.setBackgroundColor(Color.GREEN);
            //tv_check_connection.setTextColor(Color.WHITE);

            Handler handler = new Handler();
            Runnable delayrunnable = new Runnable() {
                @Override
                public void run() {
                    tv_check_connection.setVisibility(View.GONE);
                }
            };
            handler.postDelayed(delayrunnable, 3000);
        }else {
            tv_check_connection.setVisibility(View.VISIBLE);
            tv_check_connection.setText("Could not Connect to internet");
            tv_check_connection.setBackgroundColor(Color.RED);
            tv_check_connection.setTextColor(Color.WHITE);
        }
    }

    public void InsertIntoGrammageTable() {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(Constants.PRODUCTION_GRAMMAGE,"Kg");
        cv.put(Constants.PRODUCTION_KG_VALUES,"1000");
        long d=db.insert(Constants.PRODUCTION_KG_VALUES_MASTER_TABLES,null,cv);
        Log.d("Success", String.valueOf(d));
    }
}
