package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class GetDataFromMasterTablesModel implements Parcelable {


    int OEMID;
    String MasterTableName;
    String KeyIDColumnName;


    public GetDataFromMasterTablesModel(int OEMID, String masterTableName, String keyIDColumnName) {
        this.OEMID = OEMID;
        MasterTableName = masterTableName;
        KeyIDColumnName = keyIDColumnName;

    }

    protected GetDataFromMasterTablesModel(Parcel in) {
        OEMID = in.readInt();
        MasterTableName = in.readString();
        KeyIDColumnName = in.readString();

    }

    public static final Creator<GetDataFromMasterTablesModel> CREATOR = new Creator<GetDataFromMasterTablesModel>() {
        @Override
        public GetDataFromMasterTablesModel createFromParcel(Parcel in) {
            return new GetDataFromMasterTablesModel(in);
        }

        @Override
        public GetDataFromMasterTablesModel[] newArray(int size) {
            return new GetDataFromMasterTablesModel[size];
        }
    };



    public int getOEMID() {
        return OEMID;
    }

    public void setOEMID(int OEMID) {
        this.OEMID = OEMID;
    }

    public String getMasterTableName() {
        return MasterTableName;
    }

    public void setMasterTableName(String masterTableName) {
        MasterTableName = masterTableName;
    }



    public String getKeyIDColumnName() {
        return KeyIDColumnName;
    }

    public void setKeyIDColumnName(String keyIDColumnName) {
        KeyIDColumnName = keyIDColumnName;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(OEMID);
        dest.writeString(MasterTableName);
        dest.writeString(KeyIDColumnName);

    }
}
