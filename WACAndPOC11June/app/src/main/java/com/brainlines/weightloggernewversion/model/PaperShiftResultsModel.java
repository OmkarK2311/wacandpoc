package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PaperShiftResultsModel implements Parcelable {

    String OEMID;
    String MacID;
    String LoggerID;
    String Paper_shift_id;
    String TagID;
    String Shift;
    String OINumber;
    String modifieddate;

    public PaperShiftResultsModel(String OEMID, String mac_id, String loggerID, String paper_shift_id, String tagid, String shift, String oinum, String modifieddate) {
        this.OEMID = OEMID;
        this.MacID = mac_id;
        LoggerID = loggerID;
        Paper_shift_id = paper_shift_id;
        this.TagID = tagid;
        this.Shift = shift;
        this.OINumber = oinum;
        this.modifieddate = modifieddate;
    }

    protected PaperShiftResultsModel(Parcel in) {
        OEMID = in.readString();
        MacID = in.readString();
        LoggerID = in.readString();
        Paper_shift_id = in.readString();
        TagID = in.readString();
        Shift = in.readString();
        OINumber = in.readString();
        modifieddate = in.readString();
    }

    public static final Creator<PaperShiftResultsModel> CREATOR = new Creator<PaperShiftResultsModel>() {
        @Override
        public PaperShiftResultsModel createFromParcel(Parcel in) {
            return new PaperShiftResultsModel(in);
        }

        @Override
        public PaperShiftResultsModel[] newArray(int size) {
            return new PaperShiftResultsModel[size];
        }
    };

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getMac_id() {
        return MacID;
    }

    public void setMac_id(String mac_id) {
        this.MacID = mac_id;
    }

    public String getLoggerID() {
        return LoggerID;
    }

    public void setLoggerID(String loggerID) {
        LoggerID = loggerID;
    }

    public String getPaper_shift_id() {
        return Paper_shift_id;
    }

    public void setPaper_shift_id(String paper_shift_id) {
        Paper_shift_id = paper_shift_id;
    }

    public String getTagid() {
        return TagID;
    }

    public void setTagid(String tagid) {
        this.TagID = tagid;
    }

    public String getShift() {
        return Shift;
    }

    public void setShift(String shift) {
        this.Shift = shift;
    }

    public String getOinum() {
        return OINumber;
    }

    public void setOinum(String oinum) {
        this.OINumber = oinum;
    }

    public String getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(String modifieddate) {
        this.modifieddate = modifieddate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(OEMID);
        dest.writeString(MacID);
        dest.writeString(LoggerID);
        dest.writeString(Paper_shift_id);
        dest.writeString(TagID);
        dest.writeString(Shift);
        dest.writeString(OINumber);
        dest.writeString(modifieddate);
    }
}
