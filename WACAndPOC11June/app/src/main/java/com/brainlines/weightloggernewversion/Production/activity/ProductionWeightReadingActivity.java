package com.brainlines.weightloggernewversion.Production.activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainlines.weightloggernewversion.Production.Model.PWeighReadingsModel;
import com.brainlines.weightloggernewversion.Production.Model.Production_Reason_Table;
import com.brainlines.weightloggernewversion.Production.adapter.ProductionWeightReadingAdapter;
import com.brainlines.weightloggernewversion.ProductionMenuActivity;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ProductionWeightReadingActivity extends AppCompatActivity implements View.OnClickListener {
//    private static final String TAG = "ProductionWeightReading";
//
//    private String dbIpAddress = "192.168.10.67";
//    private String dbUserName = "test2";
//    private String dbPassword = "test12345";
//    private String dbDatabaseName = "decitalpool";
//    private String serverport = "1433";
//    private String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
//    private int DATA_LOGGER_PORT = 8080;
//    public static final String MQTT_BROKER = "tcp://192.168.10.67:1883";
//
//    private ProductionWeightReadingAdapter productionWeightReadingAdapter;
//    private ImageButton btnStop, img_btn_test_complete;
//    RecyclerView rv_weight_reading;
//    MqttAndroidClient client = null;
//    MqttAndroidClient clientObj = null;;
//    private ArrayList<Production_Reason_Table> productionReason_list = new ArrayList<>();
//    private ArrayList<PWeighReadingsModel> pWeighReadingsModels = new ArrayList<>();
//    private ArrayList<String> productionReasonList = new ArrayList<>();
////    private ArrayList<String>arrayList1 = new ArrayList<>();
//
//    TextView txtTagid;
//    Handler handler = new Handler();
//    Runnable runnable;
//    private double max_reading_value=0.0;
//    private double mean_of_readings = 0.0;
//    private double range = 0.0;
//    private double average = 0.0;
//    private double standard_deviation = 0.0;
//    private double totals_sum =0.0;
//    private double ducl = 0.0;
//    private double dlcl = 0.0;
//    private double dintkgvalue =0.0;
//
//    DataBaseHelper helper;
//    String Status, actualQuantityFilled,Actual_filled_pouches,unfilled_quantity,unfilled_pouches, Tolal_no_of_pouches,Total_qty,str_shifts,current_tag_id;
//    String str_batch_id,str_positive_tolerance,str_negative_tolerance,str_product,str_selected_date,str_cancel_Reason,str_cancel_remark,gmperpouch;
//    ImageButton img_btn_test_paused;
//    int count =0,uclgm=0,lclgm=0,delay = 5000;
//    SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    DecimalFormat decimalformat = new DecimalFormat("0.000");
//    private int index =0;
//    private int readingcount=0;
//    TextView txt_to_be_packed_quantity,txt_total_quantity,txt_report_product,txt_report_pro_type,txt_excess_give_Away,txt_no_of_pouches_of_pouches_packed,txt_to_be_packed_pouches;
//    private String strUom,dataloggerMacId,dataloggerName,dataloggerIp;
//    private String BatchStatus;
//    private String dbName;
//    private SQLiteDatabase db;
//    private String resToMobile = "";
//    private boolean isDone;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_production_weight_reading);
//        initUi();
//        dbName = AppPreferences.getDatabseName(this);
////        helper = new DataBaseHelper(this,dbName);
//        helper = new DataBaseHelper(this);
//        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));
////ignored weights
//       /* arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("104");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("98");
//        arrayList1.add("100");
//        arrayList1.add("101");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("93");*/
//
//       /* arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.093");
//        arrayList1.add("0.102");
//        arrayList1.add("0.104");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.098");
//        arrayList1.add("0.1");
//        arrayList1.add("0.101");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");
//        arrayList1.add("0.1");*/
//        //abort
//        /*arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("93");
//        arrayList1.add("104");
//        arrayList1.add("104");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("100");
//        arrayList1.add("101");*/
//
//        // all ok  // stop  // pause
//        /*arrayList1.add("100");
//        arrayList1.add("101");
//        arrayList1.add("100");
//        arrayList1.add("101");
//        arrayList1.add("100");
//        arrayList1.add("101");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("101");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("99");
//        arrayList1.add("101");
//        arrayList1.add("100");
//        arrayList1.add("100");
//        arrayList1.add("100");*/
//
//        //insufficient
//      /*  arrayList1.add("100");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("100");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("103");
//        arrayList1.add("100");
//        arrayList1.add("101");*/
//
//        /*int convert_gm = Integer.parseInt(Total_qty) / variable;
//        if (convert_gm > 1000)
//        {
//
//        }*/
//
//        /*PWeighReadingsModel model = new PWeighReadingsModel();
//        readingcount++;
//        String currentDateandTime = dateformat.format(new Date());
//        model.setBatchId(str_batch_id);
//        model.setWeight(arrayList1.get(index));
//        model.setShift(str_shifts);
//        model.setModifiedDate(str_selected_date);
//        model.setActualGmPerPouch(arrayList1.get(index));
//        model.setPWeightReadingID(String.valueOf(readingcount));
//        model.setTargetGmPerPouch(gmperpouch);
//        model.setExcessGiveawayPerPouch("0");
//        model.setCumilativeExcessGiveaway("0");
//        model.setCumilativeQuantity(arrayList1.get(index));
//        model.setCumilativePouches("1");
//        model.setCreatedDate(currentDateandTime);
//        model.setStatus("Okay");
//        pWeighReadingsModels.add(model);*/
//
//
//
//        masterProductionREjectionReson();
//
//        connectToMQttBroker(true);
//
//
//    }
//    private void initUi()
//    {
//        btnStop = findViewById(R.id.btnStop);
//        txtTagid = findViewById(R.id.txtTagid);
//        img_btn_test_complete = findViewById(R.id.img_btn_test_complete);
//        img_btn_test_paused = findViewById(R.id.img_btn_test_paused);
//
//        btnStop.setOnClickListener(this);
//        img_btn_test_paused.setOnClickListener(this);
//        img_btn_test_paused.setOnClickListener(this);
//
//        current_tag_id = getIntent().getStringExtra("current_tag_id");
//        Total_qty = getIntent().getStringExtra("total_qty");
//        gmperpouch = getIntent().getStringExtra("gmperpouch");
//        Tolal_no_of_pouches = getIntent().getStringExtra("no_of_pouches");
//        str_shifts = getIntent().getStringExtra("shift");
//        str_batch_id = getIntent().getStringExtra("batch_id");
//        str_positive_tolerance = getIntent().getStringExtra("positive_tolerance");
//        str_negative_tolerance = getIntent().getStringExtra("negative_tolerance");
//        str_selected_date = getIntent().getStringExtra("select_date");
//        str_product = getIntent().getStringExtra("product");
//        strUom = getIntent().getStringExtra("uom");
//        dataloggerMacId = getIntent().getStringExtra("mac_id");
//        dataloggerName = getIntent().getStringExtra("dataloggerName");
//        dataloggerIp = getIntent().getStringExtra("dataloggerIp");
//        String kgvalue = getIntent().getStringExtra("intkgvalue");
//        if(kgvalue !=null)
//        {
//            dintkgvalue = Double.parseDouble(kgvalue);
//
//        }
//        String ucl = getIntent().getStringExtra("ucl");
//        String lcl = getIntent().getStringExtra("lcl");
//
//        if (strUom.equals("gm"))
//        {
//            if (ucl != null){
//                uclgm =Integer.parseInt(ucl);
//            }
//            if (lcl != null){
//                lclgm = Integer.parseInt(lcl);
//            }
//        }
//        else
//        {
//            if (ucl != null){
//                Double uc = Double.parseDouble(ucl);
//                ducl = Double.parseDouble(decimalformat.format(uc));
//            }
//            if (lcl != null){
//                Double lc = Double.parseDouble(lcl);
//                dlcl = Double.parseDouble(decimalformat.format(lc));
//            }
//        }
//
//
//        txtTagid.setText(str_batch_id);
//        rv_weight_reading = (RecyclerView) findViewById(R.id.rv_weight_reading);
//        txt_to_be_packed_quantity = findViewById(R.id.txt_to_be_packed_quantity);
//        txt_total_quantity =  findViewById(R.id.txt_total_quantity);
//        txt_report_product = findViewById(R.id.txt_report_product);
//        txt_report_pro_type = findViewById(R.id.txt_report_pro_type);
//        txt_excess_give_Away = findViewById(R.id.txt_excess_give_Away);
//        txt_no_of_pouches_of_pouches_packed = findViewById(R.id.txt_no_of_pouches_of_pouches_packed);
//        txt_to_be_packed_pouches = findViewById(R.id.txt_to_be_packed_pouches);
//
//        txt_no_of_pouches_of_pouches_packed.setText(Tolal_no_of_pouches);
//        Double dtotalqtty = Double.valueOf(Total_qty);
//        txt_total_quantity.setText(decimalformat.format(dtotalqtty));
//    }
//    @Override
//    protected void onResume() {
//        handler.postDelayed(runnable = new Runnable() {
//            public void run() {
//                handler.postDelayed(runnable, delay);
//
//
//
//            }
//
//        }, delay);
//
//        super.onResume();
//    }
//
//    private void Calculation()
//    {
//        for (int cnt =1;cnt < pWeighReadingsModels.size(); cnt++) {
//            String status = pWeighReadingsModels.get(cnt).getStatus();
//            if (status.equals("Okay")) {
//                if (Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight()) > max_reading_value) {
//                    max_reading_value = Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
//                }
//            }
//        }
//
//        double min_reading_value = Double.parseDouble(pWeighReadingsModels.get(0).getWeight());
//        for (int cnt =1; cnt < pWeighReadingsModels.size(); cnt++)
//        {
//            String status = pWeighReadingsModels.get(cnt).getStatus();
//            if (status.equals("Okay")) {
//                if (Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight()) < min_reading_value) {
//                    min_reading_value = Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
//                }
//            } }
//        //mean calculation
//        for (int cnt = 0 ; cnt< pWeighReadingsModels.size() ; cnt++)
//        {
//            String status = pWeighReadingsModels.get(cnt).getStatus();
//            if (status.equals("Okay"))
//            {
//                totals_sum += Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
//
//            }
//
//        }
//        mean_of_readings = (max_reading_value - min_reading_value)/2 + min_reading_value;
//        //range calculation
//        range = max_reading_value - min_reading_value;
//
//        //average_calculation
//        average = mean_of_readings;
//
//
//        double[] deviations = new double[pWeighReadingsModels.size()];
//        //Taking the deviation of mean from each number
//        for (int i = 0;i < deviations.length; i++)
//        {
//            String status = pWeighReadingsModels.get(i).getStatus();
//            if (status.equals("Okay"))
//            {
//                deviations[i] = Double.parseDouble(pWeighReadingsModels.get(i).getWeight()) - average;
//            }
//        }
//        //getting the squares of deviations
//        double[] squares = new double[pWeighReadingsModels.size()];
//        for (int i = 0;i<pWeighReadingsModels.size();i++)
//        {
//            String status = pWeighReadingsModels.get(i).getStatus();
//            if (status.equals("Okay"))
//            {
//                for (int i1 = 0;i1 < squares.length; i1++)
//                {
//                    squares[i1] = deviations[i1] * deviations[i1];
//                }
//            }
//        }
//        //Adding all the squares
//        double sum_of_squares = 0;
//        for (int i =0;i<squares.length;i++)
//        {
//            sum_of_squares = sum_of_squares + squares[i];
//        }
//        double result = sum_of_squares / (pWeighReadingsModels.size() - 1);
//        standard_deviation = Math.sqrt(result);
//
//        SQLiteDatabase db = helper.getWritableDatabase();
//        String currentDateandTime = dateformat.format(new Date());
//        Double  totalexcessgiveaway = 0.000;
//
//        if (strUom.equals("Kg"))
//        {
//            try {
//                for(int i=0;i<pWeighReadingsModels.size();i++)
//                {
//                    String totalexcess = pWeighReadingsModels.get(i).getExcessGiveawayPerPouch();
//                    totalexcessgiveaway += Double.parseDouble(totalexcess);
//                }
//                totalexcessgiveaway = Double.parseDouble(txt_excess_give_Away.getText().toString());
//
//                Double doubleunfilledqty = Double.parseDouble(unfilled_quantity) * dintkgvalue;
//                Double doublefilledqty = Double.parseDouble(actualQuantityFilled) * dintkgvalue;
//                Double dmin = min_reading_value * dintkgvalue;
//                Double dmax = max_reading_value * dintkgvalue;
//                Double dmean = mean_of_readings * dintkgvalue;
//                Double daverage = average*dintkgvalue;
//                Double drange = range*dintkgvalue;
//                Double dsd = standard_deviation*dintkgvalue;
//                Double dtotalexcess = totalexcessgiveaway*dintkgvalue;
//                ContentValues cv=new ContentValues();
//                cv.put("ReportID",1);
//
//                cv.put("OEMID","1234");
//                cv.put("TagID",current_tag_id);
//                cv.put("BatchID",str_batch_id);
//                cv.put("MinWeight",Float.parseFloat(String.valueOf(dmin)));
//                cv.put("MaxWeight",Float.parseFloat(String.valueOf(dmax)));
//                cv.put("MeanWeight",Float.parseFloat(String.valueOf(dmean)));
//                cv.put("Average",Float.parseFloat(String.valueOf(daverage)));
//                cv.put("Range",Float.parseFloat(String.valueOf(drange)));
//                cv.put("SD",Float.parseFloat(String.valueOf(dsd)));
//                cv.put("UnfilledQuantity",Float.parseFloat(String.valueOf(doubleunfilledqty)));
//                cv.put("FilledQuantity", Float.parseFloat(String.valueOf(doublefilledqty)));
//                cv.put("UnfilledPouches",unfilled_pouches);
//                cv.put("FilledPouches",Integer.parseInt(Actual_filled_pouches));
//                cv.put("ExcessGiveawayPrice","");
//                cv.put("ExcessGiveawayGm",decimalformat.format(dtotalexcess));
//                cv.put("Reason",str_cancel_Reason);
//                cv.put("Remark",str_cancel_remark);
//                cv.put("IsActive",1);
//                cv.put("CreatedDate",currentDateandTime);
//                cv.put("ModifiedDate",currentDateandTime);
//                cv.put("IsSync",0);
//
//                long d=db.insert(Constants.PRODUCTION_WEIGHT_REPORT,null,cv);
//                Log.d("Success", String.valueOf(d));
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
//        else
//        {
//            try {
//                for(int i=0;i<pWeighReadingsModels.size();i++)
//                {
//                    String totalexcess = pWeighReadingsModels.get(i).getExcessGiveawayPerPouch();
//                    totalexcessgiveaway += Double.parseDouble(totalexcess);
//                }
//                totalexcessgiveaway = Double.parseDouble(txt_excess_give_Away.getText().toString());
//
//                Double doubleunfilledqty = Double.valueOf(unfilled_quantity);
//                Double doublefilledqty = Double.valueOf(actualQuantityFilled);
//                ContentValues cv=new ContentValues();
//                cv.put("ReportID",1);
//                cv.put("OEMID",1234);
//                cv.put("TagID",current_tag_id);
//                cv.put("BatchID",str_batch_id);
//                cv.put("MinWeight",Float.parseFloat(String.valueOf(min_reading_value)));
//                cv.put("MaxWeight",Float.parseFloat(String.valueOf(max_reading_value)));
//                cv.put("MeanWeight",Float.parseFloat(String.valueOf(mean_of_readings)));
//                cv.put("Average",Float.parseFloat(String.valueOf(average)));
//                cv.put("Range",Float.parseFloat(String.valueOf(range)));
//                cv.put("SD",Float.parseFloat(String.valueOf(standard_deviation)));
//                cv.put("UnfilledQuantity",Float.parseFloat(String.valueOf(doubleunfilledqty)));
//                cv.put("FilledQuantity", Float.parseFloat(String.valueOf(doublefilledqty)));
//                cv.put("UnfilledPouches",Integer.parseInt(unfilled_pouches));
//                cv.put("FilledPouches",Integer.parseInt(Actual_filled_pouches));
//                cv.put("ExcessGiveawayPrice","");
//                cv.put("ExcessGiveawayGm",Float.parseFloat(String.valueOf(totalexcessgiveaway)));
//                cv.put("Reason",str_cancel_Reason);
//                cv.put("Remark",str_cancel_remark);
//                cv.put("IsActive",1);
//                cv.put("CreatedDate",currentDateandTime);
//                cv.put("ModifiedDate",currentDateandTime);
//                cv.put("IsSync",0);
//
//                long d=db.insert(Constants.PRODUCTION_WEIGHT_REPORT,null,cv);
//                Log.d("Success", String.valueOf(d));
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
//
//
//
//    }
//
//    private void masterProductionREjectionReson()
//    {
//        Cursor masterRejectionReason = helper.getMasterProductionRejectionReason();
//        while (masterRejectionReason.moveToNext()){
//            String reasonId = masterRejectionReason.getString(0);
//            String reasonDesc = masterRejectionReason.getString(1);
//            productionReasonList.add(reasonDesc);
//
//            Production_Reason_Table model = new Production_Reason_Table();
//            model.setReasonID(reasonId);
//            model.setReasonDesc(reasonDesc);
//
//            productionReason_list.add(model);
//            Log.d("TAG", "getProductsUsedForTrial: ");
//        }
//    }
//
//    public void InsertIntoWeightReadingDb() {
//        SQLiteDatabase db = helper.getWritableDatabase();
//        String currentDateandTime = dateformat.format(new Date());
//        try {
//            if (strUom.equals("Kg"))
//            {
//                for (int i = 0; i < pWeighReadingsModels.size(); i++) {
//
//                    String weight = pWeighReadingsModels.get(i).getWeight();
//                    String status = pWeighReadingsModels.get(i).getStatus();
//                    Double dweight = Double.parseDouble(weight) * dintkgvalue;
//
//
//
//                    Double excess_giveaway = dweight-Double.parseDouble(gmperpouch) * dintkgvalue;
//                    Double dbweight = Double.parseDouble(pWeighReadingsModels.get(i).getWeight()) * dintkgvalue;
//                    Double dcumulativeexcess = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeExcessGiveaway()) * dintkgvalue;
//                    Double dactualgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getActualGmPerPouch()) * dintkgvalue;
//                    Double dtargetgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getTargetGmPerPouch()) * dintkgvalue;
//                    Double dcumulativequantity = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeQuantity()) * dintkgvalue;
//
//                    ContentValues cv = new ContentValues();
//                    cv.put(Constants.P_WEIGHT_READING_ID,i+1);
//                    cv.put(Constants.P_WEIGHT_READING_OEM_ID,1234);
//                    cv.put(Constants.P_WEIGHT_READING_MAC_ID,dataloggerMacId);
//                    cv.put(Constants.P_WEIGHT_READING_LOGGER_ID,dataloggerName);
//                    cv.put(Constants.P_WEIGHT_READING_BATCHID, str_batch_id);
//
//                    cv.put(Constants.P_WEIGHT_READING_WEIGHT_ID, i+1);
//                    cv.put(Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME, current_tag_id);
//                    cv.put(Constants.P_WEIGHT_READING_SHIFT, str_shifts);
//                    cv.put(Constants.P_WEIGHT_READING_WEIGHT, Float.parseFloat(String.valueOf(dbweight)));
//                    cv.put(Constants.P_WEIGHT_READING_TARGETGMPERPOUCH, Float.parseFloat(String.valueOf(dtargetgmperpouch)));
//                    cv.put(Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH, Float.parseFloat(String.valueOf(dactualgmperpouch)));
//                    if (status.equals("Okay"))
//                    {
//                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, Float.parseFloat(String.valueOf(excess_giveaway)));
//                    }
//                    else
//                    {
//                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, 0.0);
//                    }
//
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY,Float.parseFloat(String.valueOf(dcumulativeexcess)));
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY,Float.parseFloat(String.valueOf(dcumulativequantity)));
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES,Integer.parseInt(pWeighReadingsModels.get(i).getCumilativePouches()));
//                    cv.put(Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG, 0.0);
//                    cv.put(Constants.P_WEIGHT_READING_UCL, 0.0);
//                    cv.put(Constants.P_WEIGHT__READING_LCL,0.0);
//                    cv.put(Constants.P_WEIGHT_READING_STATUS, pWeighReadingsModels.get(i).getStatus());
//                    cv.put(Constants.P_WEIGHT_READING_CREATED_DATE,currentDateandTime);
//                    cv.put(Constants.P_WEIGHT_READING_CREATED_BY,"Pooja");
//                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_BY,"Pooja");
//                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_DATE, currentDateandTime);
//                    cv.put(Constants.P_WEIGHT_READING_IS_SYNC,0);
//
//                    long d = db.insert(Constants.P_WEIGHT_READING_TABLE, null, cv);
//                    Log.d("Success", String.valueOf(d));
//                    if (d == -1){
//                        Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                    }else {
//                        Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                        //wakeup();
//                    } }
//            }
//            else
//            {
//                for (int i = 0; i < pWeighReadingsModels.size(); i++) {
//
//                    String weight = pWeighReadingsModels.get(i).getWeight();
//                    String status = pWeighReadingsModels.get(i).getStatus();
//                    Double dweight = Double.parseDouble(weight);
//
//
//
//                    Double excess_giveaway = dweight-Double.parseDouble(gmperpouch);
//                    Double dbweight = Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
//                    Double dcumulativeexcess = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeExcessGiveaway());
//                    Double dactualgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getActualGmPerPouch());
//                    Double dtargetgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getTargetGmPerPouch());
//                    Double dcumulativequantity = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeQuantity());
//
//                    ContentValues cv = new ContentValues();
//                    cv.put(Constants.P_WEIGHT_READING_ID,i+1);
//                    cv.put(Constants.P_WEIGHT_READING_OEM_ID,1234);
//                    cv.put(Constants.P_WEIGHT_READING_MAC_ID,dataloggerMacId);
//                    cv.put(Constants.P_WEIGHT_READING_LOGGER_ID,dataloggerName);
//                    cv.put(Constants.P_WEIGHT_READING_WEIGHT_ID, i+1);
//                    cv.put(Constants.P_WEIGHT_READING_BATCHID, str_batch_id);
//                    cv.put(Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME, current_tag_id);
//                    cv.put(Constants.P_WEIGHT_READING_SHIFT, str_shifts);
//                    cv.put(Constants.P_WEIGHT_READING_UCL, 0.0);
//                    cv.put(Constants.P_WEIGHT__READING_LCL,0.0);
//                    cv.put(Constants.P_WEIGHT_READING_WEIGHT, decimalformat.format(dbweight));
//                    cv.put(Constants.P_WEIGHT_READING_TARGETGMPERPOUCH, Float.parseFloat(String.valueOf(dtargetgmperpouch)));
//                    cv.put(Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH, Float.parseFloat(String.valueOf(dactualgmperpouch)));
//                    if (status.equals("Okay"))
//                    {
//                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, Float.parseFloat(String.valueOf(excess_giveaway)));
//                    }
//                    else
//                    {
//                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, 0.0);
//                    }
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY,Float.parseFloat(String.valueOf(dcumulativeexcess)));
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY,Float.parseFloat(String.valueOf(dcumulativequantity)));
//                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES,Integer.parseInt(pWeighReadingsModels.get(i).getCumilativePouches()));
//                    cv.put(Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG, 0.0);
//                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_DATE, currentDateandTime);
//                    cv.put(Constants.P_WEIGHT_READING_CREATED_DATE,currentDateandTime);
//                    cv.put(Constants.P_WEIGHT_READING_STATUS, pWeighReadingsModels.get(i).getStatus());
//                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_BY,"Pooja");
//                    cv.put(Constants.P_WEIGHT_READING_CREATED_BY,"Pooja");
//                    cv.put(Constants.P_WEIGHT_READING_IS_SYNC,0);
//
//                    long d = db.insert(Constants.P_WEIGHT_READING_TABLE, null, cv);
//                    Log.d("Success", String.valueOf(d));
//                    if (d == -1){
//                        Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                    }else {
//                        Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
//                        //wakeup();
//                    } }
//
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//
//    public void onClick(View v) {
//        if (v.getId()==btnStop.getId())
//        {
//            final ArrayAdapter<String> productionReasonlist = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, productionReasonList);
//
////            handler.removeCallbacks(runnable);
//
//            ViewGroup viewGroup = findViewById(android.R.id.content);
//            View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.cancel_readings_layout, viewGroup, false);
//            AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//            builder.setView(dialogView);
//            Button btn_cancel_qty = dialogView.findViewById(R.id.btn_cancel_quantity);
//            Button btn_ok_qty = dialogView.findViewById(R.id.btn_ok_quantity);
//            final EditText ed_cancel_reason = dialogView.findViewById(R.id.ed_reason);
//
//            final Spinner spin_rejection_Reason = dialogView.findViewById(R.id.spin_reason);
//            productionReasonlist.setDropDownViewResource(R.layout.item_spinner_latyout);
//            spin_rejection_Reason.setAdapter(productionReasonlist);
//
//            spin_rejection_Reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    str_cancel_Reason = spin_rejection_Reason.getSelectedItem().toString();
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//
//                }
//            });
//
//            final AlertDialog alertDialog = builder.create();
//            alertDialog.show();
//
//            btn_ok_qty.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertDialog.dismiss();
//                    InsertIntoWeightReadingDb();
//                    str_cancel_remark = ed_cancel_reason.getText().toString();
//                    Double  intunfilledqty = Double.parseDouble(txt_to_be_packed_quantity.getText().toString().trim());
//                    Double totalunfilledqty = Double.parseDouble(Total_qty)-intunfilledqty;
//                    unfilled_quantity = String.valueOf(totalunfilledqty);
//
//                    actualQuantityFilled = String.valueOf(intunfilledqty);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a =  txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//                    BatchStatus ="Abort";
//                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                    Calculation();
//                    SQLiteDatabase db = helper.getWritableDatabase();
//                    String currentDateandTime = dateformat.format(new Date());
//                    Double dtotalquantity = 0.0;
//
//                    if(strUom.equals("gm"))
//                    {
//                        dtotalquantity = Double.parseDouble(Total_qty);
//
//                    }
//                    else
//                    {
//                        dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;
//
//                    }
//
//                    ContentValues cv=new ContentValues();
//                    cv.put(Constants.P_TAG_RESULT_ID,1);
//                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, decimalformat.format(dtotalquantity));
//                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                    Log.d("Success", String.valueOf(d));
//
//                    Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
//                    i.putExtra("selected_date",str_selected_date);
//                    i.putExtra("product",str_product);
//                    i.putExtra("batch_id",str_batch_id);
//                    startActivity(i);
//
//                }
//            });
//
//            btn_cancel_qty.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertDialog.dismiss();
//                    InsertIntoWeightReadingDb();
//                    str_cancel_remark = ed_cancel_reason.getText().toString();
//                    Double  intunfilledqty = Double.parseDouble(txt_to_be_packed_quantity.getText().toString().trim());
//                    Double totalunfilledqty = Double.parseDouble(Total_qty)-intunfilledqty;
//                    unfilled_quantity = String.valueOf(totalunfilledqty);
//
//                    actualQuantityFilled = String.valueOf(intunfilledqty);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a =  txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//                    BatchStatus ="Abort";
//                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                    Calculation();
//                    SQLiteDatabase db = helper.getWritableDatabase();
//                    String currentDateandTime = dateformat.format(new Date());
//                    ContentValues cv=new ContentValues();
//                    Double dtotalquantity = 0.0;
//
//                    if(strUom.equals("gm"))
//                    {
//                        dtotalquantity = Double.parseDouble(Total_qty);
//
//                    }
//                    else
//                    {
//                        dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;
//
//                    }
//
//                    cv.put(Constants.P_TAG_RESULT_ID,1);
//                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, decimalformat.format(dtotalquantity));
//                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                    Log.d("Success", String.valueOf(d));
//
//                    Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
//                    i.putExtra("selected_date",str_selected_date);
//                    i.putExtra("product",str_product);
//                    i.putExtra("batch_id",str_batch_id);
//                    startActivity(i);
//
//                }
//            });
//
//
//        }
//
//        else if(v.getId()==img_btn_test_paused.getId())
//        {
////            handler.removeCallbacks(runnable);
//            Toast.makeText(ProductionWeightReadingActivity.this,"Batch has been paused",Toast.LENGTH_SHORT).show();
//
//            ViewGroup viewGroup = findViewById(android.R.id.content);
//            View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//            AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//            builder.setView(dialogView);
//            TextView txt_status = dialogView.findViewById(R.id.txt_status);
//            txt_status.setText("Batch Paused");
//            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//            final AlertDialog alertDialog = builder.create();
//            alertDialog.show();
//
//            btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    alertDialog.dismiss();
//                    Double  intunfilledqty = Double.parseDouble(txt_to_be_packed_quantity.getText().toString().trim());
//                    Double totalunfilledqty = Double.parseDouble(Total_qty)-intunfilledqty;
//                    unfilled_quantity = String.valueOf(totalunfilledqty);
//
//                    actualQuantityFilled = String.valueOf(intunfilledqty);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a =  txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//                    BatchStatus ="Paused";
//                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                    InsertIntoWeightReadingDb();
//                    Calculation();
//                    SQLiteDatabase db = helper.getWritableDatabase();
//                    String currentDateandTime = dateformat.format(new Date());
//                    ContentValues cv=new ContentValues();
//                    Double dtotalquantity = 0.0;
//
//                    if(strUom.equals("gm"))
//                    {
//                        dtotalquantity = Double.parseDouble(Total_qty);
//
//                    }
//                    else
//                    {
//                        dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;
//
//                    }
//
//                    cv.put(Constants.P_TAG_RESULT_ID,1);
//                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Paused");
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                    Log.d("Success", String.valueOf(d));
//
//                    Intent i = new Intent(ProductionWeightReadingActivity.this, ProductionMenuActivity.class);
//                    i.putExtra("selected_date",str_selected_date);
//                    i.putExtra("product",str_product);
//                    i.putExtra("batch_id",str_batch_id);
//                    startActivity(i);
//
//                }
//            });
//        }
//    }
//
//    public void gmperpouchcalculation(String weightReading)
//    {
//        int sumoftotalweights = 0;
//        int packing_total_qty = 0;
//        int sumoftotalpackedpouches = 0;
//        int sumoftotalpackedquantity =0;
//        double previous_cumu_qty = 0.0;
//        double totalexcessgiveaway = 0.0;
//        double excessgiveawayperpouch = 0.0;
//        int remaining_pouch_qty=0;
//        double cumulativeexcessfiveaway = 0.0;
//
//        try {
//            if (pWeighReadingsModels.size()!=0)
//            {
//                int pouches_count = 1;
//
//                for(int i = 0 ; i < pWeighReadingsModels.size(); i++) {
//                    String status = pWeighReadingsModels.get(i).getStatus();
//
//                    if (status.equals("Okay")) {
//                        String weight =pWeighReadingsModels.get(i).getWeight();
//                        sumoftotalweights += Double.parseDouble(weight);
//
//                        sumoftotalpackedquantity += Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
//                        sumoftotalpackedpouches = pouches_count++;
//                        excessgiveawayperpouch = Double.parseDouble(weight) - Double.parseDouble(gmperpouch);
//
//                        String strprevious_cumu_qty = pWeighReadingsModels.get(i).getCumilativeQuantity();
//                        previous_cumu_qty = Double.parseDouble(strprevious_cumu_qty);
//                        totalexcessgiveaway += excessgiveawayperpouch;
//
//                        String strtotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
//                        Double dbtotalexcessgiveaway = Double.valueOf(strtotalexcessgiveaway);
//
//                        String strexcessgiveaway = String.valueOf(excessgiveawayperpouch);
//                        Double dbexcess = Double.valueOf(strexcessgiveaway);
//
//                        txt_excess_give_Away.setText(decimalformat.format(dbtotalexcessgiveaway));
//
//                        if (excessgiveawayperpouch > Double.parseDouble(gmperpouch)) {
//                            txt_excess_give_Away.setText(decimalformat.format(dbexcess));
//                        }
//
//                        if (i!=0) {
//                            int pos = i-1;
//                            String previous_cum= pWeighReadingsModels.get(pos).getCumilativeExcessGiveaway();
//                            cumulativeexcessfiveaway = excessgiveawayperpouch + Double.parseDouble(previous_cum);
//
//                        }
//                    }
//                }
//
//                packing_total_qty = Integer.parseInt(Total_qty) - 1;
//
//                String strtobepackedqty = String.valueOf(sumoftotalpackedquantity);
//                Double dtoobepackedqty = Double.valueOf(strtobepackedqty);
//
//                txt_to_be_packed_quantity.setText(decimalformat.format(dtoobepackedqty));
//                txt_to_be_packed_pouches.setText(sumoftotalpackedpouches + "");
//
//                //this quantity used for checking the remaining quantity for packing
//                remaining_pouch_qty = Integer.parseInt(Total_qty)- sumoftotalweights;
//
//                for (int i =0 ;i<pWeighReadingsModels.size();i++)
//                {
//                    String status = pWeighReadingsModels.get(i).getStatus();
//                    if (status.equals("Ignored"))
//                    {
//                        count++;
//                        if (count==3) {
////                            handler.removeCallbacks(runnable);
//                            abort();
//                            ViewGroup viewGroup = findViewById(android.R.id.content);
//                            View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                            AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//                            builder.setView(dialogView);
//                            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                            TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                            final AlertDialog alertDialog = builder.create();
//                            alertDialog.show();
//                            txt_status.setText("Batch Aborted");
//                            unfilled_quantity = String.valueOf(remaining_pouch_qty);
//                            actualQuantityFilled = String.valueOf(sumoftotalweights);
//                            Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                            String a =  txt_to_be_packed_pouches.getText().toString();
//                            int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                            unfilled_pouches = String.valueOf(strunfilled_pouches);
//
////                            handler.removeCallbacks(runnable);
//
//                            btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    alertDialog.dismiss();
//                                    Status ="Abort";
//                                    BatchStatus ="Abort";
//                                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                                    InsertIntoWeightReadingDb();
//                                    Calculation();
//                                    SQLiteDatabase db = helper.getWritableDatabase();
//                                    String currentDateandTime = dateformat.format(new Date());
//                                    ContentValues cv=new ContentValues();
//
//                                    Double dtotalquantity = Double.parseDouble(Total_qty);
//
//                                    cv.put(Constants.P_TAG_RESULT_ID,1);
//                                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
//                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//                                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                                    Log.d("Success", String.valueOf(d));
//
//                                    Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
//                                    i.putExtra("selected_date",str_selected_date);
//                                    i.putExtra("product",str_product);
//                                    i.putExtra("batch_id",str_batch_id);
//                                    startActivity(i);
//
//                                }
//                            });
//                        }
//                    }
//                    else {
////                        count--;
//                        count = 0;
//                    }
//                }
//
//                if (sumoftotalweights>=packing_total_qty)
//                {
//                    btnStop.setVisibility(View.GONE);
////                    handler.removeCallbacks(runnable);
//                    abort();
//                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
//                    actualQuantityFilled = String.valueOf(sumoftotalweights);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a =  txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                    ViewGroup viewGroup = findViewById(android.R.id.content);
//                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//                    builder.setView(dialogView);
//                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                    txt_status.setText("Batch Completed");
//
//
//                    final AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//
//                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            InsertIntoWeightReadingDb();
//                            BatchStatus ="Complete";
//                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                            Calculation();
//                            SQLiteDatabase db = helper.getWritableDatabase();
//                            String currentDateandTime = dateformat.format(new Date());
//                            ContentValues cv=new ContentValues();
//                            Double dtotalquantity = Double.parseDouble(Total_qty);
//
//
//
//                            cv.put(Constants.P_TAG_RESULT_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Complete");
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//
//                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                            Log.d("Success", String.valueOf(d));
//
//
//                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
//                            i.putExtra("selected_date",str_selected_date);
//                            i.putExtra("product",str_product);
//                            i.putExtra("batch_id",str_batch_id);
//                            startActivity(i);
//
//                        }
//                    });
//
//                }
//                else if (remaining_pouch_qty <lclgm)
//                {
//                    btnStop.setVisibility(View.GONE);
////                    handler.removeCallbacks(runnable);
//                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
//                    actualQuantityFilled = String.valueOf(sumoftotalweights);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a =  txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                    ViewGroup viewGroup = findViewById(android.R.id.content);
//                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//                    builder.setView(dialogView);
//                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                    final AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//                    txt_status.setText("Insufficient remaining quantity");
//
//                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            Status = "Abort";
//                            BatchStatus ="Abort";
//                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                            InsertIntoWeightReadingDb();
//                            Calculation();
//                            SQLiteDatabase db = helper.getWritableDatabase();
//                            String currentDateandTime = dateformat.format(new Date());
//                            ContentValues cv=new ContentValues();
//                            Double dtotalquantity = Double.parseDouble(Total_qty);
//
//                            cv.put(Constants.P_TAG_RESULT_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//
//                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                            Log.d("Success", String.valueOf(d));
//
//                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
//                            i.putExtra("selected_date",str_selected_date);
//                            i.putExtra("product",str_product);
//                            i.putExtra("batch_id",str_batch_id);
//                            startActivity(i);
//
//
//                        }
//                    });
//
//                }
//                else {
//                    PWeighReadingsModel model = new PWeighReadingsModel();
//                    String currentDateandTime = dateformat.format(new Date());
//                    readingcount++;
//                    model.setBatchId(str_batch_id);
//                    model.setWeight(weightReading);
//                    model.setShift(str_shifts);
//                    model.setModifiedDate(str_selected_date);
//                    model.setActualGmPerPouch(weightReading);
//                    model.setTargetGmPerPouch(gmperpouch);
//                    model.setPWeightReadingID(String.valueOf(readingcount));
//                    model.setCumilativeExcessGiveaway(String.valueOf(cumulativeexcessfiveaway));
//                    String weight = weightReading;
//                    double excess_giveaway = Double.parseDouble(weight)-Double.parseDouble(gmperpouch);
//                    model.setExcessGiveawayPerPouch(String.valueOf(excess_giveaway));
//                    model.setCreatedDate(currentDateandTime);
//
//                    double cum_quantity_field = Double.parseDouble(weightReading) + previous_cumu_qty;
//                    model.setCumilativeQuantity(String.valueOf(cum_quantity_field));
//                    String pouches = txt_to_be_packed_pouches.getText().toString();
//                    model.setCumilativePouches(txt_to_be_packed_pouches.getText().toString());
//
//                    if (Double.parseDouble(weightReading)>uclgm || Double.parseDouble(weightReading)<lclgm) {
//                        model.setStatus("Ignored");
//                    }
//                    else
//                    {
//                        model.setStatus("Okay");
//                    }
//                    pWeighReadingsModels.add(model);
//                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProductionWeightReadingActivity.this);
//                    rv_weight_reading.setLayoutManager(linearLayoutManager);
//                    productionWeightReadingAdapter = new ProductionWeightReadingAdapter(ProductionWeightReadingActivity.this,pWeighReadingsModels);
//                    rv_weight_reading.setAdapter(productionWeightReadingAdapter);
//                    productionWeightReadingAdapter.notifyDataSetChanged();
//
//                    index ++;
//                }
//            }
//            else
//            {
//                PWeighReadingsModel model = new PWeighReadingsModel();
//                readingcount++;
//                String currentDateandTime = dateformat.format(new Date());
//                model.setBatchId(str_batch_id);
//                model.setWeight(weightReading);
//                model.setShift(str_shifts);
//                model.setModifiedDate(str_selected_date);
//                model.setActualGmPerPouch(weightReading);
//                model.setPWeightReadingID(String.valueOf(readingcount));
//                model.setTargetGmPerPouch(gmperpouch);
//                double excessgiveaway = Double.parseDouble(weightReading)-Double.parseDouble(gmperpouch);
//                model.setExcessGiveawayPerPouch(String.valueOf(excessgiveawayperpouch));
//                model.setCumilativeExcessGiveaway(String.valueOf(excessgiveawayperpouch));
//                model.setCumilativeQuantity(weightReading);
//                model.setCumilativePouches("1");
//                model.setCreatedDate(currentDateandTime);
//                if (Double.parseDouble(weightReading)>uclgm || Double.parseDouble(weightReading)<lclgm) {
//                    model.setStatus("Ignored");
//                }
//                else
//                {
//                    model.setStatus("Okay");
//                }
//
//                pWeighReadingsModels.add(model);
//                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProductionWeightReadingActivity.this);
//                rv_weight_reading.setLayoutManager(linearLayoutManager);
//                productionWeightReadingAdapter = new ProductionWeightReadingAdapter(ProductionWeightReadingActivity.this,pWeighReadingsModels);
//                rv_weight_reading.setAdapter(productionWeightReadingAdapter);
//                productionWeightReadingAdapter.notifyDataSetChanged();
//
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public void kgperpouchCalculation(String weightReading)
//    {
//        Double sumoftotalweights = 0.0;
//        Double packing_total_qty = 0.0;
//        int sumoftotalpackedpouches = 0;
//        Double sumoftotalpackedquantity =0.0;
//        Double previous_cumu_qty = 0.0;
//        Double totalexcessgiveaway = 0.0;
//        Double excessgiveawayperpouch = 0.0;
//        Double remaining_pouch_qty=0.0;
//        Double cumulativeexcessfiveaway = 0.0;
//
//        try {
//            if (pWeighReadingsModels.size()!=0)
//            {
//                int pouches_count = 1;
//
//                for(int i = 0 ; i < pWeighReadingsModels.size(); i++) {
//                    String status = pWeighReadingsModels.get(i).getStatus();
//
//                    if (status.equals("Okay")) {
//                        String weight =pWeighReadingsModels.get(i).getWeight();
//                        sumoftotalweights += Double.parseDouble(weight);
//
//                        sumoftotalpackedquantity += Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
//                        sumoftotalpackedpouches = pouches_count++;
//                        excessgiveawayperpouch = Double.parseDouble(weight) - Double.parseDouble(gmperpouch);
//
//                        String strprevious_cumu_qty = pWeighReadingsModels.get(i).getCumilativeQuantity();
//                        previous_cumu_qty = Double.parseDouble(strprevious_cumu_qty);
//                        totalexcessgiveaway += excessgiveawayperpouch;
//
//                        String strtotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
//                        Double dbtotalexcessgiveaway = Double.valueOf(strtotalexcessgiveaway);
//
//                        String strexcessgiveaway = String.valueOf(excessgiveawayperpouch);
//                        Double dbexcess = Double.valueOf(strexcessgiveaway);
//
//                        txt_excess_give_Away.setText(decimalformat.format(dbtotalexcessgiveaway));
//
//                        if (excessgiveawayperpouch > Double.parseDouble(gmperpouch)) {
//                            txt_excess_give_Away.setText(decimalformat.format(dbexcess));
//                        }
//
//                        if (i!=0) {
//                            int pos = i-1;
//                            String previous_cum= pWeighReadingsModels.get(pos).getCumilativeExcessGiveaway();
//                            cumulativeexcessfiveaway = excessgiveawayperpouch + Double.parseDouble(previous_cum);
//
//                        }
//                    }
//                }
//
//                Double dvalue = 1.0/dintkgvalue;
//                packing_total_qty = Double.parseDouble(Total_qty) - dvalue;
//
//
//                String strtobepackedqty = String.valueOf(sumoftotalpackedquantity);
//                Double dtoobepackedqty = Double.valueOf(strtobepackedqty);
//
//                txt_to_be_packed_quantity.setText(decimalformat.format(dtoobepackedqty));
//                txt_to_be_packed_pouches.setText(sumoftotalpackedpouches + "");
//
//                //this quantity used for checking the remaining quantity for packing
//                Double dqty = Double.parseDouble(Total_qty) * dintkgvalue;
//                Double dsumoftotalweight = sumoftotalweights*dintkgvalue;
//                Double dremaingqty = dqty - dsumoftotalweight;
//                Double dlc = dlcl * dintkgvalue;
//
//                for (int i =0 ;i<pWeighReadingsModels.size();i++)
//                {
//                    String status = pWeighReadingsModels.get(i).getStatus();
//                    if (status.equals("Ignored"))
//                    {
//                        count++;
//                        if (count==3) {
////                            handler.removeCallbacks(runnable);
//                            ViewGroup viewGroup = findViewById(android.R.id.content);
//                            View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                            AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//                            builder.setView(dialogView);
//                            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                            TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                            final AlertDialog alertDialog = builder.create();
//                            alertDialog.show();
//                            txt_status.setText("Batch Aborted");
//                            unfilled_quantity = String.valueOf(dremaingqty);
//                            actualQuantityFilled = String.valueOf(sumoftotalweights);
//                            Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                            String a =  txt_to_be_packed_pouches.getText().toString();
//                            int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                            unfilled_pouches = String.valueOf(strunfilled_pouches);
//
////                            handler.removeCallbacks(runnable);
//
//                            btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    alertDialog.dismiss();
//                                    Status ="Abort";
//                                    BatchStatus ="Abort";
//                                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//
//                                    InsertIntoWeightReadingDb();
//                                    Calculation();
//                                    SQLiteDatabase db = helper.getWritableDatabase();
//                                    String currentDateandTime = dateformat.format(new Date());
//                                    ContentValues cv=new ContentValues();
//                                    Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;
//
//                                    cv.put(Constants.P_TAG_RESULT_ID,1);
//                                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
//                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//
//
//                                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                                    Log.d("Success", String.valueOf(d));
//
//                                    Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
//                                    i.putExtra("selected_date",str_selected_date);
//                                    i.putExtra("product",str_product);
//                                    i.putExtra("batch_id",str_batch_id);
//                                    startActivity(i);
//
//                                }
//                            });
//                        }
//                    }
//                    else {
////                        count--;
//                        count = 0;
//                    }
//                }
//
//                if (sumoftotalweights>=packing_total_qty)
//                {
//                    btnStop.setVisibility(View.GONE);
////                    handler.removeCallbacks(runnable);
//                    unfilled_quantity = String.valueOf(dremaingqty);
//                    actualQuantityFilled = String.valueOf(sumoftotalweights);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a =  txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                    ViewGroup viewGroup = findViewById(android.R.id.content);
//                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//                    builder.setView(dialogView);
//                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                    txt_status.setText("Batch Completed");
//
//
//                    final AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//
//                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            BatchStatus ="Complete";
//                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                            InsertIntoWeightReadingDb();
//                            Calculation();
//                            Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;
//
//                            SQLiteDatabase db = helper.getWritableDatabase();
//                            String currentDateandTime = dateformat.format(new Date());
//                            ContentValues cv=new ContentValues();
//
//                            cv.put(Constants.P_TAG_RESULT_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Complete");
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//
//                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                            Log.d("Success", String.valueOf(d));
//
//
//                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
//                            i.putExtra("selected_date",str_selected_date);
//                            i.putExtra("product",str_product);
//                            i.putExtra("batch_id",str_batch_id);
//                            startActivity(i);
//
//                        }
//                    });
//
//                }
//                else if (dremaingqty < dlc)
//                {
//                    btnStop.setVisibility(View.GONE);
////                    handler.removeCallbacks(runnable);
//                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
//                    actualQuantityFilled = String.valueOf(sumoftotalweights);
//                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
//                    String a =  txt_to_be_packed_pouches.getText().toString();
//                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
//                    unfilled_pouches = String.valueOf(strunfilled_pouches);
//
//                    ViewGroup viewGroup = findViewById(android.R.id.content);
//                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//                    builder.setView(dialogView);
//                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
//                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
//                    final AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//                    txt_status.setText("Insufficient remaining quantity");
//
//                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialog.dismiss();
//                            Status = "Abort";
//                            BatchStatus ="Abort";
//                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
//                            InsertIntoWeightReadingDb();
//                            Calculation();
//                            SQLiteDatabase db = helper.getWritableDatabase();
//                            String currentDateandTime = dateformat.format(new Date());
//                            ContentValues cv=new ContentValues();
//                            Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;
//
//                            cv.put(Constants.P_TAG_RESULT_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
//                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
//                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
//                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
//                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
//                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
//                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
//                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
//                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
//                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
//                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);
//
//                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
//                            Log.d("Success", String.valueOf(d));
//
//                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
//                            i.putExtra("selected_date",str_selected_date);
//                            i.putExtra("product",str_product);
//                            i.putExtra("batch_id",str_batch_id);
//                            startActivity(i);
//
//
//                        }
//                    });
//
//                }
//                else {
////                    PWeighReadingsModel model = new PWeighReadingsModel();
////                    String currentDateandTime = dateformat.format(new Date());
////                    readingcount++;
////                    model.setBatchId(str_batch_id);
////                    model.setWeight(arrayList1.get(index));
////                    model.setShift(str_shifts);
////                    model.setModifiedDate(str_selected_date);
////                    model.setActualGmPerPouch(arrayList1.get(index));
////                    model.setTargetGmPerPouch(gmperpouch);
////                    model.setPWeightReadingID(String.valueOf(readingcount));
////                    model.setCumilativeExcessGiveaway(String.valueOf(cumulativeexcessfiveaway));
////                    String weight = arrayList1.get(index);
////                    Double excess_giveaway = Double.parseDouble(weight)-Double.parseDouble(gmperpouch);
////                    model.setExcessGiveawayPerPouch(String.valueOf(excess_giveaway));
////                    model.setCreatedDate(currentDateandTime);
////
////                    Double cum_quantity_field = Double.parseDouble(arrayList1.get(index)) + previous_cumu_qty;
////                    model.setCumilativeQuantity(String.valueOf(cum_quantity_field));
////                    String pouches = txt_to_be_packed_pouches.getText().toString();
////                    model.setCumilativePouches(txt_to_be_packed_pouches.getText().toString());
////
////                    if (Double.parseDouble(arrayList1.get(index))>ducl || Double.parseDouble(arrayList1.get(index))<dlcl) {
////                        model.setStatus("Ignored");
////                    }
////                    else
////                    {
////                        model.setStatus("Okay");
////                    }
////                    pWeighReadingsModels.add(model);
////                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProductionWeightReadingActivity.this);
////                    rv_weight_reading.setLayoutManager(linearLayoutManager);
////                    productionWeightReadingAdapter = new ProductionWeightReadingAdapter(ProductionWeightReadingActivity.this,pWeighReadingsModels);
////                    rv_weight_reading.setAdapter(productionWeightReadingAdapter);
////                    productionWeightReadingAdapter.notifyDataSetChanged();
////
////                    index ++;
//                }
//            }
//            else
//            {
//                PWeighReadingsModel model = new PWeighReadingsModel();
//                readingcount++;
//                String currentDateandTime = dateformat.format(new Date());
//                model.setBatchId(str_batch_id);
//                model.setWeight(weightReading);
//                model.setShift(str_shifts);
//                model.setModifiedDate(str_selected_date);
//                model.setActualGmPerPouch(weightReading);
//                model.setPWeightReadingID(String.valueOf(readingcount));
//                model.setTargetGmPerPouch(gmperpouch);
//                model.setExcessGiveawayPerPouch("0");
//                model.setCumilativeExcessGiveaway("0");
//                model.setCumilativeQuantity(weightReading);
//                model.setCumilativePouches("1");
//                model.setCreatedDate(currentDateandTime);
//                if (Integer.parseInt(weightReading)>uclgm || Integer.parseInt(weightReading)<lclgm) {
//                    model.setStatus("Ignored");
//                }
//                else
//                {
//                    model.setStatus("Okay");
//                }
//
//                pWeighReadingsModels.add(model);
//                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProductionWeightReadingActivity.this);
//                rv_weight_reading.setLayoutManager(linearLayoutManager);
//                productionWeightReadingAdapter = new ProductionWeightReadingAdapter(ProductionWeightReadingActivity.this,pWeighReadingsModels);
//                rv_weight_reading.setAdapter(productionWeightReadingAdapter);
//                productionWeightReadingAdapter.notifyDataSetChanged();
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public void connectToMQttBroker(final boolean sendReadyToDas){
//        String clientId = MqttClient.generateClientId();
//        MemoryPersistence memPer = new MemoryPersistence();
//        final String tagId = current_tag_id;
//
//        client =  new MqttAndroidClient(this,
//                MQTT_BROKER,
//                clientId);
//        try {
//
//            client.setCallback(new ProductionWeightReadingActivity.MQttCallbackClass(this, client));
//            IMqttToken token = client.connect();
//            token.setActionCallback(new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    try {
//                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ProductionWeightReadingActivity.this);
//                        //String dataLoggerName = sharedPreferences.getString(GlobalConstants.ACTIVE_DATA_LOGGER_NAME, "");
//                        String topic = "/test/" + "DiA0AA01";
//                        int qos = 2;
//                        try {
//                            IMqttToken subToken = client.subscribe(topic, qos);
//                            subToken.setActionCallback(new IMqttActionListener() {
//                                @Override
//                                public void onSuccess(IMqttToken asyncActionToken) {
//                                    Toast.makeText(ProductionWeightReadingActivity.this, "Subscribed succesfully", Toast.LENGTH_SHORT).show();
//
//                                    try {
//                                        if(sendReadyToDas) {
//                                            sendReadyToDas1(tagId,dataloggerIp,dataloggerName);
//                                            // publishResult();
//
//                                        }
//                                    } catch (IOException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                                @Override
//                                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//                                    // set title
//                                    alertDialogBuilder.setTitle(R.string.app_name);
//                                    // set dialog message
//                                    alertDialogBuilder.setMessage("Could not connect to MQTT broker you cannot view readings, please check!!");
//                                    alertDialogBuilder.setCancelable(true);
//                                    alertDialogBuilder.setNeutralButton(android.R.string.ok,
//                                            new DialogInterface.OnClickListener() {
//                                                public void onClick(DialogInterface dialog, int id) {
//                                                    dialog.cancel();
//                                                }
//                                            });
//
//                                    AlertDialog alert11 = alertDialogBuilder.create();
//                                    alert11.show();
//
//                                }
//                            });
//                        } catch (Exception e) {
//                            Toast.makeText(ProductionWeightReadingActivity.this, "Exception occured" , Toast.LENGTH_LONG).show();
//                        }
//                    }catch(Exception ex){
//                        Toast.makeText(ProductionWeightReadingActivity.this, "Exception occured "+ ex.getMessage() , Toast.LENGTH_LONG).show();
//
//                    }
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    Toast.makeText(ProductionWeightReadingActivity.this, "Failed Exception occured" , Toast.LENGTH_LONG).show();
//
//                }
//            });
//
//        }catch(MqttException ex){
//            Toast.makeText(ProductionWeightReadingActivity.this, "Failed with outer exception , " + ex.getMessage(), Toast.LENGTH_SHORT).show();
//
//        }
//    }
//
//    private class MQttCallbackClass implements MqttCallback {
//        public  MqttAndroidClient client = null;
//        public Context actCtx = null;
//
//        public  MQttCallbackClass(Context ctx, MqttAndroidClient client){
//            actCtx = ctx;
//            clientObj = client;
//        }
//
//        public void connectionLost() throws MqttException {
//            boolean reconnected = false;
//            Toast.makeText(ProductionWeightReadingActivity.this, "Connection to MQTT broker lost", Toast.LENGTH_LONG).show();
//        }
//
//        @Override
//        public void connectionLost(Throwable cause) {
//            try {
//                connectionLost();
//            } catch (MqttException e) {
//                // We tried our best to reconnect
//                e.printStackTrace();
//            }
//        }
//
//        @Override
//        public void messageArrived(String topic, MqttMessage message) throws Exception {
//            if(message!= null && message.toString().equalsIgnoreCase("done")){
//                clientObj.disconnect();
//                isDone = true;
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
//                // set title
//                alertDialogBuilder.setTitle(R.string.app_name);
//                // set dialog message
//                alertDialogBuilder.setMessage("Weight Readings are done !!!");
//                alertDialogBuilder.setCancelable(true);
//                alertDialogBuilder.setNeutralButton(android.R.string.ok,
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.cancel();
//                            }
//                        });
//
//                AlertDialog alert11 = alertDialogBuilder.create();
//                alert11.show();
//
//            }else {
//
//                isDone = false;
//                SharedPreferences sharedPrefs = PreferenceManager
//                        .getDefaultSharedPreferences(ProductionWeightReadingActivity.this);
//                String targetWt = sharedPrefs.getString("TargetWt", "0");
//                String negwt = sharedPrefs.getString("NegWt", "No");
//                Toast.makeText(ProductionWeightReadingActivity.this, message.toString(), Toast.LENGTH_SHORT).show();
//
//
//                if(negwt != null && negwt.equalsIgnoreCase("yes")){
//                    double targetWtD = Double.parseDouble(targetWt);
//                    double reading = Double.parseDouble(message.toString());
//                    if(reading < targetWtD){
//                        //client.unsubscribe("/test/topic");
//                        Toast.makeText(actCtx, "Aborting test", Toast.LENGTH_LONG).show();
//                        if(clientObj != null && clientObj.isConnected()) {
//                            clientObj.disconnect();
//                        }
//                        // new DisplayResultsActivity.sendAbortToDas().execute("test", "Negative weight detected, aborting tests!!", "test");
//                    }
//                }
////                resultsLst.add(message.toString());
//                Log.d(TAG, "messageArrived: "+message.toString());
//
//                if (strUom.equals("gm"))
//                {
//                    gmperpouchcalculation(message.toString());
//                }
//                else
//                {
//                    kgperpouchCalculation(message.toString());
//                }
////                txtMessage.setText(resultsLst.toString());
////                gridViewObj.setAdapter(new DataGridViewAdapter(resultsLst, DisplayResultsActivity.this,false));
////                gridViewObj.invalidateViews();
//
//            }
//
//        }
//
//        @Override
//        public void deliveryComplete(IMqttDeliveryToken token) {
//            String tokenS = token.toString();
//            Log.d(TAG, "deliveryComplete: ");
//        }
//    }
//
//    public String sendReadyToDas1(String tagId, String dataLoggerIP, String dataLoggerName) throws IOException {
//        String tag = tagId;
//        HttpClient httpClient = new DefaultHttpClient();
//
//        String responseString="";
//
//        String url = "http://"+dataLoggerIP+":"+DATA_LOGGER_PORT+"/start?tagid="+tagId+"&dataLoggerIP="+dataLoggerIP+"&dataLoggerName="+dataLoggerName;
//        //String url = "http://"+dataLoggerIP+"/start?tagid="+tagId+"&dataLoggerIP="+dataLoggerIP+"&dataLoggerName="+dataLoggerName;
//        HttpResponse response1 = httpClient.execute(new HttpGet(url));
//        StatusLine statusLine = response1.getStatusLine();
//        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
//            response1.getEntity().writeTo(out);
//            responseString = out.toString();
//            Toast.makeText(ProductionWeightReadingActivity.this, responseString, Toast.LENGTH_SHORT).show();
//            out.close();
//        } else{
//            response1.getEntity().getContent().close();
//            throw new IOException(statusLine.getReasonPhrase());
//        }
//        return responseString;
//    }
//
//    private String abort() throws IOException {
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//        String tag = current_tag_id;
//        HttpClient httpClient = new DefaultHttpClient();
//
//        String responseString="";
//
//        String dataPort=Constants.DATA_LOGGER_PORT+"";
//        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
//        HttpResponse response1 = httpClient.execute(new HttpGet(url));
//        StatusLine statusLine = response1.getStatusLine();
//        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
//            response1.getEntity().writeTo(out);
//            responseString = out.toString();
//            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
//            out.close();
//        } else{
//            response1.getEntity().getContent().close();
//            throw new IOException(statusLine.getReasonPhrase());
//        }
//        return responseString;
//
//    }

    private static final String TAG = "ProductionWeightReading";

    private String dbIpAddress = "192.168.10.67";
    private String dbUserName = "test2";
    private String dbPassword = "test12345";
    private String dbDatabaseName = "decitalpool";
    private String serverport = "1433";
    private String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
    private int DATA_LOGGER_PORT = 8080;
    public static final String MQTT_BROKER = "tcp://192.168.10.67:1883";

    private ProductionWeightReadingAdapter productionWeightReadingAdapter;
    private ImageButton btnStop, img_btn_test_complete;
    RecyclerView rv_weight_reading;
    MqttAndroidClient client = null;
    MqttAndroidClient clientObj = null;;
    private ArrayList<Production_Reason_Table> productionReason_list = new ArrayList<>();
    private ArrayList<PWeighReadingsModel> pWeighReadingsModels = new ArrayList<>();
    private ArrayList<String> productionReasonList = new ArrayList<>();

    TextView txtTagid;
    Handler handler = new Handler();
    Runnable runnable;
    private double max_reading_value=0.0;
    private double mean_of_readings = 0.0;
    private double range = 0.0;
    private double average = 0.0;
    private double standard_deviation = 0.0;
    private double totals_sum =0.0;
    private double ducl = 0.0;
    private double dlcl = 0.0;
    private double dintkgvalue =0.0;

    DataBaseHelper helper;
    String Status, actualQuantityFilled,Actual_filled_pouches,unfilled_quantity,unfilled_pouches, Tolal_no_of_pouches,Total_qty,str_shifts,current_tag_id;
    String str_batch_id,str_positive_tolerance,str_negative_tolerance,str_product,str_selected_date,str_cancel_Reason,str_cancel_remark,gmperpouch;
    ImageButton img_btn_test_paused;
    int count =0,uclgm=0,lclgm=0,delay = 5000;
    SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DecimalFormat decimalformat = new DecimalFormat("0.000");
    private int index =0;
    private int readingcount=0;
    TextView txt_to_be_packed_quantity,txt_total_quantity,txt_report_product,txt_report_pro_type,txt_excess_give_Away,txt_no_of_pouches_of_pouches_packed,txt_to_be_packed_pouches;
    private String strUom,dataloggerMacId,dataloggerName,dataloggerIp;
    private String BatchStatus;
    private String dbName;
    private SQLiteDatabase db;
    private boolean isDone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production_weight_reading);
        initUi();
        dbName = AppPreferences.getDatabseName(this);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));


        masterProductionREjectionReson();

        connectToMQttBroker(true);


    }
    private void initUi()
    {
        btnStop = findViewById(R.id.btnStop);
        txtTagid = findViewById(R.id.txtTagid);
        img_btn_test_complete = findViewById(R.id.img_btn_test_complete);
        img_btn_test_paused = findViewById(R.id.img_btn_test_paused);

        btnStop.setOnClickListener(this);
        img_btn_test_paused.setOnClickListener(this);
        img_btn_test_paused.setOnClickListener(this);

        current_tag_id = getIntent().getStringExtra("current_tag_id");
        Total_qty = getIntent().getStringExtra("total_qty");
        gmperpouch = getIntent().getStringExtra("gmperpouch");
        Tolal_no_of_pouches = getIntent().getStringExtra("no_of_pouches");
        str_shifts = getIntent().getStringExtra("shift");
        str_batch_id = getIntent().getStringExtra("batch_id");
        str_positive_tolerance = getIntent().getStringExtra("positive_tolerance");
        str_negative_tolerance = getIntent().getStringExtra("negative_tolerance");
        str_selected_date = getIntent().getStringExtra("select_date");
        str_product = getIntent().getStringExtra("product");
        strUom = getIntent().getStringExtra("uom");
        dataloggerMacId = getIntent().getStringExtra("mac_id");
        dataloggerName = getIntent().getStringExtra("dataloggerName");
        dataloggerIp = getIntent().getStringExtra("dataloggerIp");
        String kgvalue = getIntent().getStringExtra("intkgvalue");
        if(kgvalue !=null)
        {
            dintkgvalue = Double.parseDouble(kgvalue);

        }
        String ucl = getIntent().getStringExtra("ucl");
        String lcl = getIntent().getStringExtra("lcl");

        if (strUom.equals("gm"))
        {
            if (ucl != null){
                uclgm =Integer.parseInt(ucl);
            }
            if (lcl != null){
                lclgm = Integer.parseInt(lcl);
            }
        }
        else
        {
            if (ucl != null){
                Double uc = Double.parseDouble(ucl);
                ducl = Double.parseDouble(decimalformat.format(uc));
            }
            if (lcl != null){
                Double lc = Double.parseDouble(lcl);
                dlcl = Double.parseDouble(decimalformat.format(lc));
            }
        }


        txtTagid.setText(str_batch_id);
        rv_weight_reading = (RecyclerView) findViewById(R.id.rv_weight_reading);
        txt_to_be_packed_quantity = findViewById(R.id.txt_to_be_packed_quantity);
        txt_total_quantity =  findViewById(R.id.txt_total_quantity);
        txt_report_product = findViewById(R.id.txt_report_product);
        txt_report_pro_type = findViewById(R.id.txt_report_pro_type);
        txt_excess_give_Away = findViewById(R.id.txt_excess_give_Away);
        txt_no_of_pouches_of_pouches_packed = findViewById(R.id.txt_no_of_pouches_of_pouches_packed);
        txt_to_be_packed_pouches = findViewById(R.id.txt_to_be_packed_pouches);

        txt_no_of_pouches_of_pouches_packed.setText(Tolal_no_of_pouches);
        Double dtotalqtty = Double.valueOf(Total_qty);
        txt_total_quantity.setText(decimalformat.format(dtotalqtty));
    }
    @Override
    protected void onResume() {
        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                handler.postDelayed(runnable, delay);



            }

        }, delay);

        super.onResume();
    }

    private void Calculation()
    {
        for (int cnt =1;cnt < pWeighReadingsModels.size(); cnt++) {
            String status = pWeighReadingsModels.get(cnt).getStatus();
            if (status.equals("Okay")) {
                if (Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight()) > max_reading_value) {
                    max_reading_value = Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
                }
            }
        }

        double min_reading_value = Double.parseDouble(pWeighReadingsModels.get(0).getWeight());
        for (int cnt =1; cnt < pWeighReadingsModels.size(); cnt++)
        {
            String status = pWeighReadingsModels.get(cnt).getStatus();
            if (status.equals("Okay")) {
                if (Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight()) < min_reading_value) {
                    min_reading_value = Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());
                }
            } }
        //mean calculation
        for (int cnt = 0 ; cnt< pWeighReadingsModels.size() ; cnt++)
        {
            String status = pWeighReadingsModels.get(cnt).getStatus();
            if (status.equals("Okay"))
            {
                totals_sum += Double.parseDouble(pWeighReadingsModels.get(cnt).getWeight());

            }

        }
        mean_of_readings = (max_reading_value - min_reading_value)/2 + min_reading_value;
        //range calculation
        range = max_reading_value - min_reading_value;

        //average_calculation
        average = mean_of_readings;


        double[] deviations = new double[pWeighReadingsModels.size()];
        //Taking the deviation of mean from each number
        for (int i = 0;i < deviations.length; i++)
        {
            String status = pWeighReadingsModels.get(i).getStatus();
            if (status.equals("Okay"))
            {
                deviations[i] = Double.parseDouble(pWeighReadingsModels.get(i).getWeight()) - average;
            }
        }
        //getting the squares of deviations
        double[] squares = new double[pWeighReadingsModels.size()];
        for (int i = 0;i<pWeighReadingsModels.size();i++)
        {
            String status = pWeighReadingsModels.get(i).getStatus();
            if (status.equals("Okay"))
            {
                for (int i1 = 0;i1 < squares.length; i1++)
                {
                    squares[i1] = deviations[i1] * deviations[i1];
                }
            }
        }
        //Adding all the squares
        double sum_of_squares = 0;
        for (int i =0;i<squares.length;i++)
        {
            sum_of_squares = sum_of_squares + squares[i];
        }
        double result = sum_of_squares / (pWeighReadingsModels.size() - 1);
        standard_deviation = Math.sqrt(result);

        SQLiteDatabase db = helper.getWritableDatabase();
        String currentDateandTime = dateformat.format(new Date());
        Double  totalexcessgiveaway = 0.000;

        if (strUom.equals("Kg"))
        {
            try {
                for(int i=0;i<pWeighReadingsModels.size();i++)
                {
                    String totalexcess = pWeighReadingsModels.get(i).getExcessGiveawayPerPouch();
                    totalexcessgiveaway += Double.parseDouble(totalexcess);
                }
                totalexcessgiveaway = Double.parseDouble(txt_excess_give_Away.getText().toString());

                Double doubleunfilledqty = Double.parseDouble(unfilled_quantity) * dintkgvalue;
                Double doublefilledqty = Double.parseDouble(actualQuantityFilled) * dintkgvalue;
                Double dmin = min_reading_value * dintkgvalue;
                Double dmax = max_reading_value * dintkgvalue;
                Double dmean = mean_of_readings * dintkgvalue;
                Double daverage = average*dintkgvalue;
                Double drange = range*dintkgvalue;
                Double dsd = standard_deviation*dintkgvalue;
                Double dtotalexcess = totalexcessgiveaway*dintkgvalue;
                ContentValues cv=new ContentValues();
                cv.put("ReportID",1);

                cv.put("OEMID","1234");
                cv.put("TagID",current_tag_id);
                cv.put("BatchID",str_batch_id);
                cv.put("MinWeight",Float.parseFloat(String.valueOf(dmin)));
                cv.put("MaxWeight",Float.parseFloat(String.valueOf(dmax)));
                cv.put("MeanWeight",Float.parseFloat(String.valueOf(dmean)));
                cv.put("Average",Float.parseFloat(String.valueOf(daverage)));
                cv.put("Range",Float.parseFloat(String.valueOf(drange)));
                cv.put("SD",Float.parseFloat(String.valueOf(dsd)));
                cv.put("UnfilledQuantity",Float.parseFloat(String.valueOf(doubleunfilledqty)));
                cv.put("FilledQuantity", Float.parseFloat(String.valueOf(doublefilledqty)));
                cv.put("UnfilledPouches",unfilled_pouches);
                cv.put("FilledPouches",Integer.parseInt(Actual_filled_pouches));
                cv.put("ExcessGiveawayPrice","");
                cv.put("ExcessGiveawayGm",decimalformat.format(dtotalexcess));
                cv.put("Reason",str_cancel_Reason);
                cv.put("Remark",str_cancel_remark);
                cv.put("IsActive",1);
                cv.put("CreatedDate",currentDateandTime);
                cv.put("ModifiedDate",currentDateandTime);
                cv.put("IsSync",0);

                long d=db.insert(Constants.PRODUCTION_WEIGHT_REPORT,null,cv);
                Log.d("Success", String.valueOf(d));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            try {
                for(int i=0;i<pWeighReadingsModels.size();i++)
                {
                    String totalexcess = pWeighReadingsModels.get(i).getExcessGiveawayPerPouch();
                    totalexcessgiveaway += Double.parseDouble(totalexcess);
                }
                totalexcessgiveaway = Double.parseDouble(txt_excess_give_Away.getText().toString());

                Double doubleunfilledqty = Double.valueOf(unfilled_quantity);
                Double doublefilledqty = Double.valueOf(actualQuantityFilled);
                ContentValues cv=new ContentValues();
                cv.put("ReportID",1);
                cv.put("OEMID",1234);
                cv.put("TagID",current_tag_id);
                cv.put("BatchID",str_batch_id);
                cv.put("MinWeight",Float.parseFloat(String.valueOf(min_reading_value)));
                cv.put("MaxWeight",Float.parseFloat(String.valueOf(max_reading_value)));
                cv.put("MeanWeight",Float.parseFloat(String.valueOf(mean_of_readings)));
                cv.put("Average",Float.parseFloat(String.valueOf(average)));
                cv.put("Range",Float.parseFloat(String.valueOf(range)));
                cv.put("SD",Float.parseFloat(String.valueOf(standard_deviation)));
                cv.put("UnfilledQuantity",Float.parseFloat(String.valueOf(doubleunfilledqty)));
                cv.put("FilledQuantity", Float.parseFloat(String.valueOf(doublefilledqty)));
                cv.put("UnfilledPouches",Integer.parseInt(unfilled_pouches));
                cv.put("FilledPouches",Integer.parseInt(Actual_filled_pouches));
                cv.put("ExcessGiveawayPrice","");
                cv.put("ExcessGiveawayGm",Float.parseFloat(String.valueOf(totalexcessgiveaway)));
                cv.put("Reason",str_cancel_Reason);
                cv.put("Remark",str_cancel_remark);
                cv.put("IsActive",1);
                cv.put("CreatedDate",currentDateandTime);
                cv.put("ModifiedDate",currentDateandTime);
                cv.put("IsSync",0);

                long d=db.insert(Constants.PRODUCTION_WEIGHT_REPORT,null,cv);
                Log.d("Success", String.valueOf(d));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }



    }

    private void masterProductionREjectionReson()
    {
        Cursor masterRejectionReason = helper.getMasterProductionRejectionReason();
        while (masterRejectionReason.moveToNext()){
            String reasonId = masterRejectionReason.getString(0);
            String reasonDesc = masterRejectionReason.getString(1);
            productionReasonList.add(reasonDesc);

            Production_Reason_Table model = new Production_Reason_Table();
            model.setReasonID(reasonId);
            model.setReasonDesc(reasonDesc);

            productionReason_list.add(model);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }
    }

    public void InsertIntoWeightReadingDb() {
        SQLiteDatabase db = helper.getWritableDatabase();
        String currentDateandTime = dateformat.format(new Date());
        try {
            if (strUom.equals("Kg"))
            {
                for (int i = 0; i < pWeighReadingsModels.size(); i++) {

                    String weight = pWeighReadingsModels.get(i).getWeight();
                    String status = pWeighReadingsModels.get(i).getStatus();
                    Double dweight = Double.parseDouble(weight) * dintkgvalue;



                    Double excess_giveaway = dweight-Double.parseDouble(gmperpouch) * dintkgvalue;
                    Double dbweight = Double.parseDouble(pWeighReadingsModels.get(i).getWeight()) * dintkgvalue;
                    Double dcumulativeexcess = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeExcessGiveaway()) * dintkgvalue;
                    Double dactualgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getActualGmPerPouch()) * dintkgvalue;
                    Double dtargetgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getTargetGmPerPouch()) * dintkgvalue;
                    Double dcumulativequantity = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeQuantity()) * dintkgvalue;

                    ContentValues cv = new ContentValues();
                    cv.put(Constants.P_WEIGHT_READING_ID,i+1);
                    cv.put(Constants.P_WEIGHT_READING_OEM_ID,1234);
                    cv.put(Constants.P_WEIGHT_READING_MAC_ID,dataloggerMacId);
                    cv.put(Constants.P_WEIGHT_READING_LOGGER_ID,dataloggerName);
                    cv.put(Constants.P_WEIGHT_READING_BATCHID, str_batch_id);

                    cv.put(Constants.P_WEIGHT_READING_WEIGHT_ID, i+1);
                    cv.put(Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME, current_tag_id);
                    cv.put(Constants.P_WEIGHT_READING_SHIFT, str_shifts);
                    cv.put(Constants.P_WEIGHT_READING_WEIGHT, Float.parseFloat(String.valueOf(dbweight)));
                    cv.put(Constants.P_WEIGHT_READING_TARGETGMPERPOUCH, Float.parseFloat(String.valueOf(dtargetgmperpouch)));
                    cv.put(Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH, Float.parseFloat(String.valueOf(dactualgmperpouch)));
                    if (status.equals("Okay"))
                    {
                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, Float.parseFloat(String.valueOf(excess_giveaway)));
                    }
                    else
                    {
                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, 0.0);
                    }

                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY,Float.parseFloat(String.valueOf(dcumulativeexcess)));
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY,Float.parseFloat(String.valueOf(dcumulativequantity)));
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES,Integer.parseInt(pWeighReadingsModels.get(i).getCumilativePouches()));
                    cv.put(Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG, 0.0);
                    cv.put(Constants.P_WEIGHT_READING_UCL, 0.0);
                    cv.put(Constants.P_WEIGHT__READING_LCL,0.0);
                    cv.put(Constants.P_WEIGHT_READING_STATUS, pWeighReadingsModels.get(i).getStatus());
                    cv.put(Constants.P_WEIGHT_READING_CREATED_DATE,currentDateandTime);
                    cv.put(Constants.P_WEIGHT_READING_CREATED_BY,"Pooja");
                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_BY,"Pooja");
                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_DATE, currentDateandTime);
                    cv.put(Constants.P_WEIGHT_READING_IS_SYNC,0);

                    long d = db.insert(Constants.P_WEIGHT_READING_TABLE, null, cv);
                    Log.d("Success", String.valueOf(d));
                    if (d == -1){
                        Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                        //wakeup();
                    } }
            }
            else
            {
                for (int i = 0; i < pWeighReadingsModels.size(); i++) {

                    String weight = pWeighReadingsModels.get(i).getWeight();
                    String status = pWeighReadingsModels.get(i).getStatus();
                    Double dweight = Double.parseDouble(weight);



                    Double excess_giveaway = dweight-Double.parseDouble(gmperpouch);
                    Double dbweight = Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
                    Double dcumulativeexcess = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeExcessGiveaway());
                    Double dactualgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getActualGmPerPouch());
                    Double dtargetgmperpouch = Double.parseDouble(pWeighReadingsModels.get(i).getTargetGmPerPouch());
                    Double dcumulativequantity = Double.parseDouble(pWeighReadingsModels.get(i).getCumilativeQuantity());

                    ContentValues cv = new ContentValues();
                    cv.put(Constants.P_WEIGHT_READING_ID,i+1);
                    cv.put(Constants.P_WEIGHT_READING_OEM_ID,1234);
                    cv.put(Constants.P_WEIGHT_READING_MAC_ID,dataloggerMacId);
                    cv.put(Constants.P_WEIGHT_READING_LOGGER_ID,dataloggerName);
                    cv.put(Constants.P_WEIGHT_READING_WEIGHT_ID, i+1);
                    cv.put(Constants.P_WEIGHT_READING_BATCHID, str_batch_id);
                    cv.put(Constants.P_WEIGHT_READING_PRODUCTION_TAG_NAME, current_tag_id);
                    cv.put(Constants.P_WEIGHT_READING_SHIFT, str_shifts);
                    cv.put(Constants.P_WEIGHT_READING_UCL, 0.0);
                    cv.put(Constants.P_WEIGHT__READING_LCL,0.0);
                    cv.put(Constants.P_WEIGHT_READING_WEIGHT, decimalformat.format(dbweight));
                    cv.put(Constants.P_WEIGHT_READING_TARGETGMPERPOUCH, Float.parseFloat(String.valueOf(dtargetgmperpouch)));
                    cv.put(Constants.P_WEIGHT_READING_ACTUALGMPERPOUCH, Float.parseFloat(String.valueOf(dactualgmperpouch)));
                    if (status.equals("Okay"))
                    {
                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, Float.parseFloat(String.valueOf(excess_giveaway)));
                    }
                    else
                    {
                        cv.put(Constants.P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH, 0.0);
                    }
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY,Float.parseFloat(String.valueOf(dcumulativeexcess)));
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVEQUANTITY,Float.parseFloat(String.valueOf(dcumulativequantity)));
                    cv.put(Constants.P_WEIGHT_READING_CUMILATIVE_POUCHES,Integer.parseInt(pWeighReadingsModels.get(i).getCumilativePouches()));
                    cv.put(Constants.P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG, 0.0);
                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_DATE, currentDateandTime);
                    cv.put(Constants.P_WEIGHT_READING_CREATED_DATE,currentDateandTime);
                    cv.put(Constants.P_WEIGHT_READING_STATUS, pWeighReadingsModels.get(i).getStatus());
                    cv.put(Constants.P_WEIGHT_READING_MODIFIED_BY,"Pooja");
                    cv.put(Constants.P_WEIGHT_READING_CREATED_BY,"Pooja");
                    cv.put(Constants.P_WEIGHT_READING_IS_SYNC,0);

                    long d = db.insert(Constants.P_WEIGHT_READING_TABLE, null, cv);
                    Log.d("Success", String.valueOf(d));
                    if (d == -1){
                        Toast.makeText(this, "Data not Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(this, "Data Inserted To Sqlite", Toast.LENGTH_SHORT).show();
                        //wakeup();
                    } }

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override

    public void onClick(View v) {
        if (v.getId()==btnStop.getId())
        {
            final ArrayAdapter<String> productionReasonlist = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, productionReasonList);

//            handler.removeCallbacks(runnable);

            ViewGroup viewGroup = findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.cancel_readings_layout, viewGroup, false);
            AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
            builder.setView(dialogView);
            Button btn_cancel_qty = dialogView.findViewById(R.id.btn_cancel_quantity);
            Button btn_ok_qty = dialogView.findViewById(R.id.btn_ok_quantity);
            final EditText ed_cancel_reason = dialogView.findViewById(R.id.ed_reason);

            final Spinner spin_rejection_Reason = dialogView.findViewById(R.id.spin_reason);
            productionReasonlist.setDropDownViewResource(R.layout.item_spinner_latyout);
            spin_rejection_Reason.setAdapter(productionReasonlist);

            spin_rejection_Reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    str_cancel_Reason = spin_rejection_Reason.getSelectedItem().toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            final AlertDialog alertDialog = builder.create();
            alertDialog.show();

            btn_ok_qty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    try {
                        abort();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    InsertIntoWeightReadingDb();
                    str_cancel_remark = ed_cancel_reason.getText().toString();
                    Double  intunfilledqty = Double.parseDouble(txt_to_be_packed_quantity.getText().toString().trim());
                    Double totalunfilledqty = Double.parseDouble(Total_qty)-intunfilledqty;
                    unfilled_quantity = String.valueOf(totalunfilledqty);

                    actualQuantityFilled = String.valueOf(intunfilledqty);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);
                    BatchStatus ="Abort";
                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                    Calculation();
                    SQLiteDatabase db = helper.getWritableDatabase();
                    String currentDateandTime = dateformat.format(new Date());
                    Double dtotalquantity = 0.0;

                    if(strUom.equals("gm"))
                    {
                        dtotalquantity = Double.parseDouble(Total_qty);

                    }
                    else
                    {
                        dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;

                    }

                    ContentValues cv=new ContentValues();
                    cv.put(Constants.P_TAG_RESULT_ID,1);
                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, decimalformat.format(dtotalquantity));
                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);

                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                    Log.d("Success", String.valueOf(d));

                    Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                    i.putExtra("selected_date",str_selected_date);
                    i.putExtra("product",str_product);
                    i.putExtra("batch_id",str_batch_id);
                    startActivity(i);

                }
            });

            btn_cancel_qty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    try {
                        abort();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    InsertIntoWeightReadingDb();
                    str_cancel_remark = ed_cancel_reason.getText().toString();
                    Double  intunfilledqty = Double.parseDouble(txt_to_be_packed_quantity.getText().toString().trim());
                    Double totalunfilledqty = Double.parseDouble(Total_qty)-intunfilledqty;
                    unfilled_quantity = String.valueOf(totalunfilledqty);

                    actualQuantityFilled = String.valueOf(intunfilledqty);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);
                    BatchStatus ="Abort";
                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                    Calculation();
                    SQLiteDatabase db = helper.getWritableDatabase();
                    String currentDateandTime = dateformat.format(new Date());
                    ContentValues cv=new ContentValues();
                    Double dtotalquantity = 0.0;

                    if(strUom.equals("gm"))
                    {
                        dtotalquantity = Double.parseDouble(Total_qty);

                    }
                    else
                    {
                        dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;

                    }

                    cv.put(Constants.P_TAG_RESULT_ID,1);
                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT, decimalformat.format(dtotalquantity));
                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);

                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                    Log.d("Success", String.valueOf(d));

                    Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                    i.putExtra("selected_date",str_selected_date);
                    i.putExtra("product",str_product);
                    i.putExtra("batch_id",str_batch_id);
                    startActivity(i);

                }
            });


        }

        else if(v.getId()==img_btn_test_paused.getId())
        {
//            handler.removeCallbacks(runnable);
            Toast.makeText(ProductionWeightReadingActivity.this,"Batch has been paused",Toast.LENGTH_SHORT).show();

            ViewGroup viewGroup = findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
            AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
            builder.setView(dialogView);
            TextView txt_status = dialogView.findViewById(R.id.txt_status);
            txt_status.setText("Batch Paused");
            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();

            btn_test_complete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    Double  intunfilledqty = Double.parseDouble(txt_to_be_packed_quantity.getText().toString().trim());
                    Double totalunfilledqty = Double.parseDouble(Total_qty)-intunfilledqty;
                    unfilled_quantity = String.valueOf(totalunfilledqty);

                    actualQuantityFilled = String.valueOf(intunfilledqty);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);
                    BatchStatus ="Paused";
                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                    InsertIntoWeightReadingDb();
                    Calculation();
                    SQLiteDatabase db = helper.getWritableDatabase();
                    String currentDateandTime = dateformat.format(new Date());
                    ContentValues cv=new ContentValues();
                    Double dtotalquantity = 0.0;

                    if(strUom.equals("gm"))
                    {
                        dtotalquantity = Double.parseDouble(Total_qty);

                    }
                    else
                    {
                        dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;

                    }

                    cv.put(Constants.P_TAG_RESULT_ID,1);
                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Paused");
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);

                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                    Log.d("Success", String.valueOf(d));

                    Intent i = new Intent(ProductionWeightReadingActivity.this, ProductionMenuActivity.class);
                    i.putExtra("selected_date",str_selected_date);
                    i.putExtra("product",str_product);
                    i.putExtra("batch_id",str_batch_id);
                    startActivity(i);

                }
            });
        }
    }

    public void gmperpouchcalculation(String weightReading)
    {
        int sumoftotalweights = 0;
        int packing_total_qty = 0;
        int sumoftotalpackedpouches = 0;
        int sumoftotalpackedquantity =0;
        double previous_cumu_qty = 0.0;
        double totalexcessgiveaway = 0.0;
        double excessgiveawayperpouch = 0.0;
        int remaining_pouch_qty=0;
        double cumulativeexcessfiveaway = 0.0;


        try {
            if (pWeighReadingsModels.size()!=0)
            {
                int pouches_count = 1;
                PWeighReadingsModel model = new PWeighReadingsModel();
                String currentDateandTime = dateformat.format(new Date());
                readingcount++;
                model.setBatchId(str_batch_id);
                model.setWeight(weightReading);
                model.setShift(str_shifts);
                model.setModifiedDate(str_selected_date);
                model.setActualGmPerPouch(weightReading);
                model.setTargetGmPerPouch(gmperpouch);
                model.setPWeightReadingID(String.valueOf(readingcount));
                model.setCumilativeExcessGiveaway(String.valueOf(cumulativeexcessfiveaway));
                String weight1 = weightReading;
                double excess_giveaway = Double.parseDouble(weight1)-Double.parseDouble(gmperpouch);
                model.setExcessGiveawayPerPouch(String.valueOf(excess_giveaway));
                model.setCreatedDate(currentDateandTime);

                double cum_quantity_field = Double.parseDouble(weightReading) + previous_cumu_qty;
                model.setCumilativeQuantity(String.valueOf(cum_quantity_field));
                String pouches = txt_to_be_packed_pouches.getText().toString();
                model.setCumilativePouches(txt_to_be_packed_pouches.getText().toString());

                if (Double.parseDouble(weightReading)>uclgm || Double.parseDouble(weightReading)<lclgm) {
                    model.setStatus("Ignored");
                }
                else
                {
                    model.setStatus("Okay");
                }
                pWeighReadingsModels.add(model);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProductionWeightReadingActivity.this);
                rv_weight_reading.setLayoutManager(linearLayoutManager);
                productionWeightReadingAdapter = new ProductionWeightReadingAdapter(ProductionWeightReadingActivity.this,pWeighReadingsModels);
                rv_weight_reading.setAdapter(productionWeightReadingAdapter);
                productionWeightReadingAdapter.notifyDataSetChanged();

                index ++;
                for(int i = 0 ; i < pWeighReadingsModels.size(); i++) {

                    String status = pWeighReadingsModels.get(i).getStatus();

                    if (status.equals("Okay")) {
                        String weight =pWeighReadingsModels.get(i).getWeight();
                        sumoftotalweights += Double.parseDouble(weight);

                        sumoftotalpackedquantity += Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
                        sumoftotalpackedpouches = pouches_count++;
                        excessgiveawayperpouch = Double.parseDouble(weight) - Double.parseDouble(gmperpouch);

                        String strprevious_cumu_qty = pWeighReadingsModels.get(i).getCumilativeQuantity();
                        previous_cumu_qty = Double.parseDouble(strprevious_cumu_qty);
                        totalexcessgiveaway += excessgiveawayperpouch;

                        String strtotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
                        Double dbtotalexcessgiveaway = Double.valueOf(strtotalexcessgiveaway);

                        String strexcessgiveaway = String.valueOf(excessgiveawayperpouch);
                        Double dbexcess = Double.valueOf(strexcessgiveaway);

                        txt_excess_give_Away.setText(decimalformat.format(dbtotalexcessgiveaway));

                        if (excessgiveawayperpouch > Double.parseDouble(gmperpouch)) {
                            txt_excess_give_Away.setText(decimalformat.format(dbexcess));
                        }

                        if (i!=0) {
                            int pos = i-1;
                            String previous_cum= pWeighReadingsModels.get(pos).getCumilativeExcessGiveaway();
                            cumulativeexcessfiveaway = excessgiveawayperpouch + Double.parseDouble(previous_cum);

                        }
                    }
                }

                packing_total_qty = Integer.parseInt(Total_qty) - 1;
                String strtobepackedqty = String.valueOf(sumoftotalpackedquantity);
                Double dtoobepackedqty = Double.valueOf(strtobepackedqty);

                txt_to_be_packed_quantity.setText(decimalformat.format(dtoobepackedqty));
                txt_to_be_packed_pouches.setText(sumoftotalpackedpouches + "");

                //this quantity used for checking the remaining quantity for packing
                remaining_pouch_qty = Integer.parseInt(Total_qty)- sumoftotalweights;

                for (int i =0 ;i<pWeighReadingsModels.size();i++)
                {
                    String status = pWeighReadingsModels.get(i).getStatus();
                    if (status.equals("Ignored"))
                    {
                        count++;
                        if (count==3) {
//                            handler.removeCallbacks(runnable);
                            abort();
                            ViewGroup viewGroup = findViewById(android.R.id.content);
                            View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                            builder.setView(dialogView);
                            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                            TextView txt_status = dialogView.findViewById(R.id.txt_status);
                            final AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                            txt_status.setText("Batch Aborted");
                            unfilled_quantity = String.valueOf(remaining_pouch_qty);
                            actualQuantityFilled = String.valueOf(sumoftotalweights);
                            Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                            String a =  txt_to_be_packed_pouches.getText().toString();
                            int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                            unfilled_pouches = String.valueOf(strunfilled_pouches);


//                            handler.removeCallbacks(runnable);

                            btn_test_complete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    Status ="Abort";
                                    BatchStatus ="Abort";
                                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                                    InsertIntoWeightReadingDb();
                                    Calculation();
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    String currentDateandTime = dateformat.format(new Date());
                                    ContentValues cv=new ContentValues();

                                    Double dtotalquantity = Double.parseDouble(Total_qty);

                                    cv.put(Constants.P_TAG_RESULT_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);

                                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                                    Log.d("Success", String.valueOf(d));

                                    Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                                    i.putExtra("selected_date",str_selected_date);
                                    i.putExtra("product",str_product);
                                    i.putExtra("batch_id",str_batch_id);
                                    startActivity(i);

                                }
                            });
                        }
                    }
                    else {
//                        count--;
                        count = 0;
                    }
                }

                if (sumoftotalweights>=packing_total_qty)
                {
                    btnStop.setVisibility(View.GONE);
//                    handler.removeCallbacks(runnable);
                    abort();
                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    txt_status.setText("Batch Completed");


                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            InsertIntoWeightReadingDb();
                            BatchStatus ="Complete";
                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            Calculation();
                            SQLiteDatabase db = helper.getWritableDatabase();
                            String currentDateandTime = dateformat.format(new Date());
                            ContentValues cv=new ContentValues();
                            Double dtotalquantity = Double.parseDouble(Total_qty);



                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Complete");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);

                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                            Log.d("Success", String.valueOf(d));
                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date",str_selected_date);
                            i.putExtra("product",str_product);
                            i.putExtra("batch_id",str_batch_id);
                            startActivity(i);

                        }
                    });

                }
                else if (remaining_pouch_qty <lclgm)
                {
                    btnStop.setVisibility(View.GONE);
//                    handler.removeCallbacks(runnable);
                    abort();

                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    txt_status.setText("Insufficient remaining quantity");
                    abort();

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            Status = "Abort";
                            BatchStatus ="Abort";

                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            InsertIntoWeightReadingDb();
                            Calculation();
                            SQLiteDatabase db = helper.getWritableDatabase();
                            String currentDateandTime = dateformat.format(new Date());
                            ContentValues cv=new ContentValues();
                            Double dtotalquantity = Double.parseDouble(Total_qty);

                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);


                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                            Log.d("Success", String.valueOf(d));

                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date",str_selected_date);
                            i.putExtra("product",str_product);
                            i.putExtra("batch_id",str_batch_id);
                            startActivity(i);


                        }
                    });

                }
                else {

                }
            }
            else
            {
                PWeighReadingsModel model = new PWeighReadingsModel();
                readingcount++;
                String currentDateandTime = dateformat.format(new Date());
                model.setBatchId(str_batch_id);
                model.setWeight(weightReading);
                model.setShift(str_shifts);
                model.setModifiedDate(str_selected_date);
                model.setActualGmPerPouch(weightReading);
                model.setPWeightReadingID(String.valueOf(readingcount));
                model.setTargetGmPerPouch(gmperpouch);
                double excessgiveaway = Double.parseDouble(weightReading)-Double.parseDouble(gmperpouch);
                model.setExcessGiveawayPerPouch(String.valueOf(excessgiveawayperpouch));
                model.setCumilativeExcessGiveaway(String.valueOf(excessgiveawayperpouch));
                model.setCumilativeQuantity(weightReading);
                model.setCumilativePouches("1");
                model.setCreatedDate(currentDateandTime);

                if (Double.parseDouble(weightReading)>uclgm || Double.parseDouble(weightReading)<lclgm) {
                    model.setStatus("Ignored");
                }
                else
                {
                    model.setStatus("Okay");
                }
                pWeighReadingsModels.add(model);

                for(int i = 0 ; i < pWeighReadingsModels.size(); i++) {
                    int pouches_count = 1;

                    String status = pWeighReadingsModels.get(i).getStatus();

                    if (status.equals("Okay")) {
                        String weight =pWeighReadingsModels.get(i).getWeight();
                        sumoftotalweights += Double.parseDouble(weight);

                        sumoftotalpackedquantity += Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
                        sumoftotalpackedpouches = pouches_count++;
                        excessgiveawayperpouch = Double.parseDouble(weight) - Double.parseDouble(gmperpouch);

                        String strprevious_cumu_qty = pWeighReadingsModels.get(i).getCumilativeQuantity();
                        previous_cumu_qty = Double.parseDouble(strprevious_cumu_qty);
                        totalexcessgiveaway += excessgiveawayperpouch;

                        String strtotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
                        Double dbtotalexcessgiveaway = Double.valueOf(strtotalexcessgiveaway);

                        String strexcessgiveaway = String.valueOf(excessgiveawayperpouch);
                        Double dbexcess = Double.valueOf(strexcessgiveaway);

                        txt_excess_give_Away.setText(decimalformat.format(dbtotalexcessgiveaway));

                        if (excessgiveawayperpouch > Double.parseDouble(gmperpouch)) {
                            txt_excess_give_Away.setText(decimalformat.format(dbexcess));
                        }

                        if (i!=0) {
                            int pos = i-1;
                            String previous_cum= pWeighReadingsModels.get(pos).getCumilativeExcessGiveaway();
                            cumulativeexcessfiveaway = excessgiveawayperpouch + Double.parseDouble(previous_cum);

                        }
                    }
                }

                packing_total_qty = Integer.parseInt(Total_qty) - 1;

                String strtobepackedqty = String.valueOf(sumoftotalpackedquantity);
                Double dtoobepackedqty = Double.valueOf(strtobepackedqty);

                txt_to_be_packed_quantity.setText(decimalformat.format(dtoobepackedqty));
                txt_to_be_packed_pouches.setText(sumoftotalpackedpouches + "");

                //this quantity used for checking the remaining quantity for packing
                remaining_pouch_qty = Integer.parseInt(Total_qty)- sumoftotalweights;

                for (int i =0 ;i<pWeighReadingsModels.size();i++)
                {
                    String status = pWeighReadingsModels.get(i).getStatus();
                    if (status.equals("Ignored"))
                    {
                        count++;
                        if (count==3) {
//                            handler.removeCallbacks(runnable);
                            abort();
                            ViewGroup viewGroup = findViewById(android.R.id.content);
                            View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                            builder.setView(dialogView);
                            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                            TextView txt_status = dialogView.findViewById(R.id.txt_status);
                            final AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                            txt_status.setText("Batch Aborted");
                            unfilled_quantity = String.valueOf(remaining_pouch_qty);
                            actualQuantityFilled = String.valueOf(sumoftotalweights);
                            Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                            String a =  txt_to_be_packed_pouches.getText().toString();
                            int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                            unfilled_pouches = String.valueOf(strunfilled_pouches);

//                            handler.removeCallbacks(runnable);

                            btn_test_complete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    Status ="Abort";
                                    BatchStatus ="Abort";
                                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                                    InsertIntoWeightReadingDb();
                                    Calculation();
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    String currentDateandTime = dateformat.format(new Date());
                                    ContentValues cv=new ContentValues();

                                    Double dtotalquantity = Double.parseDouble(Total_qty);

                                    cv.put(Constants.P_TAG_RESULT_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);

                                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                                    Log.d("Success", String.valueOf(d));

                                    Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                                    i.putExtra("selected_date",str_selected_date);
                                    i.putExtra("product",str_product);
                                    i.putExtra("batch_id",str_batch_id);
                                    startActivity(i);

                                }
                            });
                        }
                    }
                    else {
//                        count--;
                        count = 0;
                    }
                }

                if (sumoftotalweights>=packing_total_qty)
                {
                    btnStop.setVisibility(View.GONE);
//                    handler.removeCallbacks(runnable);
                    abort();
                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    txt_status.setText("Batch Completed");


                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            InsertIntoWeightReadingDb();
                            BatchStatus ="Complete";
                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            Calculation();
                            SQLiteDatabase db = helper.getWritableDatabase();
                            String currentDateandTime = dateformat.format(new Date());
                            ContentValues cv=new ContentValues();
                            Double dtotalquantity = Double.parseDouble(Total_qty);



                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Complete");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);


                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                            Log.d("Success", String.valueOf(d));


                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date",str_selected_date);
                            i.putExtra("product",str_product);
                            i.putExtra("batch_id",str_batch_id);
                            startActivity(i);

                        }
                    });

                }
                else if (remaining_pouch_qty <lclgm)
                {
                    btnStop.setVisibility(View.GONE);
//                    handler.removeCallbacks(runnable);
                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    txt_status.setText("Insufficient remaining quantity");

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            Status = "Abort";
                            BatchStatus ="Abort";
                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            InsertIntoWeightReadingDb();
                            Calculation();
                            SQLiteDatabase db = helper.getWritableDatabase();
                            String currentDateandTime = dateformat.format(new Date());
                            ContentValues cv=new ContentValues();
                            Double dtotalquantity = Double.parseDouble(Total_qty);

                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);


                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                            Log.d("Success", String.valueOf(d));

                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date",str_selected_date);
                            i.putExtra("product",str_product);
                            i.putExtra("batch_id",str_batch_id);
                            startActivity(i);


                        }
                    });

                }
                else {

                    /*model.setBatchId(str_batch_id);
                    model.setWeight(weightReading);
                    model.setShift(str_shifts);
                    model.setModifiedDate(str_selected_date);
                    model.setActualGmPerPouch(weightReading);
                    model.setTargetGmPerPouch(gmperpouch);
                    model.setPWeightReadingID(String.valueOf(readingcount));
                    model.setCumilativeExcessGiveaway(String.valueOf(cumulativeexcessfiveaway));
                    String weight = weightReading;
                    double excess_giveaway = Double.parseDouble(weight)-Double.parseDouble(gmperpouch);
                    model.setExcessGiveawayPerPouch(String.valueOf(excess_giveaway));
                    model.setCreatedDate(currentDateandTime);*/

                    double cum_quantity_field = Double.parseDouble(weightReading) + previous_cumu_qty;
                    model.setCumilativeQuantity(String.valueOf(cum_quantity_field));
                    String pouches = txt_to_be_packed_pouches.getText().toString();
                    model.setCumilativePouches(txt_to_be_packed_pouches.getText().toString());

                    if (Double.parseDouble(weightReading)>uclgm || Double.parseDouble(weightReading)<lclgm) {
                        model.setStatus("Ignored");
                    }
                    else
                    {
                        model.setStatus("Okay");
                    }
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProductionWeightReadingActivity.this);
                    rv_weight_reading.setLayoutManager(linearLayoutManager);
                    productionWeightReadingAdapter = new ProductionWeightReadingAdapter(ProductionWeightReadingActivity.this,pWeighReadingsModels);
                    rv_weight_reading.setAdapter(productionWeightReadingAdapter);
                    productionWeightReadingAdapter.notifyDataSetChanged();

                    index ++;
                }



            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    public void kgperpouchCalculation(String weightReading)
    {
        Double sumoftotalweights = 0.0;
        Double packing_total_qty = 0.0;
        int sumoftotalpackedpouches = 0;
        Double sumoftotalpackedquantity =0.0;
        Double previous_cumu_qty = 0.0;
        Double totalexcessgiveaway = 0.0;
        Double excessgiveawayperpouch = 0.0;
        Double remaining_pouch_qty=0.0;
        Double cumulativeexcessfiveaway = 0.0;

        try {
            if (pWeighReadingsModels.size()!=0)
            {
                int pouches_count = 1;
                PWeighReadingsModel model = new PWeighReadingsModel();
                readingcount++;
                String currentDateandTime = dateformat.format(new Date());
                model.setBatchId(str_batch_id);
                model.setWeight(weightReading);
                model.setShift(str_shifts);
                model.setModifiedDate(str_selected_date);
                model.setActualGmPerPouch(weightReading);
                model.setPWeightReadingID(String.valueOf(readingcount));
                model.setTargetGmPerPouch(gmperpouch);
                String weight1 = weightReading;

                double excess_giveaway = Double.parseDouble(weight1)-Double.parseDouble(gmperpouch);
                model.setExcessGiveawayPerPouch(String.valueOf(excess_giveaway));

                model.setCumilativeExcessGiveaway("0");
                model.setCumilativeQuantity(weightReading);
                model.setCumilativePouches("1");
                model.setCreatedDate(currentDateandTime);
                if (Integer.parseInt(weightReading)>uclgm || Integer.parseInt(weightReading)<lclgm) {
                    model.setStatus("Ignored");
                }
                else
                {
                    model.setStatus("Okay");
                }

                pWeighReadingsModels.add(model);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProductionWeightReadingActivity.this);
                rv_weight_reading.setLayoutManager(linearLayoutManager);
                productionWeightReadingAdapter = new ProductionWeightReadingAdapter(ProductionWeightReadingActivity.this,pWeighReadingsModels);
                rv_weight_reading.setAdapter(productionWeightReadingAdapter);
                productionWeightReadingAdapter.notifyDataSetChanged();

                for(int i = 0 ; i < pWeighReadingsModels.size(); i++) {
                    String status = pWeighReadingsModels.get(i).getStatus();

                    if (status.equals("Okay")) {
                        String weight =pWeighReadingsModels.get(i).getWeight();
                        sumoftotalweights += Double.parseDouble(weight);

                        sumoftotalpackedquantity += Double.parseDouble(pWeighReadingsModels.get(i).getWeight());
                        sumoftotalpackedpouches = pouches_count++;
                        excessgiveawayperpouch = Double.parseDouble(weight) - Double.parseDouble(gmperpouch);

                        String strprevious_cumu_qty = pWeighReadingsModels.get(i).getCumilativeQuantity();
                        previous_cumu_qty = Double.parseDouble(strprevious_cumu_qty);
                        totalexcessgiveaway += excessgiveawayperpouch;

                        String strtotalexcessgiveaway = String.valueOf(totalexcessgiveaway);
                        Double dbtotalexcessgiveaway = Double.valueOf(strtotalexcessgiveaway);

                        String strexcessgiveaway = String.valueOf(excessgiveawayperpouch);
                        Double dbexcess = Double.valueOf(strexcessgiveaway);

                        txt_excess_give_Away.setText(decimalformat.format(dbtotalexcessgiveaway));

                        if (excessgiveawayperpouch > Double.parseDouble(gmperpouch)) {
                            txt_excess_give_Away.setText(decimalformat.format(dbexcess));
                        }

                        if (i!=0) {
                            int pos = i-1;
                            String previous_cum= pWeighReadingsModels.get(pos).getCumilativeExcessGiveaway();
                            cumulativeexcessfiveaway = excessgiveawayperpouch + Double.parseDouble(previous_cum);

                        }
                    }
                }

                Double dvalue = 1.0/dintkgvalue;
                packing_total_qty = Double.parseDouble(Total_qty) - dvalue;


                String strtobepackedqty = String.valueOf(sumoftotalpackedquantity);
                Double dtoobepackedqty = Double.valueOf(strtobepackedqty);

                txt_to_be_packed_quantity.setText(decimalformat.format(dtoobepackedqty));
                txt_to_be_packed_pouches.setText(sumoftotalpackedpouches + "");

                //this quantity used for checking the remaining quantity for packing
                Double dqty = Double.parseDouble(Total_qty) * dintkgvalue;
                Double dsumoftotalweight = sumoftotalweights*dintkgvalue;
                Double dremaingqty = dqty - dsumoftotalweight;
                Double dlc = dlcl * dintkgvalue;

                for (int i =0 ;i<pWeighReadingsModels.size();i++)
                {
                    String status = pWeighReadingsModels.get(i).getStatus();
                    if (status.equals("Ignored"))
                    {
                        count++;
                        if (count==3) {
                            abort();
                            ViewGroup viewGroup = findViewById(android.R.id.content);
                            View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                            builder.setView(dialogView);
                            ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                            TextView txt_status = dialogView.findViewById(R.id.txt_status);
                            final AlertDialog alertDialog = builder.create();
                            alertDialog.show();
                            txt_status.setText("Batch Aborted");
                            unfilled_quantity = String.valueOf(dremaingqty);
                            actualQuantityFilled = String.valueOf(sumoftotalweights);
                            Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                            String a =  txt_to_be_packed_pouches.getText().toString();
                            int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                            unfilled_pouches = String.valueOf(strunfilled_pouches);

//                            handler.removeCallbacks(runnable);

                            btn_test_complete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    Status ="Abort";
                                    BatchStatus ="Abort";
                                    helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);

                                    InsertIntoWeightReadingDb();
                                    Calculation();
                                    SQLiteDatabase db = helper.getWritableDatabase();
                                    String currentDateandTime = dateformat.format(new Date());
                                    ContentValues cv=new ContentValues();
                                    Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;

                                    cv.put(Constants.P_TAG_RESULT_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                                    cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                                    cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                                    cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                                    cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                                    cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                                    cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                                    cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                                    cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                                    cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                                    cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);



                                    long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                                    Log.d("Success", String.valueOf(d));

                                    Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                                    i.putExtra("selected_date",str_selected_date);
                                    i.putExtra("product",str_product);
                                    i.putExtra("batch_id",str_batch_id);
                                    startActivity(i);

                                }
                            });
                        }
                    }
                    else {
                        count = 0;
                    }
                }

                if (sumoftotalweights>=packing_total_qty)
                {
                    btnStop.setVisibility(View.GONE);
                    abort();
                    unfilled_quantity = String.valueOf(dremaingqty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    txt_status.setText("Batch Completed");


                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            BatchStatus ="Complete";
                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            InsertIntoWeightReadingDb();
                            Calculation();
                            Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;

                            SQLiteDatabase db = helper.getWritableDatabase();
                            String currentDateandTime = dateformat.format(new Date());
                            ContentValues cv=new ContentValues();

                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Complete");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);


                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                            Log.d("Success", String.valueOf(d));


                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date",str_selected_date);
                            i.putExtra("product",str_product);
                            i.putExtra("batch_id",str_batch_id);
                            startActivity(i);

                        }
                    });

                }
                else if (dremaingqty < dlc)
                {
                    btnStop.setVisibility(View.GONE);
                    abort();
//                    handler.removeCallbacks(runnable);
                    unfilled_quantity = String.valueOf(remaining_pouch_qty);
                    actualQuantityFilled = String.valueOf(sumoftotalweights);
                    Actual_filled_pouches = txt_to_be_packed_pouches.getText().toString();
                    String a =  txt_to_be_packed_pouches.getText().toString();
                    int strunfilled_pouches = Integer.parseInt(Tolal_no_of_pouches) - Integer.parseInt(a);
                    unfilled_pouches = String.valueOf(strunfilled_pouches);

                    ViewGroup viewGroup = findViewById(android.R.id.content);
                    View dialogView = LayoutInflater.from(ProductionWeightReadingActivity.this).inflate(R.layout.alert_test_complete_dialogue, viewGroup, false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                    builder.setView(dialogView);
                    ImageButton btn_test_complete = dialogView.findViewById(R.id.img_btn_test_complete);
                    TextView txt_status = dialogView.findViewById(R.id.txt_status);
                    final AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    txt_status.setText("Insufficient remaining quantity");

                    btn_test_complete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            Status = "Abort";
                            BatchStatus ="Abort";
                            //helper.updateBatchStatusForBatchid(BatchStatus,str_batch_id);
                            InsertIntoWeightReadingDb();
                            Calculation();
                            SQLiteDatabase db = helper.getWritableDatabase();
                            String currentDateandTime = dateformat.format(new Date());
                            ContentValues cv=new ContentValues();
                            Double dtotalquantity = Double.parseDouble(Total_qty) * dintkgvalue;

                            cv.put(Constants.P_TAG_RESULT_ID,1);
                            cv.put(Constants.P_TAG_RESULT_OEM_ID,1234);
                            cv.put(Constants.P_TAG_RESULT_MAC_ID,dataloggerMacId);
                            cv.put(Constants.P_TAG_RESULT_LOGGER_ID,dataloggerName);
                            cv.put(Constants.P_TAG_RESULT_RUN_STATUS_ID,1);
                            cv.put(Constants.P_TAG_RESULT_TAG_ID,str_batch_id);
                            cv.put(Constants.P_TAG_RESULT_TARGET_WEIGHT,Float.parseFloat(String.valueOf(dtotalquantity)));
                            cv.put(Constants.P_TAG_RESULT_HEAD,"Right");
                            cv.put(Constants.P_TAG_RESULT_STATUS_ABORTED,"Abort");
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_DATE,currentDateandTime);
                            cv.put(Constants.P_TAG_RESULT_IS_AUTHORIZED,1);
                            cv.put(Constants.P_TAG_RESULT_MODIFIED_BY,"Pooja");
                            cv.put(Constants.P_TAG_RESULT_IS_SYNC,0);

                            long d=db.insert(Constants.P_TAG_RESULT_TABLE,null,cv);
                            Log.d("Success", String.valueOf(d));

                            Intent i = new Intent(ProductionWeightReadingActivity.this, BatchReportDetails.class);
                            i.putExtra("selected_date",str_selected_date);
                            i.putExtra("product",str_product);
                            i.putExtra("batch_id",str_batch_id);
                            startActivity(i);


                        }
                    });

                }
                else {

                }
            }
            else
            {

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    public void connectToMQttBroker(final boolean sendReadyToDas){
        String clientId = MqttClient.generateClientId();
        MemoryPersistence memPer = new MemoryPersistence();
        final String tagId = current_tag_id;

        client =  new MqttAndroidClient(this,
                MQTT_BROKER,
                clientId);
        try {

            client.setCallback(new ProductionWeightReadingActivity.MQttCallbackClass(this, client));
            IMqttToken token = client.connect();
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    try {
                        String topic = "/test/" + "DiA0AA01";
                        int qos = 2;
                        try {
                            IMqttToken subToken = client.subscribe(topic, qos);
                            subToken.setActionCallback(new IMqttActionListener() {
                                @Override
                                public void onSuccess(IMqttToken asyncActionToken) {
                                    Toast.makeText(ProductionWeightReadingActivity.this, "Subscribed succesfully", Toast.LENGTH_SHORT).show();

                                    try {
                                        if(sendReadyToDas) {
                                            sendReadyToDas1(tagId,dataloggerIp,dataloggerName);
                                            // publishResult();

                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                @Override
                                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                                    // set title
                                    alertDialogBuilder.setTitle(R.string.app_name);
                                    // set dialog message
                                    alertDialogBuilder.setMessage("Could not connect to MQTT broker you cannot view readings, please check!!");
                                    alertDialogBuilder.setCancelable(true);
                                    alertDialogBuilder.setNeutralButton(android.R.string.ok,
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = alertDialogBuilder.create();
                                    alert11.show();

                                }
                            });
                        } catch (Exception e) {
                            Toast.makeText(ProductionWeightReadingActivity.this, "Exception occured" , Toast.LENGTH_LONG).show();
                        }
                    }catch(Exception ex){
                        Toast.makeText(ProductionWeightReadingActivity.this, "Exception occured "+ ex.getMessage() , Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Toast.makeText(ProductionWeightReadingActivity.this, "Failed Exception occured" , Toast.LENGTH_LONG).show();

                }
            });

        }catch(MqttException ex){
            Toast.makeText(ProductionWeightReadingActivity.this, "Failed with outer exception , " + ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    private class MQttCallbackClass implements MqttCallback {
        public  MqttAndroidClient client = null;
        public Context actCtx = null;

        public  MQttCallbackClass(Context ctx, MqttAndroidClient client){
            actCtx = ctx;
            clientObj = client;
        }

        public void connectionLost() throws MqttException {
            boolean reconnected = false;
            Toast.makeText(ProductionWeightReadingActivity.this, "Connection to MQTT broker lost", Toast.LENGTH_LONG).show();
        }

        @Override
        public void connectionLost(Throwable cause) {
            try {
                connectionLost();
            } catch (MqttException e) {
                // We tried our best to reconnect
                e.printStackTrace();
            }
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) throws Exception {
            if(message!= null && message.toString().equalsIgnoreCase("done")){
                clientObj.disconnect();
                isDone = true;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProductionWeightReadingActivity.this);
                // set title
                alertDialogBuilder.setTitle(R.string.app_name);
                // set dialog message
                alertDialogBuilder.setMessage("Weight Readings are done !!!");
                alertDialogBuilder.setCancelable(true);
                alertDialogBuilder.setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = alertDialogBuilder.create();
                alert11.show();

            }else {

                isDone = false;
                SharedPreferences sharedPrefs = PreferenceManager
                        .getDefaultSharedPreferences(ProductionWeightReadingActivity.this);
                String targetWt = sharedPrefs.getString("TargetWt", "0");
                String negwt = sharedPrefs.getString("NegWt", "No");
                Toast.makeText(ProductionWeightReadingActivity.this, message.toString(), Toast.LENGTH_SHORT).show();


                if(negwt != null && negwt.equalsIgnoreCase("yes")){
                    double targetWtD = Double.parseDouble(targetWt);
                    double reading = Double.parseDouble(message.toString());
                    if(reading < targetWtD){
                        //client.unsubscribe("/test/topic");
                        Toast.makeText(actCtx, "Aborting test", Toast.LENGTH_LONG).show();
                        if(clientObj != null && clientObj.isConnected()) {
                            clientObj.disconnect();
                        }
                    }
                }
                Log.d(TAG, "messageArrived: "+message.toString());

                if (strUom.equals("gm"))
                {
                    gmperpouchcalculation(message.toString());
                }
                else
                {
                    kgperpouchCalculation(message.toString());
                }


            }

        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
            String tokenS = token.toString();
            Log.d(TAG, "deliveryComplete: ");
        }
    }

    public String sendReadyToDas1(String tagId, String dataLoggerIP, String dataLoggerName) throws IOException {
        String tag = tagId;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String url = "http://"+dataLoggerIP+":"+DATA_LOGGER_PORT+"/start?tagid="+tagId+"&dataLoggerIP="+dataLoggerIP+"&dataLoggerName="+dataLoggerName;
        //String url = "http://"+dataLoggerIP+"/start?tagid="+tagId+"&dataLoggerIP="+dataLoggerIP+"&dataLoggerName="+dataLoggerName;
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(ProductionWeightReadingActivity.this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;
    }

    private String abort() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String tag = current_tag_id;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String dataPort=Constants.DATA_LOGGER_PORT+"";
        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;

    }
}
