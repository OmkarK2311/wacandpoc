package com.brainlines.weightloggernewversion.model;

public class TestRun {
    public String tagid;
    public String oinum;
    public String targetwt;
    public String head;
    public String status;
    public String isAuthorized;


    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((head == null) ? 0 : head.hashCode());
        result = prime * result + ((isAuthorized == null) ? 0 : isAuthorized.hashCode());
        result = prime * result + ((oinum == null) ? 0 : oinum.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((tagid == null) ? 0 : tagid.hashCode());
        result = prime * result + ((targetwt == null) ? 0 : targetwt.hashCode());
        return result;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof TestRun)) {
            return false;
        }
        TestRun other = (TestRun) obj;
        if (head == null) {
            if (other.head != null) {
                return false;
            }
        } else if (!head.equals(other.head)) {
            return false;
        }
        if (isAuthorized == null) {
            if (other.isAuthorized != null) {
                return false;
            }
        } else if (!isAuthorized.equals(other.isAuthorized)) {
            return false;
        }
        if (oinum == null) {
            if (other.oinum != null) {
                return false;
            }
        } else if (!oinum.equals(other.oinum)) {
            return false;
        }
        if (status == null) {
            if (other.status != null) {
                return false;
            }
        } else if (!status.equals(other.status)) {
            return false;
        }
        if (tagid == null) {
            if (other.tagid != null) {
                return false;
            }
        } else if (!tagid.equals(other.tagid)) {
            return false;
        }
        if (targetwt == null) {
            if (other.targetwt != null) {
                return false;
            }
        } else if (!targetwt.equals(other.targetwt)) {
            return false;
        }
        return true;
    }

}
