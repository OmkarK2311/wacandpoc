package com.brainlines.weightloggernewversion.utils;

public class Constants {

    //local
    public static String dbIpAddress = "192.168.10.67";
    public static String dbUserName = "test2";
    public static String dbPassword = "test12345";
    public static String dbDatabaseName = "decitalpool";
    public static String serverport = "1433";
    public static String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
    public static int DATA_LOGGER_PORT = 8080;
    public static final String MQTT_BROKER = "tcp://"+dbIpAddress;

    //Azure
//    public static String dbIpAddress = "192.227.69.198";
//    public static String dbUserName = "weightloggeruser";
//    public static String dbPassword = "weightlogger123";
//    public static String dbDatabaseName = "WeightLoggerDev";
//    public static String serverport = "1433";
//    public static String dbUrl = "jdbc:jtds:sqlserver://"+dbIpAddress+":"+serverport+"/"+dbDatabaseName;
//    public static int DATA_LOGGER_PORT = 8080;
//    public static final String MQTT_BROKER = "tcp://"+dbIpAddress;


    public static final String TEST_ALLOWED = "TESTALLOWED";
    public static final String TEST_APPEAL = "APPEALREQUIRED";
    public static final String TEST_NOT_ALLOWED = "TESTNOTALLOWED";
    public static final String RUN_TEST_INPROGRESS_STATUS = "INPROGRESS";
    public static final String GM = "+/- GM";
    public static final String GMPERCENT = "+/- %GM";
    public static final String GMSD = "GM SD";
    public static final String PERCENTSD = "% SD";
    public static final String AVGGIVEAWAY= "AvgGiveAway";

    //C-Tags Table

    public static final String C_TAGS_TABLE = "CalibrationTags";
    public static final String C_TAG_ID = "CalTagsID";
    public static final String C_TAG_OEM_ID = "OEMID";
    public static final String C_TAG_MACID = "MacID";
    public static final String C_TAG_LOGGER_ID = "LoggerID";
    public static final String C_TAG_TAGS_ID = "TagID";
    public static final String C_TAG_OEM_NUMBER = "OINumber";
    public static final String C_TAG_TARGET_WIGHT = "TargetWeight";
    public static final String C_TAG_CUSTOMER_NAME = "CustomerName";
    public static final String C_TAG_MACHINE_TYPE_ID = "MachineTypeID";
    public static final String C_TAG_PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String C_TAG_MACHINE_TYPE_NAME = "MachineTypeName";
    public static final String C_TAG_PRODUCT_TYPE_NAME = "ProductTypeName";
    public static final String C_TAG_FILLER_TYPE = "FillerType";
    public static final String C_TAG_PRODUCT = "ProductName";
    public static final String C_TAG_OPERATOR = "Operator";
    public static final String C_TAG_TARGET_WEIGHT_UNIT = "TargetWeightUnit";
    public static final String C_TAG_INSPECTOR = "Inspector";
    public static final String C_TAG_FILEM_TYPE = "FilmType";
    public static final String C_TAG_PRODUCT_FEED = "ProductFeed";
    public static final String C_TAG_AGITATOR_MOTOR_FREQ = "AgitatorMotorFrequency";
    public static final String C_TAG_AGITATOR_MODE = "AgitatorMode";
    public static final String C_TAG_AGITATOR_OD = "AgitatorOD";
    public static final String C_TAG_AGITATOR_ID = "AgitatorID";
    public static final String C_TAG_AGITATOR_PITCH = "AgitatorPitch";
    public static final String C_TAG_TARGET_ACCURACY_TYPE = "TargetAccuracyType";
    public static final String C_TAG_TARGET_ACCURACY_VALUE = "TargetAccuracyValue";
    public static final String C_TAG_MACHINE_SERIAL_NUMBER = "MachineSerialNumber";
    public static final String C_TAG_PRODUCT_BULK_MAIN = "PrBulkMain";
    public static final String C_TAG_TARGET_SPEED = "TargetSpeed";
    public static final String C_TAG_HEAD = "Head";
    public static final String C_TAG_AIR_PRESSURE = "AirPressure";
    public static final String C_TAG_QUANTITY_SETTING = "QuantitySetting";
    public static final String C_TAG_BAG_LENGTH_SETTING = "BaglengthSetting";
    public static final String C_TAG_FILLVALVE_GAP = "FillValveGap";
    public static final String C_TAG_FILL_VFD_FREQUENCY = "FillVfdFreq";
    public static final String C_TAG_HORIZONTAL_CUT_SEAL_SETTING = "HorizCutSealSetting";
    public static final String C_TAG_HORIZONTAL_BAND_SEAL_SETTING = "HorizBandSealSetting";
    public static final String C_TAG_VERTICAL_SEAL_SETTING = "VertSealSetting";
    public static final String C_TAG_AUGER_AGITATOR_FREQUENCY = "AugerAgitatorFreq";
    public static final String C_TAG_AUGER_FILL_VALVE = "AugerFillValve";
    public static final String C_TAG_AUGER_PITCH = "AugurPitch";
    public static final String C_TAG_POUCH_WIDTH = "PouchWidth";
    public static final String C_TAG_POUCH_LENGTH = "PouchLength";
    public static final String C_TAG_POUCH_SEAL_TYPE = "PouchSealType";
    public static final String C_TAG_POUCH_TEMP_HORIZONTAL_FRONT = "PouchTempHorizFront";
    public static final String C_TAG_POUCH_TEMP_HORIZONTAL_BACK = "PouchTempHorizBack";
    public static final String C_TAG_POUCH_TEMP_VERTICAL = "PouchTempVert";
    public static final String C_TAG_POUCH_TEMP_VERT_FRONT_LEFT = "PouchTempVertFrontLeft";
    public static final String C_TAG_POUCH_TEMP_VERTICAL_FRONT_RIGHT = "PouchTempVertFrontRight";
    public static final String C_TAG_POUCH_TEMP_VERT_BACK_LEFT = "PouchTempVertBackLeft";
    public static final String C_TAG_POUCH_TEMP_VERT_BACK_RIGHT = "PouchTempVertBackRight";
    public static final String C_TAG_POUCH_GUSSET = "PouchGusset";
    public static final String C_TAG_POUCH_FILM_THICKNESS = "PouchFilmThickness";
    public static final String C_TAG_PUCH_PRODUCT_BULK = "PouchProductBulk";
    public static final String C_TAG_POUCH_EQ_HORIZ_SERRATIONS = "PouchEqHorizSerrations";
    public static final String C_TAG_POUCH_FILL_eVENT = "PouchFillEvent";
    public static final String C_TAG_OVERALL_STATUS = "OverallStatus";
    public static final String C_TAG_REMARK = "Remarks";
    public static final String C_TAG_SEAL_STATUS = "SealStatus";
    public static final String C_TAG_DROP_STATUS = "DropStatus";
    public static final String C_TAG_TARGET_SPEED_STATUS = "TargetSpeedStatus";
    public static final String C_TAG_WEIGHT_TEST_STATUS = "WeightTestStatus";
    public static final String C_TAG_BAG_STATUS = "BagStatus";
    public static final String C_TAG_PAPER_STATUS = "PaperStatus";
    public static final String C_TAG_FY_YEAR = "FinancialYear";
    public static final String C_TAG_SEAL_REJECTION_REASON = "SealRejectionReason";
    public static final String C_TAG_CUSTOMER_PRODUCT = "IsCustomerProduct";
    public static final String C_TAG_CUSTOMER_FILM = "IsCustomerFilm";
    public static final String C_TAG_NEGATIVE_WEIGHT = "IsNegativeWeight";
    public static final String C_TAG_LOGGER_CREATE_TIMESTAMP = "LoggerCreateTimestamp";
    public static final String C_TAG_CREATED_DATE = "CreatedDate";
    public static final String C_TAG_MODIFIED_DATE = "ModifiedDate";
    public static final String C_TAGS_IS_SYNC = "IsSync";


    //master Seal rejection reason Table
    public static final String SEAL_REJECTION_TABLE = "mstSealRejectionReasons";
    public static final String SEAL_REJECTION_REASON_ID = "rsn_id";
    public static final String SEAL_REJECTION_OEM_ID = "OEMID";
    public static final String SEAL_REJECTION_PRODUCT_TYPE = "product_type";
    public static final String SEAL_REJECTION_REASON_DESCIPTION = "reason_desc";
    public static final String SEAL_REJECTION_IS_ACTIVE = "IsActive";
    public static final String SEAL_REJECTION_CREATED_DATE = "CreatedDate";
    public static final String SEAL_REJECTION_MODIFIED_DATE = "ModifiedDate";
    public static final String SEAL_REJECTION_CREATED_BY = "CreatedBy";
    public static final String SEAL_REJECTION_MODIFIED_BY = "ModifiedBy";
    public static final String SEAL_REJECTION_IS_SYNC = "IsSync";

    //Tolerance Table
    public static final String TOLERANCE_TABLE = "mstCalibrationTolerance";
    public static final String TOLERANCE_ID = "CalibrationToleranceID";
    public static final String TOLERANCE_OEM_ID = "OEMID";
    public static final String TOLERANCE_PRODUCT_TYPE = "ProductType";
    public static final String TOLERANCE_TARGET_WEIGHT = "TargetWeight";
    public static final String TOLERANCE_MIN_TARGET_WEIGHT = "MinTargetWeight";
    public static final String TOLERANCE_MAX_TARGET_WEIGHT = "MaxTargetWeight";
    public static final String TOLERANCE_MIN_WEIGHT_SAMPLES = "MinWeightSamples";
    public static final String TOLERANCE_MIN_PAPER_SHIFT_SAMPLES = "MinPaperShiftSamples";
    public static final String TOLERANCE_PAPER_SHIFT_TOLERANCE = "PaperShiftTolerance";
    public static final String TOLERANCE_MIN_BAG_LENGTH_SAMPLES = "MinBagLengthSamples";
    public static final String TOLERANCE_BAG_LENGTH_TOLERANCE = "BagLengthTolerance";
    public static final String TOLERANCE_CREATED_DATE = "CreatedDate";
    public static final String TOLERANCE_MODIFIED_DATE = "ModifiedDate";
    public static final String TOLERANCE_CREATED_BY = "CreatedBy";
    public static final String TOLERANCE_MODIFIED_BY = "ModifiedBy";
    public static final String TOLERANCE_IS_ACTIVE = "IsActive";


    //PRODUCT Table
    public static final String PRODUCT_TYPE_TABLE = "mstProductType";
    public static final String PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String PRODUCT_OEM_ID = "OEMID";
    public static final String PRODUCT_PRODUCT_TYPE = "ProductType";
    public static final String PRODUCT_CREATED_BY = "CreatedBy";
    public static final String PRODUCT_MODIFIED_BY = "ModifiedBy";
    public static final String PRODUCT_UNIT_OF_MEASUREMENT = "UOM";
    public static final String PRODUCT_UNIT_OF_MEASUREMENT_ID = "UOMID";
    public static final String PRODUCT_CREATED_DATE = "CreatedDate";
    public static final String PRODUCT_MODIFIED_DATE = "ModifiedDate";
    public static final String PRODUCT_IS_ACTVIE = "IsActive";


    //Master WireCup
    public static final String MASTER_WIRE_CUP_TABLE = "mstWireCup";
    public static final String  MASTER_WIRE_CUP_ID = "WireCupID";
    public static final String  MASTER_WIRE_CUP_OEM_ID = "OEMID";
    public static final String  MASTER_WIRE_CUP = "WireCup";
    public static final String  MASTER_WIRE_CUP_CREATED_DATE = "CreatedDate";
    public static final String  MASTER_WIRE_CUP_MODIFIED_DATE = "ModifiedDate";
    public static final String  MASTER_WIRE_CUP_CREATED_BY = "CreatedBy";
    public static final String  MASTER_WIRE_CUP_MODIFIED_BY = "ModifiedBy";
    public static final String  MASTER_WIRE_CUP_IS_ACTIVE = "IsActive";



    //Master target accuracy Table
    public static final String TARGET_ACCURACY_TABLE = "mstTargetAccuracy";
    public static final String TARGET_ACCURACY_ID = "TargetAccuracyID";
    public static final String TARGET_ACCURACY_OEM_ID = "OEMID";
    public static final String TARGET_ACCURACY_TARGET_ID = "TargetID";
    public static final String TARGET_ACCURACY = "TargetAccuracy";
    public static final String TARGET_ACCURACY_DESCRIPTION = "TargetAccuracy_Desc";
    public static final String TARGET_ACCURACY_CREATED_DATE = "CreatedDate";
    public static final String TARGET_ACCURACY_MODIFIED_DATE = "ModifiedDate";
    public static final String TARGET_ACCURACY_CREATED_BY = "CreatedBy";
    public static final String TARGET_ACCURACY_MODIFIED_BY = "ModifiedBy";
    public static final String TARGET_ACCURACY_IS_ACTIVE = "IsActive";


    //Master Machine Type Table
    public static final String MACHINE_TYPE_TABLE = "mstMachineType";
    public static final String MACHINE_TYPE_ID = "MachineTypeID";
    public static final String MACHINE_TYPE_OEM_ID = "OEMID";
    public static final String MACHINE_TYPE_MACHINE_ID = "MachineID";
    public static final String MACHINE_TYPE_PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String MACHINE_TYPE_MACHINE_NAME = "MachineName";
    public static final String MACHINE_TYPE_MACHINE_DESCRIPTION = "MachineDescription";
    public static final String MACHINE_TYPE_MACHINE_PRODUCT_TYPE = "ProductType";
    public static final String MACHINE_TYPE_CREATED_DATE = "CreatedDate";
    public static final String MACHINE_TYPE_MODIFIED_DATE = "ModifiedDate";
    public static final String MACHINE_TYPE_CREATED_BY = "CreatedBy";
    public static final String MACHINE_TYPE_MODIFIED_BY = "ModifiedBy";
    public static final String MACHINE_TYPE_IS_ACTIVE = "IsActive";

    //Master POUCH ATTRIBUTES Table
    public static final String POUCH_ATTRIBUTES_TABLE = "mstPouchAttributes";
    public static final String POUCH_ATTRIBUTES_ID = "PouchAttributesID";
    public static final String POUCH_ATTRIBUTES_OEM_ID = "OEMID";
    public static final String POUCH_ATTRIBUTES_PRODUCT_TYPE_ID = "ProdutTypeID";
    public static final String POUCH_ATTRIBUTES_DESCRIPTION = "PouchAttributeDescription";
    public static final String POUCH_ATTRIBUTES_CREATED_DATE = "CreatedDate";
    public static final String POUCH_ATTRIBUTES_MODIFIED_DATE = "ModifiedDate";
    public static final String POUCH_ATTRIBUTES_CREATED_BY = "CreatedBy";
    public static final String POUCH_ATTRIBUTES_MODIFIED_BY = "ModifiedBy";
    public static final String POUCH_ATTRIBUTES_IS_ACTVIE = "IsActive";


    //Master SEAL_TYPE Table
    public static final String SEAL_TYPE_TABLE = "mstSealType";
    public static final String SEAL_TYPE = "SealType";
    public static final String SEAL_TYPE_OEM_ID = "OEMID";
    public static final String SEAL_TYPE_SEAL_ID = "SealID";
    public static final String SEAL_TYPE_PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String SEAL_TYPE_IS_ACTIVE = "IsActive";
    public static final String SEAL_TYPE_CREATED_DATE = "CreatedDate";
    public static final String SEAL_TYPE_MODIFIED_DATE = "ModifiedDate";
    public static final String SEAL_TYPE_CREATED_BY = "CreatedBy";
    public static final String SEAL_TYPE_MODIFIED_BY = "ModifiedBy";


    //Master ProductionTag Table
    public static final String PRODUCTION_TAG_TABLE = "ProductionTag";
    public static final String PRODUCTION_TAG_ID = "ProductionTagID";
    public static final String PRODUCTION_TAG = "ProductionTagName";
    public static final String PRODUCTION_TAG_OEM_ID = "OEMID";
    public static final String PRODUCTION_TAG_MAC_ID = "MacID";
    public static final String PRODUCTION_TAG_BATCH_ID = "BatchID";
    public static final String PRODUCTION_TAG_PRODUCT_ID= "ProductName";
    public static final String PRODUCTION_TAG_GM_PER_POUCH = "GmPerPouch";
    public static final String PRODUCTION_TAG_UNIT = "Unit";
    public static final String PRODUCTION_TAG_TOTAL_QTY = "TotalQuantity";
    public static final String PRODUCTION_TAG_TOTAL_QTY_UOM = "TotalQuantityUOM";
    public static final String PRODUCTION_TAG_IS_NEGATIVE_ALLOWED = "IsNegAllowed";
    public static final String PRODUCTION_TAG_MODIFIED_DATE = "ModifiedDate";
    public static final String PRODUCTION_TAG_SHIFT = "TagShift";
    public static final String PRODUCTION_TAG_NO_OF_POUCHES = "TagNumberOfPouches";
    public static final String PRODUCTION_TAG_SESSION_POSITVIE_TOLERANCE = "SessionPositiveTolerance";
    public static final String PRODUCTION_TAG_SESSION_NEAGTIVE_TOLERANCE = "SessionNegativeTolerance";
    public static final String PRODUCTION_TAG_MASTER_TOLERANCE_POSITIVE = "MasterTolerancePositive";
    public static final String PRODUCTION_TAG_MASTER_TOLERANCE_NEGATIVE = "MasterToleranceNegative";
    public static final String PRODUCTION_TAG_UCL_GM = "UCLgm";
    public static final String PRODUCTION_TAG_LCL_GM = "LCLgm";
    public static final String PRODUCTION_TAG_OPERATOR_NAME = "Operator";
    public static final String PRODUCTION_TAG_TARGET_ACCURACY_TYPE = "TargetAccurancyType";
    public static final String PRODUCTION_TAG_TARGET_ACCURACY_VALUE = "TargetAccurancyValue";
    public static final String PRODUCTION_TAG_BATCH_STATUS = "BatchStatus";
    public static final String PRODUCTION_TAG_IS_SYNC = "IsSync";

    //User Login B_devicedid_
    public static final String USER_LOGIN_TABLE = "UserLogin";
    public static final String LOGIN_ID = "LoginID";
    public static final String USER_ID = "UserID";
    public static final String ROLE_ID = "RoleID";
    public static final String USER_NAME = "UserName";
    public static final String PASSWORD = "Password";
    public static final String IS_ACTIVE = "IsActive";
    public static final String IS_DELETED = "IsDeleted";
    public static final String CREATED_DATE = "CreatedDate";
    public static final String MODIFIED_DATE = "ModifiedDate";

    //Data Logger Table
    public static final String DATA_LOGGER_TABLE = "mstLogger";
    public static final String LOGGER_ID = "LoggerID";
    public static final String OEMID = "OEMID";
    public static final String LOGGER_SERAIL_NUM = "SerialNumber";
    public static final String LOGGER_NAME = "LoggerName";
    public static final String LOGGER_IP_ADDRESS = "LocalIpAddress";
    public static final String LOGGER_MAC_ID = "logger_mac_id";//**************************Check
    public static final String LOGGER_LOCATION = "LoggerLocation";
    public static final String LOGGER_CREATE_TIMESTAMP = "CreatedDate";
    public static final String LOGGER_CREATED_BY = "CreatedBy";//************************Check
    public static final String LOGGER_LOGGERLOCATION = "LoggerLocation";
    public static final String  LOGGER_MODIFIED_DATE= "ModifiedDate";
    public static final String LOGGER_IS_ACTIVE = "IsActive";
    public static final String LOGGER_IN_USE = "IsInUse";

    //Master Filler Type Table
    public static final String MASTER_FILLER_TYPE_TABLE = "mstFillerType";
    public static final String FILLER_TYPE_ID = "FillerTypeID";
    public static final String FILLER_TYPE_OEMID = "OEMID";
    public static final String FILLER_TYPE_PRODUCT_TYPE_ID = "ProductTypeID";
    public static final String FILLER_TYPE_FILTER_TYPE = "FillerType";
    public static final String FILLER_TYPE_CREATED_DATE = "CreatedDate";
    public static final String FILLER_TYPE_MODIFIED_DATE = "ModifiedDate";
    public static final String FILLER_TYPE_CREATED_BY = "CreatedBy";
    public static final String FILLER_TYPE_MODIFIED_BY = "ModifiedBy";
    public static final String FILLER_TYPE_IS_ACTIVE = "IsActive";
    public static final String FILLER_TYPE_IS_DELETED = "IsDeleted";
    public static final String FILLER_TYPE_FILTER_ID = "FillerID";//************************Check

    //Master Head Table
    public static final String MASTER_HEAD_TABLE = "mstHead";
    public static final String MASTER_HEAD_HEAD_ID = "HeadID";
    public static final String MASTER_HEAD_OEM_ID = "OEMID";
    public static final String MASTER_HEAD_HEAD_ID_ = "Head_ID";//************************Check
    public static final String MASTER_HEAD_HEAD_TYPE_ = "HeadType";
    public static final String MASTER_HEAD_IS_ACTIVE = "IsActive";
    public static final String MASTER_HEAD_CREATED_DATE = "CreatedDate";
    public static final String MASTER_HEAD_MODIFIED_DATE = "ModifiedDate";
    public static final String MASTER_HEAD_CREATED_BY = "CreatedBy";
    public static final String MASTER_HEAD_MODIFIED_BY = "ModifiedBy";





    //Master Products
    public static final String MASTER_PRODUCTS_TABLE = "mstProducts";
    public static final String MASTER_PRODUCTS_PRODUCTS_ID = "ProductID";
    public static final String MASTER_PRODUCTS_OEM_ID = "OEMID";
    public static final String MASTER_PRODUCTS_PRODUCTS = "Product";
    public static final String MASTER_PRODUCTS_PRICEPERKG = "PricePerKG";
    public static final String MASTER_PRODUCTS_CURRENCY = "Currency";
    public static final String MASTER_PRODUCTS_TYPE_ID = "ProductTypeID";
    public static final String MASTER_PRODUCTS_IS_ACTIVE = "IsActive";
    public static final String MASTER_PRODUCTS_CREATED_DATE = "CreatedDate";
    public static final String MASTER_PRODUCTS_MODIFIED_DATE = "ModifiedDate";
    public static final String MASTER_PRODUCTS_CREATED_BY = "CreatedBy";
    public static final String MASTER_PRODUCTS_MODIFIED_BY = "ModifiedBy";


    //Master Pouch Symmetry
    public static final String MASTER_POUCH_SYMMETRY_TABLE = "mstPouchSymmetry";
    public static final String MASTER_POUCH_SYMMETRY_ID = "PouchSymmetryID";
    public static final String MASTER_POUCH_OEM_ID = "OEMID";
    public static final String MASTER_POUCH_ID = "PouchID";
    public static final String MASTER_POUCH_TYPE_ID = "PouchTypeID";
    public static final String MASTER_POUCH_DESC = "PouchDesc";
    public static final String MASTER_POUCH_IS_ACTIVE = "IsActive";
    public static final String MASTER_POUCH_IS_DELETED = "IsDeleted";
    public static final String MASTER_POUCH_CREATED_DATE = "CreatedDate";
    public static final String MASTER_POUCH_MODIFIED_DATE = "ModifiedDate";
    public static final String MASTER_POUCH_CREATED_BY = "CreatedBy";
    public static final String MASTER_POUCH_MODIFIED_BY = "ModifiedBy";






    //Seal Rejection Reason Master TAble
    public static final String SEAL_REJECTION_REASON_MASTER_TABLE = "mstSealRejectionReasons";
    public static final String SEAL_REJECTION_REASON_OEM_ID = "OEMID";
    public static final String SEAL_REJECTION_REASON_MAC_ID = "mac_id";
    public static final String SEAL_REJECTION_REASON_LOGGER_ID = "LoggerID";
    public static final String SEAL_REJECTION_REASON_REASON_ID = "rsn_id";
    public static final String SEAL_REJECTION_REASON_PRODUCT_TYPE = "product_type";
    public static final String SEAL_REJECTION_REASON_REASON_DESC = "reason_desc";
    public static final String SEAL_REJECTION_REASON_IS_ACTIVE = "IsActive";
    public static final String SEAL_REJECTION_REASON_IS_DELETED = "IsDeleted";
    public static final String SEAL_REJECTION_REASON_CREATED_DATE = "CreatedDate";
    public static final String SEAL_REJECTION_REASON_MODIFIED_DATE = "ModifiedDate";
    public static final String SEAL_REJECTION_REASON_CREATED_BY = "CreatedBy";
    public static final String SEAL_REJECTION_REASON_MODIFIED_BY = "ModifiedBy";

    //Master UOM Table
    public static final String MASTER_UOM_TABLE = "mstUomTable";
    public static final String UOM_ID = "UOMId";
    public static final String UOM_TABLE_OEMID = "OEMID";
    public static final String UOM_PRODUCT_TYPE = "ProductType";
    public static final String UOM_PRODUCT_UOM = "ProductUOM";
    public static final String UOM_PRODUCT_IS_ACTIVE = "isActive";
    public static final String UOM_PRODUCT_IS_DELETED = "isDeleted";
    public static final String UOM_PRODUCT_CREATED_DATE = "CreatedDate";
    public static final String UOM_PRODUCT_MODIFIED_DATE = "ModifiedDate";
    public static final String UOM_PRODUCT_CREATED_BY = "CreatedBy";
    public static final String UOM_PRODUCT_MODIFIED_BY = "ModifiedBy";




    //Interface to start and stop service
    public interface ACTION {
        public static String STARTFORGROUND_ACTION = "foregroundservice.action.startforeground";
        public static String STOPFORGROUND_ACTION = "foregroundservice.action.stopforeground";
    }

    //Production Reason Master
    public static final String PRODUCTION_REASON_MASTER = "mstProductionReasonMaster";
    public static final String PRODUCTION_REASON_REASONID = "ReasonID";
    public static final String PRODUCTION_REASON_REASONDESC = "ReasonDesc";
    public static final String PRODUCTION_REASON_CREATED_DATE = "CreatedDate";
    public static final String PRODUCTION_REASON_MODIFIED_DATE = "ModifiedDate";

    //Production TOLERANCE Master
    public static final String PRODUCTION_TOLERANCE_MASTER = "mstProductionTolerance";
    public static final String PRODUCTION_TOLERANCE_TOLERANCEID = "ProductionToleranceID";
    public static final String PRODUCTION_TOLERANCE_OEMID = "OEMID";
    public static final String PRODUCTION_TOLERANCE_GRAMMAGE = "Grammage";
    public static final String PRODUCTION_TOLERANCE_TOLERANCE_POSITIVE = "TolerancePositive";
    public static final String PRODUCTION_TOLERANCE_TOLERANCE_NEGATIVE = "ToleranceNegetive";
    public static final String PRODUCTION_TOLERANCE_ISACTIVE = "Isactive";
    public static final String PRODUCTION_TOLERANCE_ISDELETED = "IsDeleted";
    public static final String PRODUCTION_TOLERANCE_CREATED_DATE = "CreatedDate";
    public static final String PRODUCTION_TOLERANCE_MODIFIED_DATE= "ModifiedDate";

    //PRODUCTIION WEIGHT BATCH RESULT
    public static final String PRODUCTION_WEIGHT_REPORT = "ProductionWeightReport";
    public static final String PRODUCTION_WEIGHT_REPORT_REPORTID = "ReportID";
    public static final String PRODUCTION_WEIGHT_REPORT_OEMID = "OEMID";
    public static final String PRODUCTION_WEIGHT_REPORT_TAGID = "TagID";
    public static final String PRODUCTION_WEIGHT_REPORT_BATCH_ID = "BatchID";
    public static final String PRODUCTION_WEIGHT_REPORT_MIN_WEIGHT = "MinWeight";
    public static final String PRODUCTION_WEIGHT_REPORT_MAX_WEIGHT = "MaxWeight";
    public static final String PRODUCTION_WEIGHT_REPORT_MEAN_WEIGHT = "MeanWeight";
    public static final String PRODUCTION_WEIGHT_REPORT_AVERAGE_WEIGHT = "Average";
    public static final String PRODUCTION_WEIGHT_REPORT_RANGE = "Range";
    public static final String PRODUCTION_WEIGHT_REPORT_SD = "SD";
    public static final String PRODUCTION_WEIGHT_REPORT_UNFILLED_QUANTITY = "UnfilledQuantity";
    public static final String PRODUCTION_WEIGHT_REPORT_FILLED_QUANTITY = "FilledQuantity";
    public static final String PRODUCTION_WEIGHT_REPORT_UNFILLED_POUCHES = "UnfilledPouches";
    public static final String PRODUCTION_WEIGHT_REPORT_FILLED_POUCHES = "FilledPouches";
    public static final String PRODUCTION_WEIGHT_REPORT_EXCESS_GIVEAWAY_PRICE = "ExcessGiveawayPrice";
    public static final String PRODUCTION_WEIGHT_REPORT_EXCESS_GIVEAWAY_GM = "ExcessGiveawayGm";
    public static final String PRODUCTION_WEIGHT_REPORT_REASON = "Reason";
    public static final String PRODUCTION_WEIGHT_REPORT_REMARK = "Remark";
    public static final String PRODUCTION_WEIGHT_REPORT_ISACTIVE = "IsActive";
    public static final String PRODUCTION_WEIGHT_REPORT_CREATED_DATE = "CreatedDate";
    public static final String PRODUCTION_WEIGHT_REPORT_MODIFIED_DATE = "ModifiedDate";
    public static final String PRODUCTION_WEIGHT_REPORT_IS_SYNC = "IsSync";


    //Transaction P_WEIGHT_READING Table
    public static final String P_WEIGHT_READING_TABLE = "ProductionWeightReadings";
    public static final String P_WEIGHT_READING_ID = "PWeightReadingID";
    public static final String P_WEIGHT_READING_OEM_ID = "OEMID";
    public static final String P_WEIGHT_READING_MAC_ID = "MacID";
    public static final String P_WEIGHT_READING_LOGGER_ID = "LoggerID";
    public static final String P_WEIGHT_READING_WEIGHT_ID = "PWeightID";
    public static final String P_WEIGHT_READING_BATCHID = "BatchID";
    public static final String P_WEIGHT_READING_PRODUCTION_TAG_NAME = "ProductionTagName";
    public static final String P_WEIGHT_READING_WEIGHT = "Weight";
    public static final String P_WEIGHT_READING_SHIFT = "Shift";
    public static final String P_WEIGHT_READING_TARGETGMPERPOUCH = "TargetGmPerPouch";
    public static final String P_WEIGHT_READING_ACTUALGMPERPOUCH = "ActualGmPerPouch";
    public static final String P_WEIGHT_READING_EXCESSGIVEAWAYPERPOUCH = "ExcessGiveawayPerPouch";
    public static final String P_WEIGHT_READING_CUMILATIVEEXCESSGIVEAWAY = "CumilativeExcessGiveaway";
    public static final String P_WEIGHT_READING_CUMILATIVEQUANTITY = "CumilativeQuantity";
    public static final String P_WEIGHT_READING_CUMILATIVE_POUCHES = "CumilativePouches";
    public static final String P_WEIGHT_READING_NEGATIVE_GIVEAWAY_FROM_TAG = "NegetiveGigiveawayFromTag";
    public static final String P_WEIGHT_READING_UCL = "UCLgm";
    public static final String P_WEIGHT__READING_LCL = "LCLgm";
    public static final String P_WEIGHT_READING_STATUS = "Status";
    public static final String P_WEIGHT_READING_CREATED_DATE = "CreatedDate";
    public static final String P_WEIGHT_READING_CREATED_BY = "CreatedBy";
    public static final String P_WEIGHT_READING_MODIFIED_DATE = "ModifiedDate";
    public static final String P_WEIGHT_READING_MODIFIED_BY = "ModifiedBy";
    public static final String P_WEIGHT_READING_IS_SYNC = "IsSync";

    // P_TAG_RESULT Table
    public static final String P_TAG_RESULT_TABLE = "ProductionTagResults";
    public static final String P_TAG_RESULT_ID = "PTagResultsID";
    public static final String P_TAG_RESULT_OEM_ID = "OEMID";
    public static final String P_TAG_RESULT_MAC_ID = "MacID";
    public static final String P_TAG_RESULT_LOGGER_ID = "LoggerID";
    public static final String P_TAG_RESULT_RUN_STATUS_ID = "RunStatusID";
    public static final String P_TAG_RESULT_TAG_ID = "ProductionTagName";
    public static final String P_TAG_RESULT_TARGET_WEIGHT = "TargetWeight";
    public static final String P_TAG_RESULT_HEAD = "Head";
    public static final String P_TAG_RESULT_STATUS_ABORTED = "StatusAborted";
    public static final String P_TAG_RESULT_MODIFIED_DATE = "ModifiedDate";
    public static final String P_TAG_RESULT_IS_AUTHORIZED = "IsAuthorised";
    public static final String P_TAG_RESULT_MODIFIED_BY = "ModifiedBy";
    public static final String P_TAG_RESULT_IS_SYNC = "IsSync";


    //List Of Master Tables
    public static final String LIST_OF_MASTER_TABLES = "mstMasterTables";
    public static final String MASTER_TABLES_ID = "MasterTableId";
    public static final String MASTER_TABLES_NAME = "MasterTableName";
    public static final String MASTER_TABLES_DISLAY_NAME = "MasterTableDisplayName";

    //kg values table
    public static final String PRODUCTION_KG_VALUES_MASTER_TABLES = "mstProductionGrammageMaster";
    public static final String PRODUCTION_GRAMMAGE_ID = "grammage_id";
    public static final String PRODUCTION_GRAMMAGE = "grammage_name";
    public static final String PRODUCTION_KG_VALUES = "grammage_value";

    //Bag length variation RESULT table
    public static final String BAG_LENGTH_VARIATION_RESULT = "CalibrationBagLengthVariationResults";
    public static final String BAG_LENGTH_VARIATION_BAG_LENGTH_ID = "BagLengthID";
    public static final String BAG_LENGTH_VARIATION_OEM_ID = "OEMID";
    public static final String BAG_LENGTH_VARIATION_MAC_ID = "MacID";
    public static final String BAG_LENGTH_VARIATION_LOGGER_ID = "LoggerID";
    public static final String BAG_LENGTH_VARIATION_TAG_ID = "TagID";
    public static final String BAG_LENGTH_VARIATION_OINUM = "OINumber";
    public static final String BAG_LENGTH_VARIATION_LENGTH = "BagLength";
    public static final String BAG_LENGTH_VARIATION_MODIFIED_DATE = "ModifiedDate";
    public static final String BAG_LENGTH_VARIATION_IS_SYNC = "IsSync";

    //Bag Length Analytics
    public static final String BAG_LENGTH_ANALYTICS_TABLE = "CalibrationBagLengthAnalytics";
    public static final String BAG_LEN_ANA_ID = "BagLengthAnalyticsID";
    public static final String BAG_LEN_ANA_OEM_ID = "OEMID";
    public static final String BAG_LEN_ANA_LOGGER_ID = "LoggerID";
    public static final String BAG_LEN_ANA_TAG_ID = "TagID";
    public static final String BAG_LEN_ANA_MIN_READING = "MinReading";
    public static final String BAG_LEN_ANA_MAX_READING = "MaxReading";
    public static final String BAG_LEN_ANA_RANGE_READING = "RangeReading";
    public static final String BAG_LEN_ANA_RUN_STATUS = "RunStatus";
    public static final String BAG_LEN_ANA_MODIFIED_DATE = "ModifiedDate";
    public static final String BAG_LEN_ANA_MEAN_READING = "MeanReading";
    public static final String BAG_LEN_ANA_IS_SYNC = "IsSync";

    //PAPER SHIFT RESULT TABLE
    public static final String PAPER_SHIFT_RESULTS = "CalibrationPaperShiftResults";
    public static final String PAPER_SHIFT_RESULTS_PAPER_SHIFT_ID = "PaperShiftID";
    public static final String PAPER_SHIFT_RESULTS_OEM_ID = "OEMID";
    public static final String PAPER_SHIFT_RESULTS_MAC_ID = "MacID";
    public static final String PAPER_SHIFT_RESULTS_LOGGER_ID = "LoggerID";
    public static final String PAPER_SHIFT_RESULTS_TAG_ID = "TagID";
    public static final String PAPER_SHIFT_RESULTS_SHIFT = "Shift";
    public static final String PAPER_SHIFT_RESULTS_OINUM = "OINumber";
    public static final String PAPER_SHIFT_RESULTS_MODIFIED_DATE = "ModifiedDate";
    public static final String PAPER_SHIFT_RESULTS_IS_SYNC = "IsSync";

    //Paper Shift Analytics
    public static final String PAPER_SHIFT_ANALYTICS_TABLE = "CalibrationPaperShiftAnalytics";
    public static final String PAPER_SHIFT_ANALYTICS_ID = "PaperShiftAnalyticsID";
    public static final String PAPER_SHIFT_ANALYTICS_OEM_ID = "OEMID";
    public static final String PAPER_SHIFT_ANALYTICS_LOGGER_ID = "LoggerID";
    public static final String PAPER_SHIFT_ANALYTICS_TAG_ID = "TagID";
    public static final String PAPER_SHIFT_ANALYTICS_MIN_READING = "MinReading";
    public static final String PAPER_SHIFT_ANALYTICS_MAX_READING = "MaxReading";
    public static final String PAPER_SHIFT_ANALYTICS_RANGE_READING = "RangeReading";
    public static final String PAPER_SHIFT_ANALYTICS_RUN_STATUS = "RunStatus";
    public static final String PAPER_SHIFT_ANALYTICS_MEAN_READING = "MeanReading";
    public static final String PAPER_SHIFT_ANALYTICS_MODIFIED_DATE = "ModifiedDate";
    public static final String PAPER_SHIFT_ANALYTICS_IS_SYNC = "IsSync";

    //Weight Results Analytics.
    public static final String WEIGHT_RESULTS_ANALYTICS_TABLE = "CalibrationWeightTestResultstAnalytics";
    public static final String WEIGHT_RESULTS_ANALYTICS_ID = "WeightResultsAnalyticsID";
    public static final String WEIGHT_RESULTS_ANALYTICS_OEM_ID = "OEMID";
    public static final String WEIGHT_RESULTS_ANALYTICS_LOGGER_ID = "LoggerID";
    public static final String WEIGHT_RESULTS_ANALYTICS_TAG_ID = "TagID";
    public static final String WEIGHT_RESULTS_ANALYTICS_MIN_READING = "MinReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_MAX_READING = "MaxReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_RANGE_READING = "RangeReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_MEAN_READING = "MeanReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_SD_READING = "SDReading";
    public static final String WEIGHT_RESULTS_ANALYTICS_REMARKS = "Remarks";
    public static final String WEIGHT_RESULTS_ANALYTICS_RUN_STATUS = "RunStatus";
    public static final String WEIGHT_RESULTS_ANALYTICS_GM = "GM";
    public static final String WEIGHT_RESULTS_ANALYTICS_GM_PERCENT = "GMPercent";
    public static final String WEIGHT_RESULTS_ANALYTICS_GM_SD = "GMSD";
    public static final String WEIGHT_RESULTS_ANALYTICS_GM_SD_PERCENT = "GMSDPercent";
    public static final String WEIGHT_RESULTS_ANALYTICS_AVERAGE_GIVE_AWAY = "AverageGiveAway";
    public static final String WEIGHT_RESULTS_ANALYTICS_MODIFIED_DATE = "ModifiedDate";
    public static final String WEIGHT_RESULTS_ANALYTICS_IS_SYNC = "IsSync";

    //Weight Test Result
    public static final String WEIGHT_TEST_RESULT = "CalibrationWeightTestResults";
    public static final String WEIGHT_TEST_RESULT_TEST_WEIGHT_ID = "TestWeightID";
    public static final String WEIGHT_TEST_RESULT_OEMID = "OEMID";
    public static final String WEIGHT_TEST_RESULT_MAC_ID = "MacID";
    public static final String WEIGHT_TEST_RESULT_LOGGER_ID = "LoggerID";
    public static final String WEIGHT_TEST_RESULT_TAG_ID = "TagID";
    public static final String WEIGHT_TEST_RESULT_WEIGHT = "Weight";
    public static final String WEIGHT_TEST_RESULT_OINUM = "OINumber";
    public static final String WEIGHT_TEST_RESULT_MODIFIED_DATE = "ModifiedDate";
    public static final String WEIGHT_TEST_RESULTS_IS_SYNC = "IsSync";

    //Comments Table
    public static final String COMMENTS_TABLE = "CalibrationComments";
    public static final String COMMENTS_ID = "CommentsID";
    public static final String COMMENTS_OEM_ID = "OEMID";
    public static final String COMMENTS_MAC_ID = "MacID";
    public static final String COMMENTS_LOGGER_ID = "LoggerID";
    public static final String COMMENTS_TAG_ID = "TagID";
    public static final String COMMENTS_COMMENTS_COLUMN = "Comments";
    public static final String COMMENTS_FUN_COMMENTS_1 = "FunComment1";
    public static final String COMMENTS_FUN_COMMENTS_2 = "FunComment2";
    public static final String COMMENTS_FUN_COMMENTS_3 = "FunComment3";
    public static final String COMMENTS_FUN_COMMENTS_4 = "FunComment4";
    public static final String COMMENTS_FUN_COMMENTS_5 = "FunComment5";
    public static final String COMMENTS_MODIFIED_DATE = "ModifiedDate";
    public static final String COMMENTS_IS_SYNC = "IsSync";




    //Observations Table
    public static final String OBSERVATIONS_TABLE = "CalibrationObservations";
    public static final String OBSERVATIONS_ID = "ObservationsID";
    public static final String OBSERVATIONS_OEM_ID = "OEMID";
    public static final String OBSERVATIONS_MAC_ID = "MacID";
    public static final String OBSERVATIONS_LOGGER_ID = "LoggerID";
    public static final String OBSERVATIONS_TAG_ID = "TagID";
    public static final String OBSERVATIONS_AUGER_COUNT = "AugerCount";
    public static final String OBSERVATIONS_AUGER_SPEED = "AugerSpeed";
    public static final String OBSERVATIONS_AUGER_PRE_AUGER = "AugerPreUsed";
    public static final String OBSERVATIONS_AUGER_NO_OF_AGITATORS = "AugerNumberOfAgitators";
    public static final String OBSERVATIONS_MACHINE_VERTICAL_TEFLON = "McVerticalTeflon";
    public static final String OBSERVATIONS_MACHINE_HORIZONTAL_LAW_OPENING = "McHorizontalJawOpeneing";
    public static final String OBSERVATIONS_MACHINE_HORIZONTAL_LAW_MOVEMENT = "McHorizontalJawMovement";
    public static final String OBSERVATIONS_MACHINE_VERTICAL_ALIGN_CENTER = "McVerticalAlignCenter";
    public static final String OBSERVATIONS_MACHINE_VERTICAL_LAW_MOVEMENT = "McVerticalJawMovement";
    public static final String OBSERVATIONS_PACKAGE_WIRE_CUP = "PkgWireCup";
    public static final String OBSERVATIONS_PACKAGE_EMPTY_POUCHES = "PkgEmptyPouches";
    public static final String OBSERVATIONS_PACKAGE_BAG_LENGTH_VARIATION = "PkgBaglengthVariation";
    public static final String OBSERVATIONS_PACKAGE_PAPER_SHIFTING = "PkgPaperShifting";
    public static final String OBSERVATIONS_PACKAGE_VERTICAL_OVERLAP = "PkgVerticalOverlap";
    public static final String OBSERVATIONS_PACKAGE_POUCH_SYMMETRY = "PkgPouchSymmetry";
    public static final String OBSERVATIONS_PACKAGE_OBSERV_SPEED = "ObservSpeed";
    public static final String OBSERVATIONS_SEAL_REJECTION_REASON = "SealRejectionReason";
    public static final String OBSERVATIONS_PACKAGE_IS_SYNC = "IsSync";




    public static final String QUERY_INSERT_TAGTBL = "INSERT INTO tags " +
            "(tagid,oinum,targetwt,fillertype,customer,machinetype," +
            "producttype,product,operator,targetwtunit,inspector," +
            "filmtype,productfeed,pouchwidth,pouchlength,agitatormotorfreq," +
            "agitatormode,agitatorod,agitatorid,agitatorpitch,targetacctype," +
            "targetaccval,mcserialnum,prbulkmain,targetspeed,customerproduct," +
            "head,airpressure,qtysetting,bglensetting,fillvalvegap,fillvfdfreq," +
            "horizcutsealsetting,horizbandsealsetting,vertsealsetting,augeragitatorfreq," +
            "augerfillvalve,augurpitch,pouchsealtype,pouchtemphzf,pouchtemphzb,pouchtempvert," +
            "pouchtempvertfl,pouchtempvertfr,pouchtempvertbl,pouchtempvertbr,pouchgusset," +
            "customerfilm,pouchfilmthickness,pouchprbulk,poucheqhorizserrations,pouchfillevent,negWt,fyyear,createddate)" +
            "values (?,?,?,?,?,?,?,?,?," +
            "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";






}
