package com.brainlines.weightloggernewversion.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.text.TextUtils;
import android.util.Log;

import com.brainlines.weightloggernewversion.activity.LoginActivity;
import com.brainlines.weightloggernewversion.model.IdAndModifiedDateModel;
import com.brainlines.weightloggernewversion.utils.Constants;
import com.brainlines.weightloggernewversion.utils.GlobalClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";
    SQLiteDatabase db;

    //private static final String DATABSE_NAME = "WeightLogger_POC";
    private static final int DATABSE_VERSION = 1;

    public DataBaseHelper(Context context) {
        super(context, GlobalClass.db_global_name, null, DATABSE_VERSION);
    }

//    public DataBaseHelper(Context context,String dbName) {
//        super(context, dbName, null, DATABSE_VERSION);
//    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Swas Tables
    }

    public SQLiteDatabase openDataBase (String absolutePath) throws SQLException {
        db = SQLiteDatabase.openDatabase(absolutePath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        return db;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Cursor getFGMobileSQLLiteTableList(){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT TableName,PKColumnName from FGMobileSQLLiteSyncTableList";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public void addMasterTableDataToLocalDb1(int Id, String tableName,
                                             String pkColumnName,String createdDate,
                                             boolean isSync,SQLiteDatabase sqLiteDatabase) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(new Date());
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ID", Id);
        contentValues.put("TableName", tableName);
        contentValues.put("PKColumnName", pkColumnName);
        contentValues.put("CreatedDate", strDate);
        contentValues.put("IsSync", isSync);
        long result = sqLiteDatabase.insert("FGMobileSQLLiteSyncTableList", null, contentValues);
        if (result == -1) {
            Log.d(TAG, "addPouchSymmetryDataToTable: Not Entered");
        } else {
            Log.d(TAG, "addPouchSymmetryDataToTable: Entered");
        }
    }

    public static String dbAbsolutePath(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.getAbsolutePath();
    }

    public boolean checkDataBase(String absolutePath) {
        boolean checkDB = false;
        try {
            File file = new File(absolutePath);
            checkDB = file.exists();
        } catch(SQLiteException e) {
            Log.d(TAG, e.getMessage());
        }
        return checkDB;
    }

    public void createTableFromServerData(JSONArray dataArray) throws JSONException {
        String dropTable,createTable;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        for (int i = 0 ; i < dataArray.length() ; i++){
            if (!dataArray.getJSONObject(i).getString("TABLE_NAME").contains("mst")){
                if(dataArray.getJSONObject(i).getString("TABLE_NAME").equals("ProductionTag")){
                    dropTable = dataArray.getJSONObject(i).getString("SQL_DROP_TABLE");
                    createTable = dataArray.getJSONObject(i).getString("SQL_CREATE_TABLE").substring(0, dataArray.getJSONObject(i).getString("SQL_CREATE_TABLE").length()-1 - 3).trim() +  " , \r\n [BatchStatus] varchar(250) NULL  , \r\n [IsSync] BIT NULL \r\n);\r\n";
                    sqLiteDatabase.execSQL(dropTable);
                    sqLiteDatabase.execSQL(createTable);}
                else
                {
                    dropTable = dataArray.getJSONObject(i).getString("SQL_DROP_TABLE");
                    createTable = dataArray.getJSONObject(i).getString("SQL_CREATE_TABLE").substring(0, dataArray.getJSONObject(i).getString("SQL_CREATE_TABLE").length()-1 - 3).trim() + " , \r\n [IsSync] BIT NULL \r\n);\r\n";
                    sqLiteDatabase.execSQL(dropTable);
                    sqLiteDatabase.execSQL(createTable);
                }

            }
           /* else if(dataArray.getJSONObject(i).getString("TABLE_NAME").equals("ProductionTag")){
                dropTable = dataArray.getJSONObject(i).getString("SQL_DROP_TABLE");
                createTable = dataArray.getJSONObject(i).getString("SQL_CREATE_TABLE").substring(0, dataArray.getJSONObject(i).getString("SQL_CREATE_TABLE").length()-1 - 3).trim() +  " , \r\n [BatchStatus] varchar(250) NULL  , \r\n [IsSync] BIT NULL \r\n);\r\n";
                sqLiteDatabase.execSQL(dropTable);
                sqLiteDatabase.execSQL(createTable);
            } */else {
                dropTable = dataArray.getJSONObject(i).getString("SQL_DROP_TABLE");
                createTable = dataArray.getJSONObject(i).getString("SQL_CREATE_TABLE");
                sqLiteDatabase.execSQL(dropTable);
                sqLiteDatabase.execSQL(createTable);
            }
        }
    }

    public static File dbFilePath(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile;
    }

    public Cursor getUserName() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT * FROM " + Constants.USER_LOGIN_TABLE;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getProductionGrammageValues1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT * FROM " + Constants.PRODUCTION_KG_VALUES_MASTER_TABLES;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getProductionGrammageValues() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT * FROM " + Constants.PRODUCTION_KG_VALUES_MASTER_TABLES;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public void deleteAllUSersFromLocalDB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Constants.USER_LOGIN_TABLE, null, null);
        db.close();
    }

    public boolean addDataLoggersToLocalDB1(String logger_id, String oemId, String logger_serial_num, String logger_name, String logger_mac_id, String logger_ip_address, String logger_location, String logger_create_timestamp, String logger_create_by, String logger_update_timestamp, String logger_modifiedBy, boolean logger_is_active, String logger_in_use,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.OEMID, oemId);
        contentValues.put(Constants.LOGGER_SERAIL_NUM, logger_serial_num);
        contentValues.put(Constants.LOGGER_NAME, logger_name);
        contentValues.put(Constants.LOGGER_IP_ADDRESS, logger_mac_id);
        contentValues.put(Constants.LOGGER_MAC_ID, logger_ip_address);
        contentValues.put(Constants.LOGGER_LOCATION, logger_location);
        contentValues.put(Constants.LOGGER_CREATE_TIMESTAMP, logger_create_timestamp);
        contentValues.put(Constants.LOGGER_CREATED_BY, logger_create_by);
        contentValues.put(Constants.LOGGER_IS_ACTIVE, logger_is_active);
        contentValues.put(Constants.LOGGER_IN_USE, logger_in_use);
        long result = sqLiteDatabase.insert(Constants.DATA_LOGGER_TABLE, null, contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean addDataLoggersToLocalDB(String logger_id, String oemId, String logger_serial_num, String logger_name, String logger_mac_id, String logger_ip_address, String logger_location, String logger_create_timestamp, String logger_create_by, String logger_update_timestamp, String logger_modifiedBy, boolean logger_is_active, String logger_in_use) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.OEMID, oemId);
        contentValues.put(Constants.LOGGER_SERAIL_NUM, logger_serial_num);
        contentValues.put(Constants.LOGGER_NAME, logger_name);
        contentValues.put(Constants.LOGGER_IP_ADDRESS, logger_mac_id);
        contentValues.put(Constants.LOGGER_MAC_ID, logger_ip_address);
        contentValues.put(Constants.LOGGER_LOCATION, logger_location);
        contentValues.put(Constants.LOGGER_CREATE_TIMESTAMP, logger_create_timestamp);
        contentValues.put(Constants.LOGGER_CREATED_BY, logger_create_by);
        contentValues.put(Constants.LOGGER_IS_ACTIVE, logger_is_active);
        contentValues.put(Constants.LOGGER_IN_USE, logger_in_use);
        long result = sqLiteDatabase.insert(Constants.DATA_LOGGER_TABLE, null, contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getDataLoggers1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT * FROM " + Constants.DATA_LOGGER_TABLE;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getDataLoggers() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT * FROM " + Constants.DATA_LOGGER_TABLE;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getMachineModel1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.MACHINE_TYPE_MACHINE_NAME + "," + Constants.MACHINE_TYPE_PRODUCT_TYPE_ID + " from " + Constants.MACHINE_TYPE_TABLE + " where " + Constants.MACHINE_TYPE_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getMachineModel() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.MACHINE_TYPE_MACHINE_NAME + "," + Constants.MACHINE_TYPE_PRODUCT_TYPE_ID + " from " + Constants.MACHINE_TYPE_TABLE + " where " + Constants.MACHINE_TYPE_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getProducts1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.MASTER_PRODUCTS_PRODUCTS + " from " + Constants.MASTER_PRODUCTS_TABLE + " where " + Constants.MASTER_PRODUCTS_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getProducts() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.MASTER_PRODUCTS_PRODUCTS + " from " + Constants.MASTER_PRODUCTS_TABLE + " where " + Constants.MASTER_PRODUCTS_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getTargetAccuracyType1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.TARGET_ACCURACY + " from " + Constants.TARGET_ACCURACY_TABLE + " where " + Constants.TARGET_ACCURACY_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getTargetAccuracyType() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.TARGET_ACCURACY + " from " + Constants.TARGET_ACCURACY_TABLE + " where " + Constants.TARGET_ACCURACY_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getHead1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.MASTER_HEAD_HEAD_TYPE_ + " from " + Constants.MASTER_HEAD_TABLE + " where " + Constants.FILLER_TYPE_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getHead() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.MASTER_HEAD_HEAD_TYPE_ + " from " + Constants.MASTER_HEAD_TABLE + " where " + Constants.FILLER_TYPE_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getMasterFillersType1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.FILLER_TYPE_FILTER_TYPE + " from " + Constants.MASTER_FILLER_TYPE_TABLE + " where " + Constants.FILLER_TYPE_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getMasterFillersType() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.FILLER_TYPE_FILTER_TYPE + " from " + Constants.MASTER_FILLER_TYPE_TABLE + " where " + Constants.FILLER_TYPE_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getSealType1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.SEAL_TYPE + " from " + Constants.SEAL_TYPE_TABLE + " where " + Constants.SEAL_TYPE_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getSealType() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.SEAL_TYPE + " from " + Constants.SEAL_TYPE_TABLE + " where " + Constants.SEAL_TYPE_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public void updateMstLoggerTableForLoggerStatus1(String loggerName,SQLiteDatabase sqLiteDatabase) {
        //update mstLogger set IsInUse='Yes' where LoggerName='DiA0AA08'
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.DATA_LOGGER_TABLE + " set " + Constants.LOGGER_IN_USE + "='" + "true'" + " where " + Constants.LOGGER_NAME + "='" + loggerName + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updateMstLoggerTableForLoggerStatus(String loggerName) {
        //update mstLogger set IsInUse='Yes' where LoggerName='DiA0AA08'
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.DATA_LOGGER_TABLE + " set " + Constants.LOGGER_IN_USE + "='" + "true'" + " where " + Constants.LOGGER_NAME + "='" + loggerName + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getLoggerStatusFromLoggerTable1(String loggerName,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        //Select IsInUse from mstLogger where LoggerName='DiA0AA08'
        String quwry = "SELECT " + Constants.LOGGER_IN_USE + " from " + Constants.DATA_LOGGER_TABLE + " where " + Constants.LOGGER_NAME + "='" + loggerName + "'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getLoggerStatusFromLoggerTable(String loggerName) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        //Select IsInUse from mstLogger where LoggerName='DiA0AA08'
        String quwry = "SELECT " + Constants.LOGGER_IN_USE + " from " + Constants.DATA_LOGGER_TABLE + " where " + Constants.LOGGER_NAME + "='" + loggerName + "'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getUoMFromLocalDB1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String qwery = "SELECT DISTINCT " + Constants.UOM_PRODUCT_UOM + " from " + Constants.MASTER_UOM_TABLE + " where " + Constants.UOM_PRODUCT_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(qwery, null);
        return keys;
    }

    public Cursor getUoMFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String qwery = "SELECT DISTINCT " + Constants.UOM_PRODUCT_UOM + " from " + Constants.MASTER_UOM_TABLE + " where " + Constants.UOM_PRODUCT_IS_ACTIVE + "='" + true +"'";
        Cursor keys = sqLiteDatabase.rawQuery(qwery, null);
        return keys;
    }

    public Cursor getTagInfo1(String tagid,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_TAGS_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getTagInfo(String tagid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_TAGS_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getTargetFyyear1(String oinum,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select distinct" + "(" + Constants.C_TAG_FY_YEAR + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getTargetFyyear(String oinum) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select distinct" + "(" + Constants.C_TAG_FY_YEAR + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getTargetWeight1(String oinum,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select distinct" + "(" + Constants.C_TAG_TARGET_WIGHT + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getTargetWeight(String oinum) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select distinct" + "(" + Constants.C_TAG_TARGET_WIGHT + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor gettagid(String oinum) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + "(" + Constants.C_TAG_TAGS_ID + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getHead1(String oinum,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select distinct" + "(" + Constants.C_TAG_HEAD + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getHead(String oinum) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select distinct" + "(" + Constants.C_TAG_HEAD + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getHeadFromCTagsTable(String oiNumber) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select distinct" + "(" + Constants.C_TAG_HEAD + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oiNumber + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getFyYearFromCTagsTable(String oiNumber) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select distinct" + "(" + Constants.C_TAG_FY_YEAR + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oiNumber + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getTargetWeightFromCTagsTable(String oiNumber) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        //select distinct(TargetWeight) from C_Tags where OINumber = 'so156780'
        String query = "select distinct" + "(" + Constants.C_TAG_TARGET_WIGHT + ")" + " from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oiNumber + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateWeightTestStatusTable1(String status, String tagId, SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_WEIGHT_TEST_STATUS + "='" + status + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updateWeightTestStatusTable(String status, String tagId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_WEIGHT_TEST_STATUS + "='" + status + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updateBaglengthstatus1(String status, String tagId,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_BAG_STATUS + "='" + status + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updateBaglengthstatus(String status, String tagId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_BAG_STATUS + "='" + status + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updatepapershiftstatus1(String status, String tagId,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_PAPER_STATUS + "='" + status + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updatepapershiftstatus(String status, String tagId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_PAPER_STATUS + "='" + status + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagId + "'";
        sqLiteDatabase.execSQL(query);
    }

   public Cursor getTagInfoAgainstoinum1(String oinum,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getTagInfoAgainstoinum(String oinum) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

   public Cursor getTagInfoRestartAgainstoinum(String oinum) {
       String status = "RESUME";
       SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
       String query = "SELECT * from " + Constants.C_TAGS_TABLE + " where " + Constants.C_TAG_OEM_NUMBER + "='" + oinum + "'"+" and "+ Constants.C_TAG_OVERALL_STATUS + "='" + status + "'";
       Cursor keys = sqLiteDatabase.rawQuery(query, null);
       return keys;
   }

    public Cursor getWeightsFromWeightAnalytics(String oiNumber) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.WEIGHT_TEST_RESULT_WEIGHT + " from " + Constants.WEIGHT_TEST_RESULT + " where " + Constants.WEIGHT_TEST_RESULT_OINUM + "='" + oiNumber + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getCalculatedValuesFromWeightResultsAnalytics1(String tagid,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.WEIGHT_RESULTS_ANALYTICS_MIN_READING + "," + Constants.WEIGHT_RESULTS_ANALYTICS_MAX_READING + "," +
                Constants.WEIGHT_RESULTS_ANALYTICS_RANGE_READING + "," + Constants.WEIGHT_RESULTS_ANALYTICS_MEAN_READING + "," +
                Constants.WEIGHT_RESULTS_ANALYTICS_SD_READING + "," + Constants.WEIGHT_RESULTS_ANALYTICS_RUN_STATUS + "," +
                Constants.WEIGHT_RESULTS_ANALYTICS_GM + "," + Constants.WEIGHT_RESULTS_ANALYTICS_GM_PERCENT + "," +
                Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD + "," + Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD_PERCENT +
                " from " + Constants.WEIGHT_RESULTS_ANALYTICS_TABLE + " where " + Constants.WEIGHT_RESULTS_ANALYTICS_TAG_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getCalculatedValuesFromWeightResultsAnalytics(String tagid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.WEIGHT_RESULTS_ANALYTICS_MIN_READING + "," + Constants.WEIGHT_RESULTS_ANALYTICS_MAX_READING + "," +
                Constants.WEIGHT_RESULTS_ANALYTICS_RANGE_READING + "," + Constants.WEIGHT_RESULTS_ANALYTICS_MEAN_READING + "," +
                Constants.WEIGHT_RESULTS_ANALYTICS_SD_READING + "," + Constants.WEIGHT_RESULTS_ANALYTICS_RUN_STATUS + "," +
                Constants.WEIGHT_RESULTS_ANALYTICS_GM + "," + Constants.WEIGHT_RESULTS_ANALYTICS_GM_PERCENT + "," +
                Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD + "," + Constants.WEIGHT_RESULTS_ANALYTICS_GM_SD_PERCENT +
                " from " + Constants.WEIGHT_RESULTS_ANALYTICS_TABLE + " where " + Constants.WEIGHT_RESULTS_ANALYTICS_TAG_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getBaglengthReading(String oiNumber) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.BAG_LENGTH_VARIATION_LENGTH + " from " + Constants.BAG_LENGTH_VARIATION_RESULT + " where " + Constants.BAG_LENGTH_VARIATION_OINUM + "='" + oiNumber + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getCalculatedValuesFromBaglengthAnalytics1(String tagid,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.BAG_LEN_ANA_MIN_READING + "," + Constants.BAG_LEN_ANA_MAX_READING +
                "," + Constants.BAG_LEN_ANA_RANGE_READING + "," + Constants.BAG_LEN_ANA_RUN_STATUS + "," + Constants.BAG_LEN_ANA_MEAN_READING +
                " from " + Constants.BAG_LENGTH_ANALYTICS_TABLE + " where " + Constants.BAG_LEN_ANA_TAG_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getCalculatedValuesFromBaglengthAnalytics(String tagid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.BAG_LEN_ANA_MIN_READING + "," + Constants.BAG_LEN_ANA_MAX_READING +
                "," + Constants.BAG_LEN_ANA_RANGE_READING + "," + Constants.BAG_LEN_ANA_RUN_STATUS + "," + Constants.BAG_LEN_ANA_MEAN_READING +
                " from " + Constants.BAG_LENGTH_ANALYTICS_TABLE + " where " + Constants.BAG_LEN_ANA_TAG_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getpapershiftReading(String oiNumber) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.PAPER_SHIFT_RESULTS_SHIFT + " from " + Constants.PAPER_SHIFT_RESULTS + " where " + Constants.PAPER_SHIFT_RESULTS_OINUM + "='" + oiNumber + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getCalculatedValuesFromPaperShiftAnalytics1(String tagid,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.PAPER_SHIFT_ANALYTICS_MIN_READING + "," + Constants.PAPER_SHIFT_ANALYTICS_MAX_READING +
                "," + Constants.PAPER_SHIFT_ANALYTICS_RANGE_READING + "," + Constants.PAPER_SHIFT_ANALYTICS_RUN_STATUS + "," + Constants.PAPER_SHIFT_ANALYTICS_MEAN_READING +
                " from " + Constants.PAPER_SHIFT_ANALYTICS_TABLE + " where " + Constants.PAPER_SHIFT_ANALYTICS_TAG_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getCalculatedValuesFromPaperShiftAnalytics(String tagid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.PAPER_SHIFT_ANALYTICS_MIN_READING + "," + Constants.PAPER_SHIFT_ANALYTICS_MAX_READING +
                "," + Constants.PAPER_SHIFT_ANALYTICS_RANGE_READING + "," + Constants.PAPER_SHIFT_ANALYTICS_RUN_STATUS + "," + Constants.PAPER_SHIFT_ANALYTICS_MEAN_READING +
                " from " + Constants.PAPER_SHIFT_ANALYTICS_TABLE + " where " + Constants.PAPER_SHIFT_ANALYTICS_TAG_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getTagIdFromCTagsTable(String oiNum) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "Select " + Constants.C_TAG_TAGS_ID + " from " + Constants.C_TAGS_TABLE + " Where " + Constants.C_TAG_OEM_NUMBER + "='" + oiNum + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getRejectionReasonsFromRable1(SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.SEAL_REJECTION_REASON_REASON_DESC + " from " + Constants.SEAL_REJECTION_REASON_MASTER_TABLE;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getRejectionReasonsFromTable() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT " + Constants.SEAL_REJECTION_REASON_REASON_DESC + " from " + Constants.SEAL_REJECTION_REASON_MASTER_TABLE;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public void updateCtagsTableFromNewTestObservationPage1(String tagID, String dropStatus,
                                                            String sealStatus, String targetSpeedStatus,
                                                            SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_DROP_STATUS + "='" + dropStatus + "'," + Constants.C_TAG_SEAL_STATUS + "='" + sealStatus + "'," + Constants.C_TAG_TARGET_SPEED_STATUS + "='" + targetSpeedStatus + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagID + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updateCtagsTableFromNewTestObservationPage(String tagID, String dropStatus, String sealStatus, String targetSpeedStatus) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_DROP_STATUS + "='" + dropStatus + "'," + Constants.C_TAG_SEAL_STATUS + "='" + sealStatus + "'," + Constants.C_TAG_TARGET_SPEED_STATUS + "='" + targetSpeedStatus + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagID + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getBagLengthAnalyticsFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.BAG_LENGTH_ANALYTICS_TABLE + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateBaglengthAnalyticsTable(String bagLengthAnaId, String yes) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.BAG_LENGTH_ANALYTICS_TABLE + " set " + Constants.BAG_LEN_ANA_IS_SYNC + "='" + yes + "'" + " where " + Constants.BAG_LEN_ANA_ID + "='" + bagLengthAnaId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getAllDataFromBagLengthVariationResults() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.BAG_LENGTH_VARIATION_RESULT + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateBaglengthVarResultsTable(String bagLengthAnaId, String yes) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.BAG_LENGTH_VARIATION_RESULT + " set " + Constants.BAG_LENGTH_VARIATION_IS_SYNC + "='" + yes + "'" + " where " + Constants.BAG_LENGTH_VARIATION_BAG_LENGTH_ID + "='" + bagLengthAnaId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getPaperShiftResultsDataFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.PAPER_SHIFT_RESULTS + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updatePaperShiftResultsTable(String paperShiftId, String yes) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.PAPER_SHIFT_RESULTS + " set " + Constants.PAPER_SHIFT_RESULTS_IS_SYNC + "='" + yes + "'" + " where " + Constants.PAPER_SHIFT_RESULTS_PAPER_SHIFT_ID + "='" + paperShiftId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getPaperShiftAnalyticsDataFromFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.PAPER_SHIFT_ANALYTICS_TABLE + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updatePaperShiftAnalyticsTable(String paperShiftId, String yes) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.PAPER_SHIFT_ANALYTICS_TABLE + " set " + Constants.PAPER_SHIFT_ANALYTICS_IS_SYNC + "='" + yes + "'" + " where " + Constants.PAPER_SHIFT_ANALYTICS_ID + "='" + paperShiftId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getWeightTestResultsFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.WEIGHT_TEST_RESULT + " where IsSync = 0 "  ;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateWeightTestResultTable(String testWeightID, String yes) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.WEIGHT_TEST_RESULT + " set " + Constants.WEIGHT_TEST_RESULTS_IS_SYNC + "='" + yes + "'" + " where " + Constants.WEIGHT_TEST_RESULT_TEST_WEIGHT_ID + "='" + testWeightID + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getWeightTestAnaFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.WEIGHT_RESULTS_ANALYTICS_TABLE + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateWeightResultsAnaTable(String weightResultsAnaId, String yes) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.WEIGHT_RESULTS_ANALYTICS_TABLE + " set " + Constants.WEIGHT_RESULTS_ANALYTICS_IS_SYNC + "='" + yes + "'" + " where " + Constants.WEIGHT_RESULTS_ANALYTICS_ID + "='" + weightResultsAnaId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getObservationsResultsFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.OBSERVATIONS_TABLE + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateObservationsTable(String observationsId, String yes) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.OBSERVATIONS_TABLE + " set " + Constants.OBSERVATIONS_PACKAGE_IS_SYNC + "='" + yes + "'" + " where " + Constants.OBSERVATIONS_ID + "='" + observationsId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getCommentsFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.COMMENTS_TABLE + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateCommentsTable(String commentsID, String yes) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.COMMENTS_TABLE + " set " + Constants.COMMENTS_IS_SYNC + "='" + yes + "'" + " where " + Constants.COMMENTS_ID + "='" + commentsID + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getAllCTagsDataFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.C_TAGS_TABLE + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateCTagsTable(String id, String s) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAGS_IS_SYNC + "='" + s + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + id + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getSelectedProductionTagDetails(String strdate, String str_shift) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.PRODUCTION_TAG + "," + Constants.PRODUCTION_TAG_PRODUCT_ID + "," +
                Constants.PRODUCTION_TAG_GM_PER_POUCH + "," + Constants.PRODUCTION_TAG_UNIT + "," +
                Constants.PRODUCTION_TAG_TOTAL_QTY + "," + Constants.PRODUCTION_TAG_MODIFIED_DATE + "," +
                Constants.PRODUCTION_TAG_SHIFT + "," + Constants.PRODUCTION_TAG_IS_NEGATIVE_ALLOWED +
                " from " + Constants.PRODUCTION_TAG_TABLE + " where " + Constants.PRODUCTION_TAG_MODIFIED_DATE + "='" + strdate + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updataPWeightReadingsTable(String id, String s) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.P_WEIGHT_READING_TABLE + " set " + Constants.P_WEIGHT_READING_IS_SYNC + "='" + s + "'" + " where " + Constants.P_WEIGHT_READING_BATCHID + "='" + id + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updataPWeightReportTable(String id, String s) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.PRODUCTION_WEIGHT_REPORT + " set " + Constants.PRODUCTION_WEIGHT_REPORT_IS_SYNC + "='" + s + "'" + " where " + Constants.PRODUCTION_WEIGHT_REPORT_BATCH_ID + "='" + id + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updatePTagResult(String id, String s) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.P_TAG_RESULT_TABLE + " set " + Constants.P_TAG_RESULT_IS_SYNC + "='" + s + "'" + " where " + Constants.P_TAG_RESULT_TAG_ID + "='" + id + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getAllProductionTagData() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.PRODUCTION_TAG_TABLE + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getAllPweightReadingTable() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.P_WEIGHT_READING_TABLE + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getAllPWeightReportDataTable() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.PRODUCTION_WEIGHT_REPORT + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getAllPTagResultDataTable() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.P_TAG_RESULT_TABLE + " where IsSync = 0 ";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public int getproductiontagCount() {
        String countQuery = "SELECT  * FROM " + Constants.PRODUCTION_TAG_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void updataProductionTagTable(String id, String s) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.PRODUCTION_TAG_TABLE + " set " + Constants.PRODUCTION_TAG_IS_SYNC + "='" + s + "'" + " where " + Constants.PRODUCTION_TAG_BATCH_ID + "='" + id + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getProductionWeightReading(String tagid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select " + Constants.P_WEIGHT_READING_WEIGHT + " from " + Constants.P_WEIGHT_READING_TABLE + " where " + Constants.P_TAG_RESULT_TAG_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateCTagsTableWithModifiedDate(String id, String s, String modifiedDate) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set "
                + Constants.C_TAGS_IS_SYNC + "='" + s + "'" + ","
                + Constants.C_TAG_MODIFIED_DATE + "='" + modifiedDate + "'"
                + " where " + Constants.C_TAG_TAGS_ID + "='" + id + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updateCtagsTableWithObserSpeed(String rejectionReason, String id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set "
                + Constants.C_TAG_SEAL_REJECTION_REASON + "='" + rejectionReason + "'"
                + " where " + Constants.C_TAG_TAGS_ID + "='" + id + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getMasterProductionTolerance() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.PRODUCTION_TOLERANCE_MASTER;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getMasterProductionRejectionReason() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.PRODUCTION_REASON_MASTER;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getProductionBatchDetails(String selectdate,String product) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.PRODUCTION_TAG_TABLE + " where " + Constants.PRODUCTION_TAG_MODIFIED_DATE + " LIKE '%" + selectdate + "%'"  + " and " + Constants.PRODUCTION_TAG_PRODUCT_ID + "='" + product + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getProductionBatchWeightReport(String batchid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.PRODUCTION_WEIGHT_REPORT + " where " + Constants.PRODUCTION_WEIGHT_REPORT_BATCH_ID + "='" + batchid + "'" ;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getProductionweightReading(String batchid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.P_WEIGHT_READING_TABLE + " where " + Constants.P_WEIGHT_READING_BATCHID + "='" + batchid + "'" ;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

   /* public Cursor getPausedTestBatchId(String status) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.P_TAG_RESULT_TABLE + " where " + Constants.P_TAG_RESULT_STATUS_ABORTED + "='" + status + "'" ;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }*/

    public Cursor getPausedTestBatchId(String status) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.PRODUCTION_TAG_TABLE + " where " + Constants.PRODUCTION_TAG_BATCH_STATUS + "='" + status + "'" ;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getProductionPausedBatchDetails(String batchid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.PRODUCTION_TAG_TABLE + " where " + Constants.PRODUCTION_TAG_BATCH_ID + "='" + batchid + "'" ;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getProductionBatchDetails(String batchid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * from " + Constants.PRODUCTION_TAG_TABLE + " where " + Constants.PRODUCTION_TAG_BATCH_ID + "='" + batchid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateOverallStatusForTag1(String status, String tagId,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_OVERALL_STATUS + "='" + status + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updateOverallStatusForTag(String status, String tagId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.C_TAGS_TABLE + " set " + Constants.C_TAG_OVERALL_STATUS + "='" + status + "'" + " where " + Constants.C_TAG_TAGS_ID + "='" + tagId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public void updateBatchStatusForBatchid(String status, String batchid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + Constants.PRODUCTION_TAG_TABLE + " set " + Constants.PRODUCTION_TAG_BATCH_STATUS + "='" + status + "'" + " where " + Constants.PRODUCTION_TAG_BATCH_ID + "='" + batchid + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getTargerSpeedFromC_TagTable1(String tagid,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT " + Constants.C_TAG_TARGET_SPEED + " from " +Constants.C_TAGS_TABLE+  " where " + Constants.C_TAG_TAGS_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getTargerSpeedFromC_TagTable(String tagid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT " + Constants.C_TAG_TARGET_SPEED + " from " +Constants.C_TAGS_TABLE+  " where " + Constants.C_TAG_TAGS_ID + "='" + tagid + "'";
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void deletedatafromPweighteadingTable(String batchId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "DELETE FROM " + Constants.P_WEIGHT_READING_TABLE + " where " + Constants.P_WEIGHT_READING_BATCHID + "='" + batchId + "'";
        sqLiteDatabase.execSQL(query);
    }

    public Cursor getPouchSymmetry1(SQLiteDatabase sqLiteDatabase){
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT "+Constants.MASTER_POUCH_DESC+" FROM " + Constants.MASTER_POUCH_SYMMETRY_TABLE +
                " WHERE "+Constants.MASTER_POUCH_IS_ACTIVE+"='" + 1 + "'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getPouchSymmetry(){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT "+Constants.MASTER_POUCH_DESC+" FROM " + Constants.MASTER_POUCH_SYMMETRY_TABLE +
                " WHERE "+Constants.MASTER_POUCH_IS_ACTIVE+"='" + 1 + "'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor GetBaglengthToleranceAccordingtoweight() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT "+Constants.TOLERANCE_MIN_BAG_LENGTH_SAMPLES+" FROM " + Constants.MASTER_POUCH_SYMMETRY_TABLE +
                " WHERE "+Constants.MASTER_POUCH_IS_ACTIVE+"='" + 1 + "'";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;

    }

    public Cursor getMasterTablesFromLocalDB() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT * FROM "+ Constants.LIST_OF_MASTER_TABLES;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getLoggerList(){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT DISTINCT * FROM "+ Constants.DATA_LOGGER_TABLE;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public Cursor getIdAndModifiedDate(String tableName,String id, String modifiedDate){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT "+id+","+modifiedDate+" FROM " + tableName;
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        return keys;
    }

    public void updateDataInLocalDB(String tableName, String MasterCheckID, List<IdAndModifiedDateModel> actionList,
                                    String action, JSONArray serverDataArray, ArrayList<String> keyList) throws JSONException {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        int idColumnPos = keyList.indexOf(MasterCheckID);
        switch (action){
            case "U":
                ArrayList<String> idList = new ArrayList<>();
                for (int k = 0 ; k < serverDataArray.length() ; k++){
                    idList.add(serverDataArray.getJSONObject(k).getString(keyList.get(idColumnPos)));
                }
                Log.d(TAG, "updateDataInLocalDB: KeyList -> " +keyList.toString());
                for (int i = 0 ; i < actionList.size() ; i++){
                    String query = "UPDATE " + tableName + " set " ;
                    String subQuery = "";
                    String id = actionList.get(i).getId();
                    int index = idList.indexOf(id);
                    JSONObject currentObject = serverDataArray.getJSONObject(index);
                    for (int j = 0 ; j < keyList.size() ; j++){
                        String columnName = keyList.get(j);
                        subQuery+=columnName + "='" + currentObject.get(columnName) + "'" ;
                        if (j+1 < keyList.size()){
                            subQuery+= "," ;
                        }
                    }
                    query+=subQuery+" where " + MasterCheckID + "='" + id + "'";
                    Log.d(TAG, "updateDataInLocalDB: Query -> " +query);
                    sqLiteDatabase.execSQL(query);
                }
                break;
            case "D":
                String[] ids = new String[actionList.size()];
                for (int i = 0 ; i < actionList.size() ; i++) {
                    ids[i] = actionList.get(i).getId();
                }
                String whereClause = String.format(MasterCheckID + " in (%s)", new Object[] { TextUtils.join(",", Collections.nCopies(ids.length, "?")) });
                Log.d(TAG, "updateDataInLocalDB: WhereClause -> "+whereClause);
                sqLiteDatabase.delete(tableName, whereClause, ids);
                Log.d(TAG, "updateDataInLocalDB: Deleted -> "+ids);
                break;
            case "A":
                ArrayList<String> idListForAdd = new ArrayList<>();
                for (int k = 0 ; k < serverDataArray.length() ; k++){
                    idListForAdd.add(serverDataArray.getJSONObject(k).getString(keyList.get(idColumnPos)));
                }
                ContentValues contentValues = new ContentValues();
                for (int i = 0 ; i < actionList.size() ; i++){
                    String id = actionList.get(i).getId();
                    int index = idListForAdd.indexOf(id);
                    JSONObject currentObject = serverDataArray.getJSONObject(index);
                    for (int j = 0 ; j < keyList.size() ; j++){
                        String columnName = keyList.get(j);
                        contentValues.put(columnName,currentObject.get(columnName).toString());
                    }
                    long result = sqLiteDatabase.insert(tableName, null, contentValues);
                    if (result == -1) {
                        Log.d(TAG, "updateDataInLocalDB: Data Inserted -> False" );
                    } else {
                        Log.d(TAG, "updateDataInLocalDB: Data Inserted -> True");
                    }
                }
                Log.d(TAG, "updateDataInLocalDB: ");
                break;
            default:
                sqLiteDatabase.close();
                break;
        }
        sqLiteDatabase.close();
    }

    public int getTableCount(){
        int tableCount = 0;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String quwry = "SELECT count(*) as TableCount FROM sqlite_master WHERE type = 'table' AND name != 'android_metadata' AND name != 'sqlite_sequence';";
        Cursor keys = sqLiteDatabase.rawQuery(quwry, null);
        while (keys.moveToNext()){
            String c1 = keys.getString(0);
            tableCount = keys.getInt(0);
        }
        return tableCount;
    }

    public Cursor getweightResultdata() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + Constants.WEIGHT_TEST_RESULT;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public Cursor getCursorfromTableWhereIsSyncZero (String tablename) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + tablename + " where IsSync = 0 "  ;
        Cursor keys = sqLiteDatabase.rawQuery(query, null);
        return keys;
    }

    public void updateTableToSetIsSyncEqualtoOne1(String tablename,SQLiteDatabase sqLiteDatabase) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "UPDATE " + tablename + " set IsSync= 1  where  IsSync = 0 " ;
        sqLiteDatabase.execSQL(query);
    }
}