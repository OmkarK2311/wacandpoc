package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class LoggersFromDBModel implements Parcelable {
    String id;
    String oem_id;
    String serial_num;
    String logger_name;
    String mac_id;
    String ip_address;
    String logger_location;
    String modified_date,update_by_id;
    String create_timestamp;
    String create_by_id;
    String update_timestamp;
    String inuse;
    boolean isActive;

    public LoggersFromDBModel(String id, String oem_id, String serial_num, String logger_name, String mac_id, String ip_address, String logger_location, String modified_date, String update_by_id, String create_timestamp, String create_by_id, String update_timestamp, boolean isActive, String inuse) {
        this.id = id;
        this.oem_id = oem_id;
        this.serial_num = serial_num;
        this.logger_name = logger_name;
        this.mac_id = mac_id;
        this.ip_address = ip_address;
        this.logger_location = logger_location;
        this.modified_date = modified_date;
        this.update_by_id = update_by_id;
        this.create_timestamp = create_timestamp;
        this.create_by_id = create_by_id;
        this.update_timestamp = update_timestamp;
        this.isActive = isActive;
        this.inuse = inuse;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    protected LoggersFromDBModel(Parcel in) {
        id = in.readString();
        oem_id = in.readString();
        serial_num = in.readString();
        logger_name = in.readString();
        mac_id = in.readString();
        ip_address = in.readString();
        logger_location = in.readString();
        modified_date = in.readString();
        update_by_id = in.readString();
        create_timestamp = in.readString();
        create_by_id = in.readString();
        update_timestamp = in.readString();
        inuse = in.readString();
        isActive = in.readByte() != 0;
    }

    public static final Creator<LoggersFromDBModel> CREATOR = new Creator<LoggersFromDBModel>() {
        @Override
        public LoggersFromDBModel createFromParcel(Parcel in) {
            return new LoggersFromDBModel(in);
        }

        @Override
        public LoggersFromDBModel[] newArray(int size) {
            return new LoggersFromDBModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOem_id() {
        return oem_id;
    }

    public void setOem_id(String oem_id) {
        this.oem_id = oem_id;
    }

    public String getSerial_num() {
        return serial_num;
    }

    public void setSerial_num(String serial_num) {
        this.serial_num = serial_num;
    }

    public String getLogger_name() {
        return logger_name;
    }

    public void setLogger_name(String logger_name) {
        this.logger_name = logger_name;
    }

    public String getMac_id() {
        return mac_id;
    }

    public void setMac_id(String mac_id) {
        this.mac_id = mac_id;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getLogger_location() {
        return logger_location;
    }

    public void setLogger_location(String logger_location) {
        this.logger_location = logger_location;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public String getUpdate_by_id() {
        return update_by_id;
    }

    public void setUpdate_by_id(String update_by_id) {
        this.update_by_id = update_by_id;
    }

    public String getCreate_timestamp() {
        return create_timestamp;
    }

    public void setCreate_timestamp(String create_timestamp) {
        this.create_timestamp = create_timestamp;
    }

    public String getCreate_by_id() {
        return create_by_id;
    }

    public void setCreate_by_id(String create_by_id) {
        this.create_by_id = create_by_id;
    }

    public String getUpdate_timestamp() {
        return update_timestamp;
    }

    public void setUpdate_timestamp(String update_timestamp) {
        this.update_timestamp = update_timestamp;
    }



    public String getInuse() {
        return inuse;
    }

    public void setInuse(String inuse) {
        this.inuse = inuse;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(oem_id);
        parcel.writeString(serial_num);
        parcel.writeString(logger_name);
        parcel.writeString(mac_id);
        parcel.writeString(ip_address);
        parcel.writeString(logger_location);
        parcel.writeString(modified_date);
        parcel.writeString(update_by_id);
        parcel.writeString(create_timestamp);
        parcel.writeString(create_by_id);
        parcel.writeString(update_timestamp);
        parcel.writeString(inuse);
        parcel.writeByte((byte) (isActive ? 1 : 0));
    }


}
