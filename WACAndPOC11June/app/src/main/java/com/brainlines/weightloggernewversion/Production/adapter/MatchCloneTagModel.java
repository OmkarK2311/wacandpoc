package com.brainlines.weightloggernewversion.Production.adapter;

public class MatchCloneTagModel {

    String ProductionTag,ProductionTagID,Product,GmPerPouch,Unit,TotalQTY;

    public MatchCloneTagModel(String productionTag, String product, String gmPerPouch, String unit, String totalQTY) {
        ProductionTag = productionTag;
        Product = product;
        GmPerPouch = gmPerPouch;
        Unit = unit;
        TotalQTY = totalQTY;
    }

    public String getProductionTag() {
        return ProductionTag;
    }

    public void setProductionTag(String productionTag) {
        ProductionTag = productionTag;
    }

    public String getProductionTagID() {
        return ProductionTagID;
    }

    public void setProductionTagID(String productionTagID) {
        ProductionTagID = productionTagID;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getGmPerPouch() {
        return GmPerPouch;
    }

    public void setGmPerPouch(String gmPerPouch) {
        GmPerPouch = gmPerPouch;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getTotalQTY() {
        return TotalQTY;
    }

    public void setTotalQTY(String totalQTY) {
        TotalQTY = totalQTY;
    }
}
