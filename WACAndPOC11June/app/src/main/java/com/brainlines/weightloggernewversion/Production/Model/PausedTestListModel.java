package com.brainlines.weightloggernewversion.Production.Model;

public class PausedTestListModel {

    String ProductionTagID;
    //String ProductionTagVal;
    String ProductionTagVal;
    String OEMID;
    String MacID;
    String SO;
    String ProductID;
    String GmPerPouch;
    String Unit;
    String TotalQTY;
    String TargetAccurancyType;
    String TargetAccurancyValue;
    String IsNegAllowed;
    String TagModifiedDate,TagShift,TagNumberOfPouches,TagPositiveTolerance,NegativeTolerance;
    String IsSync;
    String batchid;
    String strUom;
    String strkgvalue;
    String strucl;
    String strlcl;

    public String getStrlcl() {
        return strlcl;
    }

    public void setStrlcl(String strlcl) {
        this.strlcl = strlcl;
    }

    public String getStrucl() {
        return strucl;
    }

    public void setStrucl(String strucl) {
        this.strucl = strucl;
    }

    public String getProductionTagID() {
        return ProductionTagID;
    }

    public void setProductionTagID(String productionTagID) {
        ProductionTagID = productionTagID;
    }

    public String getProductionTagVal() {
        return ProductionTagVal;
    }

    public void setProductionTagVal(String productionTagVal) {
        ProductionTagVal = productionTagVal;
    }

    public String getOEMID() {
        return OEMID;
    }

    public void setOEMID(String OEMID) {
        this.OEMID = OEMID;
    }

    public String getMacID() {
        return MacID;
    }

    public void setMacID(String macID) {
        MacID = macID;
    }

    public String getSO() {
        return SO;
    }

    public void setSO(String SO) {
        this.SO = SO;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public String getGmPerPouch() {
        return GmPerPouch;
    }

    public void setGmPerPouch(String gmPerPouch) {
        GmPerPouch = gmPerPouch;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }

    public String getTotalQTY() {
        return TotalQTY;
    }

    public void setTotalQTY(String totalQTY) {
        TotalQTY = totalQTY;
    }

    public String getTargetAccurancyType() {
        return TargetAccurancyType;
    }

    public void setTargetAccurancyType(String targetAccurancyType) {
        TargetAccurancyType = targetAccurancyType;
    }

    public String getTargetAccurancyValue() {
        return TargetAccurancyValue;
    }

    public void setTargetAccurancyValue(String targetAccurancyValue) {
        TargetAccurancyValue = targetAccurancyValue;
    }

    public String getIsNegAllowed() {
        return IsNegAllowed;
    }

    public void setIsNegAllowed(String isNegAllowed) {
        IsNegAllowed = isNegAllowed;
    }

    public String getTagModifiedDate() {
        return TagModifiedDate;
    }

    public void setTagModifiedDate(String tagModifiedDate) {
        TagModifiedDate = tagModifiedDate;
    }

    public String getTagShift() {
        return TagShift;
    }

    public void setTagShift(String tagShift) {
        TagShift = tagShift;
    }

    public String getTagNumberOfPouches() {
        return TagNumberOfPouches;
    }

    public void setTagNumberOfPouches(String tagNumberOfPouches) {
        TagNumberOfPouches = tagNumberOfPouches;
    }

    public String getTagPositiveTolerance() {
        return TagPositiveTolerance;
    }

    public void setTagPositiveTolerance(String tagPositiveTolerance) {
        TagPositiveTolerance = tagPositiveTolerance;
    }

    public String getNegativeTolerance() {
        return NegativeTolerance;
    }

    public void setNegativeTolerance(String negativeTolerance) {
        NegativeTolerance = negativeTolerance;
    }

    public String getIsSync() {
        return IsSync;
    }

    public void setIsSync(String isSync) {
        IsSync = isSync;
    }

    public String getBatchid() {
        return batchid;
    }

    public void setBatchid(String batchid) {
        this.batchid = batchid;
    }

    public String getStrUom() {
        return strUom;
    }

    public void setStrUom(String strUom) {
        this.strUom = strUom;
    }

    public String getStrkgvalue() {
        return strkgvalue;
    }

    public void setStrkgvalue(String strkgvalue) {
        this.strkgvalue = strkgvalue;
    }
}
