package com.brainlines.weightloggernewversion.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SpinnerMachinesModel implements Parcelable {

    String machineName;
    String productTypeID;

    public SpinnerMachinesModel(String machineName, String productTypeID) {
        this.machineName = machineName;
        this.productTypeID = productTypeID;
    }

    protected SpinnerMachinesModel(Parcel in) {
        machineName = in.readString();
        productTypeID = in.readString();
    }

    public static final Creator<SpinnerMachinesModel> CREATOR = new Creator<SpinnerMachinesModel>() {
        @Override
        public SpinnerMachinesModel createFromParcel(Parcel in) {
            return new SpinnerMachinesModel(in);
        }

        @Override
        public SpinnerMachinesModel[] newArray(int size) {
            return new SpinnerMachinesModel[size];
        }
    };

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getProductTypeID() {
        return productTypeID;
    }

    public void setProductTypeID(String productTypeID) {
        this.productTypeID = productTypeID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(machineName);
        dest.writeString(productTypeID);
    }

    @Override
    public String toString() {
        return machineName;
    }

//    @Override
//    public String toString() {
//        return productTypeID;
//    }
}
