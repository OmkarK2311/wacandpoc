package com.brainlines.weightloggernewversion.activity.restarttest;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import androidx.appcompat.app.AppCompatActivity;
import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.newtest.NewTestBagLengthActivity;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.utils.AppPreferences;

import java.util.ArrayList;

public class RestartTestActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "RestartLoggerList";

    ImageButton btn_restart_Get_target_weight,btn_sendBagLengthConfirmBtn;
    Spinner spinner_restart_target_weight,spinner_display_fy_year,spinner_display_head;
    EditText ed_restart_oinum;

    ArrayList<String> targetWeightList = new ArrayList<>();
    ArrayList<String> headList = new ArrayList<>();
    ArrayList<String> fyYearList = new ArrayList<>();
    ArrayList<String> grammage_values_list = new ArrayList<>();

    DataBaseHelper helper;

    String dataLoggerName,dataLoggerIp,oiNum,current_tagid;
    private String str_tarhetwt_uom = "";
    double str_kg_value_from_local_db;
    private double intkgvalue;
    private String dbName;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restart_test);

        initUI();
        getProductionGrammageValues();
    }

    private void initUI() {
        dbName = AppPreferences.getDatabseName(this);
//        helper = new DataBaseHelper(this,dbName);
        helper = new DataBaseHelper(this);
        db = helper.openDataBase(helper.dbAbsolutePath(this,dbName));

        dataLoggerName = getIntent().getStringExtra("LoggerName");
        dataLoggerIp = getIntent().getStringExtra("LoggerIP");

        btn_restart_Get_target_weight = findViewById(R.id.btn_restart_Get_target_weight);
        btn_sendBagLengthConfirmBtn = findViewById(R.id.btn_restart_submit);
        spinner_restart_target_weight = findViewById(R.id.spinner_restart_target_weight);
        spinner_display_fy_year = findViewById(R.id.spinner_display_fy_year);
        spinner_display_head = findViewById(R.id.spinner_display_head);
        ed_restart_oinum = findViewById(R.id.ed_restart_oinum);

        btn_restart_Get_target_weight.setOnClickListener(this);
        btn_sendBagLengthConfirmBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_restart_Get_target_weight:
                oiNum = ed_restart_oinum.getText().toString();
                getDataFor_TargetWeight_Head_FyYear();
                getTagInfoAgainstOinum();
                break;
            case R.id.btn_restart_submit:
                oiNum = ed_restart_oinum.getText().toString();
                Intent i = new Intent(this, RestartRecordBagLengthActivity.class);
                i.putExtra("LoggerName",dataLoggerName);
                i.putExtra("LoggerIP",dataLoggerIp);
                i.putExtra("current_tag_id",current_tagid);
                i.putExtra("so_oinum",oiNum);
                i.putExtra("weight_test_status","PASS");
                i.putExtra("targetwt_uom",str_tarhetwt_uom);
                i.putExtra("IntKgValue",intkgvalue);
                startActivity(i);
                break;
        }
    }

    private void getDataFor_TargetWeight_Head_FyYear() {
        String oiNumber = ed_restart_oinum.getText().toString().trim();

        getFyYear(oiNumber);
        ArrayAdapter<String> spFyYearList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, fyYearList);
        spFyYearList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spinner_display_fy_year.setAdapter(spFyYearList);

        getTargetWeight(oiNumber);
        ArrayAdapter<String> spTargetWeightList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, targetWeightList);
        spTargetWeightList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spinner_restart_target_weight.setAdapter(spTargetWeightList);

        getHeadFromDB(oiNumber);
        ArrayAdapter<String> spHeadTypeList = new ArrayAdapter<String>(this,R.layout.item_spinner_latyout, headList);
        spHeadTypeList.setDropDownViewResource(R.layout.item_spinner_latyout);
        spinner_display_head.setAdapter(spHeadTypeList);
    }

    private void getHeadFromDB(String oiNumber) {
        Cursor headType = helper.getHeadFromCTagsTable(oiNumber);
        while (headType.moveToNext()){
            String headTypeValue = headType.getString(0);
            headList.add(headTypeValue);
            Log.d(TAG, "getHeadFromDB: ");
        }
    }



    private void getTargetWeight(String oiNumber) {
        Cursor targetWeight = helper.getTargetWeightFromCTagsTable(oiNumber);
        while (targetWeight.moveToNext()){
            String targetWeightValue = targetWeight.getString(0);
            targetWeightList.add(targetWeightValue);
            Log.d(TAG, "getTargetWeight: ");
        }
    }

    private void getFyYear(String oiNumber) {
        Cursor fyYear = helper.getFyYearFromCTagsTable(oiNumber);
        while (fyYear.moveToNext()){
            String fyYearName = fyYear.getString(0);
            fyYearList.add(fyYearName);
            Log.d(TAG, "getFyYear: ");
        }
    }
    public void getTagInfoAgainstOinum()
    {
        Cursor taginfo = helper.getTagInfoRestartAgainstoinum(oiNum);
        while (taginfo.moveToNext()){

            String tagid= taginfo.getString(0);
            String oemid= taginfo.getString(1);
            String macid= taginfo.getString(2);
            String loggerid= taginfo.getString(3);
            String oinumber= taginfo.getString(5);
            String targetweight= taginfo.getString(6);
            String customername= taginfo.getString(8);
            String producttype= taginfo.getString(10);
            String product= taginfo.getString(11);
            String operator= taginfo.getString(12);
            String targetweightunit= taginfo.getString(13);
            String targetaccuracytype= taginfo.getString(24);
            String targetaccuracyvalue= taginfo.getString(25);
            String machinesrno= taginfo.getString(26);
            String productbulk_main= taginfo.getString(27);
            String targetspeed= taginfo.getString(28);
            String customer_product= taginfo.getString(29);
            String head= taginfo.getString(30);
            String machineType = taginfo.getString(9);
            //targetweightunit =str_tarhetwt_uom;
            str_tarhetwt_uom = targetweightunit;
            current_tagid = tagid;
            if (str_tarhetwt_uom.equals("Kg"))
            {

                String kgvalue = grammage_values_list.get(0);
                intkgvalue = Double.parseDouble(kgvalue);
            }




            String overall_status = taginfo.getString(58);
            String seal_quality = taginfo.getString(60);
            String drop_status = taginfo.getString(61);
            String speed1_status = taginfo.getString(62);
            String weight_status = taginfo.getString(63);
            String bag_status = taginfo.getString(64);
            String paper_status = taginfo.getString(65);

            current_tagid = tagid;

            Log.d("TAG", "getSealTypes: ");
        }
    }

    private void getProductionGrammageValues() {
        Cursor grammageValues = helper.getProductionGrammageValues();
        while (grammageValues.moveToNext()){
            String kgid = grammageValues.getString(0);
            String kgname = grammageValues.getString(1);
            String kgvalues = grammageValues.getString(2);

            grammage_values_list.add(kgvalues);
            Log.d("TAG", "getProductsUsedForTrial: ");
        }

    }
}
