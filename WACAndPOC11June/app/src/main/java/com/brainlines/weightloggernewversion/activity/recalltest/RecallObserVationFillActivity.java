package com.brainlines.weightloggernewversion.activity.recalltest;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;

import com.brainlines.weightloggernewversion.R;
import com.brainlines.weightloggernewversion.activity.LoggerFunctionActivity;
import com.brainlines.weightloggernewversion.broadcastreceiver.NetworkChangeReceiver;
import com.brainlines.weightloggernewversion.database.DataBaseHelper;
import com.brainlines.weightloggernewversion.model.ObservationsModel;
import com.brainlines.weightloggernewversion.utils.Constants;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@SuppressLint("Registered")
public class RecallObserVationFillActivity extends BaseActivity implements View.OnClickListener {

    EditText ed_auger_count,ed_auger_speed_rpm,ed_pre_auger_used,ed_no_of_agitator;
    EditText ed_vertical_overlap_mm,ed_observed_speed,ed_wire_cup;
    EditText ed_hz_jaw_movement,ed_vertical_assly_teflon_tape_shift,ed_hz_jaw_opening,ed_hz_cut_seal_Setting;
    Spinner spin_pouch_symmetry,spin_seal_failure_reasons;
    SwitchCompat switch_vert_jaw_movement,switch_vertical_alignment_centre;
    SwitchCompat switch_empty_pouches,switch_perfortation,switch_drop_test,switch_seal_quality;
    LinearLayout llAugerDetailForm,llMainDetailForm,llLiquidDetailForm;
    CardView cvMainDetails,cvLiquidDetails,cvAugerDetails;

    String str_auger_count,str_auger_speed_rpm,str_pre_auger_used,str_no_of_agitator;
    String str_vertical_overlap_mm,str_observed_speed,str_wire_cup;
    String str_hz_jaw_movement,str_vertical_assly_teflon_tape_shift,str_hz_jaw_opening,str_hz_cut_seal_Setting;

    ImageButton btn_enter_comments,btn_observation_abort;
    Button btn_sync;
    String dataloggerName,dataloggerIp,current_tag_id,so_oinum;
    ArrayList<ObservationsModel> observationsModelArrayList = new ArrayList<>();
    DataBaseHelper helper;
    private NetworkChangeReceiver networkChangeReceiver;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss.SSS");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recall_test_observations);
        initUi();
//        helper = new DataBaseHelper(this);
        dataloggerName = getIntent().getStringExtra("dataloggerName");
        dataloggerIp = getIntent().getStringExtra("dataloggerIp");
        current_tag_id = getIntent().getStringExtra("current_tag_id");
        so_oinum = getIntent().getStringExtra("so_oinum");
    }

    private void initUi()
    {
        spin_pouch_symmetry = findViewById(R.id.spin_pouch_symmetry);
        spin_seal_failure_reasons = findViewById(R.id.spin_seal_failure_reasons);

        switch_vert_jaw_movement = findViewById(R.id.switch_vert_jaw_movement);
        switch_vertical_alignment_centre = findViewById(R.id.switch_vertical_alignment_centre);
        switch_empty_pouches = findViewById(R.id.switch_empty_pouches);
        switch_perfortation = findViewById(R.id.switch_perfortation);
        switch_drop_test = findViewById(R.id.switch_drop_test);
        switch_seal_quality = findViewById(R.id.switch_seal_quality);


        ed_auger_count = findViewById(R.id.ed_auger_count);
        ed_auger_speed_rpm = findViewById(R.id.ed_auger_speed_rpm);
        ed_pre_auger_used = findViewById(R.id.ed_pre_auger_used);
        ed_no_of_agitator = findViewById(R.id.ed_no_of_agitator);
        ed_vertical_overlap_mm = findViewById(R.id.ed_vertical_overlap_mm);
        ed_observed_speed = findViewById(R.id.ed_observed_speed);
        ed_wire_cup = findViewById(R.id.ed_wire_cup);
        ed_hz_jaw_movement = findViewById(R.id.ed_hz_jaw_movement);
        ed_vertical_assly_teflon_tape_shift = findViewById(R.id.ed_vertical_assly_teflon_tape_shift);
        ed_hz_jaw_opening = findViewById(R.id.ed_hz_jaw_opening);
        ed_hz_cut_seal_Setting = findViewById(R.id.ed_hz_cut_seal_Setting);

        llAugerDetailForm = findViewById(R.id.llAugerDetailForm);
        llMainDetailForm = findViewById(R.id.llMainDetailForm);
        llLiquidDetailForm = findViewById(R.id.llLiquidDetailForm);
        cvMainDetails = findViewById(R.id.cvMainDetails);
        cvLiquidDetails = findViewById(R.id.cvLiquidDetails);
        cvAugerDetails = findViewById(R.id.cvAugerDetails);

        btn_enter_comments = findViewById(R.id.btn_enter_comments);
        btn_observation_abort = findViewById(R.id.btn_observation_abort);
        btn_sync = findViewById(R.id.btn_sync);
        btn_enter_comments.setOnClickListener(this);
        btn_observation_abort.setOnClickListener(this);
        btn_sync.setOnClickListener(this);
        cvMainDetails.setOnClickListener(this);
        cvLiquidDetails.setOnClickListener(this);
        cvAugerDetails.setOnClickListener(this);
        networkChangeReceiver = new NetworkChangeReceiver(this);
        registerNetworkBroadcastForNougat();

    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(networkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }

    private void getData()
    {
        str_auger_count = ed_auger_count.getText().toString();
        str_auger_speed_rpm = ed_auger_speed_rpm.getText().toString();
        str_pre_auger_used = ed_pre_auger_used.getText().toString();
        str_no_of_agitator = ed_no_of_agitator.getText().toString();
        str_vertical_overlap_mm = ed_vertical_overlap_mm.getText().toString();
        str_observed_speed = ed_observed_speed.getText().toString();
        str_wire_cup = ed_wire_cup.getText().toString();
        str_hz_jaw_movement = ed_hz_jaw_movement.getText().toString();
        str_vertical_assly_teflon_tape_shift = ed_vertical_assly_teflon_tape_shift.getText().toString();
        str_hz_jaw_opening = ed_hz_jaw_opening.getText().toString();
        str_hz_cut_seal_Setting = ed_hz_cut_seal_Setting.getText().toString();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_enter_comments:
                getData();
                InsertIntoDb();
                Intent i = new Intent(RecallObserVationFillActivity.this,Recall_comments_Activity.class);
                i.putExtra("LoggerName",dataloggerName);
                i.putExtra("LoggerIP",dataloggerIp);
                i.putExtra("current_tag_id",current_tag_id);
                i.putExtra("so_oinum",so_oinum);
                startActivity(i);
                break;
            case R.id.btn_abort:
                try {
                    abort();
                    Intent abort = new Intent(RecallObserVationFillActivity.this, LoggerFunctionActivity.class);
                    abort.putExtra("LoggerName",dataloggerName);
                    abort.putExtra("LoggerIP",dataloggerIp);
                    startActivity(abort);
                    finish();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            case R.id.cvMainDetails:
                if (llAugerDetailForm.getVisibility() == View.VISIBLE){
                    llAugerDetailForm.setVisibility(View.GONE);
                }else if (llAugerDetailForm.getVisibility() == View.GONE){
                    llAugerDetailForm.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.cvLiquidDetails:
                if (llMainDetailForm.getVisibility() == View.VISIBLE){
                    llMainDetailForm.setVisibility(View.GONE);
                }else if (llMainDetailForm.getVisibility() == View.GONE){
                    llMainDetailForm.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.cvAugerDetails:
                if (llLiquidDetailForm.getVisibility() == View.VISIBLE){
                    llLiquidDetailForm.setVisibility(View.GONE);
                }else if (llLiquidDetailForm.getVisibility() == View.GONE){
                    llLiquidDetailForm.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.btn_sync:
                getData();
                InsertIntoDb();
                //getAllDataForObservationsData();
                break;
        }
    }
    public  void InsertIntoDb()
    {
//        DataBaseHelper helper=new DataBaseHelper(RecallObserVationFillActivity.this);
        SQLiteDatabase db = helper.getWritableDatabase();

        String currentDateandTime = sdf.format(new Date());

        ContentValues cv=new ContentValues();
        //cv.put("ObservationsID","1");
        cv.put("OEMID","1234");
        cv.put("MacID","18:fe:34:8b:60:75");
        cv.put("LoggerID",dataloggerName);
        cv.put("TagID",current_tag_id);
        cv.put("AugerCount",str_auger_count);
        cv.put("AugerSpeed",str_auger_speed_rpm);
        cv.put("AugerPreAuger",str_pre_auger_used);
        cv.put("AugerNoOfAgitators",str_no_of_agitator);
        cv.put("McVerticalTeflon",str_vertical_assly_teflon_tape_shift);
        cv.put("McHorizJawOpeneing",str_hz_jaw_opening);
        cv.put("McHorizJawMovement",str_hz_jaw_movement);
        cv.put("McVertAlignCenter","");
        cv.put("McVertJawMovement","");
        cv.put("PkgWireCup",str_wire_cup);
        cv.put("PkgEmptyPouches","");
        cv.put("PkgBaglenVariation","");
        cv.put("PkgPaperShifting","");
        cv.put("PkgVerticalOverlap",str_vertical_overlap_mm);
        cv.put("PkgPouchSymmetry","");
        cv.put("IsSync","0");

        long d=db.insert(Constants.OBSERVATIONS_TABLE,null,cv);
        Log.d("Success", String.valueOf(d));
        Toast.makeText(RecallObserVationFillActivity.this,"You scuccessfully inserted the observations data",Toast.LENGTH_SHORT).show();

    }

    private String abort() throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String tag = current_tag_id;
        HttpClient httpClient = new DefaultHttpClient();

        String responseString="";

        String dataPort=Constants.DATA_LOGGER_PORT+"";
        String url = "http://"+dataloggerIp+":"+dataPort+"/abort?tagid=";
        HttpResponse response1 = httpClient.execute(new HttpGet(url));
        StatusLine statusLine = response1.getStatusLine();
        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            response1.getEntity().writeTo(out);
            responseString = out.toString();
            Toast.makeText(this, responseString, Toast.LENGTH_SHORT).show();
            out.close();
        } else{
            response1.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        }
        return responseString;

    }

    /*private void getAllDataForObservationsData() {
        observationsModelArrayList.clear();
        boolean issync = false;
        Cursor getObservationsData = helper.getObservationsResultsFromLocalDB();
        while (getObservationsData.moveToNext()){
            String observationsId = getObservationsData.getString(0);
            String oemId = getObservationsData.getString(1);
            String macId = getObservationsData.getString(2);
            String loggerId = getObservationsData.getString(3);
            String tagId = getObservationsData.getString(4);
            String augerCount = getObservationsData.getString(5);
            String augerSpeed = getObservationsData.getString(6);
            String augerPreAuger = getObservationsData.getString(7);
            String augerNoOfAgitator = getObservationsData.getString(8);
            String mcVerticalTeflon = getObservationsData.getString(9);
            String mcHorizJawOpening = getObservationsData.getString(10);
            String mcHorizJawMovement = getObservationsData.getString(11);
            String mcVertAlignCenter = getObservationsData.getString(12);
            String mcVertJawMovement = getObservationsData.getString(13);
            String pkgWireCup = getObservationsData.getString(14);
            String pkgEmptyPouches = getObservationsData.getString(15);
            String pkgBaglenVariation = getObservationsData.getString(16);
            String pkgPaperShifting = getObservationsData.getString(17);
            String pkgVertOverlap = getObservationsData.getString(18);
            String pkgpouchSymmetry = getObservationsData.getString(19);
            String isSync = getObservationsData.getString(20);
            if (isSync.equals("1")){
                issync = true;
            }else if (isSync.equals("0")){
                issync = false;
                observationsModelArrayList.add(new ObservationsModel(oemId,
                        observationsId,macId,loggerId,tagId,augerCount,augerSpeed,augerPreAuger,augerNoOfAgitator,
                        mcVerticalTeflon,mcHorizJawOpening,mcHorizJawMovement,mcVertAlignCenter,mcVertJawMovement,
                        pkgWireCup,pkgEmptyPouches,pkgBaglenVariation,pkgPaperShifting,pkgVertOverlap,pkgpouchSymmetry));
            }
        }
        Gson gson = new GsonBuilder().create();
        JsonArray array = gson.toJsonTree(observationsModelArrayList).getAsJsonArray();
        String strData = array.toString();
        API api = Retroconfig.retrofit().create(API.class);
        retrofit2.Call<ResponseBody> call = api.syncObservationsData(strData);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body()!=null){
                    try {
                        String res = response.body().string();
                        JSONObject object = new JSONObject(res);
                        String statusCode = object.getString("statusCode");
                        if (statusCode.equals("200")){
                            Toast.makeText(RecallObserVationFillActivity.this, "Sync Successful", Toast.LENGTH_SHORT).show();
                            for (int i = 0 ; i < observationsModelArrayList.size() ; i ++){
                                ObservationsModel model = observationsModelArrayList.get(i);
                                String observationsId = model.getObservationsID();
                                helper.updateObservationsTable(observationsId,"Yes");
                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RecallObserVationFillActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }*/
    @Override
    protected void dialog(boolean value) {
        super.dialog(value);
        if (value){
            getData();
            InsertIntoDb();
            //getAllDataForObservationsData();
            Toast.makeText(this, "DATA Pushed", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(this, "DATA Not Pushed", Toast.LENGTH_SHORT).show();
        }
    }

}
